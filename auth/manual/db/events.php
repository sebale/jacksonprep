<?php
/**
 * This plugin is to send users a notification message after logging in
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage enotification
 * @copyright  AK
 * @copyright  AK
 * @author     AK
 * @license    AK
 */

$handlers = array(
    'user_created' => array (
        'handlerfile'     => '/auth/manual/locallib.php',
        'handlerfunction' => array('system_settings_handler', 'user_created'),
        'schedule'        => 'instant',
		'internal'        => 1,
    ),
	'course_created' => array (
        'handlerfile'     => '/auth/manual/locallib.php',
        'handlerfunction' => array('system_settings_handler', 'course_created'),
        'schedule'        => 'instant',
		'internal'        => 1,
    )
);

?>

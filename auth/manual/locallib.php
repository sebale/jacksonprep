<?php
require($CFG->dataroot.'/../../systemsadmin/lib/dashboard.php');

class system_settings_handler {
    public static function user_created() {
		global $DB, $USER, $SITE, $CFG;
		$dashboard = new Dashboard();
		$limit = $DB->get_record_sql("SELECT IF(COUNT(u.id) > ss.value AND ss.value > 0, true, false) AS lim, MAX(u.id) AS id FROM {user} AS u 
																LEFT JOIN {system_settings} AS ss ON ss.name='users'
															WHERE u.deleted = 0 LIMIT 1");
		if($limit->lim){
			$limit->username = 'must exist';
			user_delete_user($limit);
			
			$email = $dashboard->get_record_sql("SELECT value,subject,sender FROM ".$dashboard->_dbprefix."notification WHERE name='email_max_count_user' ");
			$body = $email->value;
			$replaces = array(	"[[lastname]]"=>$USER->lastname, 
											"[[firstname]]"=>$USER->firstname,
											"[[user_email]]"=>$USER->email,
											"[[user_name]]"=>$USER->username,
											"[[system_name]]"=>$SITE->fullname,
											"[[system_url]]"=>$CFG->wwwroot);
			foreach($replaces as $search=>$replace){
				$body = str_replace ($search ,$replace ,$body);
			}			
			
			email_to_user(get_admin(), $email->sender, $email->subject, $body);
			redirect("/admin/user.php");
		}    
	}
	 public static function course_created() {
		global $DB, $USER, $SITE, $CFG;
		$dashboard = new Dashboard();
		$limit = $DB->get_record_sql("SELECT IF(COUNT(c.id) > ss.value AND ss.value > 0, true, false) AS lim, MAX(c.id) AS id FROM {course} AS c 
																LEFT JOIN {system_settings} AS ss ON ss.name='courses' 
															WHERE c.id >1 LIMIT 1");
		if($limit->lim){
			delete_course($limit->id, false);
			
			$email = $dashboard->get_record_sql("SELECT value,subject,sender FROM ".$dashboard->_dbprefix."notification WHERE name='email_max_count_course' ");
			$body = $email->value;
			$replaces = array(	"[[lastname]]"=>$USER->lastname, 
											"[[firstname]]"=>$USER->firstname,
											"[[user_email]]"=>$USER->email,
											"[[user_name]]"=>$USER->username,
											"[[system_name]]"=>$SITE->fullname,
											"[[system_url]]"=>$CFG->wwwroot);
			foreach($replaces as $search=>$replace){
				$body = str_replace ($search ,$replace ,$body);
			}			
			
			email_to_user(get_admin(), $email->sender, $email->subject, $body);
			redirect("/course/");
		}
	} 
}

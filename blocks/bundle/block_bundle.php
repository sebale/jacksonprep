<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bundle block
 *
 * @package   block_bundle
 * @copyright 2015 SEBALE  {@link http://sebale.net}
 */

class block_bundle extends block_base {
    function init() {
        $this->title = get_string('pluginname', 'block_bundle');
    }

    function get_content () {
		global $DB,$USER;
		$bundle_courses = $DB->get_records_sql("SELECT courseid FROM {enrol} WHERE enrol='bundle' ");
		$this->content = new stdClass();
		foreach($bundle_courses as $bundle_course){
			$context = context_course::instance($bundle_course->courseid);
			if(!is_enrolled($context ,$USER) ){
				$this->content->text = html_writer::link(new moodle_url('/enrol/bundle/index.php'),
																get_string('by_bundle','block_bundle'), array('class' => 'by_bundle'));
			}
		}
		
		return $this->content;
    }
}



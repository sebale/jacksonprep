<?php
require_once('../../config.php');

$step = required_param('step',PARAM_INT);
$offset = $step*5;

$PAGE->set_context(context_system::instance());
$renderer = $PAGE->get_renderer('block_last_activity');
	
$logs = $DB->get_records_sql("SELECT lsl.*, u.firstname, u.lastname, create_u.firstname AS create_u_firstname, create_u.lastname AS create_u_lastname, cou.fullname AS course_name, ra.roleid
													FROM {role_assignments} ra
														LEFT JOIN {context} c ON c.id=ra.contextid AND c.contextlevel=50
														LEFT JOIN {logstore_standard_log} lsl ON lsl.courseid=c.instanceid AND (lsl.crud='u' OR lsl.crud='d' OR lsl.crud='c') AND lsl.timecreated>:timecreated AND (lsl.target LIKE IF (ra.roleid < 5, '%','cours%') AND  lsl.target NOT LIKE IF (ra.roleid < 5, '_','course________%'))
														LEFT JOIN {user} u ON lsl.relateduserid=u.id 
														LEFT JOIN {user} create_u ON lsl.userid=create_u.id 
														LEFT JOIN {course} cou ON cou.id=lsl.courseid 
													WHERE ra.userid=:userid GROUP BY lsl.id ORDER BY lsl.timecreated DESC LIMIT $offset, 6",
													array('userid'=>$USER->id,'timecreated'=>strtotime('-14 day')));
echo $renderer->last_activity_get_list_item($logs);
?>
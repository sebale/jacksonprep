<?php
/**
 * Schools statistic
 *
 * @package    block_last_activity
 * @copyright  2015 SEBALE (http://sebale.net)
 */

class block_last_activity extends block_base {

    public function init() {
		global $PAGE;
        $this->title   = get_string('pluginname', 'block_last_activity');
		$PAGE->requires->js('/blocks/last_activity/javascript/script.js');
    }

    /**
     * Return contents of last_activity block
     *
     * @return stdClass contents of block
     */
    public function get_content() {
        global $USER, $CFG, $DB;
		
        if($this->content !== NULL) {
            return $this->content;
        }
							
        $config = get_config('block_last_activity');

        $this->content = new stdClass();
        $this->content->text = '';
        $this->content->footer = '';

        $renderer = $this->page->get_renderer('block_last_activity');
		
		$logs = $DB->get_records_sql("SELECT lsl.*, u.firstname, u.lastname, create_u.firstname AS create_u_firstname, create_u.lastname AS create_u_lastname, cou.fullname AS course_name, ra.roleid
															FROM {role_assignments} ra
																LEFT JOIN {context} c ON c.id=ra.contextid AND c.contextlevel=50
																LEFT JOIN {logstore_standard_log} lsl ON lsl.courseid=c.instanceid AND (lsl.crud='u' OR lsl.crud='d' OR lsl.crud='c') AND lsl.timecreated>:timecreated AND (lsl.target LIKE IF (ra.roleid < 5, '%','cours%') AND  lsl.target NOT LIKE IF (ra.roleid < 5, '_','course________%'))
																LEFT JOIN {user} u ON lsl.relateduserid=u.id 
																LEFT JOIN {user} create_u ON lsl.userid=create_u.id 
																LEFT JOIN {course} cou ON cou.id=lsl.courseid 
															WHERE ra.userid=:userid GROUP BY lsl.id ORDER BY lsl.timecreated DESC LIMIT 0, 6",
															array('userid'=>$USER->id,'timecreated'=>strtotime('-14 day')));
															
        $this->content->text .= $renderer->last_activity($logs);

        return $this->content;
    }

    /**
     * Allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * Locations where block can be displayed
     *
     * @return array
     */
    public function applicable_formats() {
	   return array('all' => true);
    }

    /**
     * Sets block header to be hidden or visible
     *
     * @return bool if true then header will be visible.
     */
    public function hide_header() {
        return true;
    }
}
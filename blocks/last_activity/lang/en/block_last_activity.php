<?php


$string['pluginname'] = 'Last activity';
$string['was_enrolled_to'] = ' was enrolled to ';
$string['was_unenrolled_from'] = ' was unenrolled from ';
$string['completed'] = ' updated status completion ';
$string['module_update'] = ' updated course module ';
$string['course_update'] = ' updated ';
$string['course_create'] = ' created ';
$string['module_create'] = ' created course module ';
$string['module_delete'] = ' deleted course module ';


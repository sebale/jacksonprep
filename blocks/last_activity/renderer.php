<?php
/**
 * last_activity block rendrer
 *
 * @package    block_schools_statistic
 * @copyright  2015 SEBALE (http://sebale.net)
 */
defined('MOODLE_INTERNAL') || die;

class block_last_activity_renderer extends plugin_renderer_base {

    public function last_activity($logs) {
		global $DB;
        $html = '';
		$list = $this->last_activity_get_list_item($logs);
		$html .= html_writer::tag('ul',$list,array('class'=>'activity-list clearfix'));
        return $html;
    }
	
	public function last_activity_get_list_item($logs){
		global $DB;
		$i = 0; $j = 0;
		$html = '';
		foreach($logs as $log){
			$srting = '';
			$context_isset = $DB->get_record('context',array('id'=>$log->contextid));
			if($i >= 5)break;
			$i++;
			
			if(empty($context_isset))
				continue;
			
				if($log->crud == 'c'){
					switch($log->target){
						case 'user_enrolment':
							$srting = html_writer::start_tag('p');
								$srting .= html_writer::link(new moodle_url('/user/profile.php',array('id'=>$log->relateduserid)),$log->firstname.' '.$log->lastname);
								$srting .= get_string('was_enrolled_to','block_last_activity');
								$srting .= html_writer::link(new moodle_url('/course/view.php',array('id'=>$log->courseid)),$log->course_name);
							$srting .= html_writer::end_tag('p');
							$srting .= html_writer::tag('div',date("d M, H:i", $log->timecreated),array('class'=>'date'));
							$html .= html_writer::tag('li',$srting);
							$j++;
						break;
						case 'course_module':
							$context = context::instance_by_id($log->contextid);
							$srting = html_writer::start_tag('p');
								$srting .= html_writer::link(new moodle_url('/user/profile.php',array('id'=>$log->userid)),$log->create_u_firstname.' '.$log->create_u_lastname);
								$srting .= get_string('module_create','block_last_activity');
								$srting .= html_writer::link($context->get_url(),$context->get_context_name(true));
							$srting .= html_writer::end_tag('p');
							$srting .= html_writer::tag('div',date("d M, H:i", $log->timecreated),array('class'=>'date'));
							$html .= html_writer::tag('li',$srting);
							$j++;
						break;
						case 'course':
							$context = context::instance_by_id($log->contextid);
							$srting = html_writer::start_tag('p');
								$srting .= html_writer::link(new moodle_url('/user/profile.php',array('id'=>$log->userid)),$log->create_u_firstname.' '.$log->create_u_lastname);
								$srting .= get_string('course_create','block_last_activity');
								$srting .= html_writer::link($context->get_url(),$context->get_context_name(true));
							$srting .= html_writer::end_tag('p');
							$srting .= html_writer::tag('div',date("d M, H:i", $log->timecreated),array('class'=>'date'));
							$html .= html_writer::tag('li',$srting);
							$j++;
						break;
					}
				}elseif($log->crud == 'd'){
					switch($log->target){
						case 'user_enrolment':
							$srting = html_writer::start_tag('p');
								$srting .= html_writer::link(new moodle_url('/user/profile.php',array('id'=>$log->relateduserid)),$log->firstname.' '.$log->lastname);
								$srting .= get_string('was_unenrolled_from','block_last_activity');
								$srting .= html_writer::link(new moodle_url('/course/view.php',array('id'=>$log->courseid)),$log->course_name);
							$srting .= html_writer::end_tag('p');
							$srting .= html_writer::tag('div',date("d M, H:i", $log->timecreated),array('class'=>'date'));
							$html .= html_writer::tag('li',$srting);
							$j++;
						break;
						case 'course_module':
							$modulename = unserialize($log->other)['modulename'];
							$srting = html_writer::start_tag('p');
								$srting .= html_writer::link(new moodle_url('/user/profile.php',array('id'=>$log->userid)),$log->create_u_firstname.' '.$log->create_u_lastname);
								$srting .= get_string('module_delete','block_last_activity');
								$srting .= $modulename;
							$srting .= html_writer::end_tag('p');
							$srting .= html_writer::tag('div',date("d M, H:i", $log->timecreated),array('class'=>'date'));
							$html .= html_writer::tag('li',$srting);
							$j++;
						break;
					}
				}elseif($log->crud == 'u'){
					switch($log->target){
						case 'course_module_completion':
							$context = context::instance_by_id($log->contextid);
							$srting = html_writer::start_tag('p');
								$srting .= html_writer::link(new moodle_url('/user/profile.php',array('id'=>$log->relateduserid)),$log->firstname.' '.$log->lastname);
								$srting .= get_string('completed','block_last_activity');
								$srting .= html_writer::link($context->get_url(),$context->get_context_name(true));
							$srting .= html_writer::end_tag('p');
							$srting .= html_writer::tag('div',date("d M, H:i", $log->timecreated),array('class'=>'date'));
							$html .= html_writer::tag('li',$srting);
							$j++;
						break;
						case 'course_module':
							$context = context::instance_by_id($log->contextid);
							$srting = html_writer::start_tag('p');
								$srting .= html_writer::link(new moodle_url('/user/profile.php',array('id'=>$log->userid)),$log->create_u_firstname.' '.$log->create_u_lastname);
								$srting .= get_string('module_update','block_last_activity');
								$srting .= html_writer::link($context->get_url(),$context->get_context_name(true));
							$srting .= html_writer::end_tag('p');
							$srting .= html_writer::tag('div',date("d M, H:i", $log->timecreated),array('class'=>'date'));
							$html .= html_writer::tag('li',$srting);
							$j++;
						break;
						case 'course':
							$context = context::instance_by_id($log->contextid);
							$srting = html_writer::start_tag('p');
								$srting .= html_writer::link(new moodle_url('/user/profile.php',array('id'=>$log->userid)),$log->create_u_firstname.' '.$log->create_u_lastname);
								$srting .= get_string('course_update','block_last_activity');
								$srting .= html_writer::link($context->get_url(),$context->get_context_name(true));
							$srting .= html_writer::end_tag('p');
							$srting .= html_writer::tag('div',date("d M, H:i", $log->timecreated),array('class'=>'date'));
							$html .= html_writer::tag('li',$srting);
							$j++;
						break;
					}
				}	
		}	
		
		if($j<2){
			return '<script>i++;get_list(i);</script>';
		}
		
		if(count($logs) > 5){
			$html .= html_writer::tag('label',get_string('showmore','core_form'),array('class'=>'more'));
		}
		return $html;
	}

}

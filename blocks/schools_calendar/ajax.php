<?php
	require_once('../../config.php');
	require_once($CFG->dirroot.'/calendar/lib.php');

    $date  = optional_param('date', '', PARAM_RAW);
	
	if($USER->id > 0){
		$context = context_user::instance($USER->id);
		$PAGE->set_context($context); 
	}
	
    $output = ''; $events = array();
    if (!empty($date)){
        
        $filtercourse = array();
        list($courses, $group, $user) = calendar_set_filters($filtercourse);
        $mcourses = enrol_get_my_courses();
        //$mcourses = array();
        if (count($mcourses) > 0){
            $courses = array(1=>1);
            foreach($mcourses as $mc){
                $courses[$mc->id] = $mc->id;
            }
        }
        
        $starttime = strtotime($date);
        $endtime = strtotime($date)+86399;
        $events_db = calendar_get_events($starttime, $endtime, $user, $group, $courses, true, true, 'widget');
        if (count($events_db) > 0){
            foreach ($events_db as $event){
                if ($event->timestart >= $starttime and $event->timestart <= $endtime){
                    $events[$event->id] = $event;
                }
            }
        }
        if (count($events) > 0){
            $output .= '<ul>';
                foreach($events as $event){
                    $event = calendar_add_event_metadata($event);
                    $output .= '<li>';
                        $output .= '<div class="event-time-box">';
                            $output .= '<span>'.date('h:i a', $event->timestart).'</span>';
                            $output .= date('m.d', $event->timestart).'<br />'.date('Y', $event->timestart);
                        $output .= '</div>';
                        $output .= '<div class="event-body">';
                            $output .= '<div class="title">'.(($event->referer) ? $event->referer : $event->name).'</div>';
/*                             $output .= $event->description;
                            if (strlen(strip_tags($event->description)) > 110){
                                $output .= '<i class="fa fa-chevron-up"></i>';
                                $output .= '<i class="fa fa-chevron-down"></i>';
                            } */
                        $output .= '</div>';
                    $output .= '</li>';
                }
            $output .= '</ul>';
            $output .= '<script>jQuery(".event-body .fa").click(function(e){ jQuery(this).parent().toggleClass("open");});</script>';
        } else {
            $output .= '<br /><div class="alert alert-success">No events for this day</div>';
        }
    }
    echo $output;
?>
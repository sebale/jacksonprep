<?php
/**
 * Schools statistic
 *
 * @package    block_schools_calendar
 * @copyright  2015 SEBALE (http://sebale.net)
 */
//require_once($CFG->dirroot.'/blocks/schools_calendar/locallib.php');
require_once($CFG->dirroot.'/calendar/lib.php');

class block_schools_calendar extends block_base {

    public function init() {
		global $PAGE;
        $this->title   = get_string('pluginname', 'block_schools_calendar');
		$PAGE->requires->js('/blocks/schools_calendar/javascript/fullcalendar.min.js');
		$PAGE->requires->js('/blocks/schools_calendar/javascript/script.js');
    }

    /**
     * Return contents of schools_calendar block
     *
     * @return stdClass contents of block
     */
    public function get_content() {
        global $USER, $CFG, $DB;		
		
        if($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->text = '';
        $this->content->footer = '';

        $renderer = $this->page->get_renderer('block_schools_calendar');
				
		$filtercourse = array();

		list($courses, $group, $user) = calendar_set_filters($filtercourse);
		$mcourses = enrol_get_my_courses();
		if (count($mcourses) > 0){
			$courses = array(1=>1);
			foreach($mcourses as $mc){
				$courses[$mc->id] = $mc->id;
			}
		}

		$events = array();
		$all_events = calendar_get_events(time()-31536000, time()+31536000, $user, $group, $courses, true, true);
		foreach($all_events as $event){
			$events[date('Y-m-d', $event->timestart)] = $event;
		}
		 if(isset($events[date('Y-m-d', usertime(time()) )]))
			$this->content->text .= '<script>$(window).ready(function(){setTimeout(function(){$(".fc-today").click();},100);});</script>';

		
		$events_data = array();
        if (isset($events) and count($events) > 0){
            foreach($events as $event){
                $events_data[] = '{id : '.$event->id.', title: "'.addslashes($event->name).'", start: "'.date('Y-m-d h:i:s', $event->timestart).'", allDay :"true"}';
            }
        }
        $events_data = implode(',', $events_data);
		
        $this->content->text .= $renderer->schools_calendar($events_data);

        return $this->content;
    }

    /**
     * Allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * Locations where block can be displayed
     *
     * @return array
     */
    public function applicable_formats() {
	   return array('all' => true);
    }
	
	function instance_allow_multiple() {
        return false;
    }
	
	function instance_create() {
		//global $PAGE;
		//$PAGE->set_context(context_course::instance(SITEID));
		return true;
	}

    /**
     * Sets block header to be hidden or visible
     *
     * @return bool if true then header will be visible.
     */
    public function hide_header() {
        return true;
    }
}
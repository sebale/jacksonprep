$(window).ready(function(){
	var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = jQuery("#course_calendar").fullCalendar({
        header: {
            left: "title",
            center: "",
            right: "prev,next"
        },
        dayClick: function(date, jsEvent, view) {
            jQuery('.fc-day').removeClass('active');
            jQuery(this).addClass('active');
            var day = jQuery(this).attr('data-date');
            jQuery(".course-calendar-events").html("<div class='loading'><i class='fa fa-spin fa-spinner'></div>").load(window.M.cfg.wwwroot+"/blocks/schools_calendar/ajax.php?action=get_day_events&date="+day);
        },
        editable: false,
        disableDragging: true,
        contentHeight: 50,
        handleWindowResize: true,
        events: events_data
    });
	setTimeout(function(){$('#course_calendar').fullCalendar('render');},100);
});
<?php
/**
 * schools_calendar block rendrer
 *
 * @package    block_schools_calendar
 * @copyright  2015 SEBALE (http://sebale.net)
 */
defined('MOODLE_INTERNAL') || die;

class block_schools_calendar_renderer extends plugin_renderer_base {

    public function schools_calendar($events_data) {
		global $USER, $OUTPUT,$CFG,$DB;
		$notifications_config = get_config('local_schools_notifications');
		
		$config = get_config('block_schools_calendar');
		
        $html = '';
		
        $html .= html_writer::start_tag('div',array('class'=>'recent-inner'));
			$html .= html_writer::start_tag('div',array('class'=>'item-box upcoming-deadline'));
				$html .= html_writer::start_tag('div',array('class'=>'item-inner'));
					$html .= html_writer::tag('div','', array('class'=>'course-calendar','id'=>'course_calendar'));
					$html .= html_writer::tag('div','', array('class'=>'course-calendar-events'));
				$html .= html_writer::end_tag('div');
			$html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('div');
		$html .= html_writer::tag('script',"var events_data=[$events_data];");
		
        return $html;
    }

}

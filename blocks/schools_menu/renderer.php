<?php
/**
 * schools_menu block rendrer
 *
 * @package    block_schools_menu
 * @copyright  2015 SEBALE (http://sebale.net)
 */
defined('MOODLE_INTERNAL') || die;

class block_schools_menu_renderer extends plugin_renderer_base {

    public function schools_menu() {
		global $USER, $OUTPUT,$CFG,$DB;
		$notifications_config = get_config('local_schools_notifications');
		
		$config = get_config('block_schools_menu');
		
        $html = '';
        $html .= html_writer::start_tag('ul', array('class'=>'user-left_menu'));			
			if($config->schools_menu_display_dashboard){
				$html .= html_writer::start_tag('li', array('class'=>'active'));				
					$content = html_writer::empty_tag('img',array('class'=>'smallicon navicon', 'src'=>$CFG->wwwroot.'/blocks/schools_menu/pix/home.svg','title'=>get_string('dashboard','block_schools_menu'), 'alt'=>''));	
					$content .= html_writer::tag('span', get_string('dashboard','block_schools_menu'),array('class'=>'item-content-wrap'));						
					$html .= html_writer::link(new moodle_url('/my/'),$content,array('title'=>get_string('dashboard','block_schools_menu')));					
				$html .= html_writer::end_tag('li');
			}
			if($config->schools_menu_display_courses){
				$html .= html_writer::start_tag('li', array('class'=>'dropdown'));				
					$content = html_writer::empty_tag('img',array('class'=>'smallicon navicon', 'src'=>$CFG->wwwroot.'/blocks/schools_menu/pix/course.svg','title'=>get_string('courses','block_schools_menu'), 'alt'=>''));	
					$content .= html_writer::tag('span', get_string('courses','block_schools_menu'),array('class'=>'item-content-wrap'));						
					$html .= html_writer::link(new moodle_url('/my/'),$content,array('title'=>get_string('courses','block_schools_menu'), 'data-toggle'=>'dropdown'));
					
					$my_courses  = enrol_get_my_courses('summary, summaryformat');
					if(count($my_courses)>0){
						$html .= html_writer::start_tag('ul', array('class'=>'dropdown-menu'));
							foreach($my_courses as $course){
								$html .= html_writer::tag('li',html_writer::link(new moodle_url('/course/view.php',array('id'=>$course->id)),'<i class="fa fa-angle-right"></i> '.$course->fullname));
							}
						$html .= html_writer::end_tag('ul');
					}
				$html .= html_writer::end_tag('li');
			}
			if($config->schools_menu_display_report_card){
				if(user_has_role_assignment($USER->id,3,1)){
					$html .= html_writer::start_tag('li');				
						$content = html_writer::empty_tag('img',array('class'=>'smallicon navicon', 'src'=>$CFG->wwwroot.'/blocks/schools_menu/pix/board.svg','title'=>get_string('grades_boock','block_schools_menu'), 'alt'=>''));	
						$content .= html_writer::tag('span', get_string('grades_boock','block_schools_menu'),array('class'=>'item-content-wrap'));						
						$html .= html_writer::link(new moodle_url('/my/',array('grades_boock'=>1)),$content,array('title'=>get_string('grades_boock','block_schools_menu')));					
					$html .= html_writer::end_tag('li');
				}else{
					$html .= html_writer::start_tag('li');				
						$content = html_writer::empty_tag('img',array('class'=>'smallicon navicon', 'src'=>$CFG->wwwroot.'/blocks/schools_menu/pix/board.svg','title'=>get_string('report_card','block_schools_menu'), 'alt'=>''));	
						$content .= html_writer::tag('span', get_string('report_card','block_schools_menu'),array('class'=>'item-content-wrap'));						
						$html .= html_writer::link(new moodle_url('/my/',array('report_card'=>1)),$content,array('title'=>get_string('report_card','block_schools_menu')));					
					$html .= html_writer::end_tag('li');
				}
			}
			if($config->schools_menu_display_search){
				$html .= html_writer::start_tag('li');				
					$content = html_writer::empty_tag('img',array('class'=>'smallicon navicon', 'src'=>$CFG->wwwroot.'/blocks/schools_menu/pix/search.svg','title'=>get_string('search','block_schools_menu'), 'alt'=>''));	
					$content .= html_writer::tag('span', get_string('search','block_schools_menu'),array('class'=>'item-content-wrap'));						
					$html .= html_writer::link(new moodle_url('/course/search.php'),$content,array('title'=>get_string('search','block_schools_menu')));					
				$html .= html_writer::end_tag('li');
			}
			if($config->schools_menu_display_notifications && isset($notifications_config->version)){
				$count_notifi = $DB->count_records('schools_notifications', array('userid'=>$USER->id,'typemessage'=>'text_message','reading'=>0));
				$html .= html_writer::start_tag('li');				
					$content = html_writer::empty_tag('img',array('class'=>'smallicon navicon', 'src'=>$CFG->wwwroot.'/blocks/schools_menu/pix/notifi.svg','title'=>get_string('notifications','block_schools_menu'), 'alt'=>''));	
					$content .= ($count_notifi>0)?html_writer::tag('i', $count_notifi,array('class'=>'notifi')):'';						
					$content .= html_writer::tag('span', get_string('notifications','block_schools_menu'),array('class'=>'item-content-wrap'));						
					$html .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/schools_notifications/notifications.php'),$content,array('title'=>get_string('notifications','block_schools_menu')));					
				$html .= html_writer::end_tag('li');
			}
			if($config->schools_menu_display_logout){
				$html .= html_writer::start_tag('li');				
					$content = html_writer::empty_tag('img',array('class'=>'smallicon navicon', 'src'=>$CFG->wwwroot.'/blocks/schools_menu/pix/logout.svg','title'=>get_string('logout','block_schools_menu'), 'alt'=>''));	
					$content .= html_writer::tag('span', get_string('logout','block_schools_menu'),array('class'=>'item-content-wrap'));						
					$html .= html_writer::link(new moodle_url('/login/logout.php'),$content,array('title'=>get_string('logout','block_schools_menu')));					
				$html .= html_writer::end_tag('li');
			}
        $html .= html_writer::end_tag('ul');
		
				
        return $html;
    }

}

<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->updateautocheck = false;

$CFG->siteurl	= 'jacksonprep.sebale.net'; // set domain name
$CFG->dbtype    = 'mysqli';         // set db type
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';      // set domain host
$CFG->domain    = 'jacksonprep';    // DO NOT CHANGE
$CFG->dbname    = 'jacksonprep';    // set db name
$CFG->dbuser    = 'jacksonprep';    // set db user
$CFG->dbpass    = 'Moodle_2016';    // set db password
$CFG->prefix    = 'mdl_';

$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);

$CFG->wwwprotocol = 'http://'; // for ssl use $CFG->wwwprotocol = 'https://';
// enabling ssl
//$CFG->sslproxy  = true;

$CFG->wwwroot   = $CFG->wwwprotocol.$CFG->siteurl;
$CFG->dataroot  = '/home/jacksonprep/public_html/moodledata'; // set moodledata path
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

//root_system_redirect();
require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!

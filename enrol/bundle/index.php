<?php
require('../../config.php');

if (!isloggedin()) {
    redirect(get_login_url());
}

$PAGE->set_pagelayout('incourse');
$PAGE->set_url('/enrol/bundle/index.php');

// do not allow enrols when in login-as session
if (\core\session\manager::is_loggedinas() and $USER->loginascontext->contextlevel == CONTEXT_COURSE) {
    print_error('loginasnoenrol', '', $CFG->wwwroot.'/course/view.php?id='.$USER->loginascontext->instanceid);
}
$bundle_courses = $DB->get_records_sql('SELECT e.*,e.id AS instanceid,c.id,c.fullname  FROM {course} AS c
																						LEFT JOIN {enrol} AS e ON e.enrol="bundle"
																					WHERE c.id=e.courseid');
		
$instance = current($bundle_courses);
$instance_bundle = enrol_get_plugin('bundle');

$i=0;
foreach($bundle_courses as $bundle_course){
	$context = context_course::instance($bundle_course->id);
	if(!is_enrolled($context ,$USER) ){$i++;}
}
if($i == 0){redirect("$CFG->wwwroot/my"); }	


$PAGE->set_title($instance_bundle->get_config('name'));
$PAGE->set_heading($instance_bundle->get_config('name'));
$PAGE->navbar->add(get_string('enrolmentoptions','enrol'));

echo $OUTPUT->header();
?>
<div class='bundle_pay clearfix'>
	<div class="left">
		<h2><?php print_string('for_all_course_bundle', 'enrol_bundle') ?></h2>
		<p><?php print_string("paymentrequired") ?></p>
		<p><b><?php echo $instance_bundle->get_config('name'); ?></b></p>
		<p><b><?php echo get_string("cost").": ".(float)$instance_bundle->get_config('cost')." ".$instance_bundle->get_config('currency'); ?></b></p>
		<p><img alt="<?php print_string('bundleaccepted', 'enrol_bundle') ?>" src="https://www.paypal.com/en_US/i/logo/PayPal_mark_60x38.gif" /></p>
		<p><?php print_string("paymentinstant") ?></p>
		<?php
			$bundleurl = empty($CFG->usebundlesandbox) ? 'https://www.paypal.com/cgi-bin/webscr' : 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		?>
		<form action="<?php echo $bundleurl ?>" method="post">
			<input type="hidden" name="cmd" value="_xclick" />
			<input type="hidden" name="charset" value="utf-8" />
			<input type="hidden" name="business" value="<?php p($instance_bundle->get_config('bundlebusiness'))?>" />
			<input type="hidden" name="item_name" value="<?php p($instance_bundle->get_config('name')) ?>" />
			<input type="hidden" name="quantity" value="1" />
			<input type="hidden" name="on0" value="<?php print_string("user") ?>" />
			<input type="hidden" name="os0" value="<?php p(fullname($USER)) ?>" />
			<input type="hidden" name="custom" value="<?php echo "{$USER->id}-{$instance->id}-{$instance->instanceid}" ?>" />

			<input type="hidden" name="currency_code" value="<?php p($instance_bundle->get_config('currency')) ?>" />
			<input type="hidden" name="amount" value="<?php p((float)$instance_bundle->get_config('cost')) ?>" />

			<input type="hidden" name="for_auction" value="false" />
			<input type="hidden" name="no_note" value="1" />
			<input type="hidden" name="no_shipping" value="1" />
			<input type="hidden" name="notify_url" value="<?php echo "$CFG->wwwroot/enrol/bundle/ipn.php"?>" />
			<input type="hidden" name="return" value="<?php echo "$CFG->wwwroot/enrol/bundle/return.php?id=$instance->id" ?>" />
			<input type="hidden" name="cancel_return" value="<?php echo $CFG->wwwroot ?>" />
			<input type="hidden" name="rm" value="2" />
			<input type="hidden" name="cbt" value="<?php print_string("continuetocourse") ?>" />

			<input type="hidden" name="first_name" value="<?php p($USER->firstname) ?>" />
			<input type="hidden" name="last_name" value="<?php p($USER->lastname) ?>" />
			<input type="hidden" name="address" value="<?php p($USER->address) ?>" />
			<input type="hidden" name="city" value="<?php p($USER->city) ?>" />
			<input type="hidden" name="email" value="<?php p($USER->email) ?>" />
			<input type="hidden" name="country" value="<?php p($USER->country) ?>" />

			<input type="submit" value="<?php print_string("sendpaymentbutton", "enrol_bundle") ?>" />
		</form>
	</div>
	<div class="right">
		<?php 			
			$availablecourses = ' ('.count($bundle_courses).')';
			echo html_writer::tag('h2',get_string('availablecourses').$availablecourses);
			
			$i=1;
			foreach($bundle_courses as $bundle_course){
				echo html_writer::tag('p',$i.' '.$bundle_course->fullname);
				$i++;
			} 
		?>
	</div>
</div>

<?php

echo $OUTPUT->footer();

moodle-local_chat
==================
Local Moodle plugin

INSTALLATION
===============================
1. Download the plugin from https://github.com/sebale/moodle-local_chat
2. Install the plugin as local plugin
3. Setup plugin settings

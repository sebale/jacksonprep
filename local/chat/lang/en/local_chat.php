<?php
// Chat
//
// Chat is built to work with any LMS designed in Moodle 
//
// Moodle
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Chat is built as a plugin for Moodle.

/**
 * Chat
 *
 *
 * @package    	local_chat
 * @copyright  	2014-2015 SEBALE LLC
 * @license    	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @created by	SEBALE LLC
 * @website		www.sebale.net
 */

$string['pluginname'] = 'Chat Plugin';
$string['chat_title'] = 'Chat';
$string['settings'] = 'Settings';
$string['enabled'] = 'Enabled Chat';
$string['enabled_desc'] = 'Enable';
$string['oncourse'] = 'Display only enrolled users on Course pages';
$string['oncourse_desc'] = 'Enable';
$string['inactivity'] = 'Inactivity';
$string['inactivity_desc'] = 'User online inactivity time (min)';
$string['showroles'] = 'Show roles';
$string['showroles_desc'] = 'Show roles on users list';
$string['savehistory'] = 'History period';
$string['savehistory_desc'] = 'Save user history during (days)';
$string['chatlist'] = 'Chat List';
$string['online_contacts'] = 'Online contacts';
$string['recently_contacted'] = 'Recently contacted';
$string['say_something'] = 'Say something';

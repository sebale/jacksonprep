<?php
// Chat
//
// Chat is built to work with any LMS designed in Moodle 
//
// Moodle
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Chat is built as a plugin for Moodle.

/**
 * Chat
 *
 *
 * @package    	local_chat
 * @copyright  	2014-2015 SEBALE LLC
 * @license    	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @created by	SEBALE LLC
 * @website		www.sebale.net
 */

 
defined('MOODLE_INTERNAL') || die();
 
$plugin->version  = 2015041000;
$plugin->requires = 2011120500;
$plugin->release = '1.0';
$plugin->component = 'local_chat';

$plugin->iltype = 'individual';
$plugin->isystems = array('demo');

<?php
// IntelliBoard.net
//
// IntelliBoard.net is built to work with any LMS designed in Moodle
// with the goal to deliver educational data analytics to single dashboard instantly.
// With power to turn this analytical data into simple and easy to read reports,
// IntelliBoard.net will become your primary reporting tool.
//
// Moodle
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// IntelliBoard.net is built as a local plugin for Moodle.

/**
 * IntelliBoard.net
 *
 *
 * @package    	intelliboard
 * @copyright  	2015 IntelliBoard, Inc
 * @license    	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @created by	IntelliBoard, Inc
 * @website		www.intelliboard.net
 */

require('../../../config.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir.'/adminlib.php');

require_login();
require_capability('local/intelliboard:view', context_system::instance());
admin_externalpage_setup('intelliboardcontrolpanel');

$action = optional_param('action', '', PARAM_RAW);
$url = optional_param('url', '', PARAM_RAW);
$time = optional_param('time', 'monthly', PARAM_RAW);
$filter = optional_param('filter', 0, PARAM_INT);

$userid = optional_param('userid', $USER->id, PARAM_INT);
$timestart = strtotime('-14 days');
$timefinish = time();

$courses = $DB->get_records_sql("SELECT c.id, c.fullname, cc.timecompleted, m.modules, cm.completedmodules FROM mdl_user_enrolments ue LEFT JOIN mdl_enrol e ON e.id = ue.enrolid LEFT JOIN mdl_course c ON c.id = e.courseid LEFT JOIN mdl_course_completions cc ON cc.course = c.id AND cc.userid = ue.userid LEFT JOIN (SELECT course, count(id) as modules FROM mdl_course_modules WHERE visible = 1 AND completion = 1 GROUP BY course) m ON m.course = c.id LEFT JOIN (SELECT cm.course, cmc.userid, count(cmc.id) as completedmodules FROM mdl_course_modules cm, mdl_course_modules_completion cmc WHERE cm.id = cmc.coursemoduleid AND cmc.completionstate > 0 AND cm.visible = 1 AND cm.completion = 1 GROUP BY cm.course, cmc.userid) cm ON cm.course = c.id AND cm.userid = ue.userid WHERE ue.userid = $userid GROUP BY c.id ORDER BY c.fullname");

/*
$data = $DB->get_records_sql("SELECT g.id, GROUP_CONCAT( DISTINCT g.id), AVG((g.finalgrade/g.rawgrademax)*100) as grade, floor(g.timemodified / 86400) * 86400 as timepoint FROM mdl_grade_items gi, mdl_grade_grades g WHERE gi.id = g.itemid AND g.userid = $userid AND gi.itemtype = 'mod' AND g.finalgrade IS NOT NULL AND g.timemodified BETWEEN $timestart AND $timefinish GROUP BY floor(g.timemodified / 86400) * 86400 ORDER BY g.timemodified");
*/

$data = $DB->get_records_sql("SELECT floor(g.timemodified / 86400) * 86400 as timepoint, AVG((g.finalgrade/g.rawgrademax)*100) as grade FROM mdl_grade_items gi, mdl_grade_grades g WHERE gi.id = g.itemid AND g.userid = $userid AND gi.itemtype = 'mod' AND g.finalgrade IS NOT NULL AND g.timemodified BETWEEN $timestart AND $timefinish GROUP BY floor(g.timemodified / 86400) * 86400 ORDER BY g.timemodified");
// my courses !!!
$data2 = $DB->get_records_sql("SELECT floor(g.timemodified / 86400) * 86400 as timepoint, AVG((g.finalgrade/g.rawgrademax)*100) as grade FROM mdl_grade_items gi, mdl_grade_grades g WHERE gi.id = g.itemid AND g.userid != $userid AND gi.itemtype = 'mod' AND g.finalgrade IS NOT NULL AND g.timemodified BETWEEN $timestart AND $timefinish GROUP BY floor(g.timemodified / 86400) * 86400 ORDER BY g.timemodified");


$json_data = array();
foreach($data as $item){
	$l = 0;
	if(isset($data2[$item->timepoint])){
		$d = $data2[$item->timepoint];
		$l = round($d->grade,2);
	}

	$item->grade = round($item->grade,2);

	$tooltip = "<div class=\"chart-tooltip\">";
	$tooltip .= "<div class=\"chart-tooltip-header\">".date('D, M d Y', $item->timepoint)."</div>";
	$tooltip .= "<div class=\"chart-tooltip-body clearfix\">";
	$tooltip .= "<div class=\"chart-tooltip-left\"><span>". round($item->grade, 2)."%</span> current grade</div>";
	$tooltip .= "<div class=\"chart-tooltip-right\"><span>". round($l, 2)."%</span> average grade</div>";
	$tooltip .= "</div>";
	$tooltip .= "</div>";
	$item->timepoint = $item->timepoint*1000;
	$json_data[] = "[new Date($item->timepoint), $item->grade, '$tooltip', $l, '$tooltip']";
}


$PAGE->set_url(new moodle_url("/local/intelliboard/index.php", array()));
$PAGE->set_pagetype('home');
$PAGE->set_pagelayout('report');
$PAGE->set_context(context_system::instance());
$PAGE->set_title(get_string('intelliboardroot', 'local_intelliboard'));
$PAGE->set_heading(get_string('intelliboardroot', 'local_intelliboard'));
$PAGE->requires->css('/local/intelliboard/assets/css/style.css');
echo $OUTPUT->header();
?>
<div class="intelliboard-page intelliboard-student">
	<div class="sheader clearfix">
		<div class="avatar">
			<?php echo $OUTPUT->user_picture($USER, array('size'=>75)); ?>
		</div>
		<div class="info">
			<h2><?php echo fullname($USER); ?> <i class="ion-checkmark-circled"></i></h2>
			<p><?php echo $USER->email; ?></p>
		</div>

		<div class="stats">
			<ul>
				<li>2345<span>Completed</span></li>
				<li>245<span>In progress</span></li>
				<li>85<span>AVG Grade</span></li>
				<li>3<span>Messages</span></li>
			</ul>
		</div>

		<div class="s-progress">
			<div class="clearfix">
				<span class="text">Course Progress</span>
				<span class="value">45%</span>
				<span class="state" style="width:45%"></span>
			</div>
			<div class="clearfix">
				<span class="text">Perfomance</span>
				<span class="value">95%</span>
				<span class="state" style="width:95%"></span>
			</div>
		</div>

	</div>


	<?php include("views/menu.php"); ?>

		<div class="intelliboard-box intelliboard-origin">
			<h3>Activity grade progress</h3>
			<div id="intelliboard-chart" class="intelliboard-chart-dash"></div>
			<div class="avg">
				<p class="user">86.34% <span>my activity grade average</span></p>
				<p class="site">81.02% <span>course activities grade average</span></p>
			</div>
		</div>

		<div class="intelliboard-box">
			<div class="box45 pull-left">
				<h3>Upcoming Assignments</h3>
				<div id="countries" style="width:100% height:400px;30px 0px"></div>
			</div>
			<div class="box50 pull-right">
				<h3>Course Progress</h3>
				<ul class="intelliboard-list">
					<?php foreach($courses as $row):  ?>
					<li class="intelliboard-tooltip" title="<?php echo "Available modules: $row->modules, Competed: $row->completedmodules"; ?>">
						<?php echo $row->fullname; ?>
						<span class="pull-right"><?php echo ($row->completedmodules) ? intval(($row->completedmodules / $row->modules) * 100) : 0; ?>%</span>
						<div class="intelliboard-progress xxl"><span style="width:<?php echo ($row->completedmodules) ? (($row->completedmodules / $row->modules) * 100) : 0; ?>%"></span></div>
					</li>
					<?php endforeach; ?>
					<li class="clearfix"><a style="float:left" href="courses.php">More courses</a>
						<span style="float:right;color:#ddd;">Showing 1 to 10 of <?php echo $report44['recordsTotal']; ?></span>
					</li>
				</ul>
			</div>
		</div>

	<?php include("../views/footer.php"); ?>
</div>


<script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart','geochart']
            }]
          }"></script>


<script type="text/javascript">
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		var data = new google.visualization.DataTable();
		data.addColumn('date', 'Time');
		data.addColumn('number', 'My grade progress');
		data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
		data.addColumn('number', 'Average grade');
		data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});


		data.addRows([<?php echo ($json_data) ? implode(",", $json_data):"";?>]);

		var options = {
			chartArea: {
				width: '95%',
				height: '76%',
				right:10,
				top:10
			},
			height: 250,
			hAxis: {
				format: 'dd MMM',
				gridlines: {},
				baselineColor: '#ccc',
         		gridlineColor: '#ccc',
			},
			vAxis: {
				baselineColor: '#CCCCCC',
				gridlines: {count: 5,color: 'transparent',},
				minValue: 0
			},
			pointSize: 6,
			lineWidth: 2,
         	 colors: ['#1db34f', '#1d7fb3'],
			backgroundColor:{fill:'transparent'},
			tooltip: {isHtml: true},
			legend: { position: 'none' }
		};
		var chart = new google.visualization.LineChart(document.getElementById('intelliboard-chart'));
		chart.draw(data, options);
	}
</script>


<?php
echo $OUTPUT->footer();

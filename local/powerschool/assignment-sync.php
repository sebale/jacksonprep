<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once('classes/Courses.php');
require_once('assignment_sync_form.php');

$course = optional_param('course', 0, PARAM_INT);

require_login($course);
$context = context_course::instance($course);
require_capability('local/powerschool:view', $context);

$plugin = new Courses();

$PAGE->set_url(new moodle_url("/local/powerschool/assignment-sync.php",array('course'=>$course)));
$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_title(get_string('powerschoolroot', 'local_powerschool').': '.get_string('assignment_sync', 'local_powerschool'));
$PAGE->set_heading(get_string('powerschoolroot', 'local_powerschool').': '.get_string('assignment_sync', 'local_powerschool'));


$form = new assignment_sync_form(new moodle_url($PAGE->url),$course);

if ($data = $form->get_data()) {
    unset($data->course,$data->submitbutton);
    foreach ($data as $recordid=>$status){
        $DB->update_record('powerschool_assignment_sync',(object)array('id'=>$recordid,'sync'=>$status));
        $plugin->create_assignment_from_sync($recordid);
    }
    redirect(new moodle_url($PAGE->url));
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('assignment_sync', 'local_powerschool'));

if($DB->record_exists('powerschool_courses',array('mcourse'=>$course))){
    $count = $DB->count_records('powerschool_assignment_sync', array('courseid' => $course));
    if($count > 0)
        $form->display();
    else
        echo html_writer::div(html_writer::span(get_string('nothingtodisplay')), 'alert alert-error');
}else{
    echo html_writer::div(html_writer::span(get_string('course_not_sync','local_powerschool')), 'alert alert-error');
}

echo $OUTPUT->footer();

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    //  It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');

class assignment_sync_form extends moodleform {

    /**
     * Define the form.
     */
    public function definition () {
        global $DB;
        $courseid = $this->_customdata;
        $mform = $this->_form;

        $data = $DB->get_records('powerschool_assignment_sync',array('courseid'=>$courseid));
        $modinfo = get_fast_modinfo($courseid);

        foreach ($data as $ass){
            $cm = $modinfo->get_cm($ass->cmid);
            $status = ($ass->sync)?html_writer::span(get_string('yes'),'alert alert-success'):html_writer::span(get_string('no'),'alert');
            $mform->addElement('advcheckbox', $ass->id, $cm->name, $status, array('group' => 1), array(0, 1));
            $mform->setDefault($ass->id, $ass->sync);
        }

        $this->add_action_buttons(true, get_string('sync', 'local_powerschool'));
    }

}



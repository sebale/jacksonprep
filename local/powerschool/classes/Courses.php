<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/powerschool/locallib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/course/modlib.php');

class Courses extends PowerSchool{
    public $categories;
    public $sync_params;
    public $enable_groups;
    public $isset_mod;

    public function __construct(){
        global $DB;
        parent::__construct();

        $this->categories = $DB->get_records_sql('SELECT cc.id,cc.name,cc.parent,pcc.courseid as pcourse,cc.idnumber
                                            FROM {course_categories} cc
                                              LEFT JOIN {powerschool_course_cat} pcc ON pcc.catid=cc.id');

        $this->sync_params = json_decode(get_config('local_powerschool', 'sync_params'));

        $this->enable_groups = (isset($this->sync_params->section_type) && $this->sync_params->section_type == 'groups')?true:false;
        $this->isset_mod = array_flip(get_module_types_names());
    }

    public function sync_courses($page, $pagesize = 0){
        $pagesize = ($pagesize == 0)?$this->metadata->metadata->schema_table_query_max_page_size:$pagesize;

        $data = new stdClass();
        $data->schoolid = get_config('local_powerschool', 'school_number');
        $data = json_encode($data);

        $data = $this->request('post','/ws/schema/query/get.school.courses?page='.$page.'&pagesize='.$pagesize,array(),$data);

        foreach($data->record as $course){
            $this->execute_course($course->tables->sections);
        }
    }
    public function execute_course($course){
        global $DB;
        $cat = $this->get_course_cat($course);
        $course->terms_firstday = strtotime($course->terms_firstday);
        if($this->enable_groups){
            return $this->execute_course_with_groups($course,$cat);
        }

        $old_course = $this->course_exist($course);
        if(isset($old_course->id)){
            $new_course = new stdClass();
            $new_course->id = $old_course->id;
            $new_course->category = $cat;
            $new_course->shortname = $course->section_expression.' '. $course->course_name. ' '.$course->terms_abbreviation.' #'.$course->section_number;
            $new_course->idnumber = $course->course_number.'.'.$course->section_number;
            $new_course->fullname = $course->section_expression.' '. $course->course_name;
            $new_course->startdate = $course->terms_firstday;
            $new_course->summary = $course->description;
            if($new_course != $old_course)
                update_course($new_course);

            $this->execute_course_fields($course,$old_course->id);
        }else{
            $new_course = new stdClass();
            $new_course->category = $cat;
            $new_course->shortname = $course->section_expression.' '. $course->course_name. ' '.$course->terms_abbreviation.' #'.$course->section_number;
            $new_course->idnumber = $course->course_number.'.'.$course->section_number;
            $new_course->fullname = $course->section_expression.' '. $course->course_name;
            $new_course->startdate = $course->terms_firstday;
            $new_course->summary = $course->description;
            $new_course->numsections = 1;
            $new_course->enablecompletion = 1;

            $new_course = create_course($new_course);
            $record = new stdClass();
            $record->mcourse = $new_course->id;
            $record->psection = $course->section_id;
            $DB->insert_record('powerschool_courses', $record, false);

            $this->execute_course_fields($course,$record->mcourse);
        }
        $this->enroll_users($new_course,$course);
        $this->execute_section_modules($course,$new_course);
        $this->execute_course_gradescale($new_course,$course);

        return true;
    }

    private function execute_course_with_groups($course,$cat){
        global $DB;

        $old_course = $this->course_exist($course);
        if(isset($old_course->id)){
            $new_course = new stdClass();
            $new_course->id = $old_course->id;
            $new_course->category = $cat;
            $new_course->shortname = $course->course_name.' ('.$course->course_number.')';
            $new_course->idnumber = $course->course_number;
            $new_course->fullname = $course->course_name;
            $new_course->startdate = $course->terms_firstday;
            $new_course->summary = $course->description;
            if($new_course != $old_course)
                update_course($new_course);

            $this->execute_course_fields($course,$old_course->id);
        }else{
            $new_course = new stdClass();
            $new_course->category = $cat;
            $new_course->shortname = $course->course_name. ' ('.$course->course_number.')';
            $new_course->idnumber = $course->course_number;
            $new_course->fullname = $course->course_name;
            $new_course->startdate = $course->terms_firstday;
            $new_course->summary = $course->description;
            $new_course->numsections = 1;
            $new_course->enablecompletion = 1;

            $new_course = create_course($new_course);
            $record = new stdClass();
            $record->mcourse = $new_course->id;
            $record->psection = $course->course_id;
            $DB->insert_record('powerschool_courses', $record, false);
            $this->execute_course_fields($course,$new_course->id);
        }
        $this->execute_course_group($new_course,$course);
        $this->enroll_users($new_course,$course);
        $this->execute_section_modules($course,$new_course);
        $this->execute_course_gradescale($new_course,$course);
    }

    private function course_exist($course){
        global $DB;
        if($this->enable_groups) {
            $id = $course->course_id;
            $idnumber = $course->course_number;
        }else {
            $id = $course->section_id;
            $idnumber = $course->course_number.'.'.$course->section_number;
        }

        $old_course = $DB->get_record_sql('SELECT  c.id,c.category,c.shortname,c.idnumber,c.fullname,c.startdate,c.summary
                                            FROM {powerschool_courses} pc
                                              LEFT JOIN {course} c ON pc.mcourse=c.id
                                            WHERE pc.psection=:psection',array('psection'=> $id));
        if(isset($old_course->id))
            return $old_course;

        $old_course = $DB->get_record_sql('SELECT  c.id,c.category,c.shortname,c.idnumber,c.fullname,c.startdate,c.summary
                                            FROM {course} c
                                            WHERE c.idnumber=:idnumber ',array('idnumber'=> $idnumber));
        if(isset($old_course->id)){
            $record = new stdClass();
            $record->mcourse = $old_course->id;
            $record->psection = ($this->enable_groups)?$course->course_id:$course->section_id;
            $DB->insert_record('powerschool_courses', $record, false);
            return $old_course;
        }else
            return null;
    }

    private function execute_course_fields($course, $courseid){
        global $DB;

        $course_field = $DB->get_record('powerschool_course_fields',array('courseid'=>$courseid),'id');

        $additional_fields = new stdClass();

        if(isset($course_field->id))
            $additional_fields->id = $course_field->id;
        else
            $additional_fields->courseid = $courseid;

        $additional_fields->program_name = $course->program_name;
        $additional_fields->section_gradescaleid = $course->section_gradescaleid;
        $additional_fields->section_maxenrollment = $course->section_maxenrollment;
        $additional_fields->section_maxcut = $course->section_maxcut;
        $additional_fields->section_wheretaught = $course->section_wheretaught;
        $additional_fields->section_excludefromhonorroll = $course->section_excludefromhonorroll;
        $additional_fields->section_exclude_ada = $course->section_exclude_ada;
        $additional_fields->section_excludefromclassrank = $course->section_excludefromclassrank;
        $additional_fields->section_excludefromgpa = $course->section_excludefromgpa;
        $additional_fields->section_wheretaughtdistrict = $course->section_wheretaughtdistrict;
        $additional_fields->sec_excludefromstoredgrades = $course->section_excludefromstoredgrades;
        $additional_fields->section_number = $course->section_number;
        $additional_fields->section_house = $course->section_house;
        $additional_fields->section_section_type = $course->section_section_type;
        $additional_fields->section_att_mode_code = $course->section_att_mode_code;
        $additional_fields->section_room = $course->section_room;
        $additional_fields->section_grade_level = $course->section_grade_level;
        $additional_fields->section_team = $course->section_team;
        $additional_fields->section_max_load_status = $course->section_max_load_status;
        $additional_fields->section_attendance_type_code = $course->section_attendance_type_code;
        $additional_fields->teacher_user_dcid = $course->teacher_user_dcid;
        $additional_fields->course_number = $course->course_number;

        if(isset($course_field->id))
            return $DB->update_record('powerschool_course_fields', $additional_fields);
        else
            return $DB->insert_record('powerschool_course_fields', $additional_fields, false);
    }

    private function get_course_cat($course){
        global $CFG,$DB;
        require_once("$CFG->libdir/coursecatlib.php");

        $parent = (object) array('id'=>1,'parent'=>0);
        if(!empty($course->sched_department)){
            foreach($this->categories as $cat){
                if($cat->parent == 0 && $cat->name == $course->sched_department){
                    $parent = $cat;
                    break;
                }
            }

            if($parent->id == 1){
                $record = array();
                $record['name'] = $course->sched_department;
                $record['description'] = '';
                $record['idnumber'] = '';
                $this->categories[] = $parent = (object) array('id'=>coursecat::create($record)->id, 'parent'=>0,'name'=>$course->sched_department);
            }
        }

        if($this->enable_groups)
            return $parent->id;

        $category = array();
        foreach($this->categories as $cat){
            if(isset($cat->pcourse) && $cat->pcourse == $course->course_id){
                $category = $cat;
                break;
            }elseif($cat->parent>0 && $cat->idnumber == $course->course_number){
                $category = $cat;

                if(!$DB->record_exists('powerschool_course_cat', array('courseid'=>$course->course_id,'catid'=>$cat->id))){
                    $record = new stdClass();
                    $record->courseid = $course->course_id;
                    $record->catid = $cat->id;
                    $DB->insert_record('powerschool_course_cat',$record,false);
                }

                break;
            }
        }

        if(empty($category)){
            $record = array();
            $record['name'] = $course->course_name;
            $record['description'] = '';
            $record['idnumber'] = $course->course_number;
            $record['parent'] = $parent->id;
            $cat_id = coursecat::create($record)->id;
            $this->categories[] = (object) array('id'=>$cat_id, 'parent'=>$parent->id,'pcourse'=>$course->course_id, 'idnumber'=>$record['idnumber'], 'name'=>$record['name']);

            $record = new stdClass();
            $record->courseid = $course->course_id;
            $record->catid = $cat_id;
            $DB->insert_record('powerschool_course_cat',$record,false);
            return $cat_id;
        }elseif($category->idnumber == $course->course_number && $category->name == $course->course_name && $category->parent == $parent->id){
            return $category->id;
        }else{
            $cat = coursecat::get($category->id);
            $cat->update(array('parent'=>$parent->id,'idnumber'=>$course->course_number, 'name'=>$course->course_name));
            return $category->id;
        }
    }

    private function execute_course_gradescale($course,$section){
        global $DB;

        $context = context_course::instance($course->id);
        $grade_scale = $this->request('get',"/ws/schema/table/gradescaleitem?q=gradescaleid=={$section->section_gradescaleid}&projection=name,cutoffpercentage");

        $DB->delete_records('grade_letters',array('contextid'=>$context->id));

        foreach($grade_scale->record as $scale){
            $scale = $scale->tables->gradescaleitem;
            $record = new stdClass();
            $record->contextid = $context->id;
            $record->lowerboundary = $scale->cutoffpercentage;
            $record->letter = $scale->name;
            $DB->insert_record('grade_letters',$record,false);
        }

    }

    private function execute_course_group($course,$section){
        global $CFG,$DB;
        require_once($CFG->dirroot.'/group/lib.php');

        $old_group = $this->group_exist($course,$section);
        $group_name = $section->section_expression.' '.$section->terms_abbreviation;
        if(isset($old_group->id)){
            if($group_name == $old_group->name)
                return true;

            $new_group = new stdClass();
            $new_group->id = $old_group->id;
            $new_group->name = $group_name;
            $new_group->courseid = $old_group->courseid;
            return groups_update_group($new_group);
        }else {
            $new_group = new stdClass();
            $new_group->courseid = $course->id;
            $new_group->name = $group_name;
            $new_group_id = groups_create_group($new_group);

            $new_record = new stdClass();
            $new_record->courseid = $section->section_id;
            $new_record->catid = $new_group_id;
            $new_record->courseid_group = $course->id;
            return $DB->insert_record('powerschool_course_cat', $new_record, false);
        }
    }

    private function group_exist($course,$section){
        global $DB;

        $old_group = $DB->get_record_sql('SELECT g.id,g.name,g.courseid
                                            FROM {powerschool_course_cat} pcc
                                              LEFT JOIN {groups} g ON g.id = pcc.catid
                                            WHERE pcc.courseid=:courseid',
            array('courseid'=>$section->section_id));

        if(isset($old_group->id))
            return $old_group;

        $group_name = $section->section_expression.' '.$section->terms_abbreviation;
        $old_group = $DB->get_record_sql('SELECT g.id,g.name,g.courseid
                                            FROM {groups} g
                                            WHERE g.name=:group_name AND g.courseid=:courseid',
            array('group_name'=>$group_name,'courseid'=>$course->id));
        if(isset($old_group->id)){
            $new_record = new stdClass();
            $new_record->courseid = $section->section_id;
            $new_record->catid = $old_group->id;
            $new_record->courseid_group = $course->id;
            $DB->insert_record('powerschool_course_cat', $new_record, false);
            return $old_group;
        }else
            return null;
    }

    private function enroll_users($course,$section){
        global $DB,$CFG;
        require_once ($CFG->dirroot.'/enrol/manual/lib.php');
        require_once ($CFG->dirroot.'/group/lib.php');

        if(!class_exists ('enrol_manual_plugin')) {
            echo 'Enrollment module "manual" must be installed and enabled';
            return false;
        }

        $enroll_plug = new enrol_manual_plugin();
        $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'manual'));

        $count = $this->request('get','/ws/v1/section/'.$section->section_id.'/section_enrollment/count');
        $pages = (isset($count->resource->count))?ceil($count->resource->count/$this->metadata->metadata->section_enrollment_max_page_size):0;


        for($i=1;$i<=$pages;$i++){
            $request = $this->request('get','/ws/v1/section/'.$section->section_id."/section_enrollment?page=$i&pagesize=".$this->metadata->metadata->section_enrollment_max_page_size);

            if(!isset($request->section_enrollments)){
                return false;
            }

            if(!is_array($request->section_enrollments->section_enrollment))
                $request->section_enrollments->section_enrollment = array('0'=>$request->section_enrollments->section_enrollment);


            foreach ($request->section_enrollments->section_enrollment as $sectin_enrollment) {
                if($this->enable_groups) {
                    $user_record = $DB->get_record_sql('SELECT d.userid, pcc.catid as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {powerschool_course_cat} pcc ON pcc.courseid=:section_id
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id
                        ', array('section_id' => $section->section_id, 'id' => $sectin_enrollment->student_id));
                }else{
                    $user_record = $DB->get_record_sql('SELECT d.userid, 0 as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id
                        ', array('id' => $sectin_enrollment->student_id));
                }

                if(isset($user_record->userid) && $user_record->userid>0){
                    $this->enroll_student($enroll_plug, $instance, $user_record->userid, $course->id, $sectin_enrollment->id, $user_record->groupid, $sectin_enrollment->entry_date, $sectin_enrollment->exit_date);
                }
            }

        }

        /* for ps > 9.2 */
        $params = new stdClass();
        $params->section_ids = array($section->section_id);

        list($count,$header) = $this->request('post','/ws/schema/query/com.powerschool.core.users.coteacher_access_roles/count',array(),json_encode($params),true);
        $pages = (isset($count->count))?ceil($count->count/$this->metadata->metadata->schema_table_query_max_page_size):0;

        for($i=1;$i<=$pages;$i++){
            list($request,$header) = $this->request('post','/ws/schema/query/com.powerschool.core.users.coteacher_access_roles?page='.$i.'&pagesize='.$this->metadata->metadata->schema_table_query_max_page_size,array(),json_encode($params),true);

            if(!isset($request->record)){
                return false;
            }

            foreach ($request->record as $sectin_enrollment) {
                $sectin_enrollment = $sectin_enrollment->tables->schoolstaff;

                $user_record = $DB->get_record_sql('SELECT d.userid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id
                        ',array('id'=>$sectin_enrollment->dcid));


                if(isset($user_record->userid) && $user_record->userid>0)
                    $enroll_plug->enrol_user($instance, $user_record->userid, $this->sync_params->teacher_role);
            }
        }
        /* */

        $count = $this->request('get','/ws/schema/table/sectionteacher/count/?q=sectionid=='.$section->section_id);
        $pages = (isset($count->count))?ceil($count->count/$this->metadata->metadata->schema_table_query_max_page_size):0;

        for($i=1;$i<=$pages;$i++){
            $data = new stdClass();
            $data->sectionid = $section->section_id;
            $data = json_encode($data);

            $request = $this->request('post','/ws/schema/query/get.section.teachers?page='.$i.'&pagesize='.$this->metadata->metadata->schema_table_query_max_page_size,array(),$data);

            if(!isset($request->record)){
                return false;
            }

            foreach ($request->record as $sectin_enrollment) {
                $sectin_enrollment = $sectin_enrollment->tables->sectionteacher;

                $user_record = $DB->get_record_sql('SELECT d.userid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id
                        ',array('id'=>$sectin_enrollment->teacherid));


                if(isset($user_record->userid) && $user_record->userid>0)
                    $enroll_plug->enrol_user($instance, $user_record->userid, $this->sync_params->teacher_role, strtotime($sectin_enrollment->start_date), strtotime($sectin_enrollment->end_date));
            }
        }

        $user_record = $DB->get_record_sql('SELECT d.userid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id
                        ',array('id'=>$section->teacher));

        if(isset($user_record->userid) && $user_record->userid>0)
            $enroll_plug->enrol_user($instance, $user_record->userid, $this->sync_params->teacher_lead_role);



    }

    public function enroll_student($enroll_plug, $instance, $userid, $course_id, $sectin_enrollment_id, $groupid, $entry_date, $exit_date){
        global $DB,$CFG;

        if($this->enable_groups){
            require_once ($CFG->dirroot.'/group/lib.php');
            $enroll_plug->enrol_user($instance, $userid, $this->sync_params->student_role);

            if(!$DB->record_exists('powerschool_enrollments', array('courseid'=>$course_id,'userid'=>$userid,'penrollid'=>$sectin_enrollment_id, 'groupid'=>$groupid))){
                $new_record = new stdClass();
                $new_record->courseid = $course_id;
                $new_record->userid = $userid;
                $new_record->penrollid = $sectin_enrollment_id;
                $new_record->groupid = $groupid;
                $DB->insert_record('powerschool_enrollments',$new_record,false);
            }
            return groups_add_member($groupid,$userid);
        }else{
            $enroll_plug->enrol_user($instance, $userid, $this->sync_params->student_role, strtotime($entry_date), strtotime($exit_date));

            if(!$DB->record_exists('powerschool_enrollments', array('courseid'=>$course_id,'userid'=>$userid,'penrollid'=>$sectin_enrollment_id))){
                $new_record = new stdClass();
                $new_record->courseid = $course_id;
                $new_record->userid = $userid;
                $new_record->penrollid = $sectin_enrollment_id;
                $DB->insert_record('powerschool_enrollments',$new_record,false);
            }
            return true;
        }
    }

    public function unenroll_student($enroll_plug, $instance, $userid, $penrollid){
        global $DB,$CFG;
        if($this->enable_groups){
            require_once ($CFG->dirroot.'/group/lib.php');
            $record = $DB->get_record('powerschool_enrollments',array('penrollid'=>$penrollid));

            $DB->delete_records('powerschool_enrollments',array('penrollid'=>$penrollid));
            groups_remove_member($record->groupid, $record->userid);

            $count = $DB->count_records('powerschool_enrollments',array('userid'=>$record->userid, 'courseid'=>$record->courseid));
            if($count == 0)
                $enroll_plug->unenrol_user($instance, $userid);
        }else{
            $enroll_plug->unenrol_user($instance, $userid);
            $DB->delete_records('powerschool_enrollments',array('penrollid'=>$penrollid));
        }
    }

    private function execute_section_modules($section, $course){
        global $DB;
        $terms_firstday = date('Y-m-d',$section->terms_firstday);
        $pages = ceil($this->request('get','/ws/schema/table/pgassignments/count?q=sectionid=='.$section->section_dcid.';datedue=ge='.$terms_firstday)->count/$this->metadata->metadata->schema_table_query_max_page_size);

        for($i=1;$i<=$pages;$i++){
            $assignments = $this->request('get',"/ws/schema/table/pgassignments/?q=sectionid==$section->section_dcid;datedue=ge=$terms_firstday&page=$i&pagesize=".$this->metadata->metadata->schema_table_query_max_page_size."&projection=ID,AssignmentID,Name,Abbreviation,PGCategoriesID,DateDue,PointsPossible,Weight,Description");
            
            foreach($assignments->record as $assignment){
                $assignment = $assignment->tables->pgassignments;

                $data = new stdClass();
                $data->assignment_id = $assignment->dcid;
                $data = json_encode($data);

                $item = $this->request('post','/ws/schema/query/get.section.assignment',array(),$data);
                $item = array_shift($item->record);
                $item = $item->tables->sync_pgassignmentsmap;

                $this->execute_ps_assignment($item,$assignment);
            }
        }

        $modules = $DB->get_records_sql("SELECT cm.id as cmid, cm.course as courseid
                                         FROM {course_modules} cm
                                            LEFT JOIN {powerschool_assignment_sync} pas ON pas.courseid=cm.course AND pas.cmid=cm.id
                                         WHERE pas.id IS NULL AND cm.course=:courseid"
                                        , array('courseid'=>$course->id));
        foreach($modules as $module){
            $this->create_assignment_sync($module);
        }
    }

    public function create_assignment_sync($module){
        global $DB;
        $record = new stdClass();
        $record->cmid = $module->cmid;
        $record->courseid = $module->courseid;
        $record->sync = 0;
        return $DB->insert_record('powerschool_assignment_sync',$record,false);
    }

    public function create_assignment_from_sync($recordid){
        global $DB;
        $records = $DB->get_records_sql('SELECT pa.id, pas.cmid, pas.courseid, pa.assignmentid, pas.sync
                                        FROM {powerschool_assignment_sync} pas
                                          LEFT JOIN {powerschool_assignment} pa ON pa.cmid=pas.cmid AND pa.courseid=pas.courseid
                                        WHERE pas.id=:id',array('id'=>$recordid));
        
        foreach ($records as $record) {
            if ($record->sync && empty($record->assignmentid)) {
                $this->create_assignment((object)array('courseid' => $record->courseid, 'cmid' => $record->cmid));
            } elseif ($record->sync) {
                $this->update_assignment((object)array('courseid' => $record->courseid, 'cmid' => $record->cmid));
            }
        }
    }

    public function create_assignment($module,$groups = array()){
        global $DB;

        if(!$this->cm_assignment_sync($module))
            return true;

        $modinfo = get_fast_modinfo($module->courseid);
        $cm = $modinfo->get_cm($module->cmid);

        $instance = $DB->get_record($cm->modname,array('id'=>$cm->instance));
        $desc = clean_param($instance->intro,PARAM_NOTAGS);

        $assignment = new stdClass();
        $assignment->name = $cm->name;
        $assignment->abbreviation = $cm->name;
        $assignment->description = (!empty($desc))?$desc:$cm->name;
        $assignment->date_assignment_due = ($cm->completionexpected>0)?date('Y-m-d',$cm->completionexpected):date('Y-m-d',time());

        $data = new stdClass();
        $data->assignment = $assignment;
        $data = json_encode($data);

        $sections = array();
        if($this->enable_groups){
            if(empty($groups))
                $groups = $this->get_module_groups(json_decode($cm->availability),$cm);
            if(!empty($groups))
                $sections = $DB->get_records_sql('SELECT courseid as psection, catid as groupid
                                                    FROM {powerschool_course_cat}
                                                  WHERE catid IN ('.implode(',',$groups).')');
        }else{
            $sections[] = $DB->get_record('powerschool_courses',array('mcourse'=> $module->courseid),'psection');
        }

        foreach($sections as $section){
            list($response,$header) = $this->request('post','/powerschool-ptg-api/v2/section/'.$section->psection.'/assignment',array(),$data,true);
            if(empty($response)){
                $location = explode('/',explode("\r\n",$header)[3]);
                $id = array_pop($location);
            }else{
                throw new moodle_exception($response->errorMessage->message, 'local_powerschool', new moodle_url('/admin/'), null, print_r($response,true));
            }

            $record = new stdClass();
            $record->cmid = $module->cmid;
            $record->courseid = $module->courseid;
            $record->assignmentid = $id;
            if($this->enable_groups){
                $record->groupid = $section->groupid;
            }
            $DB->insert_record('powerschool_assignment',$record,false);

            $this->export_grades($cm);
        }
    }

    public function update_assignment($module){
        global $DB;

        if(!$this->cm_assignment_sync($module))
            return true;

        $modinfo = get_fast_modinfo($module->courseid);
        $cm = $modinfo->get_cm($module->cmid);

        $assignment_record = $DB->get_records('powerschool_assignment',array('cmid'=>$cm->id));

        if(empty($assignment_record) && $this->enable_groups){
            $this->check_availability($cm,$module);
        }elseif(empty($assignment_record)){
            return true;
        }

        $intro = optional_param_array('introeditor',array(),PARAM_RAW);
        if(!empty($intro)){
            $desc = (isset($intro['text']))?clean_param($intro['text'],PARAM_NOTAGS):'';
        }else{
            $instance = $DB->get_record($cm->modname,array('id'=>$cm->instance));
            $desc = clean_param($instance->intro,PARAM_NOTAGS);
        }

        foreach($assignment_record as $record) {
            $assignment = new stdClass();
            $assignment->name = $cm->name;
            if (!empty($desc))
                $assignment->description = $desc;
            if ($cm->completionexpected > 0)
                $assignment->date_assignment_due = date('Y-m-d', $cm->completionexpected);

            $data = new stdClass();
            $data->assignment = $assignment;

            list($response, $header) = $this->request('put', '/powerschool-ptg-api/v2/assignment/' . $record->assignmentid, array(), json_encode($data), true);
            if (!empty($response))
                throw new moodle_exception('test', 'local_powerschool', new moodle_url('/admin/'), null, print_r($header, true));

            if($this->enable_groups){
                $this->check_availability($cm,$module);
            }

            $this->export_grades($cm);
        }
        return true;
    }
    protected function export_grades($cm){
        global $CFG;
        require_once("$CFG->libdir/gradelib.php");
        $users = get_enrolled_users(context_module::instance($cm->id),'', 0, 'u.id');
        $grade_items = grade_item::fetch_all(array('itemtype'=>'mod', 'itemmodule'=>$cm->modname, 'iteminstance'=>$cm->instance, 'courseid'=>$cm->course));

        foreach($grade_items as $grade_item){
            $grade_grades = grade_grade::fetch_users_grades($grade_item, array_keys($users), true);
            foreach($grade_grades as $grade_grade){
                $grade_grade->grade_item = $grade_grade->load_grade_item();
                $this->graded_assignments(null,$grade_grade);
            }
        }
    }

    protected function get_module_groups($availability, $cm){
        $groups = array();
        if(empty($availability))
            return $groups;

        $all_groups = array_keys(groups_get_activity_allowed_groups($cm));

        if(strpos($availability->op,'!') !== false)
            $groups = $all_groups;

        foreach($availability->c as $child){
            if(isset($child->type) && $child->type == 'group'){
                if(strpos($availability->op,'!') === false) {
                    if (isset($child->id))
                        $groups[] = $child->id;
                    else
                        $groups = array_merge($groups, $all_groups);
                }else{
                    if (isset($child->id))
                        unset($groups[array_search($child->id,$groups)]);
                    else
                        $groups = array();
                }
            }
        }

        if(empty($groups)){
            foreach($availability->c as $child){
                if(isset($child->op)){
                    $groups = $this->get_module_groups($child, $cm);
                    if(!empty($groups)){
                        break;
                    }
                }
            }
        }
        $groups = array_unique($groups);

        return $groups;
    }

    private function check_availability($cm,$module){
        global $DB;
        if(!$this->cm_assignment_sync($module))
            return true;

        $availabilityconditionsjson = optional_param('availabilityconditionsjson','',PARAM_RAW);
        if(empty($availabilityconditionsjson)){
            $data = $DB->get_records('powerschool_assignment',array('cmid'=>$module->cmid, 'courseid'=>$module->courseid));
            $old_groups = array();
            foreach ($data as $item){
                $old_groups[] = $item->groupid;
            }

            $new_groups = $this->get_module_groups(json_decode($cm->availability),$cm);
        }else{
            $old_groups = $this->get_module_groups(json_decode($cm->availability),$cm);
            $new_groups = $this->get_module_groups(json_decode($availabilityconditionsjson),$cm);
        }

        $need_delete = array_diff($old_groups,$new_groups);
        $need_create = array_diff($new_groups,$old_groups);

        if(!empty($need_create))
            $this->create_assignment($module,$need_create);

        if(!empty($need_delete))
            $this->delete_assignment($module,$need_delete);

        return true;

    }

    public function delete_assignment($module,$groups = array()){
        global $DB;

        if(!$this->cm_assignment_sync($module) && empty($groups)){
            $DB->delete_records('powerschool_assignment', array('cmid' => $module->cmid));
            $DB->delete_records('powerschool_assignment_sync',array('cmid'=>$module->cmid));
        }elseif(!$this->cm_assignment_sync($module)){
            return true;
        }

        if(empty($groups)){
            $assignment_record = $DB->get_records('powerschool_assignment',array('cmid'=>$module->cmid));
        }else{
            $assignment_record = $DB->get_records_sql('SELECT *
                                                       FROM {powerschool_assignment}
                                                       WHERE cmid=:cmid AND groupid IN ('.implode(',',$groups).')',array('cmid'=>$module->cmid));
        }

        if(empty($assignment_record))
            return;

        foreach($assignment_record as $record){
            list($response,$header) = $this->request('delete','/powerschool-ptg-api/v2/assignment/'.$record->assignmentid,array(),'',true);
            if(!empty($response))
                throw new moodle_exception('test', 'local_powerschool', new moodle_url('/admin/'), null, print_r($response,true));

            if(empty($groups)) {
                $DB->delete_records('powerschool_assignment', array('cmid' => $module->cmid));
                $DB->delete_records('powerschool_assignment_sync',array('cmid'=>$module->cmid));
            }else{
                $DB->execute('DELETE FROM {powerschool_assignment} WHERE groupid IN ('.implode(',',$groups).') AND cmid=:cmid',array('cmid'=>$module->cmid));
            }
        }

        return true;
    }

    public function cm_assignment_sync($module){
        global $DB;
        $record = $DB->get_record('powerschool_assignment_sync',array('cmid'=>$module->cmid,'courseid'=>$module->courseid));
        return $record->sync;
    }
    
    public function sync_ps_assignment($timestart){
        $data = new stdClass();
        $data->lastupdated = date('Y-m-dG.i.s',$timestart);
        $data = json_encode($data);

        $response = $this->request('post','/ws/schema/query/get.section.assignments.update',array(),$data);
        if(!isset($response->record))
            return true;

        foreach($response->record as $item){
            $item = $item->tables->sync_pgassignmentsmap;
            $this->execute_ps_assignment($item);
        }
    }

    private function execute_ps_assignment($item,$assignment = null){
        global $DB,$PAGE, $CFG;
        if(!$assignment)
            $assignment = $this->request('get','/powerschool-ptg-api/v2/assignment/'.$item->assignment_id)->assignment;

        if($this->enable_groups){
            $info = $DB->get_record('powerschool_course_cat',array('courseid'=>$item->section_id),'catid as groupid, courseid_group as courseid');

            $availability = new stdClass();
            $availability->op = '&';
            $availability->c = array();
            $availability->c[0] = new stdClass();
            $availability->c[0]->type = 'group';
            $availability->c[0]->id = (int)$info->groupid;
            $availability->showc = array();
            $availability->showc[0] = true;
            $availability = json_encode($availability);

            $passignment = $this->assignment_exist($item,$assignment,$info->courseid,$info->groupid);
        }else{
            $info = $DB->get_record('powerschool_courses',array('psection'=>$item->section_id),'mcourse as courseid');
            $passignment = $this->assignment_exist($item,$assignment,$info->courseid);
        }


        $course = get_course($info->courseid);
        $PAGE->set_context(context_course::instance($info->courseid));

        if(isset($passignment->cmid)){
            $cm = get_coursemodule_from_id('', $passignment->cmid, 0, true, MUST_EXIST);
            $mod = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);

            if($record = $DB->get_record($cm->modname, array('id'=>$cm->instance))){
                $module = $record;
            }else{
                $module = new stdClass();
            }

            $module->modulename = $cm->modname;
            $module->course = $course->id;
            $module->module = $mod->id;
            $module->coursemodule = $passignment->cmid;
            $module->name = $assignment->name;
            $module->introeditor = array();
            $module->introeditor['text'] = (isset($assignment->description))?$assignment->description:'';
            $module->introeditor['format'] = 0;
            $module->introeditor['itemid'] = 0;
            $module->quizpassword = '';
            $module->visible = $cm->visible;
            $module->cmidnumber = $cm->idnumber;
            $module->completionexpected = (isset($assignment->date_assignment_due))?strtotime($assignment->date_assignment_due):strtotime($assignment->datedue);
            if(empty($cm->availability) && isset($availability))
                $module->availability = $availability;
            elseif(!empty($cm->availability) && isset($availability))
                $module->availability = $this->get_availability_from_instance($cm->availability, (int)$info->groupid);

            return update_moduleinfo($cm, $module, $course);
        }else{
            if(isset($this->isset_mod[$item->category])){
                $mod_name = $this->isset_mod[$item->category];
            }else{
                return true; // no module for create
            }

            $mod = $DB->get_record('modules', array('name'=>$mod_name), 'id', MUST_EXIST);

            $module = new stdClass();
            $module->modulename = $mod_name;
            $module->course = $course->id;
            $module->module = $mod->id;
            $module->section = 1;
            $module->name = $assignment->name;
            $module->introeditor = array();
            $module->introeditor['text'] = (isset($assignment->description))?$assignment->description:'';
            $module->introeditor['format'] = 0;
            $module->introeditor['itemid'] = 0;
            $module->quizpassword = '';
            $module->visible = 1;
            $module->completionexpected = (isset($assignment->date_assignment_due))?strtotime($assignment->date_assignment_due):strtotime($assignment->datedue);
            $module->instance         = '';
            $module->coursemodule     = '';
            $module->cmidnumber     = null;
            $module->availability     = (isset($availability))?$availability:null;

            require_once($CFG->libdir."/gradelib.php");
            require_once("../../mod/$mod_name/mod_form.php");
            $form_class = "mod_".$mod_name."_mod_form";
            $form = new $form_class($module,1,null,$course);


            $sweetsThief = Closure::bind(function ($form) {
                return $form->_form;
            }, null, $form);

            $data = $sweetsThief($form)->exportValues();
            $module = (object)array_merge($data,(array)$module);
            unset($module->feedbacktext);

            $instance = add_moduleinfo($module, $course);

            $record = new stdClass();
            $record->cmid = $instance->coursemodule;
            $record->courseid = $instance->course;
            $record->assignmentid = $item->assignment_id;
            if($this->enable_groups){
                $record->groupid = ($this->enable_groups)?$info->groupid:'';
            }
            $DB->insert_record('powerschool_assignment',$record,false);
            return $DB->execute('UPDATE {powerschool_assignment_sync} SET sync=1 WHERE cmid=:cmid AND courseid=:courseid',array('cmid'=>$instance->coursemodule,'courseid'=>$instance->course));
        }
    }

    private function get_availability_from_instance($availability, $groupid){
        $availability = json_decode($availability);
        foreach($availability->c as $item){
            if($item->type == 'group' && $item->id == $groupid)
                return json_encode($availability);
        }
        $instance = new stdClass();
        $instance->type = 'group';
        $instance->id = $groupid;
        $availability->c[] = $instance;
        $availability->showc[] = true;

        return json_encode($availability);
    }

    private function assignment_exist($item, $assignment, $courseid, $groupid = null){
        global $DB;

        $passignment = $DB->get_record('powerschool_assignment',array('assignmentid'=> $item->assignment_id));
        if(isset($passignment->id))
            return $passignment;

        if(isset($this->isset_mod[$item->category])){
            $mod_name = $this->isset_mod[$item->category];
        }else{
            return null; // no module for create
        }

        $mod = $DB->get_record($mod_name, array('name'=>$assignment->name,'course'=>$courseid));
        if(isset($mod->id)){
            $cm = get_coursemodule_from_instance($mod_name,$mod->id,$courseid);
            $powerschool_assignment = new stdClass();
            $powerschool_assignment->cmid = $cm->id;
            $powerschool_assignment->courseid = $courseid;
            if($groupid)
                $powerschool_assignment->groupid = $groupid;
            $powerschool_assignment->assignmentid = $item->assignment_id;
            $DB->insert_record('powerschool_assignment',$powerschool_assignment,false);

            if($record = $DB->get_record('powerschool_assignment_sync',array('cmid'=>$cm->id, 'courseid'=>$courseid))){
                $record->sync = 1;
                $DB->update_record('powerschool_assignment_sync',$record);
            }else{
                $record = new stdClass();
                $record->cmid = $cm->id;
                $record->courseid = $courseid;
                $record->sync = 1;
                $DB->insert_record('powerschool_assignment_sync',$record,false);
            }


            return $powerschool_assignment;
        }else
            return null;

    }

    public function graded_assignments(\core\event\user_graded $instant = null, $grade = null){
        global $DB;
        if($instant == null && $grade == null)
            throw new moodle_exception('error_grade', 'local_powerschool', new moodle_url('/admin/'), null, 'Give me please \core\event\user_graded instant or grade instant');

        if($grade == null)
            $grade = $instant->get_grade();


        if($grade->grade_item->itemtype == 'course'){
            $grades = $DB->get_records_sql('SELECT gg.*
                                                FROM {grade_items} gi
                                                  LEFT JOIN {grade_grades}  gg ON gg.itemid=gi.id AND gg.userid=:userid
                                                WHERE gi.courseid=:courseid AND gi.itemtype="mod" '
                                            ,array('userid'=>$grade->userid,'courseid'=>$grade->grade_item->courseid));

            foreach($grades as $grade_record){
                $grade_object = new grade_grade($grade_record, false);
                $grade_object->grade_item = $grade_object->load_grade_item();

                \core\event\user_graded::create_from_grade($grade_object)->trigger();
            }

        }elseif($grade->grade_item->itemtype == 'mod') {

            $cm = get_coursemodule_from_instance($grade->grade_item->itemmodule,$grade->grade_item->iteminstance,$grade->grade_item->courseid,false,MUST_EXIST);

            if(!$this->cm_assignment_sync((object)array('cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid )))
                return true;

            if($this->enable_groups){
                $groups = groups_get_user_groups($grade->grade_item->courseid,$grade->userid);
                $groups = array_pop($groups);
                if(!empty($groups)){
                    $info_records = $DB->get_records_sql('SELECT a.id, d.data as userid, a.assignmentid
                                                    FROM {user_info_field} f
                                                       LEFT JOIN {user_info_data} d ON d.fieldid=f.id AND d.userid=:userid
                                                       JOIN {powerschool_assignment} a ON a.cmid=:cmid AND a.courseid=:courseid AND a.groupid IN ('.implode(',',$groups).')
                                                    WHERE f.shortname = "powerschool777id"'
                            ,array('userid'=>$grade->userid,'cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid));
                }else{
                    $info_records = array();
                }

            }else{
                $info_records = $DB->get_records_sql('SELECT a.id, d.data as userid, a.assignmentid
                                                FROM {user_info_field} f
                                                   LEFT JOIN {user_info_data} d ON d.fieldid=f.id AND d.userid=:userid
                                                   JOIN {powerschool_assignment} a ON a.cmid=:cmid AND a.courseid=:courseid
                                                WHERE f.shortname = "powerschool777id"'
                            ,array('userid'=>$grade->userid,'cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid));
            }

            foreach($info_records as $info) {
                $score = new stdClass();
                $score->assignment_score = new stdClass();
                $score->assignment_score->score_entered = $grade->finalgrade - $grade->get_grade_min();
                $score->assignment_score->points_possible = $grade->get_grade_max() - $grade->get_grade_min();
                $score->assignment_score->exempt = ($grade->is_excluded()) ? true : false;

                $this->request('put', "/powerschool-ptg-api/v2/assignment/{$info->assignmentid}/student/{$info->userid}/score", array(), json_encode($score), true);
            }
            return true;
        }
    }

}
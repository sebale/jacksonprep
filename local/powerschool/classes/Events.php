<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/powerschool/locallib.php');
require_once($CFG->dirroot.'/local/powerschool/classes/Users.php');
require_once($CFG->dirroot.'/local/powerschool/classes/Courses.php');


class events_handler {

    public static function moodle_user_updated($user){
        if(!get_config('local_powerschool', 'sync_moodle_user_change')){
            return true;
        }

        $plugin = new Users();
        return $plugin->sync_moodle_user($user);
    }
    public static function moodle_course_deleted($course){
        global $DB;
        $plugin = new Courses();

        $DB->delete_records('powerschool_courses',array('mcourse'=>$course->id));
        $DB->delete_records('powerschool_course_fields',array('courseid'=>$course->id));
        $DB->delete_records('powerschool_enrollments',array('courseid'=>$course->id));
        $DB->delete_records('powerschool_assignment',array('courseid'=>$course->id));
        $DB->delete_records('powerschool_assignment_sync',array('courseid'=>$course->id));
        
        /*$modules = $DB->get_records('powerschool_assignment',array('courseid'=>$course->id));
        foreach($modules as $item){
            $module = new stdClass();
            $module->cmid = $item->cmid;
            $plugin->delete_assignment($module);
        }*/

        if($plugin->enable_groups)
            $DB->delete_records('powerschool_course_cat',array('courseid_group'=>$course->id));

        return true;
    }
    public static function moodle_mod_created($module){
        $plugin = new Courses();
        return $plugin->create_assignment_sync($module);
    }
    public static function moodle_mod_updated($module){
        $plugin = new Courses();
        return $plugin->update_assignment($module);
    }
    public static function moodle_mod_deleted($module){
        global $DB;
        $plugin = new Courses();
        return $plugin->delete_assignment($module);
    }
    public static function moodle_user_unenrolled($instant){
        global $DB;
        $DB->delete_records('powerschool_enrollments',array('userid'=>$instant->userid, 'courseid'=>$instant->courseid));
        return true;
    }
    public static function moodle_user_graded(\core\event\user_graded $instant){
        $plugin = new Courses();
        $plugin->graded_assignments($instant);
        return true;
    }
    public static function moodle_course_category_deleted ($instant){
        global $DB;
        $plugin = new Courses();

        if(!$plugin->enable_groups)
            $DB->delete_records('powerschool_course_cat',array('catid'=>$instant->id));

        return true;
    }

}
















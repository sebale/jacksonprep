<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$handlers = array(
    'user_updated' => array (
        'handlerfile'     => '/local/powerschool/classes/Events.php',
        'handlerfunction' => array('events_handler', 'moodle_user_updated'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
    'course_deleted' => array (
        'handlerfile'     => '/local/powerschool/classes/Events.php',
        'handlerfunction' => array('events_handler', 'moodle_course_deleted'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
    'mod_created' => array (
        'handlerfile'     => '/local/powerschool/classes/Events.php',
        'handlerfunction' => array('events_handler', 'moodle_mod_created'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
    'mod_updated' => array (
        'handlerfile'     => '/local/powerschool/classes/Events.php',
        'handlerfunction' => array('events_handler', 'moodle_mod_updated'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
    'mod_deleted' => array (
        'handlerfile'     => '/local/powerschool/classes/Events.php',
        'handlerfunction' => array('events_handler', 'moodle_mod_deleted'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
    'user_unenrolled' => array (
        'handlerfile'     => '/local/powerschool/classes/Events.php',
        'handlerfunction' => array('events_handler', 'moodle_user_unenrolled'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
    'course_category_deleted' => array (
        'handlerfile'     => '/local/powerschool/classes/Events.php',
        'handlerfunction' => array('events_handler', 'moodle_course_category_deleted'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
);

$observers = array(
    array(
        'eventname'   => '\core\event\user_graded',
        'callback'    => 'events_handler::moodle_user_graded',
        'includefile' => '/local/powerschool/classes/Events.php'
    )
);

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    //  It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');
require_once('classes/Users.php');

class sync_users_form extends moodleform {

    /**
     * Define the form.
     */
    public function definition () {
		global $plugin;
		$params = json_decode(get_config('local_powerschool', 'sync_params'));

		$data = (array)$plugin->request('get',"/ws/v1/school/{$plugin->school_id}/student?page=1&pagesize=1")->students;
		$data['@expansions'] = (!empty($data['@expansions']))?str_replace(', fees','',$data['@expansions']):''; // do not include fees, maibe nex time
		$expansions_student = (!empty($data['@expansions']))?explode(',',str_replace(' ','',$data['@expansions'])):array();
		
		$data = (array)$plugin->request('get',"/ws/v1/school/{$plugin->school_id}/staff?page=1&pagesize=1")->staffs;
		$data['@expansions'] = (!empty($data['@expansions']))?str_replace(', school_affiliations','',$data['@expansions']):''; // do not include school_affiliations, maybe nex time
		$expansions_staff = (!empty($data['@expansions']))?explode(',',str_replace(' ','',$data['@expansions'])):array();
		
        $mform = $this->_form;

		if(!isset($params->section_type)) {
			$mform->addElement('header', 'moodle', get_string('section_setting', 'local_powerschool'));
			$types = array('groups' => get_string('groups', 'local_powerschool'), 'course' => get_string('course', 'local_powerschool'));
			$section_type = $mform->addElement('select', 'section_type', get_string('section_type', 'local_powerschool'), $types);
			$section_type->setSelected('course');
			$mform->addHelpButton('section_type', 'section_type', 'local_powerschool');
		}else{
			$mform->addElement('hidden', 'section_type', $params->section_type);
			$mform->setType('section_type', PARAM_RAW);
		}


        $mform->addElement('header', 'moodle', get_string('students', 'local_powerschool'));
		$mform->addElement('checkbox', 'students', get_string('include'));
		foreach($expansions_student as $item){
			$mform->addElement('checkbox', 'student_expansions_'.$item, get_string($item, 'local_powerschool'));
		}
		
        $mform->addElement('header', 'moodle', get_string('staffs', 'local_powerschool'));
		$mform->addElement('checkbox', 'staffs', get_string('include'));
		foreach($expansions_staff as $item){
			$mform->addElement('checkbox', 'staff_expansions_'.$item, get_string($item, 'local_powerschool'));
		}

		$mform->addElement('header', 'moodle', get_string('users_role', 'local_powerschool'));
		$roles = role_fix_names(get_all_roles());
		$roles_arr = array();
		foreach($roles as $role){
			$roles_arr[$role->id] = $role->localname;
		}
		$student = $mform->addElement('select', 'student_role', get_string('student_role', 'local_powerschool'), $roles_arr);
		$student->setSelected(5);

		$teacher = $mform->addElement('select', 'teacher_role', get_string('co_teacher_role', 'local_powerschool'), $roles_arr);
		$teacher->setSelected(4);

		$teacher = $mform->addElement('select', 'teacher_lead_role', get_string('lead_teacher_role', 'local_powerschool'), $roles_arr);
		$teacher->setSelected(3);

        $this->add_action_buttons(false, get_string('sync', 'local_powerschool'));

		$this->set_data($params);
    }


    public function validation($params, $files) {
		$errors = parent::validation($params, $files);
		if(!isset($params['students']) && !isset($params['staffs'])){
			$errors['students'] = get_string('must_exist_students_or_staffs', 'local_powerschool');
			$errors['staffs'] = get_string('must_exist_students_or_staffs', 'local_powerschool');
		}
		
        return $errors;
    }
}



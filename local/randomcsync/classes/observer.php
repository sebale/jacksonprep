<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local stuff for category enrolment plugin.
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Event observer for transcripts.
 */
class local_randomcsync_observer {
    
    /**
     * Triggered when 'course_completed' event is triggered.
     *
     * @param \core\event\user_created $event
     */
    public static function randomcsync_user_created(\core\event\user_created $event) {
        global $DB, $CFG, $USER;
        
        require_once($CFG->dirroot . '/cohort/lib.php');
        
        $enable = get_config('local_randomcsync', 'enabled');
        
        if ($enable){
            $user = $DB->get_record('user', array('id'=>$event->objectid));
            $ismember = $DB->get_record('cohort_members', array('userid'=>$user->id));
            if ($user and !$ismember){
                $cohort = $DB->get_record_sql('SELECT c.id FROM {cohort} c ORDER BY RAND() LIMIT 1');
                if ($cohort->id > 0){
                    cohort_add_member($cohort->id, $user->id);
                }
            }
        }
        
    }
    
    /**
     * Triggered when 'course_completed' event is triggered.
     *
     * @param \core\event\user_loggedin $event
     */
    public static function randomcsync_user_loggedin(\core\event\user_loggedin $event) {
        global $DB, $CFG, $USER;
        
        $enable = get_config('local_randomcsync', 'enabled');
        
        if ($enable){
            $user = $DB->get_record('user', array('id'=>$event->objectid));
            $cohort_member = $DB->get_record('cohort_members', array('userid'=>$user->id));
            
            if ($cohort_member){
                $enrol = $DB->get_record('enrol', array('enrol'=>'cohort', 'customint1'=>$cohort_member->cohortid));
                if (isset($enrol->courseid)){
                    redirect($CFG->wwwroot.'/course/view.php?id='.$enrol->courseid);
                }
            } else {
                $courses = enrol_get_my_courses();
                if (count($courses)){
                    foreach ($courses as $course){
                        redirect($CFG->wwwroot.'/course/view.php?id='.$course->id);
                        break;
                    }
                }
            }
        }
        
    }
}

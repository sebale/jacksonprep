<?php
// Chat
//
// Chat is built to work with any LMS designed in Moodle 
//
// Moodle
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Chat is built as a plugin for Moodle.

/**
 * Chat
 *
 *
 * @package    	local_randomcsync
 * @copyright  	2014-2015 SEBALE LLC
 * @license    	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @created by	SEBALE LLC
 * @website		www.sebale.net
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('local_randomcsync', get_string('settings', 'local_randomcsync'));

if (!$ADMIN->locate('randomcsync')){
	$ADMIN->add('localplugins', new admin_category('randomcsync', get_string('pluginname', 'local_randomcsync')));
	$ADMIN->add('randomcsync', $settings);
}
$settings->add(new admin_setting_heading('local_randomcsync/title', get_string('title', 'local_randomcsync'), ''));

$name = 'local_randomcsync/enabled';
$title = get_string('enabled', 'local_randomcsync');
$description = get_string('enabled_desc', 'local_randomcsync');
$default = false;
$setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
$settings->add($setting);



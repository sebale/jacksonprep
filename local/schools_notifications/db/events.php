<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    schools_notifications
 * @copyright  2015 SEBALE LLC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$handlers = array(
    'course_updated' => array (
        'handlerfile'     => '/local/schools_notifications/locallib.php',
        'handlerfunction' => array('schools_notifications_handler', 'course_updated'),
        'schedule'        => 'instant',
		'internal'        => 1,
    ),   
    'mod_updated' => array (
        'handlerfile'     => '/local/schools_notifications/locallib.php',
        'handlerfunction' => array('schools_notifications_handler', 'course_module_updated'),
        'schedule'        => 'instant',
		'internal'        => 1,
    ), 
    'mod_created' => array (
        'handlerfile'     => '/local/schools_notifications/locallib.php',
        'handlerfunction' => array('schools_notifications_handler', 'course_module_created'),
        'schedule'        => 'instant',
		'internal'        => 1,
    ), 
    'course_completed' => array (
        'handlerfile'     => '/local/schools_notifications/locallib.php',
        'handlerfunction' => array('schools_notifications_handler', 'course_module_completion_updated'),
        'schedule'        => 'instant',
		'internal'        => 1,
    ), 
);

?>

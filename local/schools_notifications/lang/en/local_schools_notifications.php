<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    schools_notifications
 * @copyright  2015 SEBALE LLC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Schools Notifications';

$string['settings'] = 'Schools notifications settings';
$string['title'] = 'Enable notification';

$string['email'] = 'Email';
$string['text_message'] = 'Text message';
$string['course_update_title'] = 'Course updated';
$string['mod_created_title'] = 'Course module created';
$string['mod_updated_title'] = 'Course module updated';
$string['certificate_received_title'] = 'Certificate received';
$string['course_incompleted_title'] = 'Incomplete course';
$string['course_failure_title'] = 'Course failure';
$string['course_inactivity_title'] = 'Course inactivity';
$string['mod_due_date_title'] = 'Course module due date';
$string['enable'] = 'Enable';
$string['users'] = 'For user roles';
$string['typemessage'] = 'Message type';
$string['courses'] = 'For courses';

$string['course_updated'] = 'Course updated';
$string['course_module_updated'] = 'Course module updated';
$string['course_module_created'] = 'Course module created';
$string['course_failure'] = 'Course failure';
$string['certificate_received'] = 'Certificate received';
$string['course_incompleted'] = 'Course incompleted';
$string['course_inactivity'] = 'Course inactivity';
$string['course_module_due_date'] = 'Course module due date';

$string['have_notification'] = 'You have {$a} notification';

$string['mark_read'] = 'Mark as read';
$string['mark_unread'] = 'Mark as unread';
$string['delete'] = 'Delete';

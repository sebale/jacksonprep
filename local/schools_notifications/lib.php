<?php 

function local_schools_notifications_send_emails(){
	
	global $DB,$SITE,$CFG;
	require_once($CFG->libdir.'/csvlib.class.php');
			
	$records = $DB->get_records('schools_notifications', array('typemessage'=>'email','reading'=>0));
	$sender = get_admin();
	foreach($records as $record){
		$info = $DB->get_record_sql("SELECT  CONCAT(u.firstname, ' ', u.lastname) as user, u.email, c.fullname AS course_name
															FROM {user} u, {course} c
															WHERE u.id=$record->userid AND c.id=$record->courseid
														");
		$user = $sender;
		$result = 0;
		$user->email = $info->email;
		
		if($record->type == 'course_updated'){
			$subject = $SITE->fullname.': update course';
			$body = "Ger $info->user, course $info->course_name is updated";
			$result = (int) email_to_user($user, $sender, $subject, $body, $body);
		}elseif($record->type == 'course_module_updated'){
			$data = unserialize($record->data);
			$updated_user = $DB->get_record_sql("SELECT  CONCAT(u.firstname, ' ', u.lastname) as user FROM {user} u WHERE u.id=$data->userid ")->user;			
			$subject = $SITE->fullname.': update course activiti';
			$body = "Ger $info->user, user $updated_user updated course module $data->name";
			$result = (int) email_to_user($user, $sender, $subject, $body, $body);
		}elseif($record->type == 'course_module_created'){
			$data = unserialize($record->data);
			$updated_user = $DB->get_record_sql("SELECT  CONCAT(u.firstname, ' ', u.lastname) as user FROM {user} u WHERE u.id=$data->userid ")->user;			
			$subject = $SITE->fullname.': created course activiti';
			$body = "Ger $info->user, user $updated_user created course module $data->name";
			$result = (int) email_to_user($user, $sender, $subject, $body, $body);
		}elseif($record->type == 'course_failure'){				
			$data = unserialize($record->data);
	
			$export = new csv_export_writer();
			$export->set_filename('course_failure-report');
			$export->add_data( array(
				'User',
				'Course',
				'Grade',
				'Graded',
				'Modules',
			));
			foreach($data as $row){
				unset($row->id);
				$row = (array)$row;
				$export->add_data($row);
			}
			$content = $export->print_csv_data(true);
			$attachname = 'course_failure.csv';
			$attachment = $CFG->dataroot . '/'. $attachname;

			$fd = fopen ($attachment, "w+");
			fputs($fd, $content);
			fclose($fd);			
			$subject = $SITE->fullname.': course failure';
			$body = "Ger $info->user, on course $info->course_name some users have status course failure. Details on attachment";
			$result = (int) email_to_user($user, $sender, $subject, $body, $body, $attachname, $attachname);
		}elseif($record->type == 'certificate_received'){				
			$data = unserialize($record->data);
			
			$export = new csv_export_writer();
			$export->set_filename('certificate_received-report');
			$export->add_data( array(
				'User',
				'Course',
				'Certificate',
				'Code',
				'Date',
			));
			foreach($data as $row){
				$export->add_data(array($row->user,
														$row->course_name,
														$row->certificate_name,
														$row->code,
														date('d/m/Y',$row->timecreated)
												));
			}
			$content = $export->print_csv_data(true);
			$attachname = 'certificate_received.csv';
			$attachment = $CFG->dataroot . '/'. $attachname;

			$fd = fopen ($attachment, "w+");
			fputs($fd, $content);
			fclose($fd);			
			$subject = $SITE->fullname.': certificate received';
			$body = "Ger $info->user, on course $info->course_name some users received certificate. Details on attachment";
			$result = (int) email_to_user($user, $sender, $subject, $body, $body, $attachname, $attachname);
		}elseif($record->type == 'course_incompleted'){				
			$data = unserialize($record->data);
			
			$export = new csv_export_writer();
			$export->set_filename('course_incompleted-report');
			$export->add_data( array(
				'User',
				'Course',
				'Time enrolled',
				'Time started',
			));
			foreach($data as $row){
				$export->add_data(array($row->user,
														$row->course_name,
														date('d/m/Y',$row->timeenrolled),
														date('d/m/Y',$row->timestarted)
												));
			}
			$content = $export->print_csv_data(true);
			$attachname = 'course_incompleted.csv';
			$attachment = $CFG->dataroot . '/'. $attachname;

			$fd = fopen ($attachment, "w+");
			fputs($fd, $content);
			fclose($fd);			
			$subject = $SITE->fullname.': course incompleted';
			$body = "Ger $info->user, some users incompleted course. Details on attachment";
			$result = (int) email_to_user($user, $sender, $subject, $body, $body, $attachname, $attachname);
		}elseif($record->type == 'course_inactivity'){				
			$data = unserialize($record->data);
			
			$export = new csv_export_writer();
			$export->set_filename('course_inactivity-report');
			$export->add_data( array(
				'Course',
				'Last access'
			));
			foreach($data as $row){
				$export->add_data(array($row->course_name,
														date('d/m/Y',$row->lastaccess)
												));
			}
			$content = $export->print_csv_data(true);
			$attachname = 'course_inactivity.csv';
			$attachment = $CFG->dataroot . '/'. $attachname;

			$fd = fopen ($attachment, "w+");
			fputs($fd, $content);
			fclose($fd);			
			$subject = $SITE->fullname.': course inactivity';
			$body = "Ger $info->user, some courses are inactivity. Details on attachment";
			$result = (int) email_to_user($user, $sender, $subject, $body, $body, $attachname, $attachname);
		}elseif($record->type == 'course_module_due_date'){				
			$data = (object)unserialize($record->data);
			
			$subject = $SITE->fullname.': course module due date';
			(!empty($data->assign_name) && !empty($data->quiz_name))?$and="and":$and="";
			$body = "Ger $info->user, on course $info->course_name module $data->assign_name $and $data->quiz_name end today";
			$result = (int) email_to_user($user, $sender, $subject, $body, $body);
		}		
			
		$update = new stdClass();
		$update->id = $record->id;
		$update->reading = $result;
		$DB->update_record('schools_notifications', $update);
		
	}
	
	return true;
}

function local_schools_notifications_cron(){
	 global $DB;
	$config = get_config('local_schools_notifications');
 	if(isset($config->mod_due_date_enabled) && $config->mod_due_date_enabled == 1){
		$timestart = time();
		$timefinish = strtotime("+1 day");
		$roleid = (empty($config->mod_due_date_users))?'-1':str_replace(',', ' OR ra.roleid=',$config->mod_due_date_users);
		$courseid = (empty($config->mod_due_date_courses))?'-1':str_replace(',', ' OR c.instanceid=',$config->mod_due_date_courses);												
		$data = $DB->get_records_sql(" SELECT ra.userid, c.instanceid as courseid, ass_t.name as assign_name, q_t.name as quiz_name
													FROM {role_assignments} ra
														LEFT JOIN {context} c ON c.contextlevel=50 AND (c.instanceid=$courseid) AND ra.contextid=c.id
														LEFT JOIN {assign} ass_t ON ass_t.duedate > $timestart AND ass_t.duedate < $timefinish  AND ass_t.course = c.instanceid																			
														LEFT JOIN {quiz} q_t ON q_t.timeclose > $timestart AND q_t.timeclose < $timefinish  AND q_t.course = c.instanceid
													WHERE ra.roleid=$roleid GROUP BY ra.userid
													"); 
		$record = new stdClass();
		$record->type = 'course_module_due_date';
		$record->timecreate = time();
			
		$data = (count($data) <= 1)?array():$data;
		$mod_due_date_typemessage = (empty($config->mod_due_date_typemessage))?array():explode(',',$config->mod_due_date_typemessage);
		foreach($mod_due_date_typemessage as $typemessage){
			$record->typemessage = $typemessage;
			foreach($data as $item){
				if(empty($item->assign_name) && empty($item->quiz_name))continue;
				$record->userid  = $item->userid;
				$record->courseid = $item->courseid;
				$record->data = serialize(array('assign_name'=>$item->assign_name,'quiz_name'=>$item->quiz_name));
				$DB->insert_record('schools_notifications', $record);
			}			
		}
	}
	
 	if(isset($config->course_incompleted_enabled) && $config->course_incompleted_enabled == 1){
		$timestart = strtotime("-1 day");
		$courseid = (empty($config->course_incompleted_courses))?'-1':str_replace(',', ' OR cc.course=',$config->course_incompleted_courses);		
		$roleid = (empty($config->course_incompleted_users))?'-1':str_replace(',', ' OR ra.roleid=',$config->course_incompleted_users);
		$instanceid = (empty($config->course_incompleted_courses))?'-1':str_replace(',', ' OR c.instanceid=',$config->course_incompleted_courses);
		$for_users = $DB->get_records_sql(" SELECT ra.userid, c.instanceid AS course
													FROM  {context} c 
														LEFT JOIN {role_assignments} ra ON (ra.roleid=$roleid) AND ra.contextid=c.id
													WHERE c.contextlevel=50 AND (c.instanceid=$instanceid) GROUP BY ra.userid
													"); 
		
		$data = $DB->get_records_sql(" SELECT cc.*, CONCAT(u.firstname, ' ', u.lastname) as user, c.fullname AS course_name
													FROM {course_completions} cc 
														LEFT JOIN {user} u ON u.id=cc.userid
														LEFT JOIN {course} c ON c.id=cc.course
													WHERE (cc.course=$courseid) AND cc.timestarted>0 AND cc.timecompleted IS NULL 
													"); 
													
		if(!empty($data) && !empty($for_users)){					
			$record = new stdClass();
			$record->type = 'course_incompleted';
			$record->timecreate = time();
			$record->data = serialize($data);
			
			$course_incompleted_typemessage = (empty($config->course_incompleted_typemessage))?array():explode(',',$config->course_incompleted_typemessage);
			foreach($course_incompleted_typemessage as $typemessage){
				$record->typemessage = $typemessage;
				foreach($for_users as $for_user){
					$record->userid  = $for_user->userid;
					$record->courseid = $for_user->course;
					$DB->insert_record('schools_notifications', $record);
				}				
			}
		}
	} 
	
	if(isset($config->certificate_received_enabled) && $config->certificate_received_enabled == 1){
		$timestart = strtotime("-1 day");
		$instanceid = (empty($config->certificate_received_courses))?'-1':str_replace(',', ' OR c.instanceid=',$config->certificate_received_courses);		
		$courses = (empty($config->certificate_received_courses))?'-1':str_replace(',', ' OR cr.course=',$config->certificate_received_courses);		
		$roleid = (empty($config->certificate_received_users))?'-1':str_replace(',', ' OR ra.roleid=',$config->certificate_received_users);	
		$for_users = $DB->get_records_sql(" SELECT ra.userid, c.instanceid AS course
													FROM {context} c 
														LEFT JOIN {role_assignments} ra ON (ra.roleid=$roleid) AND ra.contextid=c.id
													WHERE c.contextlevel=50 AND (c.instanceid=$instanceid) GROUP BY ra.userid
													"); 
		$data_all = $DB->get_records_sql(" SELECT ci.*, CONCAT(u.firstname, ' ', u.lastname) as user, c.fullname AS course_name, crr.name as certificate_name
													FROM {certificate} cr
														LEFT JOIN {certificate_issues} ci ON cr.id=ci.certificateid AND ci.timecreated>$timestart
														LEFT JOIN {certificate} crr ON crr.id=ci.certificateid
														LEFT JOIN {user} u ON u.id=ci.userid
														LEFT JOIN {course} c ON c.id=cr.course
													WHERE (cr.course=$courses)
													"); 
													
		$data = array();
		foreach($data_all as $item){
			if(!empty($item->user)) $data[] = $item;
		}
 		if(!empty($data) && !empty($for_users)){											
			$record = new stdClass();
			$record->type = 'certificate_received';
			$record->timecreate = time();		
			$record->data = serialize($data);
			
			$certificate_received_typemessage = (empty($config->certificate_received_typemessage))?array():explode(',',$config->certificate_received_typemessage);
			foreach($certificate_received_typemessage as $typemessage){
				$record->typemessage = $typemessage;
				foreach($for_users as $for_user){
					$record->userid  = $for_user->userid;
					$record->courseid = $for_user->course;
					$DB->insert_record('schools_notifications', $record);
				}			
			}
		} 
	}
	
	if(isset($config->course_failure_enabled) && $config->course_failure_enabled == 1){
		$instanceid = (empty($config->course_failure_courses))?'-1':str_replace(',', ' OR c.instanceid=',$config->course_failure_courses);		
		$courses = (empty($config->course_failure_courses))?'-1':str_replace(',', ' OR e.courseid=',$config->course_failure_courses);		
		$roleid = (empty($config->course_failure_users))?'-1':str_replace(',', ' OR ra.roleid=',$config->course_failure_users);	
		$for_users = $DB->get_records_sql(" SELECT ra.userid, c.instanceid AS course
													FROM {context} c 
														LEFT JOIN {role_assignments} ra ON (ra.roleid=$roleid) AND ra.contextid=c.id
													WHERE c.contextlevel=50 AND (c.instanceid=$instanceid) GROUP BY ra.userid
													"); 
		$data = $DB->get_records_sql("SELECT 
			SQL_CALC_FOUND_ROWS ue.id, 
			CONCAT(u.firstname, ' ', u.lastname) as user,
			c.fullname as course,
			g.grade,
			gm.graded,
			cm.modules
			  FROM {user_enrolments} as ue
			   LEFT JOIN {user} as u ON u.id = ue.userid
			   LEFT JOIN {enrol} as e ON e.id = ue.enrolid
									LEFT JOIN {course} as c ON c.id = e.courseid
			   LEFT JOIN {course_completions} as cc ON cc.course = e.courseid AND cc.userid = ue.userid
			   LEFT JOIN (SELECT gi.courseid, gg.userid, (gg.finalgrade/gg.rawgrademax)*100 AS grade FROM {grade_items} gi, {grade_grades} gg WHERE gi.itemtype = 'course' AND gg.itemid = gi.id GROUP BY  gi.courseid, gg.userid) as g ON g.courseid = c.id AND g.userid = u.id
			   LEFT JOIN (SELECT gi.courseid, gg.userid, count(gg.id) graded FROM {grade_items} gi, {grade_grades} gg WHERE gi.itemtype = 'mod' AND gg.itemid = gi.id GROUP BY  gi.courseid, gg.userid) as gm ON gm.courseid = c.id AND gm.userid = u.id
			   LEFT JOIN (SELECT courseid, count(id) as modules FROM {grade_items} WHERE itemtype = 'mod' GROUP BY courseid) as cm ON cm.courseid = c.id
			  WHERE (cc.timecompleted IS NULL OR cc.timecompleted = 0) AND gm.graded >= cm.modules AND (e.courseid = $courses) GROUP BY ue.userid, e.courseid ");
	
		 if(!empty($data) && !empty($for_users)){											
			$record = new stdClass();
			$record->type = 'course_failure';
			$record->timecreate = time();		
			$record->data = serialize($data);
			
			$course_failure_typemessage = (empty($config->course_failure_typemessage))?array():explode(',',$config->course_failure_typemessage);
			foreach($course_failure_typemessage as $typemessage){
				$record->typemessage = $typemessage;
				foreach($for_users as $for_user){
					$record->userid  = $for_user->userid;
					$record->courseid = $for_user->course;
					$DB->insert_record('schools_notifications', $record);
				}			
			}
		} 
	}	
	
	if(isset($config->course_inactivity_enabled) && $config->course_inactivity_enabled == 1){
		$instanceid = (empty($config->course_inactivity_courses))?'-1':str_replace(',', ' OR c.instanceid=',$config->course_inactivity_courses);		
		$courses = (empty($config->course_inactivity_courses))?'-1':str_replace(',', ' OR log.courseid=',$config->course_inactivity_courses);		
		$roleid = (empty($config->course_inactivity_users))?'-1':str_replace(',', ' OR ra.roleid=',$config->course_inactivity_users);	
		$for_users = $DB->get_records_sql(" SELECT ra.userid, c.instanceid AS course
													FROM {context} c 
														LEFT JOIN {role_assignments} ra ON (ra.roleid=$roleid) AND ra.contextid=c.id
													WHERE c.contextlevel=50 AND (c.instanceid=$instanceid) GROUP BY ra.userid
													"); 
		$data = $DB->get_records_sql("SELECT log.id, log.courseid, log.timecreated AS lastaccess, c.fullname AS course_name
															FROM {logstore_standard_log} log
															LEFT JOIN {course} c ON c.id=log.courseid
															WHERE (log.courseid=$courses) GROUP BY log.courseid DESC							
														");
														
		$timestart = strtotime("-1 week");
		$course_inactivity = array();
		foreach($data as $item){
			if($item->lastaccess < $timestart){
				$course_inactivity[] = $item;
			}
		}
		 if(!empty($course_inactivity) && !empty($for_users)){											
			$record = new stdClass();
			$record->type = 'course_inactivity';
			$record->timecreate = time();
			$record->data = serialize($course_inactivity);
			
			$course_inactivity_typemessage = (empty($config->course_inactivity_typemessage))?array():explode(',',$config->course_inactivity_typemessage);
			foreach($course_inactivity_typemessage as $typemessage){
				$record->typemessage = $typemessage;
				foreach($for_users as $for_user){
					$record->userid  = $for_user->userid;
					$record->courseid = $for_user->course;
					$DB->insert_record('schools_notifications', $record);
				}			
			}
		} 
	} 
 	 
	local_schools_notifications_send_emails();
}

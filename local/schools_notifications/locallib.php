<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    schools_notifications
 * @copyright  2014 SEBALE LLC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
class schools_notifications_handler {
    public static function course_updated($course){
		global $CFG, $DB;
		
		$config = get_config('local_schools_notifications');
		if($config->course_update_enabled != 1)
			return true;
		$courses = explode(',', $config->course_update_courses);
		if(!in_array($course->id, $courses))
			return true;
		$roleid = (empty($config->course_update_users))?'-1':str_replace(',', ' OR ra.roleid=',$config->course_update_users);
		$users = $DB->get_records_sql(" SELECT ra.userid
												FROM {context} c
													LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND (ra.roleid=$roleid)
												WHERE c.contextlevel=50 AND c.instanceid=$course->id GROUP BY ra.userid
												");
		
		$record = new stdClass();
		$record->courseid = $course->id;
		$record->type = 'course_updated';
		$record->data = '';
		$record->timecreate = time();
		
		$users = ($roleid == -1)?array():$users;
		$course_update_typemessage = (empty($config->course_update_typemessage))?array():explode(',',$config->course_update_typemessage);
		foreach($course_update_typemessage as $typemessage){
			$record->typemessage = $typemessage;
			foreach($users as $user){
				$record->userid  = $user->userid;
				$DB->insert_record('schools_notifications', $record);
			}			
		}

		return true;
	}
	
    public static function course_module_updated($course_module){
		global $CFG, $DB;
		
		$config = get_config('local_schools_notifications');
		if($config->mod_updated_enabled != 1)
			return true;
		$courses = explode(',', $config->mod_updated_courses);
		if(!in_array($course_module->courseid, $courses))
			return true;
		$roleid = (empty($config->mod_updated_users))?'-1':str_replace(',', ' OR ra.roleid=',$config->mod_updated_users);
		$users = $DB->get_records_sql(" SELECT ra.userid
												FROM {context} c
													LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND (ra.roleid=$roleid)
												WHERE c.contextlevel=50 AND c.instanceid=$course_module->courseid GROUP BY ra.userid
												");
		$record = new stdClass();
		$record->courseid = $course_module->courseid;
		$record->type = 'course_module_updated';
		$record->data = serialize($course_module);
		$record->timecreate = time();
		
		$users = ($roleid == -1)?array():$users;
		$mod_updated_typemessage = (empty($config->mod_updated_typemessage))?array():explode(',',$config->mod_updated_typemessage);
		foreach($mod_updated_typemessage as $typemessage){
			$record->typemessage = $typemessage;
			foreach($users as $user){
				$record->userid  = $user->userid;
				$DB->insert_record('schools_notifications', $record);
			}			
		}
		
		return true;
	}
	
    public static function course_module_created($course_module){
		global $CFG, $DB;
		$config = get_config('local_schools_notifications');
		if($config->mod_created_enabled != 1)
			return true;
		$courses = explode(',', $config->mod_created_courses);
		if(!in_array($course_module->courseid, $courses))
			return true;
		$roleid = (empty($config->mod_created_users))?'-1':str_replace(',', ' OR ra.roleid=',$config->mod_created_users);
		$users = $DB->get_records_sql(" SELECT ra.userid
												FROM {context} c
													LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND (ra.roleid=$roleid)
												WHERE c.contextlevel=50 AND c.instanceid=$course_module->courseid GROUP BY ra.userid
												");
		$record = new stdClass();
		$record->courseid = $course_module->courseid;
		$record->type = 'course_module_created';
		$record->data = serialize($course_module);
		$record->timecreate = time();
		
		$users = ($roleid == -1)?array():$users;
		$mod_updated_typemessage = (empty($config->mod_created_typemessage))?array():explode(',',$config->mod_created_typemessage);
		foreach($mod_updated_typemessage as $typemessage){
			$record->typemessage = $typemessage;
			foreach($users as $user){
				$record->userid  = $user->userid;
				$DB->insert_record('schools_notifications', $record);
			}			
		}
		
		return true;
	}

}

<?php
/**
 *
 * @package    schools_notifications
 * @copyright  2014 SEBALE LLC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');

$id = required_param('id', PARAM_INT);

require_login();
$PAGE->set_url('/local/schools_notifications/notification.php',array('id'=>$id));
$context = context_user::instance($USER->id);
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);
$PAGE->set_title('Notification');

$notification = $DB->get_record_sql("SELECT sn.*,c.fullname AS course_name
																FROM {schools_notifications} sn
																	LEFT JOIN {course} c ON c.id=sn.courseid
																WHERE sn.userid=:userid AND sn.typemessage=:typemessage AND sn.id=:id 
																",array('userid'=>$USER->id,'typemessage'=>'text_message', 'id'=>$id));
																
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string($notification->type,'local_schools_notifications'));

		if($notification->type == 'course_updated'){
			$body = html_writer::tag('h5',"Course $notification->course_name is updated");
			$body .= html_writer::tag('p',"Time: ".date('h:i a m/d/Y',$notification->timecreate), array('class'=>'notification_date'));
		}elseif($notification->type == 'course_module_updated'){
			$data = unserialize($notification->data);
			$updated_user = $DB->get_record_sql("SELECT  CONCAT(u.firstname, ' ', u.lastname) as user FROM {user} u WHERE u.id=$data->userid ")->user;			
			$body = html_writer::tag('h5',"On course $notification->course_name user $updated_user updated course module $data->name");
			$body .= html_writer::tag('p',"Time: ".date('h:i a m/d/Y',$notification->timecreate), array('class'=>'notification_date'));
		}elseif($notification->type == 'course_module_created'){
			$data = unserialize($notification->data);
			$updated_user = $DB->get_record_sql("SELECT  CONCAT(u.firstname, ' ', u.lastname) as user FROM {user} u WHERE u.id=$data->userid ")->user;			
			$body = html_writer::tag('h5',"On course $notification->course_name user $updated_user created course module $data->name");
			$body .= html_writer::tag('p',"Time: ".date('h:i a m/d/Y',$notification->timecreate), array('class'=>'notification_date'));
		}elseif($notification->type == 'course_failure'){				
			$data = unserialize($notification->data);
			
			$table = new html_table();

			$table->head = array('User','Course','Grade','Graded',	'Modules');
			$table->attributes['class'] = 'generaltable notification';
			foreach($data as $rows){
				$id = $rows->id;
				unset($rows->id);
				$rows->grade = round($rows->grade,2);
				$rows = (array)$rows;
				$row = new html_table_row($rows);
				$row->attributes['data-id'] = $id;
				$table->data[] = $row;
			}	
			
			$body = html_writer::tag('h5',"On course $notification->course_name some users have status course failure. Details in table");
			$body .= html_writer::tag('p',"Time: ".date('h:i a m/d/Y',$notification->timecreate), array('class'=>'notification_date'));
			$body .= html_writer::table($table);
		}elseif($notification->type == 'certificate_received'){	
			$data = unserialize($notification->data);
			
			$table = new html_table();

			$table->head = array('User','Course','Certificate',	'Code',	'Date');
			$table->attributes['class'] = 'generaltable notification';
			foreach($data as $rows){
				$row = new html_table_row(array($rows->user,
														$rows->course_name,
														$rows->certificate_name,
														$rows->code,
														date('d/m/Y',$rows->timecreated)
												));
				$row->attributes['data-id'] = $rows->id;
				$table->data[] = $row;
			}	
			
			$body = html_writer::tag('h5',"On course $info->course_name some users received certificate. Details in table");
			$body .= html_writer::tag('p',"Time: ".date('h:i a m/d/Y',$notification->timecreate), array('class'=>'notification_date'));
			$body .= html_writer::table($table);
		}elseif($notification->type == 'course_incompleted'){
			$data = unserialize($notification->data);
			
			$table = new html_table();

			$table->head = array('User','Course','Time enrolled','Time started');
			$table->attributes['class'] = 'generaltable notification';
			foreach($data as $rows){
				$row = new html_table_row(array($rows->user,
														$rows->course_name,
														date('d/m/Y',$rows->timeenrolled),
														date('d/m/Y',$rows->timestarted)
												));
				$row->attributes['data-id'] = $rows->id;
				$table->data[] = $row;
			}	
			
			$body = html_writer::tag('h5',"Some users incompleted course. Details in table");
			$body .= html_writer::tag('p',"Time: ".date('h:i a m/d/Y',$notification->timecreate), array('class'=>'notification_date'));
			$body .= html_writer::table($table);
		}elseif($notification->type == 'course_inactivity'){	
			$data = unserialize($notification->data);
			
			$table = new html_table();

			$table->head = array('Course','Last access');
			$table->attributes['class'] = 'generaltable notification';
			foreach($data as $rows){
				$row = new html_table_row(array($row->course_name,
														date('d/m/Y',$row->lastaccess)
												));
				$row->attributes['data-id'] = $rows->id;
				$table->data[] = $row;
			}	

			$body = html_writer::tag('h5',"Some courses are inactivity. Details on attachment");
			$body .= html_writer::tag('p',"Time: ".date('h:i a m/d/Y',$notification->timecreate), array('class'=>'notification_date'));
			$body .= html_writer::table($table);
		}elseif($notification->type == 'course_module_due_date'){				
			$data = (object)unserialize($notification->data);
			
			(!empty($data->assign_name) && !empty($data->quiz_name))?$and="and":$and="";
			$body = html_writer::tag('h5',"On course $notification->course_name module $data->assign_name $and $data->quiz_name end today");
			$body .= html_writer::tag('p',"Time: ".date('h:i a m/d/Y',$notification->timecreate), array('class'=>'notification_date'));
		}		
		if($notification->reading == 0){
			$update = new stdClass();
			$update->id = $notification->id;
			$update->reading = 1;
			$DB->update_record('schools_notifications', $update);
		}
		
echo $body;

echo $OUTPUT->footer();
?>
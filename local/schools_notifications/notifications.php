<?php
/**
 *
 * @package    schools_notifications
 * @copyright  2014 SEBALE LLC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');

$curent_page = optional_param('page', 1, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$id = optional_param('id', 0, PARAM_INT);

require_login();
$PAGE->set_url('/local/schools_notifications/notifications.php');
$context = context_user::instance($USER->id);
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);
$PAGE->set_title('Notifications');


if($action == 'mark_unread'){
	$update = new stdClass();
	$update->id = $id;
	$update->reading = 0;
	$update->userid = $USER->id;
	$DB->update_record('schools_notifications', $update);
}elseif($action == 'mark_read'){
	$update = new stdClass();
	$update->id = $id;
	$update->reading = 1;
	$update->userid = $USER->id;
	$DB->update_record('schools_notifications', $update);
}elseif($action == 'delete'){
	$delete = array();
	$delete['id'] = $id;
	$delete['userid'] = $USER->id;
	$DB->delete_records('schools_notifications', $delete);
}

$curent_page = ($curent_page<=0)?1:$curent_page;
$offset = ($curent_page-1)*30;
$notifications = $DB->get_records_sql("SELECT sn.*,c.fullname AS course_name
																FROM {schools_notifications} sn
																	LEFT JOIN {course} c ON c.id=sn.courseid
																WHERE sn.userid=:userid AND sn.typemessage=:typemessage ORDER BY sn.timecreate DESC LIMIT $offset, 30
																",array('userid'=>$USER->id,'typemessage'=>'text_message'));
$count_notifi = $DB->count_records('schools_notifications', array('userid'=>$USER->id,'typemessage'=>'text_message'));
$pages = ceil($count_notifi/30);

$renderer = $PAGE->get_renderer('local_schools_notifications');

if($count_notifi==0)
	$PAGE->add_body_class('notifi_empty');

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('have_notification','local_schools_notifications',$count_notifi));
if($count_notifi>0)
	echo $renderer->notifications_table($notifications,$pages,$curent_page);

echo $OUTPUT->footer();
?>
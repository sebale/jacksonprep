<?php
/**
 *
 * @package    schools_notifications
 * @copyright  2014 SEBALE LLC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
defined('MOODLE_INTERNAL') || die;

class local_schools_notifications_renderer extends plugin_renderer_base {
	public function notifications_table($notifications,$pages,$curent_page) {
		global $PAGE, $CFG;
		$html = html_writer::start_tag('ul',array('class'=>'notification-list clearfix'));
			$i = 1;
			foreach($notifications as $notification){
				$html .= html_writer::start_tag('li',array('class'=>'item','read'=>$notification->reading));
					$html .= $this->get_nofication_menu($notification);
					$item = html_writer::tag('i',$i++,array('class'=>'item-number'));
					$item .= html_writer::tag('h4',$notification->course_name,array('class'=>'course'));
					$item .= html_writer::tag('h4',get_string($notification->type,'local_schools_notifications'),array('class'=>'notifi-name'));
					$item .= html_writer::tag('p',date('h:i a m/d/Y',$notification->timecreate),array('class'=>'notifi-time'));
					$html .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/schools_notifications/notification.php',array('id'=>$notification->id)),$item);
				$html .= html_writer::end_tag('li');
			}
		$html .= html_writer::end_tag('ul');//notification_list
			 
		if($pages>1){	
			 $next = ($pages < $curent_page+1)?$pages:$curent_page+1;
			 $prev = (1 > $curent_page-1)?1:$curent_page-1;
			 $nawigation_next = html_writer::link(new moodle_url($PAGE->url, array('page'=>$next)), '<i class="fa fa-angle-right"></i>');
			 $nawigation_prev = html_writer::link(new moodle_url($PAGE->url, array('page'=>$prev)), '<i class="fa fa-angle-left"></i>');
			 $nawigation_all = '';
			 
			 for($i=1;$i<=$pages;$i++){
				 $nawigation_all .= html_writer::link(new moodle_url($PAGE->url, array('page'=>$i)), $i);
			 }
			$html .= html_writer::tag('div',$nawigation_prev.$nawigation_all.$nawigation_next, array('class'=>'navigation'));
		}
		return $html;

	}
	
	public function get_nofication_menu($notification){
		global $CFG;
		$menu = html_writer::tag('i','',array('class'=>'fa fa-ellipsis-v','data-toggle'=>'dropdown'));
		if($notification->reading){
			$link = html_writer::link(new moodle_url($CFG->wwwroot.'/local/schools_notifications/notifications.php', array('action'=>'mark_unread','id'=>$notification->id)),get_string('mark_unread','local_schools_notifications'));
			$list = html_writer::tag('li',$link);			
		}else{			
			$link = html_writer::link(new moodle_url($CFG->wwwroot.'/local/schools_notifications/notifications.php', array('action'=>'mark_read','id'=>$notification->id)),get_string('mark_read','local_schools_notifications'));
			$list = html_writer::tag('li',$link);
		}
		$link = html_writer::link(new moodle_url($CFG->wwwroot.'/local/schools_notifications/notifications.php', array('action'=>'delete','id'=>$notification->id)),get_string('delete','local_schools_notifications'));
		$list .= html_writer::tag('li',$link);
		$menu .= html_writer::tag('ul',$list,array('class'=>'dropdown-menu'));
		return html_writer::tag('div',$menu,array('class'=>'dropdown'));
	}
}
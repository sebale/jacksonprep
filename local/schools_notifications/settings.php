<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * IntelliBoard.net
 *
 *
 * @package    schools_notifications
 * @copyright  2014 SEBALE LLC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;
global $DB;

$roles = get_all_roles();
foreach($roles as $user_role){
	if($user_role->id == 8 || $user_role->id == 7 ||$user_role->id == 6) continue;
	$user_roles[$user_role->id] = $user_role->shortname;
}
$types = array('email'=>get_string('email', 'local_schools_notifications'), 'text_message'=>get_string('text_message', 'local_schools_notifications'));
$all_courses = get_courses();
foreach($all_courses as $course){
	if($course->id == 1) continue;
	$courses[$course->id] = $course->fullname;
}

$settings = new admin_settingpage('local_schools_notifications', get_string('settings', 'local_schools_notifications'));

$ADMIN->add('localplugins', $settings);

$settings->add(new admin_setting_heading('local_schools_notifications/course_update_title', get_string('course_update_title', 'local_schools_notifications'), ''));

	$name = 'local_schools_notifications/course_update_enabled';
	$title = get_string('enable', 'local_schools_notifications');
	$default = false;
	$setting = new admin_setting_configcheckbox($name, $title, null, $default, true, false);
	$settings->add($setting); 
	
	$name = 'local_schools_notifications/course_update_courses';
	$title = get_string('courses', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $courses);
	$settings->add($setting); 

	$name = 'local_schools_notifications/course_update_users';
	$title = get_string('users', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $user_roles);
	$settings->add($setting);

	$name = 'local_schools_notifications/course_update_typemessage';
	$title = get_string('typemessage', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $types);
	$settings->add($setting); 

$settings->add(new admin_setting_heading('local_schools_notifications/mod_updated_title', get_string('mod_updated_title', 'local_schools_notifications'), ''));

	$name = 'local_schools_notifications/mod_updated_enabled';
	$title = get_string('enable', 'local_schools_notifications');
	$default = false;
	$setting = new admin_setting_configcheckbox($name, $title, null, $default, true, false);
	$settings->add($setting); 
	
	$name = 'local_schools_notifications/mod_updated_courses';
	$title = get_string('courses', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $courses);
	$settings->add($setting); 

	$name = 'local_schools_notifications/mod_updated_users';
	$title = get_string('users', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $user_roles);
	$settings->add($setting); 

	$name = 'local_schools_notifications/mod_updated_typemessage';
	$title = get_string('typemessage', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $types);
	$settings->add($setting); 

$settings->add(new admin_setting_heading('local_schools_notifications/mod_created_title', get_string('mod_created_title', 'local_schools_notifications'), ''));

	$name = 'local_schools_notifications/mod_created_enabled';
	$title = get_string('enable', 'local_schools_notifications');
	$default = false;
	$setting = new admin_setting_configcheckbox($name, $title, null, $default, true, false);
	$settings->add($setting); 
	
	$name = 'local_schools_notifications/mod_created_courses';
	$title = get_string('courses', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $courses);
	$settings->add($setting); 

	$name = 'local_schools_notifications/mod_created_users';
	$title = get_string('users', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $user_roles);
	$settings->add($setting); 

	$name = 'local_schools_notifications/mod_created_typemessage';
	$title = get_string('typemessage', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $types);
	$settings->add($setting); 
	
$settings->add(new admin_setting_heading('local_schools_notifications/mod_due_date_title', get_string('mod_due_date_title', 'local_schools_notifications'), ''));

	$name = 'local_schools_notifications/mod_due_date_enabled';
	$title = get_string('enable', 'local_schools_notifications');
	$default = false;
	$setting = new admin_setting_configcheckbox($name, $title, null, $default, true, false);
	$settings->add($setting); 

	$name = 'local_schools_notifications/mod_due_date_courses';
	$title = get_string('courses', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $courses);
	$settings->add($setting); 
	
	$name = 'local_schools_notifications/mod_due_date_users';
	$title = get_string('users', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $user_roles);
	$settings->add($setting); 

	$name = 'local_schools_notifications/mod_due_date_typemessage';
	$title = get_string('typemessage', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $types);
	$settings->add($setting); 
	
$settings->add(new admin_setting_heading('local_schools_notifications/course_incompleted_title', get_string('course_incompleted_title', 'local_schools_notifications'), ''));

	$name = 'local_schools_notifications/course_incompleted_enabled';
	$title = get_string('enable', 'local_schools_notifications');
	$default = false;
	$setting = new admin_setting_configcheckbox($name, $title, null, $default, true, false);
	$settings->add($setting); 

	$name = 'local_schools_notifications/course_incompleted_courses';
	$title = get_string('courses', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $courses);
	$settings->add($setting); 
	
	$name = 'local_schools_notifications/course_incompleted_users';
	$title = get_string('users', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $user_roles);
	$settings->add($setting); 

	$name = 'local_schools_notifications/course_incompleted_typemessage';
	$title = get_string('typemessage', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $types);
	$settings->add($setting); 
	
$settings->add(new admin_setting_heading('local_schools_notifications/certificate_received_title', get_string('certificate_received_title', 'local_schools_notifications'), ''));

	$name = 'local_schools_notifications/certificate_received_enabled';
	$title = get_string('enable', 'local_schools_notifications');
	$default = false;
	$setting = new admin_setting_configcheckbox($name, $title, null, $default, true, false);
	$settings->add($setting); 

	$name = 'local_schools_notifications/certificate_received_courses';
	$title = get_string('courses', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $courses);
	$settings->add($setting); 
	
	$name = 'local_schools_notifications/certificate_received_users';
	$title = get_string('users', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $user_roles);
	$settings->add($setting); 

	$name = 'local_schools_notifications/certificate_received_typemessage';
	$title = get_string('typemessage', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $types);
	$settings->add($setting);
	
$settings->add(new admin_setting_heading('local_schools_notifications/course_failure_title', get_string('course_failure_title', 'local_schools_notifications'), ''));

	$name = 'local_schools_notifications/course_failure_enabled';
	$title = get_string('enable', 'local_schools_notifications');
	$default = false;
	$setting = new admin_setting_configcheckbox($name, $title, null, $default, true, false);
	$settings->add($setting); 

	$name = 'local_schools_notifications/course_failure_courses';
	$title = get_string('courses', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $courses);
	$settings->add($setting); 
	
	$name = 'local_schools_notifications/course_failure_users';
	$title = get_string('users', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $user_roles);
	$settings->add($setting); 

	$name = 'local_schools_notifications/course_failure_typemessage';
	$title = get_string('typemessage', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $types);
	$settings->add($setting); 
	
$settings->add(new admin_setting_heading('local_schools_notifications/course_inactivity_title', get_string('course_inactivity_title', 'local_schools_notifications'), ''));

	$name = 'local_schools_notifications/course_inactivity_enabled';
	$title = get_string('enable', 'local_schools_notifications');
	$default = false;
	$setting = new admin_setting_configcheckbox($name, $title, null, $default, true, false);
	$settings->add($setting); 

	$name = 'local_schools_notifications/course_inactivity_courses';
	$title = get_string('courses', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $courses);
	$settings->add($setting); 
	
	$name = 'local_schools_notifications/course_inactivity_users';
	$title = get_string('users', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $user_roles);
	$settings->add($setting); 

	$name = 'local_schools_notifications/course_inactivity_typemessage';
	$title = get_string('typemessage', 'local_schools_notifications');
	$setting = new admin_setting_configmulticheckbox($name, $title, null, null, $types);
	$settings->add($setting); 

This is part of the Google Docs plugin for Moodle assignments.

Full instructions and information can be found in the submission part of this assignment at:
----- https://moodle.org/plugins/view.php?plugin=assignsubmission_googledocs
OR
----- https://github.com/sebale/moodle-assignsubmission_googledocs

ABOUT PLUGIN

....

INSTALLATION

1. Setup Google Drive repository in your Moodle system:
	a. Enable repository: https://docs.moodle.org/28/en/Google_Drive_repository
	b. Setup Google OAuth: https://docs.moodle.org/27/en/Google_OAuth_2.0_setup
2. Allow capability to "View Google Drive repository" (repository/googledocs:view) for all users which will use Google Docs Submission (Site administration > Users > Permissions > Define roles)
3. Download the plugin and unzip it in moodle/mod/assign/submission/ and Upgrade Moodle database or install it from the Moodle plugin instalation page (Site administration > Plugins > Install add-ons)
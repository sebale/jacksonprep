<?php

define('AJAX_SCRIPT', true);

require_once('../../../../config.php');


$id		= optional_param('id', 0, PARAM_INT);
$userid		= optional_param('userid', 0, PARAM_INT);
$path		= optional_param('path', 'root|Google+Drive', PARAM_RAW);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)optional_param_array('form', array(), PARAM_RAW);

if ($action == 'load_googlfilearea') {
	
	require_once($CFG->dirroot.'/lib/filelib.php');
	require_once($CFG->dirroot.'/repository/lib.php');
	$script = '<script type="text/javascript" src="'.$CFG->wwwroot.'/mod/assign/submission/googledocs/jquery/jquery-2.1.0.min.js"></script>';
	$script .= '<script type="text/javascript" src="'.$CFG->wwwroot.'/mod/assign/submission/googledocs/jquery/script.js"></script>';
	
	$items = array(); $bc = ''; $table = ''; $filepicker = '';
	if ($repos = repository::get_instances(array('type'=>'googledocs'))){
		foreach ($repos as $r_id=>$r){
			$repo = repository::get_repository_by_id($r_id, SYSCONTEXTID, array('ajax' => true, 'mimetypes' => '*'));
		}
	} else {
		$repo = false;
	}
	
	$repo->check_capability();
	
	$gfiles = array();
	if ($id > 0){
		$googledocs = $DB->get_record('assignsubmission_googledocs', array('id'=>$id));
		if ($googledocs->files){
			$gfiles = unserialize($googledocs->files);
		}
	}
	
	if ($repo->check_login()) {
		$listing = repository::prepare_listing($repo->get_listing($path));
		$items = array();
		foreach ($listing['list'] as $item){
			if (!isset($item['path']) and !stristr($item["source"], "docs.google.com")) continue;
			$items[] = $item;
		}
	
		$table = '';
		if (count($items) > 0) {
			$table .= '<div class="over-flow"><table>'; $is_files = false;
			foreach($items as $item){
				$itemid = explode("/" , $item["source"]);
				if (!isset($item['path'])){
					$parts = parse_url($item["source"]); 
					parse_str($parts['query'], $file_id);
				}
				$ext = pathinfo($item["title"], PATHINFO_EXTENSION);
				if($item["icon"])
					$thumbnail = '<div class="image" '.((isset($item['path'])) ? 'onclick="load_googlfilearea(\''.urlencode($item["path"]).'\');"' : '').'><img style="" src="'.$item["icon"].'" /></div>';
				$table .= '<tr class="'.((isset($gfiles[$file_id["id"]])) ? 'inserted' : '').'">';
					$table .= '<td class="t1">';
						$table .= $thumbnail;
					$table .= '</td>';
					$table .= '<td class="t2">';
						if(isset($item['path'])){
							$table .= '<span onclick="load_googlfilearea(\''.urlencode($item["path"]).'\');">'.$item["title"].'</span>';
						} else {
							$table .= '<a href="javascript:void(0);">'.$item["title"].'</a>';
							$is_files = true;
						}
					$table .= '</td>';
					$table .= '<td id="gf_'.$file_id["id"].'" class="t3">';
						if (!isset($item['path'])){
							$item["title"] = str_replace('"', '0__0', $item["title"]);
							$item["title"] = str_replace("'", "0_0", $item["title"]);
							$table .= '<button class="insert-btn" onclick="insert_googledoc(\''.$file_id["id"].'\', \''.$item["title"].'\', \''.$item["icon"].'\', \''.$ext.'\'); return false;" style="'.((isset($gfiles[$file_id["id"]])) ? 'display:none' : '').'"><i class="fa fa-share-square-o"></i> '.get_string('insert', 'assignsubmission_googledocs').'</button>';
							$table .= '<button class="preview-btn" onclick="preview_googledoc(\''.$file_id["id"].'\', \''.$ext.'\');return false;"><i class="fa fa-search"></i> '.get_string('preview', 'assignsubmission_googledocs').'</button>';
						}
					$table .= '</td>';
				$table .= '</tr>';
			}
			$table .= '</table></div>';
			if (!$is_files) $table .= '<div class="noty">'.get_string('no_files', 'assignsubmission_googledocs').'</div>';
		} else {
			$table .= '<div class="noty">No files available</div>';
		}
		$bc = '<div class="googledocs-breadcrumbs fp-pathbar"><ul class="clearfix">';
		$breadcrumbs_path = $listing['path'];
		foreach ($breadcrumbs_path as $bitem){
			$bc .= '<li class="fp-path-folder '.(($bitem['path'] == $path) ? 'active' : '').'"><a href="javascript:void(0);" onclick="load_googlfilearea(\''.urlencode($bitem['path']).'\');">'.$bitem['name'].'</a></li>';
			$i++;
		}
		$bc .= '</ul></div>';
		
		$filepicker .= '<script>';
		
			$filepicker .= 'function insert_googledoc(file_id, file_name, icon, ext){
								var filenum = parseInt(jQuery("input[name=\'googledocs_files\']").val());
								var newnum = filenum+1;
								jQuery("input[name=\'googledocs_files\']").val(newnum);
								jQuery(".googledocs-filepicker").append("<input id=\'fid_"+file_id+"\' type=\'hidden\' name=\'file["+newnum+"]\' value=\'"+file_id+"\' />");
								jQuery(".googledocs-filepicker").append("<input id=\'fn_"+file_id+"\' type=\'hidden\' name=\'filename["+newnum+"]\' value=\'"+file_name+"\' />");
								jQuery("#gf_"+file_id+" .insert-btn").hide();
								jQuery(".inserted-files-table").append("<tr id=\'if_"+file_id+"\' class=\"tr-files\"><td class=\"icon\"><img src=\'"+icon+"\' /></td><td><a href=\"javascript:void(0);\">"+decodeSChars(file_name)+"</a></td><td class=\"actions\"><button onclick=\'delete_googledoc(\""+file_id+"\"); return false;\'><i class=\"fa fa-trash\"></i> '.get_string('remove', 'assignsubmission_googledocs').'</button><button onclick=\'preview_googledoc(\""+file_id+"\", \""+ext+"\");return false;\'><i class=\"fa fa-search\"></i> Preview</button></td></tr>");
								jQuery("#gf_"+file_id+" .insert-btn").parent().parent().addClass("inserted");
								jQuery(".inserted-title, .inserted-files-inner").show();
							}
							function load_googlfilearea(path){
								jQuery(".googledocs-filepicker").html("<div style=\"padding-top:50px;\" class=\'noty\'><i class=\"fa fa-spin fa-spinner\"></i> '.get_string('loading', 'assignsubmission_googledocs').'</div>").load( "'.$CFG->wwwroot.'/mod/assign/submission/googledocs/ajax.php?action=load_googlfilearea&path="+path+"&id='.$id.'", function() {
									jQuery(".googledocs-filepicker .load").remove();
								});
							}';
		$filepicker .= '</script>';
	} else {
		$listing = $repo->print_login();
		$filepicker  .= '<div class="noty">'.get_string('not_connected', 'assignsubmission_googledocs').'<br /><a href="javascript:void(0);" class="btn" onclick="set_connect();" >'.get_string('connect', 'assignsubmission_googledocs').'</a></div>';
		$filepicker  .= '
			<script>
				function set_connect(){
					var logWin = window.open(\''.$listing["login"][0]->url.'\',\'Google drive\',\'height=600,width=500\'); 
					jQuery(".btn-gc").html("<i class=\"fa fa-spin fa-spinner\"></i> '.get_string('connect', 'assignsubmission_googledocs').'").addClass("btn-warning");
					var check_connect = setInterval(function(){
						jQuery.getJSON( "'.$CFG->wwwroot.'/mod/assign/submission/googledocs/ajax.php?action=check_connect", function( data ) {
							if (data.result == 1){
								logWin.close();
								jQuery(".btn-gc").removeClass("btn-warning").addClass("btn-success");
								jQuery(".googledocs-filepicker").load( "'.$CFG->wwwroot.'/mod/assign/submission/googledocs/ajax.php?action=load_googlfilearea&id='.(($googledocs) ? $googledocs->id : 0).'", function() {});
								clearInterval(check_connect);
							}
						});
					}, 1000);
				};
			</script>
		';
	}
	
	echo $script.$bc.$table.$filepicker;
} elseif ($action == 'check_connect') {
	require_once($CFG->dirroot.'/lib/filelib.php');
	require_once($CFG->dirroot.'/repository/lib.php');
	$result = 0;
	
	if ($repos = repository::get_instances(array('type'=>'googledocs'))){
		foreach ($repos as $r_id=>$r){
			$repo = repository::get_repository_by_id($r_id, SYSCONTEXTID, array('ajax' => true, 'mimetypes' => '*'));
		}
	} else {
		$repo = false;
	}
	if ($repo){
		$repo->check_capability();
		if ($repo->check_login()) {
			$result = 1;
		}
	}
	echo json_encode(array('result'=>$result));
}

exit;

function decodeSChars(text) {
  return text
      .replace(/0__0/g, "&quot;")
      .replace(/0_0/g, "&#039;");
}
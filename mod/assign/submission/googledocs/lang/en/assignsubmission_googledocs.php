<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignsubmission_googledocs', language 'en'
 *
 * @package   assignsubmission_googledocs
 * @copyright  	2014-2015 SEBALE LLC
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @created by	SEBALE LLC
 * @website		www.sebale.net
 */


$string['configmaxbytes'] = 'Maximum file size';
$string['countfiles'] = '{$a} files';
$string['default'] = 'Enabled by default';
$string['default_help'] = 'If set, this submission method will be enabled by default for all new assignments.';
$string['enabled'] = 'Google Docs';
$string['enabled_help'] = 'If enabled, students are able to share Google Docs file.';
$string['event_assessable_uploaded'] = 'A Google Docs files has been shared.';
$string['googledocs'] = 'Google Docs';
$string['maxbytes'] = 'Maximum file size';
$string['maxfilessubmission'] = 'Maximum number of uploaded files';
$string['maxfilessubmission_help'] = 'If Google Docs submissions are enabled, each student will be able to upload up to this number of files for their submission.';
$string['maximumsubmissionsize'] = 'Maximum submission size';
$string['maximumsubmissionsize_help'] = 'Files uploaded by students may be up to this size.';
$string['numfilesforlog'] = 'The number of file(s) : {$a} file(s).';
$string['pluginname'] = 'Google Docs';
$string['siteuploadlimit'] = 'Site upload limit';
$string['submissionfilearea'] = 'Uploaded submission Google docs';
$string['insert'] = 'Insert';
$string['preview'] = 'Preview';
$string['remove'] = 'Remove';
$string['not_connected'] = 'Google Drive not connected.';
$string['connect'] = 'Connect';
$string['no_files'] = 'No files available';
$string['loading'] = 'Loading...';
$string['shared_files'] = 'Shared files';

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the definition for the library class for googledocs submission plugin
 *
 * This class provides all the functionality for the new assign module.
 *
 * @package assignsubmission_googledocs
 * @copyright  	2014-2015 SEBALE LLC
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @created by	SEBALE LLC
 * @website		www.sebale.net
 */

require_once($CFG->libdir.'/eventslib.php');
require_once($CFG->dirroot.'/lib/filelib.php');
require_once($CFG->dirroot.'/repository/lib.php');

defined('MOODLE_INTERNAL') || die();

// File areas for googledocs submission assignment.
define('ASSIGNSUBMISSION_GOOGLEDOCS_MAXFILES', 20);
define('ASSIGNSUBMISSION_GOOGLEDOCS_MAXSUMMARYFILES', 5);
define('ASSIGNSUBMISSION_GOOGLEDOCS_FILEAREA', 'submission_googledocs');

/**
 * Library class for googledocs submission plugin extending submission plugin base class
 *
 * @package   assignsubmission_googledocs
 * @copyright 2012 NetSpot {@link http://www.netspot.com.au}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class assign_submission_googledocs extends assign_submission_plugin {

    /**
     * Get the name of the googledocs submission plugin
     * @return string
     */
    public function get_name() {
        return get_string('googledocs', 'assignsubmission_googledocs');
    }

    /**
     * Get googledocs submission information from the database
     *
     * @param int $submissionid
     * @return mixed
     */
    private function get_googledocs_submission($submissionid) {
        global $DB;
        return $DB->get_record('assignsubmission_googledocs', array('submission'=>$submissionid));
    }
	
	/**
     * Get googledocs repository
     *
     * @return repository
     */
    private function get_googledocs_repository() {
        if ($repos = repository::get_instances(array('type'=>'googledocs'))){
			foreach ($repos as $r_id=>$r){
				$repo = repository::get_repository_by_id($r_id, SYSCONTEXTID, array('ajax' => true, 'mimetypes' => '*'));
			}
		} else {
			$repo = false;
		}
		return $repo;
    }

    /**
     * Get the default setting for googledocs submission plugin
     *
     * @param MoodleQuickForm $mform The form to add elements to
     * @return void
     */
    public function get_settings(MoodleQuickForm $mform) {
        global $CFG, $COURSE;

    }

    /**
     * Save the settings for googledocs submission plugin
     *
     * @param stdClass $data
     * @return bool
     */
    public function save_settings(stdClass $data) {
        
        return true;
    }

    /**
     * Add elements to submission form
     *
     * @param mixed $submission stdClass|null
     * @param MoodleQuickForm $mform
     * @param stdClass $data
     * @return bool
     */
    public function get_form_elements($submission, MoodleQuickForm $mform, stdClass $data) {
		global $CFG, $OUTPUT, $PAGE;
		$script = '<script type="text/javascript" src="'.$CFG->wwwroot.'/mod/assign/submission/googledocs/jquery/jquery-2.1.0.min.js"></script>';
	
        $submissionid = $submission ? $submission->id : 0;
		$files = $submission ? $this->get_files($submission) : array();
		$filesubmission = $submission ? $this->get_googledocs_submission($submission->id) : 0;
		
		$repo = $this->get_googledocs_repository();
		if (!$repo){
			return false;
		}
		
		$repo->check_capability();
		$table = $script.'';
		if ($repo->check_login()) {
				$table .= '<script>jQuery(".googledocs-filepicker").load( "'.$CFG->wwwroot.'/mod/assign/submission/googledocs/ajax.php?action=load_googlfilearea&id='.(($filesubmission) ? $filesubmission->id : 0).'", function() {});</script>';
		}else{
			$listing = $repo->print_login();
			$table  .= '<div class="noty">Google Drive not connected.<br /><a href="javascript:void(0);" class="btn btn-gc" onclick="set_connect();" >'.get_string('connect', 'assignsubmission_googledocs').'</a></div>';
			$table  .= '<script>function set_connect(){
							var logWin = window.open(\''.$listing["login"][0]->url.'\',\'Google drive\',\'height=600,width=500\'); 
							jQuery(".btn-gc").html("<i class=\"fa fa-spin fa-spinner\"></i> Connect").addClass("btn-warning");
							var check_connect = setInterval(function(){
								jQuery.getJSON( "'.$CFG->wwwroot.'/mod/assign/submission/googledocs/ajax.php?action=check_connect", function( data ) {
									if (data.result == 1){
										logWin.close();
										jQuery(".btn-gc").removeClass("btn-warning").addClass("btn-success");
										jQuery(".googledocs-filepicker").load( "'.$CFG->wwwroot.'/mod/assign/submission/googledocs/ajax.php?action=load_googlfilearea&id='.(($filesubmission) ? $filesubmission->id : 0).'", function() {});
										clearInterval(check_connect);
									}
								});
							}, 1000);
						};</script>';
		}
        
		
		$inserted_files = '<div class="inserted-title" style="'.((count($files) > 0) ? '' : 'display:none').'">'.get_string('shared_files', 'assignsubmission_googledocs').'</div>';
		$inserted_files .= '<div class="inserted-files-inner" style="'.((count($files) > 0) ? '' : 'display:none').'">';
			$inserted_files .= '<table class="inserted-files-table">';
				if (count($files) > 0){
					foreach ($files as $file_id => $file_name){
						$ext = pathinfo($file_name, PATHINFO_EXTENSION);
						$fileicon = $OUTPUT->pix_url(file_extension_icon($file_name, 24))->out(false);
						$inserted_files .= '<tr id="if_'.$file_id.'" class="tr-files">';
							$inserted_files .= '<td class="icon"><img src="'.$fileicon.'" /></td>';
							$inserted_files .= '<td><a href="javascript:void(0);">'.$file_name.'</a></td>';
							$inserted_files .= '<td class="actions">';
								$inserted_files .= '<button onclick="delete_googledoc(\''.$file_id.'\'); return false;"><i class="fa fa-trash"></i> '.get_string('remove', 'assignsubmission_googledocs').'</button>';
								$inserted_files .= '<button onclick="preview_googledoc(\''.$file_id.'\', \''.$ext.'\'); return false;"><i class="fa fa-search"></i> '.get_string('preview', 'assignsubmission_googledocs').'</button>';
							$inserted_files .= '</td>';
						$inserted_files .= '</tr>';
					}
					$inserted_files .= '<tr><td colspan="2">'; $i = 1;
						foreach ($files as $file_id => $file_name){
							$inserted_files .= '<input id="fid_'.$file_id.'" type="hidden" name="file['.$i.']" value='.$file_id.' />';
							$inserted_files .= '<input id="fn_'.$file_id.'" type="hidden" name="filename['.$i.']" value="'.$file_name.'" />';
							$i++;
						}
						$inserted_files .= '';
					$inserted_files .= '</td></tr>';
				}
			$inserted_files .= '</table>';
		$inserted_files .= '</div>';
		
		$filepicker = '';
		$filepicker .= '<div class="googledocs-filepicker-title">'.get_string('pluginname', 'assignsubmission_googledocs').'</div><div class="googledocs-filepicker" subid="'.(($filesubmission) ? $filesubmission->id : 0).'">';
			$filepicker .= $table;
		$filepicker .= '</div>';
		$filepicker .= '<div class="googledocs-filearea">';
			$filepicker .= $inserted_files;
		$filepicker .= '</div>';
		$filepicker .= '<script>';
		$filepicker .= 'function delete_googledoc(file_id){
							jQuery("#if_"+file_id).remove();
							jQuery("#fid_"+file_id).remove();
							jQuery("#fn_"+file_id).remove();
							jQuery("input[value=\""+file_id+"\"]").remove();
							jQuery("#gf_"+file_id+" .insert-btn").show();
							jQuery("#gf_"+file_id+" .insert-btn").parent().parent().removeClass("inserted");
						}
						function preview_googledoc(file_id, ext){
							if (ext == "xlsx" || ext == "xls"){
								var type = "spreadsheets";
							} else if (ext == "pptx" || ext == "ppt"){
								var type = "presentation";
							} else {
								var type = "document";
							}
							window.open("https://docs.google.com/"+type+"/d/"+file_id+"/edit?usp=sharing", "_blank");
						}
						';
		$filepicker .= '</script>';
		
		$mform->addElement('html', $filepicker);
		
		$mform->addElement('hidden', 'googledocs_files');
		$mform->setDefault('googledocs_files', ((count($files) > 0) ? count($files) : 0));
		$mform->setType('googledocs_files', PARAM_INT);
		
        return true;
    }

    /**
     * Count the number of files
     *
     * @param int $submissionid
     * @param string $area
     * @return int
     */
    private function count_files($submissionid, $area) {
		
        $filesubmission = $this->get_googledocs_submission($submissionid);
        return $filesubmission->numfiles;
    }
	
    /**
     * Save the files and trigger plagiarism plugin, if enabled,
     * to scan the uploaded files via events trigger
     *
     * @param stdClass $submission
     * @param stdClass $data
     * @return bool
     */
    public function save(stdClass $submission, stdClass $data) {
        global $USER, $DB, $CFG;
		
		require_once($CFG->dirroot.'/lib/filelib.php');
		require_once($CFG->dirroot.'/repository/lib.php');
		
		$repo = $this->get_googledocs_repository();
		if (!$repo){
			return false;
		}
		$repo->check_capability();

		$post_data = $_POST;
        $filesubmission = $this->get_googledocs_submission($submission->id);
		
		$fs = get_file_storage();

        $files = $fs->get_area_files($this->assignment->get_context()->id,
                                     'assignsubmission_googledocs',
                                     ASSIGNSUBMISSION_ONLINETEXT_GOOGLEDOCS,
                                     $submission->id,
                                     'id',
                                     false);
        
        // Plagiarism code event trigger when files are uploaded.
        $params = array(
            'context' => context_module::instance($this->assignment->get_course_module()->id),
            'objectid' => $submission->id,
            'other' => array(
                'pathnamehashes' => array_keys($files),
                'content' => ''
            )
        );
        $event = \assignsubmission_googledocs\event\assessable_uploaded::create($params);
        $event->trigger();
		
		$files = array();
		if (count($post_data['file']) > 0){
			foreach ($post_data['file'] as $k=>$v){
				if (!isset($post_data['filename'][$k])) continue;
				$ftitle = $post_data['filename'][$k];
				$ftitle = str_replace('0__0', '"', $ftitle);
				$ftitle = str_replace("0_0", "'", $ftitle);
				$files[$v] = $ftitle;
			}
		}
		
        if ($filesubmission) {
            $filesubmission->files = (count($files) > 0) ? serialize($files) : null;
			$filesubmission->numfiles = count($files);
            return $DB->update_record('assignsubmission_googledocs', $filesubmission);
        } else {
            $filesubmission = new stdClass();
            $filesubmission->files = (count($files) > 0) ? serialize($files) : null;
			$filesubmission->numfiles = count($files);
            $filesubmission->submission = $submission->id;
            $filesubmission->assignment = $this->assignment->get_instance()->id;
            return $DB->insert_record('assignsubmission_googledocs', $filesubmission) > 0;
        }
    }
	

    /**
     * Produce a list of files suitable for export that represent this feedback or submission
     *
     * @param stdClass $submission The submission
     * @param stdClass $user The user record - unused
     * @return array - return an array of files indexed by filename
     */
    public function get_files(stdClass $submission) {
        $result = array();
        
		$filesubmission = $this->get_googledocs_submission($submission->id);
		
		if ($filesubmission->numfiles > 0 and $filesubmission->files){
			$result = unserialize($filesubmission->files);
		}
        return $result;
    }

    /**
     * Display the list of files  in the submission status table
     *
     * @param stdClass $submission
     * @param bool $showviewlink Set this to true if the list of files is long
     * @return string
     */
    public function view_summary(stdClass $submission, & $showviewlink) {
		global $OUTPUT, $CFG;
		
        $count = $this->count_files($submission->id, ASSIGNSUBMISSION_GOOGLEDOCS_FILEAREA);
        $files = $this->get_files($submission);
		$script = '<script type="text/javascript" src="'.$CFG->wwwroot.'/mod/assign/submission/googledocs/jquery/jquery-2.1.0.min.js"></script>';
		
		$result = $script.'';
		if (count($files) > 0){
			$result .= '<table class="googledocs-files-table">';
				foreach ($files as $file_id => $file_name){
					$ext = pathinfo($file_name, PATHINFO_EXTENSION);
					$fileicon = $OUTPUT->pix_url(file_extension_icon($file_name, 24))->out(false);
					$result .= '<tr id="if_'.$file_id.'" class="tr-files">';
						$result .= '<td class="icon"><img src="'.$fileicon.'" /></td>';
						$result .= '<td><a href="javascript:void(0);" onclick="open_googledocs_file(\''.$file_id.'\', \''.$ext.'\');">'.$file_name.'</a></td>';
					$result .= '</tr>';
				}
			$result .= '</table>';
			$result .= '<script>
							function open_googledocs_file(file_id, ext){
								if (ext == "xlsx" || ext == "xls"){
									var type = "spreadsheets";
								} else if (ext == "pptx" || ext == "ppt"){
									var type = "presentation";
								} else {
									var type = "document";
								}
								jQuery(".google-overlay").remove();
								jQuery("body").append("<div class=\"google-overlay\"><div class=\"googledocs-overlay-close\" onclick=\"close_googlebox();\" title=\"Close\"><i class=\"fa fa-times\"></i></div></div>");
								jQuery(".google-overlay").addClass("open");
								jQuery(".google-overlay").append("<iframe src=\"https://docs.google.com/"+type+"/d/"+file_id+"/edit?usp=sharing\" class=\"googledocs-iframe\" ></iframe>");
							}
							function close_googlebox(){
								jQuery(".google-overlay").remove();
							}
						</script>';
		}
		
		return $result;
    }

    /**
     * No full submission view - the summary contains the list of files and that is the whole submission
     *
     * @param stdClass $submission
     * @return string
     */
    public function view(stdClass $submission) {
        return $this->assignment->render_area_files('assignsubmission_googledocs',
                                                    ASSIGNSUBMISSION_GOOGLEDOCS_FILEAREA,
                                                    $submission->id);
    }



    /**
     * Return true if this plugin can upgrade an old Moodle 2.2 assignment of this type
     * and version.
     *
     * @param string $type
     * @param int $version
     * @return bool True if upgrade is possible
     */
    public function can_upgrade($type, $version) {

        $uploadsingletype ='uploadsingle';
        $uploadtype ='upload';

        if (($type == $uploadsingletype || $type == $uploadtype) && $version >= 2011112900) {
            return true;
        }
        return false;
    }


    /**
     * Upgrade the settings from the old assignment
     * to the new plugin based one
     *
     * @param context $oldcontext - the old assignment context
     * @param stdClass $oldassignment - the old assignment data record
     * @param string $log record log events here
     * @return bool Was it a success? (false will trigger rollback)
     */
    public function upgrade_settings(context $oldcontext, stdClass $oldassignment, & $log) {
        global $DB;

        if ($oldassignment->assignmenttype == 'uploadsingle') {
            $this->set_config('maxfilesubmissions', 1);
            return true;
        } else if ($oldassignment->assignmenttype == 'upload') {
            $this->set_config('maxfilesubmissions', $oldassignment->var1);

            // Advanced file upload uses a different setting to do the same thing.
            $DB->set_field('assign',
                           'submissiondrafts',
                           $oldassignment->var4,
                           array('id'=>$this->assignment->get_instance()->id));

            // Convert advanced file upload "hide description before due date" setting.
            $alwaysshow = 0;
            if (!$oldassignment->var3) {
                $alwaysshow = 1;
            }
            $DB->set_field('assign',
                           'alwaysshowdescription',
                           $alwaysshow,
                           array('id'=>$this->assignment->get_instance()->id));
            return true;
        }
    }

    /**
     * Upgrade the submission from the old assignment to the new one
     *
     * @param context $oldcontext The context of the old assignment
     * @param stdClass $oldassignment The data record for the old oldassignment
     * @param stdClass $oldsubmission The data record for the old submission
     * @param stdClass $submission The data record for the new submission
     * @param string $log Record upgrade messages in the log
     * @return bool true or false - false will trigger a rollback
     */
    public function upgrade(context $oldcontext,
                            stdClass $oldassignment,
                            stdClass $oldsubmission,
                            stdClass $submission,
                            & $log) {
        global $DB;

        $filesubmission = new stdClass();

        $filesubmission->numfiles = $oldsubmission->numfiles;
        $filesubmission->submission = $submission->id;
        $filesubmission->assignment = $this->assignment->get_instance()->id;

        if (!$DB->insert_record('assignsubmission_googledocs', $filesubmission) > 0) {
            $log .= get_string('couldnotconvertsubmission', 'mod_assign', $submission->userid);
            return false;
        }

        // Now copy the area files.
        $this->assignment->copy_area_files_for_upgrade($oldcontext->id,
                                                        'mod_assignment',
                                                        'submission',
                                                        $oldsubmission->id,
                                                        $this->assignment->get_context()->id,
                                                        'assignsubmission_googledocs',
                                                        ASSIGNSUBMISSION_GOOGLEDOCS_FILEAREA,
                                                        $submission->id);

        return true;
    }

    /**
     * The assignment has been deleted - cleanup
     *
     * @return bool
     */
    public function delete_instance() {
        global $DB;
        // Will throw exception on failure.
        $DB->delete_records('assignsubmission_googledocs',
                            array('assignment'=>$this->assignment->get_instance()->id));

        return true;
    }

    /**
     * Formatting for log info
     *
     * @param stdClass $submission The submission
     * @return string
     */
    public function format_for_log(stdClass $submission) {
        // Format the info for each submission plugin (will be added to log).
        $filecount = $this->count_files($submission->id, ASSIGNSUBMISSION_GOOGLEDOCS_FILEAREA);

        return get_string('numfilesforlog', 'assignsubmission_googledocs', $filecount);
    }

    /**
     * Return true if there are no submission files
     * @param stdClass $submission
     */
    public function is_empty(stdClass $submission) {
        return $this->count_files($submission->id, ASSIGNSUBMISSION_GOOGLEDOCS_FILEAREA) == 0;
    }

    /**
     * Get file areas returns a list of areas this plugin stores files
     * @return array - An array of fileareas (keys) and descriptions (values)
     */
    public function get_file_areas() {
        return array(ASSIGNSUBMISSION_GOOGLEDOCS_FILEAREA=>$this->get_name());
    }

    /**
     * Copy the student's submission from a previous submission. Used when a student opts to base their resubmission
     * on the last submission.
     * @param stdClass $sourcesubmission
     * @param stdClass $destsubmission
     */
    public function copy_submission(stdClass $sourcesubmission, stdClass $destsubmission) {
        global $DB;

        // Copy the files across.
        $contextid = $this->assignment->get_context()->id;
        $fs = get_file_storage();
        $files = $fs->get_area_files($contextid,
                                     'assignsubmission_googledocs',
                                     ASSIGNSUBMISSION_GOOGLEDOCS_FILEAREA,
                                     $sourcesubmission->id,
                                     'id',
                                     false);
        foreach ($files as $file) {
            $fieldupdates = array('itemid' => $destsubmission->id);
            $fs->create_file_from_storedfile($fieldupdates, $file);
        }

        // Copy the assignsubmission_file record.
        if ($filesubmission = $this->get_file_submission($sourcesubmission->id)) {
            unset($filesubmission->id);
            $filesubmission->submission = $destsubmission->id;
            $DB->insert_record('assignsubmission_googledocs', $filesubmission);
        }
        return true;
    }

    /**
     * Return a description of external params suitable for uploading a file submission from a webservice.
     *
     * @return external_description|null
     */
    public function get_external_parameters() {
        return array(
            'files_filemanager' => new external_value(
                PARAM_INT,
                'The id of a draft area containing files for this submission.'
            )
        );
    }
}

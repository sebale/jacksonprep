<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the version information for the googledocs submission plugin
 *
 * @package    assignsubmission_googledocs
 * @copyright  	2014-2015 SEBALE LLC
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @created by	SEBALE LLC
 * @website		www.sebale.net
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2013103500;
$plugin->requires  = 2013103500;
$plugin->component = 'assignsubmission_googledocs';
$plugin->dependencies = array('repository_googledocs' => 2013103500);

$plugin->iltype = 'individual';
$plugin->isystems = array('demo','jacksonprep','jacksondemo');

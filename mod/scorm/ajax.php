<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// This page is called via AJAX to repopulte the TOC when LMSFinish() is called.

require_once('../../config.php');


$page = optional_param('ilink', 0, PARAM_INT);
$relid = optional_param('scormid', 0, PARAM_INT);
$location = optional_param('location', 0, PARAM_INT);

if($data = $DB->get_record("scorm_ajax_info", array("userid"=>$USER->id,"relid"=>$relid,"page"=>$page,"location"=>$location))){
    $data->timemodified = time();
    $DB->update_record('scorm_ajax_info', $data);
}else{
    $data = new stdClass();
    $data->userid = $USER->id;
    $data->relid = $relid;
    $data->page = $page;
    $data->location = $location;
    $data->timemodified = time();
    $DB->insert_record('scorm_ajax_info', $data);
}
?>
<script type="text/javascript">
    self.close();
</script>

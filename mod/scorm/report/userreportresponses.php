<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * This page displays the user data from a single attempt
 *
 * @package mod_scorm
 * @copyright 1999 onwards Martin Dougiamas {@link http://moodle.com}
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once($CFG->dirroot.'/mod/scorm/report/reportlib.php');
require_once($CFG->libdir . '/tablelib.php');

$id = required_param('id', PARAM_INT); // Course Module ID.
$userid = required_param('user', PARAM_INT); // User ID.
$attempt = optional_param('attempt', 1, PARAM_INT); // attempt number.
$download = optional_param('download', '', PARAM_ALPHA);

// Building the url to use for links.+ data details buildup.
$url = new moodle_url('/mod/scorm/report/userreportresponses.php', array('id' => $id,
    'user' => $userid,
    'attempt' => $attempt));

$cm = get_coursemodule_from_id('scorm', $id, 0, false, MUST_EXIST);
$course = get_course($cm->course);
$scorm = $DB->get_record('scorm', array('id' => $cm->instance), '*', MUST_EXIST);
$user = $DB->get_record('user', array('id' => $userid), user_picture::fields(), MUST_EXIST);
// Get list of attempts this user has made.
$attemptids = scorm_get_all_attempts($scorm->id, $userid);

$PAGE->set_url($url);
// END of url setting + data buildup.

// Checking login +logging +getting context.
require_login($course, false, $cm);
$contextmodule = context_module::instance($cm->id);
require_capability('mod/scorm:viewreport', $contextmodule);

// Trigger a user interactions viewed event.
$event = \mod_scorm\event\interactions_viewed::create(array(
    'context' => $contextmodule,
    'relateduserid' => $userid,
    'other' => array('attemptid' => $attempt, 'instanceid' => $scorm->id)
));
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('scorm', $scorm);
$event->trigger();

$trackdata = $DB->get_records('scorm_scoes_track', array('userid' => $user->id, 'scormid' => $scorm->id,
    'attempt' => $attempt));
$usertrack = scorm_format_interactions($trackdata);

$questioncount = get_scorm_question_count($scorm->id);

$courseshortname = format_string($course->shortname, true,
    array('context' => context_course::instance($course->id)));
$exportfilename = $courseshortname . '-' . format_string($scorm->name, true) . '-' . get_string('interactions', 'scorm');


// Set up the table.
$table2 = new flexible_table('mod-scorm-userreport-ajaxinfo');
$table2->define_baseurl($PAGE->url);
$table2->define_columns(array('button','status','location','date'));
$table2->define_headers(array('Button','Status','Location','Time Clicked'));
$table2->set_attribute('class', 'generaltable generalbox boxaligncenter boxwidthwide');
$table2->show_download_buttons_at(array(TABLE_P_BOTTOM));
$table2->setup();


$table = new flexible_table('mod-scorm-userreport-interactions');
if (!$table->is_downloading($download, $exportfilename)) {

    // Print the page header.
    $strattempt = get_string('attempt', 'scorm');
    $strreport = get_string('report', 'scorm');

    $PAGE->set_title("$course->shortname: ".format_string($scorm->name));
    $PAGE->set_heading($course->fullname);
    $PAGE->navbar->add($strreport, new moodle_url('/mod/scorm/report.php', array('id' => $cm->id)));

    $PAGE->navbar->add(fullname($user). " - $strattempt $attempt");

    echo $OUTPUT->header();
    echo $OUTPUT->heading(format_string($scorm->name));
    // End of Print the page header.
    $currenttab = 'responses';
    require($CFG->dirroot . '/mod/scorm/report/userreporttabs.php');

    // Printing user details.
    $output = $PAGE->get_renderer('mod_scorm');
    echo $output->view_user_heading($user, $course, $PAGE->url, $attempt, $attemptids);

}

$buttons = $DB->get_records("scorm_ajax_buttons", array());
foreach($buttons as $button){
    $item = $DB->get_record_sql("SELECT ai.* FROM {scorm_ajax_info} ai, {scorm_ajax} a WHERE a.scormid= $scorm->id AND a.relid=ai.relid AND ai.userid=$userid AND ai.page =$button->id");

    $row = array();
    $row[] = $button->name;
    $row[] = ($item->page)?"Clicked":"";
    $row[] = ($item->location)?$item->location:'';
    $row[] = ($item->timemodified)? date("m/d/Y h:i:s", $item->timemodified):'-';
    $table2->add_data($row);

}

$table2->finish_output();


$columns = array('id');
$headers = array('Slide');

$count = 0;
for ($i = 0; $i < $questioncount; $i++) {
    $element = 'cmi.interactions_'.$i.'.id';
    if (isset($usertrack->$element)) {
        $element = 'cmi.interactions_'.$i.'.student_response';
        if (isset($usertrack->$element)) {
            $line = explode("___", s($usertrack->$element));
            $count = ($count < count($line))?count($line):$count;
        }
    }
}

$columns = array('id', 'time', 'correctanswer', 'result', 'calcweight');
$headers = array(get_string('trackid', 'scorm'), get_string('time', 'scorm'),
    get_string('rightanswer', 'scorm'), get_string('result', 'scorm'),
    get_string('calculatedweight', 'scorm'));


//$headers[]= "Time";
//$columns[] = "time";




$count = $count / 2;
for ($i = 0; $i < $count; $i++) {
    $h = $i + 1;
    $headers[]= "Question $h";
    $columns[] = "question$h";

    $headers[]= "Response $h";
    $columns[] = "response$h";
}

$table->define_baseurl($PAGE->url);
$table->define_columns($columns);
$table->define_headers($headers);
$table->set_attribute('class', 'generaltable generalbox boxaligncenter boxwidthwide');

$table->show_download_buttons_at(array(TABLE_P_BOTTOM));
$table->setup();

$timeSlide = 0;
for ($i = 0; $i < $questioncount; $i++) {
    $row = array();
    $element = 'cmi.interactions_'.$i.'.id';
    if (isset($usertrack->$element)) {
        $slide = explode("_", s($usertrack->$element));


        $row[] = substr($slide[1], 5);



        $element = 'cmi.interactions_'.$i.'.time';
        $t1 = s($usertrack->$element);

        $element = 'cmi.interactions_'.($i+1).'.time';
        $t2 = s($usertrack->$element);


        $u1 = strtotime($t1);
        $u2 = strtotime($t2);
        if($u1 and $u2){
            $date_a = new DateTime(date("h:i:s", $u1));
            $date_b = new DateTime(date("h:i:s", $u2));
            $interval = date_diff($date_a,$date_b);
            $t = $interval->format('%H:%I:%S');
        }else{
            $t = "00:01:00";
        }
        $row[] = $t;

        $j = 0;
        $element = 'cmi.interactions_'.$i.'.correct_responses_'.$j.'.pattern';
        $rightans = '';
        if (isset($usertrack->$element)) {
            while (isset($usertrack->$element)) {
                if ($j > 0) {
                    $rightans .= ',';
                }
                $rightans .= s($usertrack->$element);
                $j++;
                $element = 'cmi.interactions_'.$i.'.correct_responses_'.$j.'.pattern';
            }
            $row[] = $rightans;
        } else {
            $row[] = '&nbsp;';
        }
        $element = 'cmi.interactions_'.$i.'.result';
        $weighting = 'cmi.interactions_'.$i.'.weighting';
        if (isset($usertrack->$element)) {
            $row[] = s($usertrack->$element);
            if ($usertrack->$element == 'correct' &&
                isset($usertrack->$weighting)) {
                $row[] = s($usertrack->$weighting);
            } else {
                $row[] = '0';
            }
        } else {
            $row[] = '&nbsp;';
        }



        $element = 'cmi.interactions_'.$i.'.student_response';
        if (isset($usertrack->$element)) {
            $line = explode("___", s($usertrack->$element));

            $e = count($line);
            if($e){
                for ($f = 0; $f < $e; $f++) {
                    if(isset($line[$f]) and isset($line[$f+1])){
                        $row[] = str_replace("_", " ", $line[$f]);
                        $f++;
                        $row[] = str_replace("_", " ", $line[$f]);
                    }else{
                        $row[] =  implode(" ", $slide);
                        $row[] = str_replace("_", " ", $line[$f]);
                    }
                }
            }
            $g = $count - ($e/2);
            if($g){
                for ($d = 0; $d < $g; $d++) {
                    $row[] = "";
                    $row[] = "";
                }
            }


            //$row[] = s($usertrack->$element);
        } else {
            $row[] = '&nbsp;';
        }
        $table->add_data($row);
    }
}



$table->finish_output();

if (!$table->is_downloading()) {
    echo $OUTPUT->footer();
}


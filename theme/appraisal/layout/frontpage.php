<?php
/**
 * @package   theme_appraisal
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$tab = optional_param('tab', 0, PARAM_INT);
 
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$maincss = ($hassidepre and $hassidepost) ? 'bothside' : '';
$maincss = ($hassidepre and !$hassidepost) ? 'leftside' : $maincss;
$maincss = (!$hassidepre and $hassidepost) ? 'rightside' : $maincss;
$html = theme_appraisal_get_html_for_settings($OUTPUT, $PAGE);

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_appraisal_generate_favicon('faviconico'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_appraisal_generate_favicon('favicon'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_appraisal_generate_favicon('favicon'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_appraisal_generate_favicon('favicon'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<?php if($tab>0):?>
		<script>
			$(window).ready(function(){
				$('label[for="tab<?php echo $tab;?>"]').click();
			});		
		</script>
	<?php endif;?>
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>
	<?php include("includes/header.php"); ?>
	<section class="head <?php echo ($USER->id) ? '' : 'full'; ?>">
		<div class="inner clearfix">
			<?php if(!$USER->id): ?>
			<div class="home-form">
				<h2>Login:</h2>
				<form action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post" id="login">																																								
					<input placeholder="Email" id="username" class="validate-username" size="25" value="" name="username" type="text">
					<input placeholder="Password" id="password" class="validate-password" size="25" value="" name="password" type="password">
					
					<a href="login/forgot_password.php">Forgot your password?</a>
					<button type="submit" class="button">Enter</button>
					<a href="login/signup.php" class="signup">Sign Up</a>
				</form>
			</div>
			<div class="home-text">
				<h1><?php  echo $html->front_welcome_no_login_user;?></h1>
				<h3><?php  echo $html->front_welcome_full_no_login_user;?></h3>
				
			</div>
			<?php else: ?>
				<h1 class="home-wellcome"><?php  echo $html->front_welcome_login_user;?></h1>
			<?php endif; ?>
		</div>
	</section>
	
	<section id="page" class="page inner clearfix">
		<div class="content front clearfix <?php echo $maincss; ?>"  id="region-main">
			<div class="home-intro" style="display:none;"><?php echo $SITE->summary;?></div>
			<?php echo "<!--".$OUTPUT->main_content()."-->"; 
				global $SESSION;
				$editing = $PAGE->user_is_editing();
				$courserenderer = $PAGE->get_renderer('core', 'course');

			/// Print Section or custom info
				$siteformatoptions = course_get_format($SITE)->get_format_options();
				$modinfo = get_fast_modinfo($SITE);
				$modnames = get_module_types_names();
				$modnamesplural = get_module_types_names(true);
				$modnamesused = $modinfo->get_used_module_names();
				$mods = $modinfo->get_cms();

				if (!empty($CFG->customfrontpageinclude)) {
					include($CFG->customfrontpageinclude);

				} else if ($siteformatoptions['numsections'] > 0) {
					if ($editing) {
						// make sure section with number 1 exists
						course_create_sections_if_missing($SITE, 1);
						// re-request modinfo in case section was created
						$modinfo = get_fast_modinfo($SITE);
					}
					$section = $modinfo->get_section_info(1);
					if (($section && (!empty($modinfo->sections[1]) or !empty($section->summary))) or $editing) {
						echo $OUTPUT->box_start('generalbox sitetopic');

						/// If currently moving a file then show the current clipboard
						if (ismoving($SITE->id)) {
							$stractivityclipboard = strip_tags(get_string('activityclipboard', '', $USER->activitycopyname));
							echo '<p><font size="2">';
							echo "$stractivityclipboard&nbsp;&nbsp;(<a href=\"course/mod.php?cancelcopy=true&amp;sesskey=".sesskey()."\">". get_string('cancel') .'</a>)';
							echo '</font></p>';
						}

						$context = context_course::instance(SITEID);

						// If the section name is set we show it.
						if (!is_null($section->name)) {
							echo $OUTPUT->heading(
								format_string($section->name, true, array('context' => $context)),
								2,
								'sectionname'
							);
						}

						$summarytext = file_rewrite_pluginfile_urls($section->summary, 'pluginfile.php', $context->id, 'course', 'section', $section->id);
						$summaryformatoptions = new stdClass();
						$summaryformatoptions->noclean = true;
						$summaryformatoptions->overflowdiv = true;

						echo format_text($summarytext, $section->summaryformat, $summaryformatoptions);

						if ($editing && has_capability('moodle/course:update', $context)) {
							$streditsummary = get_string('editsummary');
							echo "<a title=\"$streditsummary\" ".
								 " href=\"course/editsection.php?id=$section->id\"><img src=\"" . $OUTPUT->pix_url('t/edit') . "\" ".
								 " class=\"iconsmall\" alt=\"$streditsummary\" /></a><br /><br />";
						}

						$courserenderer = $PAGE->get_renderer('core', 'course');
						echo $courserenderer->course_section_cm_list($SITE, $section);

						echo $courserenderer->course_section_add_cm_control($SITE, $section->section);
						echo $OUTPUT->box_end();
					}
				}
				// Include course AJAX
				include_course_ajax($SITE, $modnamesused);

				if (isloggedin() and !isguestuser() and isset($CFG->frontpageloggedin)) {
					$frontpagelayout = $CFG->frontpageloggedin;
				} else {
					$frontpagelayout = $CFG->frontpage;
				}

				foreach (explode(',',$frontpagelayout) as $v) {
					switch ($v) {     /// Display the main part of the front page.
						case FRONTPAGENEWS:
							if ($SITE->newsitems) { // Print forums only when needed
								require_once($CFG->dirroot .'/mod/forum/lib.php');

								if (! $newsforum = forum_get_course_forum($SITE->id, 'news')) {
									print_error('cannotfindorcreateforum', 'forum');
								}
								echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab1','type' => 'radio','name' =>'tabs'));
								echo html_writer::tag('label',$OUTPUT->heading($newsforum->name), array('for' => 'tab1'));
							}
						break;

						case FRONTPAGEENROLLEDCOURSELIST:
							$mycourseshtml = $courserenderer->frontpage_my_courses();
							if (!empty($mycourseshtml)) {
								echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab2','type' => 'radio','name' =>'tabs'));
								echo html_writer::tag('label',$OUTPUT->heading(get_string('mycourses')), array('for' => 'tab2'));
								break;
							}
							// No "break" here. If there are no enrolled courses - continue to 'Available courses'.

						case FRONTPAGEALLCOURSELIST:
							$availablecourseshtml = $courserenderer->frontpage_available_courses();
							if (!empty($availablecourseshtml)) {
								echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab3','type' => 'radio','name' =>'tabs'));
								echo html_writer::tag('label',$OUTPUT->heading(get_string('availablecourses')), array('for' => 'tab3'));
							}
						break;

						case FRONTPAGECATEGORYNAMES:
							echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab4','type' => 'radio','name' =>'tabs'));
							echo html_writer::tag('label',$OUTPUT->heading(get_string('categories')), array('for' => 'tab4'));
						break;

						case FRONTPAGECATEGORYCOMBO:
							echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab5','type' => 'radio','name' =>'tabs'));
							echo html_writer::tag('label',$OUTPUT->heading(get_string('courses')), array('for' => 'tab5'));
						break;
					}
				}
				echo html_writer::empty_tag('div', array('class' => 'clear'));
				echo html_writer::end_tag('div');
				echo html_writer::start_tag('div', array('class' => 'tab-content'));
					foreach (explode(',',$frontpagelayout) as $v) {
					switch ($v) {     /// Display the main part of the front page.
						case FRONTPAGENEWS:
							if ($SITE->newsitems) { // Print forums only when needed
								require_once($CFG->dirroot .'/mod/forum/lib.php');

								if (! $newsforum = forum_get_course_forum($SITE->id, 'news')) {
									print_error('cannotfindorcreateforum', 'forum');
								}

								// fetch news forum context for proper filtering to happen
								$newsforumcm = get_coursemodule_from_instance('forum', $newsforum->id, $SITE->id, false, MUST_EXIST);
								$newsforumcontext = context_module::instance($newsforumcm->id, MUST_EXIST);
								$forumname = format_string($newsforum->name, true, array('context' => $newsforumcontext));
								// wraps site news forum in div container.
								echo html_writer::start_tag('div', array('id'=>'site-news-forum'));

								if (isloggedin()) {
									$SESSION->fromdiscussion = $CFG->wwwroot;
									$subtext = '';
									if (\mod_forum\subscriptions::is_subscribed($USER->id, $newsforum)) {
										if (!\mod_forum\subscriptions::is_forcesubscribed($newsforum)) {
											$subtext = get_string('unsubscribe', 'forum');
										}
									} else {
										$subtext = get_string('subscribe', 'forum');
									}
									$suburl = new moodle_url('/mod/forum/subscribe.php', array('id' => $newsforum->id, 'sesskey' => sesskey()));
									echo html_writer::tag('div', html_writer::link($suburl, $subtext), array('class' => 'subscribelink'));
								} else {
									echo $OUTPUT->heading($forumname);
								}

								forum_print_latest_discussions($SITE, $newsforum, $SITE->newsitems, 'plain', 'p.modified DESC');

								//end site news forum div container
								echo html_writer::end_tag('div');
							}
						break;

						case FRONTPAGEENROLLEDCOURSELIST:
							$mycourseshtml = $courserenderer->frontpage_my_courses();
							if (!empty($mycourseshtml)) {

								//wrap frontpage course list in div container
								echo html_writer::start_tag('div', array('id'=>'frontpage-course-enroll-list'));
								echo $mycourseshtml;
								//end frontpage course list div container
								echo html_writer::end_tag('div');
								break;
							}
							// No "break" here. If there are no enrolled courses - continue to 'Available courses'.

						case FRONTPAGEALLCOURSELIST:
							$availablecourseshtml = $courserenderer->frontpage_available_courses();
							if (!empty($availablecourseshtml)) {
								//wrap frontpage course list in div container
								echo html_writer::start_tag('div', array('id'=>'frontpage-course-all-list'));
								echo $availablecourseshtml;
								//end frontpage course list div container
								echo html_writer::end_tag('div');
							}
						break;

						case FRONTPAGECATEGORYNAMES:
							//wrap frontpage category names in div container
							echo html_writer::start_tag('div', array('id'=>'frontpage-category-names'));
							echo $courserenderer->frontpage_categories_list();
							//end frontpage category names div container
							echo html_writer::end_tag('div');
						break;

						case FRONTPAGECATEGORYCOMBO:
							//wrap frontpage category combo in div container
							echo html_writer::start_tag('div', array('id'=>'frontpage-category-combo'));
							echo $courserenderer->frontpage_combo_list();
							//end frontpage category combo div container
							echo html_writer::end_tag('div');
						break;

					}
				}
				echo html_writer::end_tag('div');
				if ($editing && has_capability('moodle/course:create', context_system::instance())) {
					echo $courserenderer->add_new_course_button();
				}
			?>
		</div>
		<?php echo ($hassidepost) ? $OUTPUT->blocks('side-post', 'side-post') : ''; ?>
	</section>
	<?php include('includes/footer.php'); ?>
<nav class="hidden breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
<script>
	if($( '#region-main').children().length < 5){
		$('.home-intro').css("display","block");
	}
	$( '.for-tab:first-of-type').attr( 'checked', true );
		function ajax_search(id,type){
				var  search = $('#shortsearchbox_'+id).val();
				var  category = $('#menu_'+id+'_courses').val();
				if(type == 'search' && search == ''){
					return;
				}
					if(id == 'all'){
						var clas = '.frontpage-course-list-all';
						var block = '#frontpage-course-all-list';
					}else if(id == 'my'){
						var clas = '.frontpage-course-list-enrolled';
						var block = '#frontpage-course-enroll-list';
					}
						jQuery.ajax({
							url: '<?php echo $CFG->wwwroot; ?>/theme/appraisal/ajax.php?action=search&search='+search+'&type='+id+'&category='+category,
							dataType: "html",
							async:false,
							beforeSend: function(){
								jQuery(clas).html('<i class="search"></i>');
							}
						}).done(function( data ) {
								jQuery(clas).remove();
								jQuery(block).append(data);
								jQuery(block+' .paging-morelink').remove();
								jQuery(block+' .clear').remove();
								jQuery('.coursebox').css({'opacity':'0','transition':'opacity 2s ease 0s'});
								setTimeout(function(){ jQuery('.coursebox').css({'opacity':'1','-webkit-animation':'animation 2000ms linear both','animation':'animation 2000ms linear both'}); }, 10);
						});	
		}	
		
		function ajax_get_courses(id,step){
			if(id == 'all'){
				var clas = '.frontpage-course-list-all';
				var block = '#frontpage-course-all-list';
			}else if(id == 'my'){
				var clas = '.frontpage-course-list-enrolled';
				var block = '#frontpage-course-enroll-list';
			}
				jQuery.ajax({
					url: '<?php echo $CFG->wwwroot; ?>/theme/appraisal/ajax.php?action=get_courses&step='+step+'&type='+id,
					dataType: "html",
					async:false,
					beforeSend: function(){
						jQuery(clas).append('<i class="search"></i>');
						jQuery(block+' .paging-morelink').remove();
					}
				}).done(function( data ) {
						jQuery(clas+' .search').remove();
						jQuery(clas+' .clear').remove();
						jQuery(clas).append(data);
						jQuery(clas).append(jQuery('#temp').html());
						jQuery(clas).append('<div class="clear"></div>');
						jQuery(clas).append(jQuery('#temp2').html());
						jQuery('#temp, #temp2').remove();
						jQuery('.paging-morelink').css({'opacity':'0','transition':'opacity 2s ease 0s'});
						setTimeout(function(){ jQuery('.coursebox.last ~ .coursebox, .paging-morelink').css({'opacity':'1','-webkit-animation':'animation 2000ms linear both','animation':'animation 2000ms linear both'}); }, 10);
				});	
		}		
		
		$("#shortsearchbox_all").keyup(function(event){
			if(event.keyCode == 13){
				$("#all-courses label").click();
			}
		});
		$("#shortsearchbox_my").keyup(function(event){
			if(event.keyCode == 13){
				$("#my-courses label").click();
			}
		});
		var button1 = Y.one("#shortsearchbox_all");
		if(button1 != null){
			button1.on('key', function (e) {e.preventDefault();e.stopPropagation();}, 'enter');
		}
		var button2 = Y.one("#shortsearchbox_my");
		if(button2 != null){
			button2.on('key', function (e) {e.preventDefault();	e.stopPropagation();}, 'enter');
		}
</script>
</body>
</html>


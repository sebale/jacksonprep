<?php
require_once($CFG->libdir. "/../course/renderer.php");
class theme_appraisal_core_course_renderer extends core_course_renderer{
		
		public $tabs;
		
		public function course_section_cm_list_item($course, &$completioninfo, cm_info $mod, $sectionreturn, $displayoptions = array()) {
			global $tabs;
			$output = '';
			if ($modulehtml = $this->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions)) {
				$modclasses = 'activity ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;
				$output .= html_writer::tag('li', $modulehtml, array('class' => $modclasses, 'id' => 'module-' . $mod->id));
			}
			
			if(!in_array($tabs,(array)$mod->modname)){
				$tabs[$mod->modname] = $mod->modfullname;
			}
			return $output;
		}
		
				public function course_section_cm_list($course, $section, $sectionreturn = null, $displayoptions = array()) {
			global $USER, $tabs;
			$tabs = array('all' => get_string('allactivities'));
			$output = '';
			$modinfo = get_fast_modinfo($course);
			if (is_object($section)) {
				$section = $modinfo->get_section_info($section->section);
			} else {
				$section = $modinfo->get_section_info($section);
			}
			$completioninfo = new completion_info($course);

			// check if we are currently in the process of moving a module with JavaScript disabled
			$ismoving = $this->page->user_is_editing() && ismoving($course->id);
			if ($ismoving) {
				$movingpix = new pix_icon('movehere', get_string('movehere'), 'moodle', array('class' => 'movetarget'));
				$strmovefull = strip_tags(get_string("movefull", "", "'$USER->activitycopyname'"));
			}

			// Get the list of modules visible to user (excluding the module being moved if there is one)
			$moduleshtml = array();
			if (!empty($modinfo->sections[$section->section])) {
				foreach ($modinfo->sections[$section->section] as $modnumber) {
					$mod = $modinfo->cms[$modnumber];

					if ($ismoving and $mod->id == $USER->activitycopy) {
						// do not display moving mod
						continue;
					}

					if ($modulehtml = $this->course_section_cm_list_item($course,
							$completioninfo, $mod, $sectionreturn, $displayoptions)) {
						$moduleshtml[$modnumber] = $modulehtml;
					}
				}
			}

			$sectionoutput = '';
			if (!empty($moduleshtml) || $ismoving) {
				foreach ($moduleshtml as $modnumber => $modulehtml) {
					if ($ismoving) {
						$movingurl = new moodle_url('/course/mod.php', array('moveto' => $modnumber, 'sesskey' => sesskey()));
						$sectionoutput .= html_writer::tag('li',
								html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)),
								array('class' => 'movehere'));
					}

					$sectionoutput .= $modulehtml;
				}

				if ($ismoving) {
					$movingurl = new moodle_url('/course/mod.php', array('movetosection' => $section->id, 'sesskey' => sesskey()));
					$sectionoutput .= html_writer::tag('li',
							html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)),
							array('class' => 'movehere'));
				}
			}
			$output .= '<i class="fa fa-angle-up show-activity"></i>';
			if($course->format == 'topics' && count($tabs)>2){
				$output .= '<i class="fa fa-align-left dropdown"></i>';
				$tab_box = ' ';
				foreach($tabs as $mod => $mod_name){
					$tab_box .= html_writer::tag('li',$mod_name,array('mod_name' => $mod));
				}
				$output .= html_writer::tag('ul', $tab_box, array('class' => 'section tabs clearfix'));
			}	
			// Always output the section module list.
			$output .= html_writer::tag('ul', $sectionoutput, array('class' => 'section img-text'));

			return $output;
		}
		
		    protected function coursecat_coursebox(coursecat_helper $chelper, $course, $additionalclasses = '') {
				global $CFG,$PAGE,$DB,$USER,$OUTPUT;				
					if($PAGE->pagelayout == "frontpage"){						
						if (!isset($this->strings->summary)) {
							$this->strings->summary = get_string('summary');
						}
						if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
							return '';
						}
						if ($course instanceof stdClass) {
							require_once($CFG->libdir. '/coursecatlib.php');
							$course = new course_in_list($course);
						}
						$content = '';
						$classes = trim('coursebox clearfix '. $additionalclasses);
						$nametag = 'h3';

						// .coursebox
						$content .= html_writer::start_tag('div', array(
							'class' => $classes,
							'data-courseid' => $course->id,
							'data-type' => self::COURSECAT_TYPE_COURSE,
						));
						
								// display course overview files
						$contentimages = $contentfiles = '';
							foreach ($course->get_course_overviewfiles() as $file) {
								$isimage = $file->is_valid_image();
								$url = file_encode_url("$CFG->wwwroot/pluginfile.php",
										'/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
										$file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
								if ($isimage) {
									$contentimages .= html_writer::empty_tag('img', array('src' => $url));
								} 
							}
							if($contentimages == ""){
								$contentimages .= html_writer::empty_tag('img', array('src' => $CFG->wwwroot."/theme/appraisal/pix/frontcoursebg.png"));
							}
							
						/* new moodle_url('/course/view.php', array('id' => $course->id)) */
						$content .= html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),$contentimages);

						// course name
						$coursename = $chelper->get_course_formatted_name($course);				
						$content .= html_writer::tag($nametag, html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)) ,$coursename), array('class' => 'coursename'));
						$teachers = $DB->get_record_sql("SELECT  u.firstname,u.lastname, COUNT(ra.id) AS count_teachers                    
																						FROM {course} AS c
																						JOIN {context} AS ct ON c.id = ct.instanceid
																						JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
																						JOIN {user} AS u ON u.id = ra.userid
																						WHERE c.id=".$course->id);
 					 	if($teachers->count_teachers == 1 && !empty($teachers->firstname)){
							$teacher = 'By '.$teachers->firstname.' '.$teachers->lastname;
						}elseif($teachers->count_teachers > 1){
							$teachers->count_teachers--;
							$teacher = 'By '.$teachers->firstname.' '.$teachers->lastname.' (and '.$teachers->count_teachers.' other)';
						} 	 			
						$content .= html_writer::tag('h4',$teacher, array('class' => 'course-teacher'));
						
						if($USER->id && $chelper->get_courses_display_options()['course_tab'] == 'my'){
							
							
							$data = $DB->get_record_sql("SELECT
                                (SELECT (gg.finalgrade/gg.rawgrademax)*100 FROM  {grade_items} gi, {grade_grades} gg  WHERE gi.itemtype = 'course' AND gg.itemid = gi.id AND gi.courseid=$course->id AND gg.userid=$USER->id) AS average
                            ");
                            $completion_info = $this->get_course_completion($course);
                            $progress = $completion_info->completion;
							
							$grade_info = html_writer::tag('div',
																					html_writer::tag('span',get_string('grade','theme_appraisal'),array('class' => 'name')).
																					html_writer::tag('span',intval($data->average).'%',array('class' => 'procent')).
																					html_writer::start_tag('div',array('class' => 'progres-bar')).
																					html_writer::tag('div','',array('class' => 'progres','style'=>'width:'.intval($data->average).'%;')).
																					html_writer::end_tag('div'),
																					array('class'=>'clearfix')
																					);
							$progress_info = html_writer::tag('div',
																						html_writer::tag('span',get_string('course_progress','theme_appraisal'),array('class' => 'name')).
																						html_writer::tag('span',intval($progress).'%',array('class' => 'procent')).
																						html_writer::start_tag('div',array('class' => 'progres-bar')).
																						html_writer::tag('div','',array('class' => 'progres','style'=>'width:'.$progress.'%;')).
																						html_writer::end_tag('div'),
																						array('class'=>'clearfix')
																					);
							if($completion_info->status == "completed"){
								$content .= html_writer::tag('i','',array('class'=>'fa fa-check course-done'));
							}
							$content .= $OUTPUT->box($grade_info.$progress_info,'user-progress clearfix');
							
						}else{
							$users = $DB->get_record_sql("SELECT COUNT( DISTINCT ra.userid) AS count_users                    
																						FROM {course} AS c
																						JOIN {context} AS ct ON c.id = ct.instanceid
																						JOIN {role_assignments} AS ra ON ra.contextid = ct.id 
																						JOIN {user} AS u ON u.id = ra.userid
																						WHERE c.id=".$course->id);
							$count_user = '<i class="fa  fa-group"></i> '.$users->count_users.' users enrolled';
							$content .= html_writer::tag('h4',$count_user, array('class' => 'course-users'));
						}
						
						// If we display course in collapsed form but the course has summary or course contacts, display the link to the info page.
			
						$content .= html_writer::end_tag('div'); // .coursebox
						return $content;
					 }else{						 
						if (!isset($this->strings->summary)) {
							$this->strings->summary = get_string('summary');
						}
						if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
							return '';
						}
						if ($course instanceof stdClass) {
							require_once($CFG->libdir. '/coursecatlib.php');
							$course = new course_in_list($course);
						}
						$content = '';
						$classes = trim('coursebox clearfix '. $additionalclasses);
						if ($chelper->get_show_courses() >= self::COURSECAT_SHOW_COURSES_EXPANDED) {
							$nametag = 'h3';
						} else {
							$classes .= ' collapsed';
							$nametag = 'div';
						}

						// .coursebox
						$content .= html_writer::start_tag('div', array(
							'class' => $classes,
							'data-courseid' => $course->id,
							'data-type' => self::COURSECAT_TYPE_COURSE,
						));

						$content .= html_writer::start_tag('div', array('class' => 'info'));

						// course name
						$coursename = $chelper->get_course_formatted_name($course);
						$coursenamelink = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),
															$coursename, array('class' => $course->visible ? '' : 'dimmed'));
						$content .= html_writer::tag($nametag, $coursenamelink, array('class' => 'coursename'));
						// If we display course in collapsed form but the course has summary or course contacts, display the link to the info page.
						$content .= html_writer::start_tag('div', array('class' => 'moreinfo'));
						if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
							if ($course->has_summary() || $course->has_course_contacts() || $course->has_course_overviewfiles()) {
								$url = new moodle_url('/course/info.php', array('id' => $course->id));
								$image = html_writer::empty_tag('img', array('src' => $this->output->pix_url('i/info'),
									'alt' => $this->strings->summary));
								$content .= html_writer::link($url, $image, array('title' => $this->strings->summary));
								// Make sure JS file to expand course content is included.
								$this->coursecat_include_js();
							}
						}
						$content .= html_writer::end_tag('div'); // .moreinfo

						// print enrolmenticons
						if ($icons = enrol_get_course_info_icons($course)) {
							$content .= html_writer::start_tag('div', array('class' => 'enrolmenticons'));
							foreach ($icons as $pix_icon) {
								$content .= $this->render($pix_icon);
							}
							$content .= html_writer::end_tag('div'); // .enrolmenticons
						}

						$content .= html_writer::end_tag('div'); // .info

						$content .= html_writer::start_tag('div', array('class' => 'content'));
						$content .= $this->coursecat_coursebox_content($chelper, $course);
						$content .= html_writer::end_tag('div'); // .content

						$content .= html_writer::end_tag('div'); // .coursebox
						return $content;
					 }
			}
	
	 protected function coursecat_coursebox_content(coursecat_helper $chelper, $course) {
        global $CFG,$PAGE;
		if($PAGE->pagelayout == "frontpage"){		
			if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
				return '';
			}
			if ($course instanceof stdClass) {
				require_once($CFG->libdir. '/coursecatlib.php');
				$course = new course_in_list($course);
			}
			$content = '';

			// display course summary
			if ($course->has_summary()) {
				$content .= $chelper->get_course_formatted_summary($course,
						array('overflowdiv' => true, 'noclean' => true, 'para' => false));
			}
								
			return $content;
		}else{
				if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
					return '';
				}
				if ($course instanceof stdClass) {
					require_once($CFG->libdir. '/coursecatlib.php');
					$course = new course_in_list($course);
				}
				$content = '';

				// display course summary
				if ($course->has_summary()) {
					$content .= html_writer::start_tag('div', array('class' => 'summary'));
					$content .= $chelper->get_course_formatted_summary($course,
							array('overflowdiv' => true, 'noclean' => true, 'para' => false));
					$content .= html_writer::end_tag('div'); // .summary
				}

				// display course overview files
				$contentimages = $contentfiles = '';
				foreach ($course->get_course_overviewfiles() as $file) {
					$isimage = $file->is_valid_image();
					$url = file_encode_url("$CFG->wwwroot/pluginfile.php",
							'/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
							$file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
					if ($isimage) {
						$contentimages .= html_writer::tag('div',
								html_writer::empty_tag('img', array('src' => $url)),
								array('class' => 'courseimage'));
					} else {
						$image = $this->output->pix_icon(file_file_icon($file, 24), $file->get_filename(), 'moodle');
						$filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
								html_writer::tag('span', $file->get_filename(), array('class' => 'fp-filename'));
						$contentfiles .= html_writer::tag('span',
								html_writer::link($url, $filename),
								array('class' => 'coursefile fp-filename-icon'));
					}
				}
				$content .= $contentimages. $contentfiles;

				// display course contacts. See course_in_list::get_course_contacts()
				if ($course->has_course_contacts()) {
					$content .= html_writer::start_tag('ul', array('class' => 'teachers'));
					foreach ($course->get_course_contacts() as $userid => $coursecontact) {
						$name = $coursecontact['rolename'].': '.
								html_writer::link(new moodle_url('/user/view.php',
										array('id' => $userid, 'course' => SITEID)),
									$coursecontact['username']);
						$content .= html_writer::tag('li', $name);
					}
					$content .= html_writer::end_tag('ul'); // .teachers
				}

				// display course category if necessary (for example in search results)
				if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT) {
					require_once($CFG->libdir. '/coursecatlib.php');
					if ($cat = coursecat::get($course->category, IGNORE_MISSING)) {
						$content .= html_writer::start_tag('div', array('class' => 'coursecat'));
						$content .= get_string('category').': '.
								html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $cat->id)),
										$cat->get_formatted_name(), array('class' => $cat->visible ? '' : 'dimmed'));
						$content .= html_writer::end_tag('div'); // .coursecat
					}
				}

				return $content;
			
		}
	 }

	public function frontpage_available_courses() {
        global $CFG, $OUTPUT;			
        require_once($CFG->libdir. '/coursecatlib.php');

			$chelper = new coursecat_helper();
			$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
					set_courses_display_options(array(
						'recursive' => true,
						'course_tab' => 'available',
						'limit' => $CFG->frontpagecourselimit));
			
			$chelper->set_attributes(array('class' => 'clearfix frontpage-course-list-all'));
			$courses = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
			//$my_courses  = ;
			foreach(enrol_get_my_courses('summary, summaryformat', $sortorder) as $id=>$val){
				unset($courses[$id]);
			}
			
			$totalcount = coursecat::get(0)->get_courses_count($chelper->get_courses_display_options());
			if (!$totalcount && !$this->page->user_is_editing() && has_capability('moodle/course:create', context_system::instance())) {
				// Print link to create a new course, for the 1st available category.
				return $this->add_new_course_button();
			}elseif(!$totalcount){
				return html_writer::tag('div',get_string('no_course_info', 'theme_appraisal'),array('class'=>'alert alert-warning'));
			}elseif($totalcount){			
				//$output = html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab2','type' => 'radio','name' =>'tabs'));
				//$output .= html_writer::tag('label',$OUTPUT->heading(get_string('availablecourses')), array('for' => 'tab2'));
					
					static $count = 0;
					$inputid = 'shortsearchbox_all';
					$inputsize = 12;
					
					$category = coursecat::make_categories_list();
					$output = html_writer::start_tag('form', array('id' => 'all-courses', 'class' => 'coursesearch', 'method' => 'get'));
					$output .= html_writer::start_tag('fieldset', array('class' => 'coursesearchbox invisiblefieldset clearfix'));
					$output .= html_writer::empty_tag('label', array('class' => 'fa  fa-search', 'onclick' => 'ajax_search("all","search");',));		
					$output .= html_writer::end_tag('label');
					$output .= html_writer::select($category, '_all_courses', null,'All categories',array('onchange' => 'ajax_search("all","cat");'));
					$output .= html_writer::empty_tag('input', array('type' => 'text', 'placeholder' => get_string('search') , 'id' => $inputid,
						'size' => $inputsize, 'name' => 'search', 'class' => 'shortsearchbox'));
					$output .= html_writer::end_tag('fieldset');
					$output .= html_writer::end_tag('form');
				$output .= $this->coursecat_courses($chelper, $courses, $totalcount);
			
				$chelper2 = new coursecat_helper();
				$chelper2->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->set_courses_display_options(array('recursive' => true));
				
				$chelper2->set_attributes(array('class' => 'frontpage-course-list-all2'));
				$courses = coursecat::get(0)->get_courses($chelper2->get_courses_display_options());
				if(count($courses) > $CFG->frontpagecourselimit){
					$output .= html_writer::start_tag('div', array('class' => 'paging-morelink'));
					$output .= html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("all",1);',));			
					$output .= html_writer::end_tag('div');					
				}
			}
			return $output;
		}
    
	
	 
	     public function frontpage_my_courses() {
			global $USER, $CFG, $DB, $OUTPUT;
			require_once($CFG->libdir. '/coursecatlib.php');
			
					if (!isloggedin() or isguestuser()) {
						return '';
					}

					$output = '';
					
					static $count = 0;
					$inputid = 'shortsearchbox_my';
					$inputsize = 12;
					
					$category = coursecat::make_categories_list();
					$strsearchcourses= get_string("searchcourses");
					$searchurl = new moodle_url('/course/search.php');
					$output = html_writer::start_tag('form', array('id' => 'my-courses', 'class' => 'coursesearch', 'method' => 'get'));
					$output .= html_writer::start_tag('fieldset', array('class' => 'coursesearchbox invisiblefieldset clearfix'));
					$output .= html_writer::empty_tag('label', array( 'class' => 'fa  fa-search','onclick' => 'ajax_search("my","search");'));		
					$output .= html_writer::end_tag('label');							
					$output .= html_writer::select($category, '_my_courses', null,'All categories',array('onchange' => 'ajax_search("my","cat");'));
					$output .= html_writer::empty_tag('input', array('type' => 'text', 'placeholder' => get_string('search') , 'id' => $inputid,
						'size' => $inputsize, 'name' => 'search', 'class' => 'shortsearchbox'));
					$output .= html_writer::end_tag('fieldset');
					$output .= html_writer::end_tag('form');
					
					if (!empty($CFG->navsortmycoursessort)) {
						// sort courses the same as in navigation menu
						$sortorder = 'visible DESC,'. $CFG->navsortmycoursessort.' ASC';
					} else {
						$sortorder = 'visible DESC,sortorder ASC';
					}
					$courses  = enrol_get_my_courses('summary, summaryformat', $sortorder);
					if(count($courses) == 0){
						return html_writer::tag('div',get_string('no_course_info', 'theme_appraisal'),array('class'=>'alert alert-warning'));
					}
					$rhosts   = array();
					$rcourses = array();
					if (!empty($CFG->mnet_dispatcher_mode) && $CFG->mnet_dispatcher_mode==='strict') {
						$rcourses = get_my_remotecourses($USER->id);
						$rhosts   = get_my_remotehosts();
					}

					if (!empty($courses) || !empty($rcourses) || !empty($rhosts)) {

						$chelper = new coursecat_helper();
						if (count($courses) > $CFG->frontpagecourselimit) {
							// There are more enrolled courses than we can display, display link to 'My courses'.
							$totalcount = count($courses);
							$courses = array_slice($courses, 0, $CFG->frontpagecourselimit, true);
							
						} else {
							// All enrolled courses are displayed, display link to 'All courses' if there are more courses in system.
							
							$totalcount = $DB->count_records('course') - 1;
						}
						$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->set_courses_display_options(array('course_tab' => 'my'));
						$chelper->set_attributes(array('class' => 'clearfix frontpage-course-list-enrolled'));
						$output .= $this->coursecat_courses($chelper, $courses, $totalcount);

						// MNET
						if (!empty($rcourses)) {
							// at the IDP, we know of all the remote courses
							$output .= html_writer::start_tag('div', array('class' => 'courses'));
							foreach ($rcourses as $course) {
								$output .= $this->frontpage_remote_course($course);
							}
							$output .= html_writer::end_tag('div'); // .courses
						} elseif (!empty($rhosts)) {
							// non-IDP, we know of all the remote servers, but not courses
							$output .= html_writer::start_tag('div', array('class' => 'courses'));
							foreach ($rhosts as $host) {
								$output .= $this->frontpage_remote_host($host);
							}
							$output .= html_writer::end_tag('div'); // .courses
						}
					}
					
					if((count(enrol_get_my_courses('summary, summaryformat')) - $CFG->frontpagecourselimit) >= $CFG->frontpagecourselimit){
						$output .= html_writer::start_tag('div', array('class' => 'paging-morelink'));
						$output .= html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("my",1);',));			
						$output .= html_writer::end_tag('div');					
					}
					return $output;
			}
    
	
	 public function frontpage_combo_list() {
		global $CFG,$OUTPUT;
		//echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab3','type' => 'radio','name' =>'tabs'));
		//echo html_writer::tag('label',$OUTPUT->heading(get_string('courses')), array('for' => 'tab3'));
        require_once($CFG->libdir. '/coursecatlib.php');
        $chelper = new coursecat_helper();
        $chelper->set_subcat_depth($CFG->maxcategorydepth)->
            set_categories_display_options(array(
                'limit' => $CFG->coursesperpage,
                'viewmoreurl' => new moodle_url('/course/index.php',
                        array('browse' => 'categories', 'page' => 1))
            ))->
            set_courses_display_options(array(
                'limit' => $CFG->coursesperpage,
                'viewmoreurl' => new moodle_url('/course/index.php',
                        array('browse' => 'courses', 'page' => 1))
            ))->
            set_attributes(array('class' => 'frontpage-category-combo'));
        echo $this->coursecat_tree($chelper, coursecat::get(0));
        return " ";
    }
	
	 public function frontpage_categories_list() {
		global $CFG,$OUTPUT;
		//echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab4','type' => 'radio','name' =>'tabs'));
		//echo html_writer::tag('label',$OUTPUT->heading(get_string('categories')), array('for' => 'tab4'));
        require_once($CFG->libdir. '/coursecatlib.php');
        $chelper = new coursecat_helper();
        $chelper->set_subcat_depth($CFG->maxcategorydepth)->
                set_show_courses(self::COURSECAT_SHOW_COURSES_COUNT)->
                set_categories_display_options(array(
                    'limit' => $CFG->coursesperpage,
                    'viewmoreurl' => new moodle_url('/course/index.php',
                            array('browse' => 'categories', 'page' => 1))
                ))->
                set_attributes(array('class' => 'frontpage-category-names'));
        return $this->coursecat_tree($chelper, coursecat::get(0));
    }

	    function course_search_form($value = '', $format = 'plain') {
				global $CFG,$PAGE;				
				require_once($CFG->libdir. '/coursecatlib.php');
					if($PAGE->pagelayout == "frontpage"){
						return ;
		}else{
			        static $count = 0;
					$formid = 'coursesearch';
					if ((++$count) > 1) {
						$formid .= $count;
					}

					switch ($format) {
						case 'navbar' :
							$formid = 'coursesearchnavbar';
							$inputid = 'navsearchbox';
							$inputsize = 20;
							break;
						case 'short' :
							$inputid = 'shortsearchbox';
							$inputsize = 12;
							break;
						default :
							$inputid = 'coursesearchbox';
							$inputsize = 30;
					}

					$strsearchcourses= get_string("searchcourses");
					$searchurl = new moodle_url('/course/search.php');

					$output = html_writer::start_tag('form', array('id' => $formid, 'action' => $searchurl, 'method' => 'get'));
					$output .= html_writer::start_tag('fieldset', array('class' => 'coursesearchbox invisiblefieldset'));
					$output .= html_writer::tag('label', $strsearchcourses.': ', array('for' => $inputid));
					$output .= html_writer::empty_tag('input', array('type' => 'text', 'id' => $inputid,
						'size' => $inputsize, 'name' => 'search', 'value' => s($value)));
					$output .= html_writer::empty_tag('input', array('type' => 'submit',
						'value' => get_string('go')));
					$output .= html_writer::end_tag('fieldset');
					$output .= html_writer::end_tag('form');

					return $output;
				}
		}
	
	
		public function frontpage_available_courses_search($category, $search) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
				$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
						set_courses_display_options(array(
							'recursive' => true,
							'viewmoreurl' => new moodle_url('/course/index.php'),
							'viewmoretext' => new lang_string('fulllistofcourses')));
				$chelper->set_attributes(array('class' => 'frontpage-course-list-all'));
				if($category>=0){
					$courses_all = coursecat::get($category)->get_courses($chelper->get_courses_display_options());
				}else{
					$courses_all = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
				}
				
				foreach($courses_all as $course_all){
					 if($search == '' || strripos($course_all->fullname, $search) !== false || strripos($course_all->shortname, $search) !== false){
						$courses[$course_all->id] = $course_all; 
					 }	 
				 }
				 if(count($courses) == 0) {
					 $out = html_writer::tag('div',html_writer::tag('div',get_string('not_found_course', 'theme_appraisal')."'".$search."'", array('class' => 'alert alert-warning')), array('class' => 'courses frontpage-course-list-all'));
				 }else{
					 $out = $this->coursecat_courses($chelper, $courses);
				 }
				
				
				echo $out;
	}
	public function frontpage_my_courses_search($category, $search) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
				$courses_all  = enrol_get_my_courses('summary, summaryformat');
				 foreach($courses_all as $course_all){
					 if(($category == $course_all->category || $category == '') && ($search == '' ||strripos($course_all->fullname, $search) !== false || strripos($course_all->shortname, $search) !== false)){
						$courses[$course_all->id] = $course_all; 
					 }	 
				 }
				 
				if(count($courses) == 0) echo html_writer::tag('div',$OUTPUT->heading('Not Found'), array('class' => 'courses frontpage-course-list-enrolled'));
				$chelper = new coursecat_helper();
				$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
						set_courses_display_options(array(
							'recursive' => true,
							'limit' => $CFG->frontpagecourselimit,
							'viewmoreurl' => new moodle_url('/course/index.php'),
							'viewmoretext' => new lang_string('fulllistofcourses')));
				$chelper->set_attributes(array('class' => 'frontpage-course-list-enrolled'));
				$out = $this->coursecat_courses($chelper, $courses);
				
				echo $out;
	} 
	public function frontpage_available_courses_get_courses($step) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
			$chelper->set_attributes(array('id' => 'temp'));
			$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->set_courses_display_options(array('recursive' => true));
			$courses = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
			$courses = array_splice($courses, ($CFG->frontpagecourselimit * $step));
			$output = '';
			$count = count($courses);
			
				if(count($courses) > $CFG->frontpagecourselimit){
					while(count($courses) > $CFG->frontpagecourselimit){
						array_pop($courses);
					}	
				}
			$output .= $this->coursecat_courses($chelper, $courses);
			if($count > $CFG->frontpagecourselimit){
				echo html_writer::start_tag('div', array('id' => 'temp2')).html_writer::start_tag('div', array('class' => 'paging-morelink')).html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("all", '.++$step.');',)).html_writer::end_tag('div').html_writer::end_tag('div');		
			}
			echo $output;
	} 
	public function frontpage_my_courses_get_courses($step) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
			$chelper->set_attributes(array('id' => 'temp'));
			$courses = enrol_get_my_courses('summary, summaryformat');
			$courses = array_splice($courses, ($CFG->frontpagecourselimit * $step));
			$output = '';	
			$count = count($courses);
			
				if(count($courses) > $CFG->frontpagecourselimit){	
					while(count($courses) > $CFG->frontpagecourselimit){
						array_pop($courses);
					}	 			
				}
			$output .= $this->coursecat_courses($chelper, $courses);
			echo $output;
			if($count > $CFG->frontpagecourselimit){
				echo html_writer::start_tag('div', array('id' => 'temp2')).html_writer::start_tag('div', array('class' => 'paging-morelink')).html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("my", '.++$step.');',)).html_writer::end_tag('div').html_writer::end_tag('div');		
			}
	}
	
	public function course_category($category) {
        global $CFG,$PAGE;
		if($PAGE->pagetype == 'course-index' && !is_siteadmin()){
			redirect($CFG->wwwroot);
		}
        require_once($CFG->libdir. '/coursecatlib.php');
        $coursecat = coursecat::get(is_object($category) ? $category->id : $category);
        $site = get_site();
        $output = '';

        if (can_edit_in_category($coursecat->id)) {
            // Add 'Manage' button if user has permissions to edit this category.
            $managebutton = $this->single_button(new moodle_url('/course/management.php',
                array('categoryid' => $coursecat->id)), get_string('managecourses'), 'get');
            $this->page->set_button($managebutton);
        }
        if (!$coursecat->id) {
            if (coursecat::count_all() == 1) {
                // There exists only one category in the system, do not display link to it
                $coursecat = coursecat::get_default();
                $strfulllistofcourses = get_string('fulllistofcourses');
                $this->page->set_title("$site->shortname: $strfulllistofcourses");
            } else {
                $strcategories = get_string('categories');
                $this->page->set_title("$site->shortname: $strcategories");
            }
        } else {
            $this->page->set_title("$site->shortname: ". $coursecat->get_formatted_name());

            // Print the category selector
            $output .= html_writer::start_tag('div', array('class' => 'categorypicker'));
            $select = new single_select(new moodle_url('/course/index.php'), 'categoryid',
                    coursecat::make_categories_list(), $coursecat->id, null, 'switchcategory');
            $select->set_label(get_string('categories').':');
            $output .= $this->render($select);
            $output .= html_writer::end_tag('div'); // .categorypicker
        }

        // Print current category description
        $chelper = new coursecat_helper();
        if ($description = $chelper->get_category_formatted_description($coursecat)) {
            $output .= $this->box($description, array('class' => 'generalbox info'));
        }

        // Prepare parameters for courses and categories lists in the tree
        $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_AUTO)
                ->set_attributes(array('class' => 'category-browse category-browse-'.$coursecat->id));

        $coursedisplayoptions = array();
        $catdisplayoptions = array();
        $browse = optional_param('browse', null, PARAM_ALPHA);
        $perpage = optional_param('perpage', $CFG->coursesperpage, PARAM_INT);
        $page = optional_param('page', 0, PARAM_INT);
        $baseurl = new moodle_url('/course/index.php');
        if ($coursecat->id) {
            $baseurl->param('categoryid', $coursecat->id);
        }
        if ($perpage != $CFG->coursesperpage) {
            $baseurl->param('perpage', $perpage);
        }
        $coursedisplayoptions['limit'] = $perpage;
        $catdisplayoptions['limit'] = $perpage;
        if ($browse === 'courses' || !$coursecat->has_children()) {
            $coursedisplayoptions['offset'] = $page * $perpage;
            $coursedisplayoptions['paginationurl'] = new moodle_url($baseurl, array('browse' => 'courses'));
            $catdisplayoptions['nodisplay'] = true;
            $catdisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'categories'));
            $catdisplayoptions['viewmoretext'] = new lang_string('viewallsubcategories');
        } else if ($browse === 'categories' || !$coursecat->has_courses()) {
            $coursedisplayoptions['nodisplay'] = true;
            $catdisplayoptions['offset'] = $page * $perpage;
            $catdisplayoptions['paginationurl'] = new moodle_url($baseurl, array('browse' => 'categories'));
            $coursedisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'courses'));
            $coursedisplayoptions['viewmoretext'] = new lang_string('viewallcourses');
        } else {
            // we have a category that has both subcategories and courses, display pagination separately
            $coursedisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'courses', 'page' => 1));
            $catdisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'categories', 'page' => 1));
        }
        $chelper->set_courses_display_options($coursedisplayoptions)->set_categories_display_options($catdisplayoptions);
        // Add course search form.
        $output .= $this->course_search_form();

        // Display course category tree.
        $output .= $this->coursecat_tree($chelper, $coursecat);

        // Add action buttons
        $output .= $this->container_start('buttons');
        $context = get_category_or_system_context($coursecat->id);
        if (has_capability('moodle/course:create', $context)) {
            // Print link to create a new course, for the 1st available category.
            if ($coursecat->id) {
                $url = new moodle_url('/course/edit.php', array('category' => $coursecat->id, 'returnto' => 'category'));
            } else {
                $url = new moodle_url('/course/edit.php', array('category' => $CFG->defaultrequestcategory, 'returnto' => 'topcat'));
            }
            $output .= $this->single_button($url, get_string('addnewcourse'), 'get');
        }
        ob_start();
        if (coursecat::count_all() == 1) {
            print_course_request_buttons(context_system::instance());
        } else {
            print_course_request_buttons($context);
        }
        $output .= ob_get_contents();
        ob_end_clean();
        $output .= $this->container_end();

        return $output;
    }
	
	
	public function course_info_box(stdClass $course) {
		global $PAGE,$DB,$CFG,$forms;		

		if($PAGE->pagetype == 'enrol-index'){
			$teachers = $DB->get_records_sql("SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS name
											FROM {context} c
											LEFT JOIN {role_assignments} ra ON ra.roleid=3 AND ra.contextid=c.id
											LEFT JOIN {user} u ON ra.userid=u.id 
											WHERE c.contextlevel=50 AND c.instanceid= $course->id AND u.id IS NOT NULL
										");
			$additional_info = $DB->get_record_sql("SELECT 
														(SELECT id FROM {course_categories} WHERE id= $course->category ) as category_id,
														(SELECT name FROM {course_categories} WHERE id= $course->category ) as category,
														(SELECT count(id) FROM {course_sections} WHERE course = $course->id AND name IS NOT NULL ) as topics,
														(SELECT count(id) FROM {course_modules} WHERE course = $course->id AND idnumber IS NOT NULL ) as modules,
														(SELECT count(id) FROM {user} WHERE id>2 ) as all_users,
														(SELECT count(DISTINCT u.id)
															FROM {context} c
															LEFT JOIN {role_assignments} ra ON ra.contextid=c.id
															LEFT JOIN {user} u ON ra.userid=u.id 
														WHERE c.contextlevel=50 AND c.instanceid= $course->id AND u.id IS NOT NULL) as enrolled_users 
											");
											
			$i=0;$insctructor ='';
			if(count($teachers)>0){
				$insctructor .='Instructed by ';
				foreach($teachers as $item){
					if($i>0)
						$insctructor .= ', ';
					$insctructor .= $item->name;
					$i++;
				}
			}else{
				$insctructor .='';
			}
			
			$stars = '';$j=0;
			$rating_number = ($additional_info->enrolled_users/$additional_info->all_users)*100;
			for($i=1;$i<=5;$i++){
				if($rating_number > $j)
					$stars .= html_writer::tag('span','',array('class'=>'star full'));
				else
					$stars .= html_writer::tag('span','',array('class'=>'star empty'));
				$j += 20;
			}
			
			$enrols = enrol_get_plugins(true);
			$enrolinstances = enrol_get_instances($course->id, true);
			$min_cost = 9999999999;
			$min_currency = '';
			foreach($enrolinstances as $instance) {
				if (!isset($enrols[$instance->enrol])) {
					continue;
				}
				if($instance->cost > 0){
					if($min_cost > $instance->cost){
						$min_cost = $instance->cost;
						$min_currency = $instance->currency;
					}
				}
			}
			
			$contentimages = $contentfiles = '';
			$cours = new course_in_list($course);
			foreach ($cours->get_course_overviewfiles() as $file) {
				$isimage = $file->is_valid_image();
				 $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
						'/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
						$file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage); 
				 if ($isimage) {
					$contentimages .= html_writer::tag('div',
							html_writer::empty_tag('img', array('src' => $url)),
							array('class' => 'courseimage'));
				} else {
					$image = $this->output->pix_icon(file_file_icon($file, 24), $file->get_filename(), 'moodle');
					$filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
							html_writer::tag('span', $file->get_filename(), array('class' => 'fp-filename'));
					$contentfiles .= html_writer::tag('span',
							html_writer::link($url, $filename),
							array('class' => 'coursefile fp-filename-icon'));
				} 
			}
			$course_files = $contentimages. $contentfiles;
			
			$content = '';
			$content .= html_writer::start_tag('div',array('class'=>'forms-wrapper clearfix'));
				$content .= html_writer::tag('span','',array('class'=>'fa fa-close hide-forms'));
				foreach ($forms as $form) {
					$content .= str_replace('<p>&nbsp;</p>','',$form);
				}
			$content .= html_writer::end_tag('div');
			$GLOBALS['forms'] = array('');
			
			$content .= $this->output->box_start('generalbox info enrol-info clearfix');
			
			$content .= html_writer::start_tag('div',array('class'=>'course-info'));			
			$content .= html_writer::tag('h2',$course->fullname);				
			$content .= html_writer::tag('div',$stars.
											html_writer::tag('span',' '.$additional_info->enrolled_users.' users enrolled',array('class'=>'users'))
											, array('class'=>'rating'));			
			$content .= html_writer::tag('div',html_writer::tag('span',$insctructor,array('class'=>'insctructor')).
												html_writer::tag('span','Category / '.  
													html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $additional_info->category_id)),$additional_info->category) 
												,array('class'=>'category'))
										, array('class'=>'additional-info'));			
			$content .= html_writer::tag('div',$course_files,array('class'=>'course-files'));
			$content .= html_writer::end_tag('div');
			
			$content .= html_writer::start_tag('div',array('class'=>'enrol-price'));
			$content .= html_writer::start_tag('div',array('class'=>'enrol'));
			if($min_cost != 9999999999)
				$content .= html_writer::tag('h2',$min_cost.' '.$min_currency,array('class'=>'price'));
			$content .= html_writer::tag('button','Enroll',array('class'=>'show-forms'));
			$content .= html_writer::end_tag('div');

			$content .= html_writer::start_tag('ul',array('class'=>'price-info'));
				$content .= html_writer::tag('li',
											html_writer::tag('span','Topics/Modules').
											html_writer::tag('span',$additional_info->topics.'/'.$additional_info->modules)
											);
				$content .= html_writer::tag('li',
											html_writer::tag('span','Length').
											html_writer::tag('span','5 hours')
											);
				$content .= html_writer::tag('li',
											html_writer::tag('span','Skill level').
											html_writer::tag('span','Advanced')
											);
				$content .= html_writer::tag('li',
											html_writer::tag('span','Languages').
											html_writer::tag('span','English, Spanish')
											);
			$content .= html_writer::end_tag('ul');
			$content .= html_writer::end_tag('div');
			
			$content .= $this->output->box_end();
			
			$sections = $DB->get_records_sql("SELECT * FROM {course_sections} WHERE course = $course->id AND name IS NOT NULL");
			$recommended = $DB->get_records_sql("SELECT c.*, 
													(SELECT MIN(cost) FROM {enrol} WHERE c.id=courseid) as cost,
													(SELECT currency FROM {enrol} WHERE c.id=courseid AND cost IS NOT NULL ORDER BY cost LIMIT 1) as currency,
													(SELECT count(DISTINCT u.id)
															FROM {context} co
															LEFT JOIN {role_assignments} ra ON ra.contextid=co.id
															LEFT JOIN {user} u ON ra.userid=u.id 
														WHERE co.contextlevel=50 AND co.instanceid= c.id AND u.id IS NOT NULL) as enrolled_users
													FROM {course} c 
													WHERE c.category = $course->category AND c.id NOT LIKE '$course->id' ORDER BY c.timemodified LIMIT 3");
			$modinfo = get_fast_modinfo($course);
			
			$content .= html_writer::start_tag('div',array('class'=>'enrol-bottom clearfix'));
				$content .= html_writer::start_tag('div',array('class'=>'desription'));
					$content .= html_writer::tag('h3','Course Description');
					$content .= html_writer::tag('div',$course->summary);
					$content .= html_writer::tag('h3','Curriculum');
					$content .= html_writer::start_tag('ul',array('class'=>'curriculum'));
					foreach($sections as $section){
						if(empty($section->summary)){
							$content .= html_writer::tag('li',$section->name,array('class'=>'section'));
						}else{
							$content .= html_writer::tag('li',$section->name.
															html_writer::tag('i','',array('class'=>'fa fa-sort-desc')).
															html_writer::tag('i','',array('class'=>'fa fa-sort-asc')).
															html_writer::tag('div',$section->summary,array('class'=>'summary'))
														,array('class'=>'section'));
						}

						$modules = explode(',',$section->sequence);
						foreach($modules as $module){
							$cm = $modinfo->get_cm($module);
							$content .= html_writer::tag('li',
														html_writer::tag('i','',array('class'=>'fa fa-file-o')).
														$cm->name);
						}
					}
					$content .= html_writer::end_tag('ul');
				$content .= html_writer::end_tag('div');
				$content .= html_writer::start_tag('div',array('class'=>'recommended'));
					$content .= html_writer::tag('h3','Recommended Courses');
					foreach($recommended as $item){
						$cours = new course_in_list($item);
						$contentimages = '';
						foreach ($cours->get_course_overviewfiles() as $file) {
							$isimage = $file->is_valid_image();
							 $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
									'/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
									$file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage); 
							 if ($isimage) {
								$contentimages .= html_writer::tag('div',
										html_writer::empty_tag('img', array('src' => $url)),
										array('class' => 'courseimage'));
								break;
							} 
						}
						if($contentimages == ""){
							$contentimages .= html_writer::tag('div',html_writer::empty_tag('img', array('src' => $CFG->wwwroot."/theme/appraisal/pix/frontcoursebg.png")),array('class'=>'courseimage'));
						}
						$stars = '';$j=0;
						$rating_number = ($item->enrolled_users/$additional_info->all_users)*100;
						for($i=1;$i<=5;$i++){
							if($rating_number > $j)
								$stars .= html_writer::tag('span','',array('class'=>'star full'));
							else
								$stars .= html_writer::tag('span','',array('class'=>'star empty'));
							$j += 20;
						}
			
						$content .= html_writer::start_tag('div',array('class'=>'course clearfix'));
							$content .= $contentimages;
							$content .= html_writer::start_tag('div',array('class'=>'content'));
								$content .= html_writer::tag('h3',html_writer::link(new moodle_url('/course/view.php', array('id' => $item->id)),$item->fullname));	
								$content .= html_writer::tag('span',$item->cost.' '.$item->currency);	
								$content .= html_writer::tag('div',$stars, array('class'=>'rating'));
								
							$content .= html_writer::end_tag('div');
						$content .= html_writer::end_tag('div');
					}
				$content .= html_writer::end_tag('div');
			$content .= html_writer::end_tag('div');
		}else{
			$content .= $this->output->box_start('generalbox info');
			$chelper = new coursecat_helper();
			$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED);
			$content .= $this->coursecat_coursebox($chelper, $course);
			$content .= $this->output->box_end();
		}
		

        return $content;
    }
    
    public function get_course_completion($course) {
        global $CFG, $PAGE, $DB, $USER, $OUTPUT;				
        
        require_once("{$CFG->libdir}/completionlib.php");
        
        $result = new stdClass();
        $result->completion = 0;
        $result->status = 'notyetstarted';

        // Get course completion data.
        $info = new completion_info($course);

        // Load criteria to display.
        $completions = $info->get_completions($USER->id);

        if ($info->is_tracked_user($USER->id)) {

            // For aggregating activity completion.
            $activities = array();
            $activities_complete = 0;

            // For aggregating course prerequisites.
            $prerequisites = array();
            $prerequisites_complete = 0;

            // Flag to set if current completion data is inconsistent with what is stored in the database.
            $pending_update = false;

            // Loop through course criteria.
            foreach ($completions as $completion) {
                $criteria = $completion->get_criteria();
                $complete = $completion->is_complete();

                if (!$pending_update && $criteria->is_pending($completion)) {
                    $pending_update = true;
                }

                // Activities are a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                    $activities[$criteria->moduleinstance] = $complete;

                    if ($complete) {
                        $activities_complete++;
                    }

                    continue;
                }

                // Prerequisites are also a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                    $prerequisites[$criteria->courseinstance] = $complete;

                    if ($complete) {
                        $prerequisites_complete++;
                    }

                    continue;
                }
            }

            $itemsCompleted  = $activities_complete + $prerequisites_complete;
            $itemsCount      = count($activities) + count($prerequisites);

            // Aggregate completion.
            if ($itemsCount > 0) {
                $result->completion = round(($itemsCompleted / $itemsCount) * 100);
            }

            // Is course complete?
            $coursecomplete = $info->is_course_complete($USER->id);

            // Load course completion.
            $params = array(
                'userid' => $USER->id,
                'course' => $course->id
            );
            $ccompletion = new completion_completion($params);

            // Has this user completed any criteria?
            $criteriacomplete = $info->count_course_user_data($USER->id);

            if ($pending_update) {
                $status = 'pending';
            } else if ($coursecomplete) {
                $status = 'completed';
            } else if (!$criteriacomplete && !$ccompletion->timestarted) {
                $status = 'notyetstarted';
            } else {
                $status = 'inprogress';
            }

            $result->status = $status;
        }
        
        return $result;
    }
}

?>
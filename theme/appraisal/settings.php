<?php
/**
 * @package   theme_appraisal
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {


    // Logo file setting.
    $name = 'theme_appraisal/logo';
    $title = get_string('logo','theme_appraisal');
    $description = get_string('logodesc', 'theme_appraisal');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
    
    $name = 'theme_appraisal/faviconico';
    $title = get_string('faviconico','theme_appraisal');
    $description = get_string('faviconicodesc', 'theme_appraisal');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'faviconico');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
    
    $name = 'theme_appraisal/favicon';
    $title = get_string('favicon','theme_appraisal');
    $description = get_string('favicondesc', 'theme_appraisal');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    $name = 'theme_appraisal/backgroundhead';
    $title = get_string('backgroundhead', 'theme_appraisal');
    $description = get_string('backgroundhead_desc', 'theme_appraisal');
	$default = '#0A4478';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
    $name = 'theme_appraisal/backgroundfoot';
    $title = get_string('backgroundfoot', 'theme_appraisal');
    $description = get_string('backgroundfoot_desc', 'theme_appraisal');
	$default = '#0A4478';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
    // Custom CSS file.
    $name = 'theme_appraisal/customcss';
    $title = get_string('customcss', 'theme_appraisal');
    $description = get_string('customcssdesc', 'theme_appraisal');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Footnote setting.
    $name = 'theme_appraisal/footnote';
    $title = get_string('footnote', 'theme_appraisal');
    $description = get_string('footnotedesc', 'theme_appraisal');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
	// Custom front Welcome to login user.
    $name = 'theme_appraisal/front_welcome_login_user';
    $title = get_string('front_welcome_login_user', 'theme_appraisal');
    $description = get_string('front_welcome_login_user_desc', 'theme_appraisal');
    $default = 'SEBALE Learning Management System';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
	// Custom front Welcome to no login user.
    $name = 'theme_appraisal/front_welcome_no_login_user';
    $title = get_string('front_welcome_no_login_user', 'theme_appraisal');
    $description = get_string('front_welcome_no_login_user_desc', 'theme_appraisal');
    $default = 'Designing Your Learning Endeavour!';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
	// Custom front Welcome to no login user.
    $name = 'theme_appraisal/front_welcome_full_no_login_user';
    $title =get_string('front_welcome_full_no_login_user', 'theme_appraisal');
    $description = get_string('front_welcome_full_no_login_user_desc', 'theme_appraisal');
    $default = 'Welcome to SEBALE LMS, simple yet intuitive and easy to set up learning management system running on Moodle.<a href="http://www.appraisal.com" style="display:block;"><button style="margin:15px 0 0;">Learn More</button></a>';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
}

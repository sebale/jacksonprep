<header>
	<?php echo $OUTPUT->standard_top_of_body_html() ?>
	<div class="inner clearfix">
		<div class="logo">
			<a href="<?php echo $CFG->wwwroot; ?>" ><?php echo theme_guthyrenker_get_logo(); ?></a>
		</div>		
		<?php if(!$USER->id): ?>
		<nav class="menu">
			<div class="header-menu-toggle btn-navbar"  data-toggle="collapse" data-target=".nav-collapse"><i class="fa fa-bars"></i></div>


		</nav>
		<?php else: ?>
		<nav class="user">
			<ul class="nav">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo $CFG->wwwroot; ?>/user/pix.php?file=/<?php echo $USER->id; ?>/f1.jpg" title="<?php echo $USER->firstname; ?> <?php echo $USER->lastname; ?>" alt="<?php echo $USER->firstname; ?> <?php echo $USER->lastname; ?>"/>
						<span class="name"><?php echo $USER->firstname; ?> <?php echo $USER->lastname; ?></span>
						<span class="icon"><b class="caret"></b></span>
					</a>
					<ul class = "dropdown-menu">
						<li><a href = "<?php echo $CFG->wwwroot; ?>/my"><?php echo get_string('mycourses'); ?></a></li>
						<li><a href = "<?php echo $CFG->wwwroot; ?>/user/profile.php"><?php echo get_string('viewprofile'); ?></a></li>
						<li><a href = "<?php echo $CFG->wwwroot; ?>/user/edit.php"><?php echo get_string('editmyprofile'); ?></a></li>
						<li><a href = "<?php echo $CFG->wwwroot; ?>/user/files.php"><?php echo get_string('myfiles'); ?></a></li>
						<li><a href = "<?php echo $CFG->wwwroot; ?>/calendar/view.php?view=month"><?php echo get_string('calendar', 'calendar'); ?></a></li>
						<li><a href = "<?php echo $CFG->wwwroot; ?>/login/logout.php"><?php echo get_string('logout'); ?></a></li>
					</ul>
				</li>
			</ul>
		</nav>
		<?php endif; ?>
		<nav class="menu clearfix"><?php echo $OUTPUT->custom_menu(); ?></nav>
	</div>
</header>
<div class="background-content">
<div class="inner"></div>
</div>
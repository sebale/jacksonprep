<?php
/**
 * @package   theme_mayoclinic
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Mayoclinic</h2>
<p><img class=img-polaroid src="mayoclinic/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>Mayoclinic is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
<h3>Parents</h3>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
<h3>Theme Credits</h3>
<p>Authors: Bas Brands, David Scotson, Mary Evans<br>
Contact: bas@sonsbeekmedia.nl<br>
Website: <a href="http://www.basbrands.nl">www.basbrands.nl</a>
</p>
<h3>Report a bug:</h3>
<p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
<h3>More information</h3>
<p><a href="mayoclinic/README.txt">How to copy and customise this theme.</a></p>
</div></div>';

$string['configtitle'] = 'mayoclinic';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';

$string['invert'] = 'Invert navbar';
$string['invertdesc'] = 'Swaps text and background for the navbar at the top of the page between black and white.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.<br>
If the height of your logo is more than 75px add the following CSS rule to the Custom CSS box below.<br>
a.logo {height: 100px;} or whatever height in pixels the logo is.';

$string['pluginname'] = 'MAYOCLINIC LMS';
$string['choosereadme'] = '<p>More is a theme that allows you to easily customise Moodle\'s look and feel directly from the web interface.</p>
<p>Visit the admin settings to change colors, add an image as a background, add your logo and more.</p>';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['backgroundhead'] = 'Head color';
$string['backgroundhead_desc'] = 'The head color to use for the background.';
$string['backgroundfoot'] = 'Footer colour';
$string['backgroundfoot_desc'] = 'The footer color to use for the background.';
$string['front_welcome_login_user'] = 'Welcome text';
$string['front_welcome_login_user_desc'] = 'The Welcome text to logged in user';
$string['front_welcome_no_login_user'] = 'Welcome text h1 login ';
$string['front_welcome_no_login_user_desc'] = 'The Welcome text h1 to not logged in user';
$string['front_welcome_full_no_login_user'] = 'Welcome text h3 login';
$string['front_welcome_full_no_login_user_desc'] = 'The Welcome text h3 to not logged in user';
$string['front_message_no_login_user'] = 'Message text to not logged in user';
$string['front_message_no_login_user_desc'] = 'The Welcome text on front page to not logged in user';
$string['no_course_info'] = 'No course information to show';
$string['not_found_course'] = 'No courses were found with the words ';
$string['course_progress'] = 'Course progress: ';
$string['grade'] = 'Avg grade: ';
$string['faviconico'] = 'Favicon (.ico): ';
$string['faviconicodesc'] = 'Please upload your custom favicon file (.ico)';
$string['favicon'] = 'Favicon (.png)';
$string['favicondesc'] = 'Please upload your custom favicon file (.png)';

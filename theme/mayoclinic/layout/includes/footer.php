<?php $html = theme_mayoclinic_get_html_for_settings($OUTPUT, $PAGE);?>
<footer>
	<div class="inner clearfix">
		<nav class="menu clearfix"><?php echo $OUTPUT->custom_menu(); ?></nav>
		<?php  echo $html->footnote;?>
		<div class="copyright">SEBALE LMS 2014 - <?php echo date("Y");?> Copyright &copy;. All Rights Reserved.</div>
		
		<?php echo $OUTPUT->standard_footer_html();  ?>
		<?php echo $OUTPUT->standard_end_of_body_html() ?>
	</div>
	<style>

	</style>
	<script>
	/*jQuery(document).ready(function(){
		<?php if(!$USER->id): ?>
		//jQuery('header nav ul').append('<li class="signup"><a href="<?php echo $CFG->wwwroot;?>/login/signup.php">Sign up</a></li>');
		<?php else: ?>
			//jQuery('header nav ul').append('<li class="logout"><a href="<?php echo $CFG->wwwroot;?>/login/index.php">Logout</a></li>');
		<?php endif; ?>
	});*/
	</script>
</footer>
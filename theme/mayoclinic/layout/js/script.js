$(window).ready(function(){
	if(Y.one(".course-content") != null){		
		$('.course-content .tabs li').click(function(){
			var section = '#'+$($($($(this).parent()).parent()).parent()).attr('id');
			var mod_name = $(this).attr('mod_name');
			
			$(section+' .tabs').removeClass('active');
			$(section+' .dropdown').toggleClass('active');
			
			if(mod_name == 'all'){
				$(section+' .img-text li').removeClass('hidden');
			}else{
				$(section+' .img-text li').addClass('hidden');
				$(section+' .img-text .'+mod_name).removeClass('hidden');
			}
		});
			
			
			
 		$(window).click(function(){		
				$('.course-content .dropdown').removeClass('active');
				$('.course-content .section.tabs').removeClass('active');
		}); 
		$('.course-content .dropdown').click(function(event){
			if(!$(this).hasClass('active')){
				$('.course-content .dropdown').removeClass('active');
				$('.course-content .tabs').removeClass('active');
			}
				var section = '#'+$($($(this).parent()).parent()).attr('id');
				$(section+' .tabs').toggleClass('active');
				$(this).toggleClass('active');
				event.stopPropagation();
		}); 
		

		
		$('.course-content .show-activity').click(function(){
			var section = '#'+$($($(this).parent()).parent()).attr('id');
			
			if($(this).hasClass('fa-angle-down')){
				$(section+' .img-text li, '+section+' .summary').removeClass('hidden');
			}else{
				$(section+' .img-text li, '+section+' .summary').addClass('hidden');
			}		
			
			$(this).toggleClass('fa-angle-down');
			$(this).toggleClass('fa-angle-up');
		});

		
		$('.course-content .show-activity').trigger('click');
		$('.course-content .current .show-activity').trigger('click');
	}
});

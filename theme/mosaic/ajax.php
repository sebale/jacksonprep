<?php
global $CFG, $OUTPUT,$PAGE;		
require_once('../../config.php');

$action = required_param('action', PARAM_TEXT);
if($action == 'get_courses'){
	$step = required_param('step', PARAM_TEXT);
	$type = required_param('type', PARAM_TEXT);
	$PAGE->set_pagelayout('frontpage');
	$PAGE->set_course($SITE);
	$courserenderer = $PAGE->get_renderer('core', 'course');

	if($type == 'all'){
		$courserenderer->frontpage_available_courses_get_courses($step);
	}elseif($type == 'my'){
		$courserenderer->frontpage_my_courses_get_courses($step);
	}
	
}elseif($action == 'search'){
	$type = required_param('type', PARAM_TEXT);
	$category = required_param('category', PARAM_TEXT);
	$search = required_param('search', PARAM_TEXT);

	$searchcriteria = array('search' =>$search);

	$PAGE->set_pagelayout('frontpage');
	$PAGE->set_course($SITE);
	$courserenderer = $PAGE->get_renderer('core', 'course');

	if($type == 'all'){
		$courserenderer->frontpage_available_courses_search($category, $search);
	}elseif($type == 'my'){
		$courserenderer->frontpage_my_courses_search($category, $search);
	}
}
?>
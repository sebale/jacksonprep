<?php
/**
 * @package   theme_mosaic
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'SEBALELMS Mosaic';
$string['configtitle'] = 'SEBALELMS Mosaic';
$string['choosereadme'] = '';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';
$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';
$string['invert'] = 'Invert navbar';
$string['invertdesc'] = 'Swaps text and background for the navbar at the top of the page between black and white.';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.<br>
If the height of your logo is more than 75px add the following CSS rule to the Custom CSS box below.<br>
a.logo {height: 100px;} or whatever height in pixels the logo is.';
$string['choosereadme'] = '<p>More is a theme that allows you to easily customise Moodle\'s look and feel directly from the web interface.</p>
<p>Visit the admin settings to change colors, add an image as a background, add your logo and more.</p>';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['backgroundhead'] = 'Head color';
$string['backgroundhead_desc'] = 'The head color to use for the background.';
$string['front_welcome_no_login_user'] = 'Welcome text no login user login ';
$string['front_welcome_no_login_user_desc'] = 'The Welcome text to not logged in user, after logo';
$string['backgroundcolorleft'] = 'Background color to left section';
$string['backgroundcolorleft_desc'] = '';
$string['leftbg'] = 'Image for left section';
$string['leftbg_desc'] = 'Please upload your custom image here if you want to add it to the left section';
$string['no_course_info'] = 'No course information to show';
$string['not_found_course'] = 'No courses were found with the words';
$string['faviconico'] = 'Favicon (.ico): ';
$string['faviconicodesc'] = 'Please upload your custom favicon file (.ico)';
$string['favicon'] = 'Favicon (.png)';
$string['favicondesc'] = 'Please upload your custom favicon file (.png)';
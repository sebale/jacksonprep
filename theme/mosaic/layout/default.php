<?php
/**
 * @package   theme_mosaic
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$maincss = ($hassidepost) ? 'rightside' : 'bothside';

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_mosaic_generate_favicon('faviconico'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_mosaic_generate_favicon('favicon'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_mosaic_generate_favicon('favicon'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_mosaic_generate_favicon('favicon'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:200,300,800,700,600,400" rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type='text/css'>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="<?php echo $CFG->wwwroot?>/theme/mosaic/js/script.js"></script>
</head>

<body <?php  echo  str_replace("dir-ltr", "",$OUTPUT->body_attributes(array('class'=>'jsenabled'))); ?>>
<?php if(!$USER->id)include('includes/header.php');?>
	<section class="main-left <?php echo ($USER->id) ? '' : 'full'; ?>">
		<div class="container">
			<div class="logo">
				<a href="<?php echo $CFG->wwwroot; ?>" ><?php echo theme_mosaic_get_logo(); ?></a>
			</div>
			<?php if($USER->id): ?>
				<nav class="user">
					<ul class="nav">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo $CFG->wwwroot; ?>/user/pix.php?file=/<?php echo $USER->id; ?>/f1.jpg" title="<?php echo $USER->firstname; ?> <?php echo $USER->lastname; ?>" alt="<?php echo $USER->firstname; ?> <?php echo $USER->lastname; ?>"/>
								<span class="name"><?php echo $USER->firstname; ?> <?php echo $USER->lastname; ?></span>
								<span class="icon"><b class="caret"></b></span>
								<span class="email"><?php echo $USER->email;?></span>
							</a>
							<ul class = "dropdown-menu">
								<li><a href = "<?php echo $CFG->wwwroot; ?>/my"> <i class="fa fa-mortar-board"></i> <?php echo get_string('mycourses'); ?></a></li>
								<li><a href = "<?php echo $CFG->wwwroot; ?>/user/profile.php"><i class="fa  fa-user"></i> <?php echo get_string('viewprofile'); ?></a></li>
								<li><a href = "<?php echo $CFG->wwwroot; ?>/user/edit.php"><i class="fa  fa-pencil"></i> <?php echo get_string('editmyprofile'); ?></a></li>
								<li><a href = "<?php echo $CFG->wwwroot; ?>/user/files.php"><i class="fa fa-file"></i> <?php echo get_string('myfiles'); ?></a></li>
								<li><a href = "<?php echo $CFG->wwwroot; ?>/calendar/view.php?view=month"><i class="fa fa-calendar"></i> <?php echo get_string('calendar', 'calendar'); ?></a></li>
								<li class="last"><a href = "<?php echo $CFG->wwwroot; ?>/login/logout.php"><i class="fa  fa-sign-out"></i> <?php echo get_string('logout'); ?></a></li>
							</ul>
						</li>
					</ul>
				</nav>
			<?php endif;?>
			<?php echo ($hassidepre) ? $OUTPUT->blocks('side-pre', 'side-pre') : '';  ?>		
		</div>
	
	</section>
	
	<section id="page" class="page clearfix">
		<div class="content inner clearfix <?php echo $maincss; ?>"  id="region-main">			
				<div class="clearfix">
					<div class="main-left-toggle" id='left_toggle'><i class="fa fa-bars"></i></div>
					<?php if($OUTPUT->navbar() || $OUTPUT->page_heading_button()):?>
						<div id="page-navbar" class="clearfix">
							<nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
							<div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
						</div>
					<?php endif;?>
				</div>
			 <?php
				echo $OUTPUT->course_content_header();
				echo $OUTPUT->main_content();
				echo $OUTPUT->course_content_footer();
			?>		
		<?php echo ($hassidepost) ? $OUTPUT->blocks('side-post', 'side-post') : ''; ?>
		</div>
	</section>
	<?php include('includes/footer.php'); ?>

	<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
<?php $html = theme_mosaic_get_html_for_settings($OUTPUT, $PAGE);?>
<footer class="<?php echo ($USER->id && $PAGE->pagelayout != "frontpage") ? '' : 'full'; ?>">
	<div class="inner clearfix">
		<nav class="menu clearfix"><?php echo $OUTPUT->custom_menu(); ?></nav>
		<?php  echo $html->footnote;?>
		<div class="copyright">SEBALE LMS 2014 - <?php echo date("Y");?> Copyright &copy; All Rights Reserved.</div>
		
		<?php echo $OUTPUT->standard_footer_html();  ?>
	</div>
</footer>
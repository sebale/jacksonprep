<?php
/**
 * @package   theme_mosaic
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function theme_mosaic_page_init(moodle_page $page) {
    //$page->requires->jquery();
    //$page->requires->jquery_plugin('slider', 'theme_mosaic');
}

function theme_mosaic_process_css($css, $theme) {

    // Set the backgroundcolorleft image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_mosaic_set_logo($css, $logo);

    $leftbg = $theme->setting_file_url('leftbg', 'leftbg');
    $css = theme_mosaic_set_leftbg($css, $leftbg);

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_mosaic_set_customcss($css, $customcss);
	
    if (!empty($theme->settings->backgroundcolorleft)) {
        $backgroundcolorleft = $theme->settings->backgroundcolorleft;
    } else {
        $backgroundcolorleft = null;
    }
    $css = theme_mosaic_set_backgroundcolorleft($css, $backgroundcolorleft);

	
    return $css;
}
/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_mosaic_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_mosaic_set_backgroundcolorleft($css, $backgroundcolorleft) {
    $tag = '[[setting:backgroundcolorleft]]';
    $replacement = $backgroundcolorleft;
    if (is_null($replacement)) {
        $replacement = '#6FC091';
    }
    $css = str_replace($tag, $replacement, $css);
     
	$tag = '[[setting:backgroundcourse]]';
    $replacement = $backgroundcolorleft;
    if (is_null($replacement)) {
        $replacement = '#0a4478';
    }
	$rgba = theme_mosaic_hex2rgba($replacement,"0.6");
    $css = str_replace($tag, $rgba, $css);

    return $css;
}

/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */
function theme_mosaic_get_logo() {
	global $SITE, $CFG;
	
	$logo  = get_config('theme_mosaic', 'logo');
	if($logo){
		$path = $CFG->wwwroot.'/pluginfile.php/1/theme_mosaic/logo/'.context_system::instance()->id.'/'.$logo;
		
		return '<img src="'.$path.'" title="'.$SITE->shortname.'" alt="'.$SITE->shortname.'"/>';
	}else{
		$path = $CFG->wwwroot.'/theme/mosaic/pix/logo.png';
		return '<img src="'.$path.'" title="'.$SITE->shortname.'" alt="'.$SITE->shortname.'"/>';
	}
}
function theme_mosaic_set_logo($css, $logo) {
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

function theme_mosaic_generate_favicon($type = 'favicon') {
	global $SITE, $CFG, $OUTPUT;
    $path = '';
	
    if ($type == 'favicon'){
        $logo  = get_config('theme_mosaic', 'favicon');    
    } else {
        $logo  = get_config('theme_mosaic', 'faviconico');    
    }
    
	if($logo){
		$path = $CFG->wwwroot.'/pluginfile.php/1/theme_mosaic/'.$type.'/'.context_system::instance()->id.$logo;
	} else {
	    $path = $OUTPUT->favicon();
	}
    return $path;
}

function theme_mosaic_set_leftbg($css, $leftbg) {
    $tag = '[[setting:leftbg]]';
    $replacement = $leftbg;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

function theme_mosaic_hex2rgba($hex,$a) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgba = "rgba($r,$g,$b,$a)";/* array("r"=>$r, "g"=>$g, "b"=>$b) */
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgba; // returns an array with the rgb values
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_mosaic_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM) {
        $theme = theme_config::load('mosaic');
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}



/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * Do not add mosaic specific logic in here, child themes should be able to
 * rely on that function just by declaring settings with similar names.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_mosaic_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }

    $return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote text-center">'.format_text($page->theme->settings->footnote).'</div>';
    } 

	
	$return-> front_welcome_no_login_user = 'Welcome to SEBALE LMS';
    if (!empty($page->theme->settings-> front_welcome_no_login_user)) {
        $return-> front_welcome_no_login_user =$page->theme->settings-> front_welcome_no_login_user;
    }
    return $return;
}

/**
 * All theme functions should start with theme_mosaic_
 * @deprecated since 2.5.1
 */
function mosaic_process_css() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_mosaic_
 * @deprecated since 2.5.1
 */
function mosaic_set_logo() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_mosaic_
 * @deprecated since 2.5.1
 */
function mosaic_set_customcss() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

<?php
require_once($CFG->libdir. "/../course/renderer.php");
class theme_mosaic_core_course_renderer extends core_course_renderer{
		
	protected function coursecat_coursebox(coursecat_helper $chelper, $course, $additionalclasses = '') {
		global $CFG,$PAGE;				
			if($PAGE->pagelayout == "frontpage"){						
				if (!isset($this->strings->summary)) {
					$this->strings->summary = get_string('summary');
				}
				if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
					return '';
				}
				if ($course instanceof stdClass) {
					require_once($CFG->libdir. '/coursecatlib.php');
					$course = new course_in_list($course);
				}
				$content = '';
				$classes = trim('coursebox clearfix '. $additionalclasses);
				$nametag = 'h3';
						
				$course_bg = array_rand(array('#eebf46'=>null,'#4ec2cf'=>null,'#eb774a'=>null,'#6fc091'=>null));
						// .coursebox
				$content .= html_writer::start_tag('div', array(
					'class' => $classes,
					'style' => 'background-color:'. $course_bg.' ;',
					'data-courseid' => $course->id,
					'data-type' => self::COURSECAT_TYPE_COURSE,
				));
				$files = $course->get_course_overviewfiles();
				if(empty($files)){
					$effect = 'effect-romeo';
				}else{
					$effect = array_rand(array('effect-goliath-left'=>null)) ;
				}
				$content .= html_writer::start_tag('figure', array('class' => $effect));
						
						// display course overview files
				$contentimages = $contentfiles = '';
					foreach ($course->get_course_overviewfiles() as $file) {
						$isimage = $file->is_valid_image();
						$url = file_encode_url("$CFG->wwwroot/pluginfile.php",
								'/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
								$file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
						if ($isimage) {
							$contentimages .= html_writer::empty_tag('img', array('src' => $url));
						} 
					}
					$content .= $contentimages;
						
					$content .= html_writer::start_tag('figcaption');
					$content .= html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),"",array("class" => "course-href"));
					$content .= html_writer::start_tag('div', array('class' => 'info'));

						// course name
					$coursename = $chelper->get_course_formatted_name($course);				
					$content .= html_writer::tag($nametag, html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),$coursename), array('class' => 'coursename'));
						// If we display course in collapsed form but the course has summary or course contacts, display the link to the info page.

						// display course category if necessary (for example in search results)
					require_once($CFG->libdir. '/coursecatlib.php');
					if ($cat = coursecat::get($course->category, IGNORE_MISSING)) {
						$content .= html_writer::start_tag('div', array('class' => 'coursecat'));
						$content .= get_string('category').': '.
								html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $cat->id)),
										$cat->get_formatted_name(), array('class' => $cat->visible ? '' : 'dimmed'));
						$content .= html_writer::end_tag('div'); // .coursecat
					}
						
					$content .= html_writer::end_tag('div'); // .info
					$content .= $this->coursecat_coursebox_content($chelper, $course);
						
					$content .= html_writer::end_tag('figcaption'); // .content

					$content .= html_writer::end_tag('figure');

					$content .= html_writer::end_tag('div'); // .coursebox
					return $content;
				 }else{						 
					if (!isset($this->strings->summary)) {
						$this->strings->summary = get_string('summary');
					}
					if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
						return '';
					}
					if ($course instanceof stdClass) {
						require_once($CFG->libdir. '/coursecatlib.php');
						$course = new course_in_list($course);
					}
					$content = '';
					$classes = trim('coursebox clearfix '. $additionalclasses);
					if ($chelper->get_show_courses() >= self::COURSECAT_SHOW_COURSES_EXPANDED) {
						$nametag = 'h3';
					} else {
						$classes .= ' collapsed';
						$nametag = 'div';
					}

						// .coursebox
					$content .= html_writer::start_tag('div', array(
						'class' => $classes,
						'data-courseid' => $course->id,
						'data-type' => self::COURSECAT_TYPE_COURSE,
					));

					$content .= html_writer::start_tag('div', array('class' => 'info'));

						// course name
					$coursename = $chelper->get_course_formatted_name($course);
					$coursenamelink = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),
														$coursename, array('class' => $course->visible ? '' : 'dimmed'));
					$content .= html_writer::tag($nametag, $coursenamelink, array('class' => 'coursename'));
					// If we display course in collapsed form but the course has summary or course contacts, display the link to the info page.
					$content .= html_writer::start_tag('div', array('class' => 'moreinfo'));
					if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
						if ($course->has_summary() || $course->has_course_contacts() || $course->has_course_overviewfiles()) {
							$url = new moodle_url('/course/info.php', array('id' => $course->id));
							$image = html_writer::empty_tag('img', array('src' => $this->output->pix_url('i/info'),
								'alt' => $this->strings->summary));
							$content .= html_writer::link($url, $image, array('title' => $this->strings->summary));
							// Make sure JS file to expand course content is included.
							$this->coursecat_include_js();
						}
					}
					$content .= html_writer::end_tag('div'); // .moreinfo

						// print enrolmenticons
						if ($icons = enrol_get_course_info_icons($course)) {
							$content .= html_writer::start_tag('div', array('class' => 'enrolmenticons'));
							foreach ($icons as $pix_icon) {
								$content .= $this->render($pix_icon);
							}
							$content .= html_writer::end_tag('div'); // .enrolmenticons
						}

						$content .= html_writer::end_tag('div'); // .info

						$content .= html_writer::start_tag('div', array('class' => 'content'));
						$content .= $this->coursecat_coursebox_content($chelper, $course);
						$content .= html_writer::end_tag('div'); // .content

						$content .= html_writer::end_tag('div'); // .coursebox
						return $content;
					 }
			}
	
	 protected function coursecat_coursebox_content(coursecat_helper $chelper, $course) {
        global $CFG,$PAGE;
		if($PAGE->pagelayout == "frontpage"){		
				if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
					return '';
				}
				if ($course instanceof stdClass) {
					require_once($CFG->libdir. '/coursecatlib.php');
					$course = new course_in_list($course);
				}
				$content = '';

				// display course summary
				if ($course->has_summary()) {
					$content .= $chelper->get_course_formatted_summary($course,
							array('overflowdiv' => true, 'noclean' => true, 'para' => false));
				}
				return $content;
		}else{
			if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
				return '';
			}
			if ($course instanceof stdClass) {
				require_once($CFG->libdir. '/coursecatlib.php');
				$course = new course_in_list($course);
			}
			$content = '';
				// display course summary
			if ($course->has_summary()) {
				$content .= html_writer::start_tag('div', array('class' => 'summary'));
				$content .= $chelper->get_course_formatted_summary($course,
						array('overflowdiv' => true, 'noclean' => true, 'para' => false));
				$content .= html_writer::end_tag('div'); // .summary
			}
				// display course overview files
			$contentimages = $contentfiles = '';
			foreach ($course->get_course_overviewfiles() as $file) {
				$isimage = $file->is_valid_image();
				$url = file_encode_url("$CFG->wwwroot/pluginfile.php",
						'/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
						$file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
				if ($isimage) {
					$contentimages .= html_writer::tag('div',
							html_writer::empty_tag('img', array('src' => $url)),
							array('class' => 'courseimage'));
				} else {
					$image = $this->output->pix_icon(file_file_icon($file, 24), $file->get_filename(), 'moodle');
					$filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
							html_writer::tag('span', $file->get_filename(), array('class' => 'fp-filename'));
					$contentfiles .= html_writer::tag('span',
							html_writer::link($url, $filename),
							array('class' => 'coursefile fp-filename-icon'));
				}
			}
			$content .= $contentimages. $contentfiles;

				// display course contacts. See course_in_list::get_course_contacts()
			if ($course->has_course_contacts()) {
				$content .= html_writer::start_tag('ul', array('class' => 'teachers'));
				foreach ($course->get_course_contacts() as $userid => $coursecontact) {
					$name = $coursecontact['rolename'].': '.
							html_writer::link(new moodle_url('/user/view.php',
									array('id' => $userid, 'course' => SITEID)),
								$coursecontact['username']);
					$content .= html_writer::tag('li', $name);
				}
				$content .= html_writer::end_tag('ul'); // .teachers
			}

				// display course category if necessary (for example in search results)
			if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT) {
				require_once($CFG->libdir. '/coursecatlib.php');
				if ($cat = coursecat::get($course->category, IGNORE_MISSING)) {
					$content .= html_writer::start_tag('div', array('class' => 'coursecat'));
					$content .= get_string('category').': '.
							html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $cat->id)),
									$cat->get_formatted_name(), array('class' => $cat->visible ? '' : 'dimmed'));
					$content .= html_writer::end_tag('div'); // .coursecat
				}
			}
			return $content;			
	}
	}
	
	public function frontpage_available_courses() {
        global $CFG, $OUTPUT;			
        require_once($CFG->libdir. '/coursecatlib.php');

			$chelper = new coursecat_helper();
			$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
					set_courses_display_options(array(
						'recursive' => true,
						'limit' => $CFG->frontpagecourselimit));
			
			$chelper->set_attributes(array('class' => 'frontpage-course-list-all'));
			$courses = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
			$totalcount = coursecat::get(0)->get_courses_count($chelper->get_courses_display_options());
			if (!$totalcount && !$this->page->user_is_editing() && has_capability('moodle/course:create', context_system::instance())) {
				// Print link to create a new course, for the 1st available category.
				return $this->add_new_course_button();
			}elseif(!$totalcount){
				return html_writer::tag('div',get_string('no_course_info', 'theme_mosaic'),array('class'=>'alert alert-warning'));
			}elseif($totalcount){			
				//$output = html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab2','type' => 'radio','name' =>'tabs'));
				//$output .= html_writer::tag('label',$OUTPUT->heading(get_string('availablecourses')), array('for' => 'tab2'));
					
					static $count = 0;
					$inputid = 'shortsearchbox_all';
					$inputsize = 12;
					
					$category = coursecat::make_categories_list();
					$output = html_writer::start_tag('form', array('id' => 'all-courses', 'class' => 'coursesearch', 'method' => 'get'));
					$output .= html_writer::start_tag('fieldset', array('class' => 'coursesearchbox invisiblefieldset clearfix'));
					$output .= html_writer::empty_tag('label', array('class' => 'fa  fa-search', 'onclick' => 'ajax_search("all","search");',));		
					$output .= html_writer::end_tag('label');
					$output .= html_writer::select($category, '_all_courses', null,'All categories',array('onchange' => 'ajax_search("all","cat");'));
					$output .= html_writer::empty_tag('input', array('type' => 'text', 'placeholder' => get_string('search') , 'id' => $inputid,
						'size' => $inputsize, 'name' => 'search', 'class' => 'shortsearchbox'));
					$output .= html_writer::end_tag('fieldset');
					$output .= html_writer::end_tag('form');
				$output .= html_writer::start_tag('div', array('style' => 'overflow:hidden;'));
				$output .= $this->coursecat_courses($chelper, $courses, $totalcount);
				$output .= html_writer::end_tag('div');
			
				$chelper2 = new coursecat_helper();
				$chelper2->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->set_courses_display_options(array('recursive' => true));
				
				$chelper2->set_attributes(array('class' => 'frontpage-course-list-all2'));
				$courses = coursecat::get(0)->get_courses($chelper2->get_courses_display_options());
				
				if(count($courses) > $CFG->frontpagecourselimit){
					$output .= html_writer::start_tag('div', array('class' => 'paging-morelink'));
					$output .= html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("all",1);',));			
					$output .= html_writer::end_tag('div');					
				}
			}
			return $output;
		}
    
	
	 
	public function frontpage_my_courses() {
		global $USER, $CFG, $DB, $OUTPUT;
		require_once($CFG->libdir. '/coursecatlib.php');
		
			if (!isloggedin() or isguestuser()) {
				return '';
			}

			$output = '';
				
			static $count = 0;
			$inputid = 'shortsearchbox_my';
			$inputsize = 12;
				
			$category = coursecat::make_categories_list();
			$strsearchcourses= get_string("searchcourses");
			$searchurl = new moodle_url('/course/search.php');
			$output = html_writer::start_tag('form', array('id' => 'my-courses', 'class' => 'coursesearch', 'method' => 'get'));
			$output .= html_writer::start_tag('fieldset', array('class' => 'coursesearchbox invisiblefieldset clearfix'));
			$output .= html_writer::empty_tag('label', array( 'class' => 'fa  fa-search','onclick' => 'ajax_search("my","search");'));		
			$output .= html_writer::end_tag('label');							
			$output .= html_writer::select($category, '_my_courses', null,'All categories',array('onchange' => 'ajax_search("my","cat");'));
			$output .= html_writer::empty_tag('input', array('type' => 'text', 'placeholder' => get_string('search') , 'id' => $inputid,
				'size' => $inputsize, 'name' => 'search', 'class' => 'shortsearchbox'));
			$output .= html_writer::end_tag('fieldset');
			$output .= html_writer::end_tag('form');
					
			if (!empty($CFG->navsortmycoursessort)) {
				// sort courses the same as in navigation menu
				$sortorder = 'visible DESC,'. $CFG->navsortmycoursessort.' ASC';
			} else {
				$sortorder = 'visible DESC,sortorder ASC';
			}
			$courses  = enrol_get_my_courses('summary, summaryformat', $sortorder);
			if(count($courses) == 0){
				return html_writer::tag('div',get_string('no_course_info', 'theme_mosaic'),array('class'=>'alert alert-warning'));
			}
			$rhosts   = array();
			$rcourses = array();
			if (!empty($CFG->mnet_dispatcher_mode) && $CFG->mnet_dispatcher_mode==='strict') {
				$rcourses = get_my_remotecourses($USER->id);
				$rhosts   = get_my_remotehosts();
			}

				if (!empty($courses) || !empty($rcourses) || !empty($rhosts)) {

					$chelper = new coursecat_helper();
					if (count($courses) > $CFG->frontpagecourselimit) {
						// There are more enrolled courses than we can display, display link to 'My courses'.
						$totalcount = count($courses);
						$courses = array_slice($courses, 0, $CFG->frontpagecourselimit, true);
						
					} else {
						// All enrolled courses are displayed, display link to 'All courses' if there are more courses in system.
						
						$totalcount = $DB->count_records('course') - 1;
					}
					$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
							set_attributes(array('class' => 'frontpage-course-list-enrolled'));
					$output .= html_writer::start_tag('div', array('style' => 'overflow:hidden;'));
					$output .= $this->coursecat_courses($chelper, $courses, $totalcount);
					$output .= html_writer::end_tag('div');
						// MNET
					if (!empty($rcourses)) {
						// at the IDP, we know of all the remote courses
						$output .= html_writer::start_tag('div', array('class' => 'courses'));
						foreach ($rcourses as $course) {
							$output .= $this->frontpage_remote_course($course);
						}
						$output .= html_writer::end_tag('div'); // .courses
					} elseif (!empty($rhosts)) {
						// non-IDP, we know of all the remote servers, but not courses
						$output .= html_writer::start_tag('div', array('class' => 'courses'));
						foreach ($rhosts as $host) {
							$output .= $this->frontpage_remote_host($host);
						}
						$output .= html_writer::end_tag('div'); // .courses
					}
			}
					
				if((count(enrol_get_my_courses('summary, summaryformat')) - $CFG->frontpagecourselimit) >= $CFG->frontpagecourselimit){
					$output .= html_writer::start_tag('div', array('class' => 'paging-morelink'));
					$output .= html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("my",1);',));			
					$output .= html_writer::end_tag('div');					
				}
				return $output;
		}
    
	
	 public function frontpage_combo_list() {
		global $CFG,$OUTPUT;
		//echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab3','type' => 'radio','name' =>'tabs'));
		//echo html_writer::tag('label',$OUTPUT->heading(get_string('courses')), array('for' => 'tab3'));
        require_once($CFG->libdir. '/coursecatlib.php');
        $chelper = new coursecat_helper();
        $chelper->set_subcat_depth($CFG->maxcategorydepth)->
            set_categories_display_options(array(
                'limit' => $CFG->coursesperpage,
                'viewmoreurl' => new moodle_url('/course/index.php',
                        array('browse' => 'categories', 'page' => 1))
            ))->
            set_courses_display_options(array(
                'limit' => $CFG->coursesperpage,
                'viewmoreurl' => new moodle_url('/course/index.php',
                        array('browse' => 'courses', 'page' => 1))
            ))->
            set_attributes(array('class' => 'frontpage-category-combo'));
        echo $this->coursecat_tree($chelper, coursecat::get(0));
        return " ";
    }
	
	 public function frontpage_categories_list() {
		global $CFG,$OUTPUT;
		//echo html_writer::start_tag('input', array('class' => 'for-tab','id' => 'tab4','type' => 'radio','name' =>'tabs'));
		//echo html_writer::tag('label',$OUTPUT->heading(get_string('categories')), array('for' => 'tab4'));
        require_once($CFG->libdir. '/coursecatlib.php');
        $chelper = new coursecat_helper();
        $chelper->set_subcat_depth($CFG->maxcategorydepth)->
                set_show_courses(self::COURSECAT_SHOW_COURSES_COUNT)->
                set_categories_display_options(array(
                    'limit' => $CFG->coursesperpage,
                    'viewmoreurl' => new moodle_url('/course/index.php',
                            array('browse' => 'categories', 'page' => 1))
                ))->
                set_attributes(array('class' => 'frontpage-category-names'));
        return $this->coursecat_tree($chelper, coursecat::get(0));
    }

	function course_search_form($value = '', $format = 'plain') {
				global $CFG,$PAGE;				
				require_once($CFG->libdir. '/coursecatlib.php');
					if($PAGE->pagelayout == "frontpage"){
						return ;
		}else{
			        static $count = 0;
					$formid = 'coursesearch';
					if ((++$count) > 1) {
						$formid .= $count;
					}

					switch ($format) {
						case 'navbar' :
							$formid = 'coursesearchnavbar';
							$inputid = 'navsearchbox';
							$inputsize = 20;
							break;
						case 'short' :
							$inputid = 'shortsearchbox';
							$inputsize = 12;
							break;
						default :
							$inputid = 'coursesearchbox';
							$inputsize = 30;
					}

					$strsearchcourses= get_string("searchcourses");
					$searchurl = new moodle_url('/course/search.php');

					$output = html_writer::start_tag('form', array('id' => $formid, 'action' => $searchurl, 'method' => 'get'));
					$output .= html_writer::start_tag('fieldset', array('class' => 'coursesearchbox invisiblefieldset'));
					$output .= html_writer::tag('label', $strsearchcourses.': ', array('for' => $inputid));
					$output .= html_writer::empty_tag('input', array('type' => 'text', 'id' => $inputid,
						'size' => $inputsize, 'name' => 'search', 'value' => s($value)));
					$output .= html_writer::empty_tag('input', array('type' => 'submit',
						'value' => get_string('go')));
					$output .= html_writer::end_tag('fieldset');
					$output .= html_writer::end_tag('form');

					return $output;
				}
		}
	
	
	public function frontpage_available_courses_search($category, $search) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
				$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
						set_courses_display_options(array(
							'recursive' => true,
							'viewmoreurl' => new moodle_url('/course/index.php'),
							'viewmoretext' => new lang_string('fulllistofcourses')));
				$chelper->set_attributes(array('class' => 'frontpage-course-list-all'));
				if($category>=0){
					$courses_all = coursecat::get($category)->get_courses($chelper->get_courses_display_options());
				}else{
					$courses_all = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
				}
				
				foreach($courses_all as $course_all){
					 if($search == '' || strripos($course_all->fullname, $search) !== false || strripos($course_all->shortname, $search) !== false){
						$courses[$course_all->id] = $course_all; 
					 }	 
				 }
				 if(count($courses) == 0) {
					 $out = html_writer::tag('div',html_writer::tag('div',get_string('not_found_course', 'theme_mosaic')."'".$search."'", array('class' => 'alert alert-warning')), array('class' => 'courses frontpage-course-list-all'));
				 }else{
				$out = html_writer::start_tag('div', array('style' => 'overflow:hidden;'));
				$out .= $this->coursecat_courses($chelper, $courses);
				$out .= html_writer::end_tag('div');
					
				 }
				
				
				echo $out;
	}
	public function frontpage_my_courses_search($category, $search) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
				$courses_all  = enrol_get_my_courses('summary, summaryformat');
				 foreach($courses_all as $course_all){
					 if(($category == $course_all->category || $category == '') && ($search == '' ||strripos($course_all->fullname, $search) !== false || strripos($course_all->shortname, $search) !== false)){
						$courses[$course_all->id] = $course_all; 
					 }	 
				 }
				 
				if(count($courses) == 0) echo html_writer::tag('div',get_string('no_course_info', 'theme_mosaic'),array('class'=>'courses frontpage-course-list-enrolled alert alert-warning'));
				$chelper = new coursecat_helper();
				$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
						set_courses_display_options(array(
							'recursive' => true,
							'limit' => $CFG->frontpagecourselimit,
							'viewmoreurl' => new moodle_url('/course/index.php'),
							'viewmoretext' => new lang_string('fulllistofcourses')));
				$chelper->set_attributes(array('class' => 'frontpage-course-list-enrolled'));
				
				$out = html_writer::start_tag('div', array('style' => 'overflow:hidden;'));
				$out .= $this->coursecat_courses($chelper, $courses);
				$out .= html_writer::end_tag('div');
				
				echo $out;
	} 
	public function frontpage_available_courses_get_courses($step) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
			$chelper->set_attributes(array('id' => 'temp'));
			$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->set_courses_display_options(array('recursive' => true));
			$courses = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
			$courses = array_splice($courses, ($CFG->frontpagecourselimit * $step));
			$output = '';
			$count = count($courses);
			
				if(count($courses) > $CFG->frontpagecourselimit){
					while(count($courses) > $CFG->frontpagecourselimit){
						array_pop($courses);
					}	
				}
			$output .= $this->coursecat_courses($chelper, $courses);
			if($count > $CFG->frontpagecourselimit){
				echo html_writer::start_tag('div', array('id' => 'temp2')).html_writer::start_tag('div', array('class' => 'paging-morelink')).html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("all", '.++$step.');',)).html_writer::end_tag('div').html_writer::end_tag('div');		
			}
			echo $output;
	} 
	public function frontpage_my_courses_get_courses($step) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
			$chelper->set_attributes(array('id' => 'temp'));
			$courses = enrol_get_my_courses('summary, summaryformat');
			$courses = array_splice($courses, ($CFG->frontpagecourselimit * $step));
			$output = '';	
			$count = count($courses);
			
				if(count($courses) > $CFG->frontpagecourselimit){	
					while(count($courses) > $CFG->frontpagecourselimit){
						array_pop($courses);
					}	 			
				}
			$output .= $this->coursecat_courses($chelper, $courses);
			echo $output;
			if($count > $CFG->frontpagecourselimit){
				echo html_writer::start_tag('div', array('id' => 'temp2')).html_writer::start_tag('div', array('class' => 'paging-morelink')).html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("my", '.++$step.');',)).html_writer::end_tag('div').html_writer::end_tag('div');		
			}
	}

}

?>
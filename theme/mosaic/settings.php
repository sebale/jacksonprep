<?php
/**
 * @package   theme_mosaic
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    // Logo file setting.
    $name = 'theme_mosaic/logo';
    $title = get_string('logo','theme_mosaic');
    $description = get_string('logodesc', 'theme_mosaic');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
    
    $name = 'theme_mosaic/faviconico';
    $title = get_string('faviconico','theme_mosaic');
    $description = get_string('faviconicodesc', 'theme_mosaic');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'faviconico');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
    
    $name = 'theme_mosaic/favicon';
    $title = get_string('favicon','theme_mosaic');
    $description = get_string('favicondesc', 'theme_mosaic');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    $name = 'theme_mosaic/leftbg';
    $title = get_string('leftbg','theme_mosaic');
    $description = get_string('leftbg_desc', 'theme_mosaic');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'leftbg');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    $name = 'theme_mosaic/backgroundcolorleft';
    $title = get_string('backgroundcolorleft', 'theme_mosaic');
    $description = get_string('backgroundcolorleft_desc', 'theme_mosaic');
	$default = '#6FC091';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
    // Custom CSS file.
    $name = 'theme_mosaic/customcss';
    $title = get_string('customcss', 'theme_mosaic');
    $description = get_string('customcssdesc', 'theme_mosaic');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Footnote setting.
    $name = 'theme_mosaic/footnote';
    $title = get_string('footnote', 'theme_mosaic');
    $description = get_string('footnotedesc', 'theme_mosaic');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
	
	// Custom front Welcome to no login user.
    $name = 'theme_mosaic/front_welcome_no_login_user';
    $title =get_string('front_welcome_no_login_user', 'theme_mosaic');
    $description = get_string('front_welcome_no_login_user_desc', 'theme_mosaic');
    $default = 'Welcome to SEBALE LMS';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
}

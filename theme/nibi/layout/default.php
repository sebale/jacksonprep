<?php
/**
 * @package   theme_nibi
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$maincss = ($hassidepre and $hassidepost) ? 'bothside' : '';
$maincss = ($hassidepre and !$hassidepost) ? 'leftside' : $maincss;
$maincss = (!$hassidepre and $hassidepost) ? 'rightside' : $maincss;

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_nibi_generate_favicon('faviconico'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_nibi_generate_favicon('favicon'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_nibi_generate_favicon('favicon'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_nibi_generate_favicon('favicon'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/nibi/layout/js/script.js"></script>
</head>

<body <?php  echo  str_replace("dir-ltr", "",$OUTPUT->body_attributes()); ?>>
	<?php include("includes/header.php"); ?>
	<section class="head">
		<div class="inner clearfix">
			<div class="pathway">
				<?php echo $html->heading; ?>
				<div id="page-navbar" class="clearfix">
					<nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
					<div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
				</div>
				<div id="course-header">
					<?php echo $OUTPUT->course_header(); ?>
				</div>
			</div>
		</div>
	</section>
	
	<section id="page" class="page inner clearfix">
		<?php echo ($hassidepre) ? $OUTPUT->blocks('side-pre', 'side-pre') : ''; ?>
		<div class="content clearfix <?php echo $maincss; ?>"  id="region-main">
			 <?php
				echo $OUTPUT->course_content_header();
				echo $OUTPUT->main_content();
				echo $OUTPUT->course_content_footer();
			?>
		</div>
		<?php echo ($hassidepost) ? $OUTPUT->blocks('side-post', 'side-post') : ''; ?>
	</section>
    <?php include('includes/footer.php'); ?>
</body>
</html>
$(window).ready(function(){
	if(Y.one(".topics") != null){		
		$('.topics .tabs li').click(function(){
			var section = '#'+$($($($(this).parent()).parent()).parent()).attr('id');
			var mod_name = $(this).attr('mod_name');
			
			$(section+' .tabs').removeClass('active');
			$(section+' .dropdown').toggleClass('active');
			
			if(mod_name == 'all'){
				$(section+' .img-text li').removeClass('hidden');
			}else{
				$(section+' .img-text li').addClass('hidden');
				$(section+' .img-text .'+mod_name).removeClass('hidden');
			}
		});
			
			
			
 		$(window).click(function(){		
				$('.topics .dropdown').removeClass('active');
				$('.topics .section.tabs').removeClass('active');
		}); 
		$('.topics .dropdown').click(function(event){
			if(!$(this).hasClass('active')){
				$('.topics .dropdown').removeClass('active');
				$('.topics .tabs').removeClass('active');
			}
				var section = '#'+$($($(this).parent()).parent()).attr('id');
				$(section+' .tabs').toggleClass('active');
				$(this).toggleClass('active');
				event.stopPropagation();
		}); 
		

		
		$('.topics .show-activity').click(function(){
			var section = '#'+$($($(this).parent()).parent()).attr('id');
			
			if($(this).hasClass('fa-angle-down')){
				$(section+' .img-text li, '+section+' .summary').removeClass('hidden');
			}else{
				$(section+' .img-text li, '+section+' .summary').addClass('hidden');
			}		
			
			$(this).toggleClass('fa-angle-down');
			$(this).toggleClass('fa-angle-up');
		});

		
		$('.topics .show-activity').trigger('click');
		$('.topics .current .show-activity').trigger('click');
	}
});

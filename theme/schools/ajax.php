<?php
require_once('../../config.php');

$action = required_param('action',PARAM_TEXT);

if($action == 'add-bookmark'){
	$name = required_param('name',PARAM_TEXT);
	$userid = required_param('userid',PARAM_INT);
	$url = required_param('url',PARAM_TEXT);

	$record = new stdClass();
	$record->user = $userid;
	$record->name = $name;
	$record->url = $url;
	$record->timecreate = time();

	$DB->insert_record('schools_quick_links',$record);
}elseif($action == 'delete-bookmark'){
	$id = required_param('id',PARAM_TEXT);
	
	$DB->delete_records('schools_quick_links', array('id'=>$id));
}

?>
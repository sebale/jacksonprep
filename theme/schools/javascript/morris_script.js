$(window).ready(function(){
	$('.block.block_schools_statistic .line-control label').click(function(){
		$('#line-month, #line-year, #line-day').css('display','none');
		$('#'+$(this).attr('for')).css('display','block');
		$('.line-control label').removeClass('active');
		$(this).addClass('active');
	});
	setTimeout(function(){$('.block.block_schools_statistic .line-control label:first-child').click();},100);
});
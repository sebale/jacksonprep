function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}
$(window).ready(function(){
	if(Y.one(".course-content") != null){
		$('.course-content .show-activity').click(function(){
			var section = '#'+$($($(this).parent()).parent()).attr('id');
			
			if($(this).hasClass('fa-angle-down')){
				$('.content '+section+' .img-text, '+section+' .summary').removeClass('hidden');
				$('.content '+section+' .img-text, '+section+' .content').addClass('open');
			}else{
				$('.content '+section+' .img-text, '+section+' .summary').addClass('hidden');
				$('.content '+section+' .img-text, '+section+' .content').removeClass('open');
			}		
			
			$(this).toggleClass('fa-angle-down');
			$(this).toggleClass('fa-angle-up');
		});
		
		$('.course-content .show-activity').trigger('click');
		$('.course-content .section:first-child .show-activity').trigger('click');
		$('.course-content .current .show-activity').trigger('click');
	}
	if(Y.one(".page > .content .block_course_overview > .content .course_grades .course-content li .more") != null){
		$('.page > .content .block_course_overview > .content .course_grades .course-content li .more').click(function(){
			
 			if($(this).hasClass('fa-angle-down')){				
				$($($($(this).parent()).parent()).parent()).addClass('open');
			}else{
				$($($($(this).parent()).parent()).parent()).removeClass('open');
			} 		
			
			$(this).toggleClass('fa-angle-down');
			$(this).toggleClass('fa-angle-up');
		});
	}
	if(Y.one(".page > .content .block_course_overview > .content .list .course-content li .more") != null){
		$('.page > .content .block_course_overview > .content .list .course-content li .more').click(function(){
			
 			if($(this).hasClass('fa-angle-down')){				
				$($($($(this).parent()).parent()).parent()).addClass('open');
			}else{
				$($($($(this).parent()).parent()).parent()).removeClass('open');
			} 		
			
			$(this).toggleClass('fa-angle-down');
			$(this).toggleClass('fa-angle-up');
		});
	}
	
	$(' .page > .content .dashboard-menu.table_nav label').click(function(){
		$(' .page > .content .dashboard-menu.table_nav label').removeClass('active');
		$(this).addClass('active');
		$(' .page > .content .block_course_overview > .content .course-wrapper').removeClass('active');
		$(' .page > .content .block_course_overview > .content .course-wrapper.'+$(this).attr('for')).addClass('active');
		$('.page > .content ul.dashboard li.action').removeClass('active');
		$('.page > .content ul.dashboard li.action[for="'+$(this).attr('for')+'"]').addClass('active');
	});
	$('.page > .content .dashboard-menu.table_nav label:first-child').click();

	$('.page > .content ul.dashboard li.action').click(function(){
		var section = $(this).attr('for');
		$('.page > .content .dashboard-menu.table_nav label[for="'+section+'"]').click();
	});
	
	if(getURLParameter('report_card')==1 || getURLParameter('grades_boock')==1){
		$('.page > .content .dashboard-menu.table_nav label:last-child').click();
	}
	
	$('#toggle-menu').click(function(){
		$(this).toggleClass('fa-angle-double-down fa-angle-double-up');
		if($('body > .page').hasClass('menu-open')){
			$('html, body').animate({ scrollTop: 0 }, 2000);			
		}
		$('body > .page ').toggleClass('menu-open');
	});

	$('#bookmark-form').submit(function(e){		
		e.preventDefault();
		var name = jQuery('#bookmark-name').val();
		var userid = jQuery('#bookmark-userid').val();
		if(name == ''){
			jQuery('#bookmark-name').addClass('req');
		}else{
				$.ajax({
				   type: "POST",
				   url: window.M.cfg.wwwroot+"/theme/schools/ajax.php",
				   data: "action=add-bookmark&name="+name+"&userid="+userid+'&url='+window.location,
				   success: function(msg){
					   jQuery('.bookmark > a').addClass('active');
					   jQuery('.dropdown.bookmark').removeClass('open');
				   }
				 });
		}
	});
	$('.bookmark-delete').click(function(e){
		var id = jQuery(this).attr('link_id');
		var element = $(this).parent();
		$.ajax({
		   type: "POST",
		   url: window.M.cfg.wwwroot+"/theme/schools/ajax.php",
		   data: "action=delete-bookmark&id="+id,
		   success: function(msg){
			  $(element).remove();
		   }
		 });
	});
	
	if($('#block-region-content .block_course_overview > .content div').length < 1){
		$('#block-region-content .block_course_overview').css('display','none');
	}
	
	$('.gradeparent, .report > div > .no-overflow').scroll(function(event){
		event.stopImmediatePropagation();
		var scrollLeft = $('.gradeparent, .report > div > .no-overflow').scrollLeft();
		var scrollTop = $('.gradeparent, .report > div > .no-overflow').scrollTop();
		
			//$('.gradeparent .floater.sideonly.heading, .report > div > .no-overflow .floater.sideonly.heading').css('top','62px');
		if(scrollTop>63){
			$('.gradeparent .floater.heading, .report > div > .no-overflow .floater.heading').css({'top':scrollTop+'px','display':'block'});
			$('.gradeparent .floater.sideonly.heading, .report > div > .no-overflow .floater.sideonly.heading').css('left',0);
		}else{
			$('.gradeparent .floater.heading, .report > div > .no-overflow .floater.heading').css('display','none');
		}
		
		if(scrollLeft > 0){
			$('.gradeparent .floater.sideonly, .report > div > .no-overflow .floater.sideonly').css({'left':scrollLeft+'px','display':'block'});
		}else{
			$('.gradeparent .floater.sideonly, .report > div > .no-overflow .floater.sideonly').css('display','none');
		}
		
	});
	setTimeout(function(){
		$('.gradeparent, .report > div > .no-overflow').trigger('scroll');
	},4000);
	
	$(window).scroll(function(e){
		setTimeout(function(){
			$('.gradeparent, .report > div > .no-overflow').trigger('scroll');
		},0);
	});
	
});

$('.section-modchooser-link').click(function(){
	setTimeout(function(){
		$('.choosercontainer #chooseform .moduletypetitle label').click(function(){
			$('.choosercontainer #chooseform .module_resources, .choosercontainer #chooseform .module_activities').css('display','none');
			$('.choosercontainer #chooseform .moduletypetitle').removeClass('active');
			$('.choosercontainer #chooseform .'+$(this).attr('for') ).css('display','block');
			$($(this).parent()).addClass('active');
		});
		$('.choosercontainer #chooseform .moduletypetitle:first-child label').click();
	},500);
	
});


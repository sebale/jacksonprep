<?php
/**
 * @package   theme_schools
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>schools</h2>
<p><img class=img-polaroid src="schools/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>schools is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
<h3>Parents</h3>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
<h3>Theme Credits</h3>
<p>Authors: Bas Brands, David Scotson, Mary Evans<br>
Contact: bas@sonsbeekmedia.nl<br>
Website: <a href="http://www.basbrands.nl">www.basbrands.nl</a>
</p>
<h3>Report a bug:</h3>
<p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
<h3>More information</h3>
<p><a href="schools/README.txt">How to copy and customise this theme.</a></p>
</div></div>';

$string['configtitle'] = 'Schools';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.<br>
If the height of your logo is more than 75px add the following CSS rule to the Custom CSS box below.<br>
a.logo {height: 100px;} or whatever height in pixels the logo is.';

$string['pluginname'] = 'Schools';
$string['choosereadme'] = '<p>More is a theme that allows you to easily customise Moodle\'s look and feel directly from the web interface.</p>
<p>Visit the admin settings to change colors, add an image as a background, add your logo and more.</p>';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['welcome_user'] = 'Welcome text';
$string['welcome_user_desc'] = 'The Welcome text to user (header)';
$string['footnote'] = 'Footer text';
$string['footnote_desc'] = 'Footer text in right side';

$string['backgrounduserblock'] = 'Background user menu';
$string['backgrounduserblock_desc'] = 'User menu, right-top';

$string['backgroundcourseprogress'] = 'Background course progress';
$string['backgroundcourseprogress_desc'] = 'Course progress bar, right, after user menu';

$string['backgroundleftside'] = 'Background left side';
$string['backgroundleftside_desc'] = '';

$string['backgroundrightside'] = 'Background right side';
$string['backgroundrightside_desc'] = '';

$string['no_course_info'] = 'No course information to show';
$string['not_found_course'] = 'No courses were found with the words ';
$string['course_progress'] = 'Course progress: ';
$string['grade'] = 'Avg grade: ';


$string['region-side-course'] = 'Region side course';
$string['region-side-right'] = 'Region side right';
$string['my_courses'] = 'My Courses';
$string['all_courses'] = 'Available Courses';
$string['grades_boock'] = 'Gradebook';


$string['login_width'] = 'Or login width';
$string['sign_in'] = 'Sign In';
$string['sign_up'] = 'Sign Up';
$string['login'] = 'Login';

$string['start_date'] = 'Start date: ';
$string['completed_date'] = 'Due date: ';
$string['curent_prodress'] = 'Current progress: {$a} activities';
$string['report_card'] = 'Report card';


$string['grade_date'] = 'Date: ';
$string['grade_type'] = 'Type: ';

$string['gpa'] = 'GPA';
$string['ass_due_today'] = 'Assignments due today: ';
$string['quiz_due_today'] = 'Tests scheduled today: ';

$string['no_quick_links'] = 'Empty';

$string['enrol'] = 'Enroll';
$string['myteachcourses'] = 'Courses I Teach';
$string['users_enroled'] = ' Students enrolled';
$string['modules'] = ' Modules';
$string['quizzes'] = ' Quizzes';

$string['enroled_users'] = 'Enrolled students';
$string['subm_need_grading'] = 'Assignments need grading';
$string['upcoming_events'] = 'Upcoming events';
$string['inactive_users'] = 'Inactive students';
$string['failuere_users'] = 'Failing students';

$string['create_new_course'] = 'Create New Course';

$string['average'] = 'Average score';

$string['entersearch'] = 'Please enter query to search';

$string['enrol_courses'] = 'Enrolled courses';
$string['completed_courses'] = 'Finished courses';

$string['missing_assignments'] = 'Missing assignments: {$a}';
$string['completed_activities'] = 'Completed activities: {$a}';
$string['curent_grade'] = 'Current grade: {$a}';
$string['course_status'] = 'Status: {$a}';
$string['final_grade'] = 'Final grade: {$a}';
$string['mod_time_completed'] = 'Graded: {$a}';
$string['mod_grade'] = 'Grade: {$a}';
$string['inprogress'] = 'In progress';
$string['completed'] = 'Finished';
$string['assign_missing'] = 'Missing Assignments: {$a}';
$string['activity_missing'] = 'Missing activities: {$a}';
$string['mod_time_submited'] = 'Last submit: {$a}';

$string['faviconico'] = 'Favicon (.ico): ';
$string['faviconicodesc'] = 'Please upload your custom favicon file (.ico)';
$string['favicon'] = 'Favicon (.png)';
$string['favicondesc'] = 'Please upload your custom favicon file (.png)';

<?php
include('../../../config.php');
require_once('../../../auth/googleoauth2/lib.php');

/* if($USER->id != 0)
	redirect(new moodle_url('/my/')); */

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->add_body_class('login');

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $SITE->shortname; ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_schools_generate_favicon('faviconico'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_schools_generate_favicon('favicon'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_schools_generate_favicon('favicon'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_schools_generate_favicon('favicon'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<div id="page" class="page">
    <div id="page-content" class="clearfix">
		<div class="login-info">
			<div class="logo">
				<a href="<?php echo $CFG->wwwroot; ?>" ><?php echo theme_schools_get_logo(); ?></a>
			</div>	
			<h2><?php echo $SITE->fullname;?></h2>
		</div>
        	<form action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post" id="login" class="login-form clearfix">	
				<div class="title">
					<span class="active"><?php echo get_string('login','theme_schools');?></span>
					<?php if (!empty($CFG->registerauth)):?>
					/
					<a href="<?php echo $CFG->wwwroot; ?>/login/signup.php"><?php echo get_string('sign_up','theme_schools');?></a>
					<?php endif;?>
				</div>
				<input placeholder="Email" id="username" class="validate-username" size="25" value="" name="username" type="text">
				<input placeholder="Password" id="password" class="validate-password" size="25" value="" name="password" type="password">
				<div class="button-box clearfix">
					<button type="submit" class="button"><?php echo get_string('sign_in','theme_schools');?></button>
					<div class="social">
						<a class="facebook" href="https://www.facebook.com/dialog/oauth?client_id=<?php echo get_config('auth/googleoauth2', 'facebookclientid');?>&redirect_uri=<?php echo $CFG->wwwroot;?>/auth/googleoauth2/facebook_redirect.php&state=<?php echo auth_googleoauth2_get_state_token();?>&scope=email&response_type=code"><i class="fa fa-facebook"></i></a>
						<a class="googleplus" href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo get_config('auth/googleoauth2', 'googleclientid');?>&redirect_uri=<?php echo $CFG->wwwroot;?>/auth/googleoauth2/google_redirect.php&state=<?php echo auth_googleoauth2_get_state_token();?>&scope=profile email&response_type=code"><i class="fa fa-google"></i></a>
					</div>
				</div>
			</form>
    </div>
	<div class="login-bottom">
		<span>Don't have an </span><a href="<?php echo $CFG->wwwroot; ?>/login/signup.php">account?</a> <span class="separator">/</span> <span>Forgot </span><a href="<?php echo $CFG->wwwroot; ?>/login/forgot_password.php">password?</a>		
	</div>
</div>

</body>
</html>
<!-- http://development.sebale.net/moodle29/theme/schools/layout/login.php-->
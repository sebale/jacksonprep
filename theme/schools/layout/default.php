<?php
/**
 * @package   theme_schools
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
if (!isloggedin() or isguestuser() && $PAGE->pagelayout != 'frontpage') {
	include("guest.php");
	return;
}
$PAGE->requires->jquery();
$PAGE->requires->js('/theme/'.$CFG->theme.'/javascript/script.js');

$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$maincss = ($hassidepre and $hassidepost) ? 'bothside' : '';
$maincss = ($hassidepre and !$hassidepost) ? 'leftside' : $maincss;
$maincss = (!$hassidepre and $hassidepost) ? 'rightside' : $maincss;
$html = theme_schools_get_html_for_settings($OUTPUT, $PAGE);
$update = (has_capability('moodle/course:update', context_course::instance($this->page->course->id))) ? true : false;

$my_courses  = enrol_get_my_courses();
global $DB;
$quick_links = $DB->get_records('schools_quick_links',array('user'=>$USER->id),'timecreate');
$first_category = $DB->get_record_sql('SELECT id FROM {course_categories} ORDER BY id LIMIT 1');

$isteacher = user_has_role_assignment($USER->id,3,1);
$usertime = usertime(time());

if(date('h', $usertime) < 12){
	$time =  "Good morning";
}elseif(date('h', $usertime) < 17){
	$time = "Good afternoon";
}else{
	$time = "Good evening";
}
echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head rel="<?php echo $usertime; ?>">
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_schools_generate_favicon('faviconico'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_schools_generate_favicon('favicon'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_schools_generate_favicon('favicon'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_schools_generate_favicon('favicon'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

</head>

<body <?php  echo  str_replace("dir-ltr", "",$OUTPUT->body_attributes()); ?>>
	<?php echo $OUTPUT->standard_top_of_body_html(); ?>
	<section id="page" class="page clearfix">
		<div class="toggle-box">
			<i class="fa fa-angle-double-down" id="toggle-menu"></i>
		</div>
		<div class="leftside">
			<div class="container">
				<?php echo $OUTPUT->blocks('side-right', 'side-right'); ?>
			</div>
		</div>		
		<div class="content clearfix"  id="region-main">
			<header class="clearfix">
				<div class="left">
					<div class="logo">
						<a href="<?php echo $CFG->wwwroot; ?>" ><?php echo theme_schools_get_logo(); ?></a>
					</div>	
					<?php echo $html->welcome_user;?>
				</div>
				<div class="right">
					<ul class="nav">
						<?php if($COURSE->id > 1):?>
							<li class="header-editing">
								<?php echo $OUTPUT->page_heading_button(); ?>
							</li>
						<?php endif;?>
						<?php if($isteacher):?>
							<li><a href="<?php echo new moodle_url('/course/edit.php', array('category' => $first_category->id));?>"><?php echo get_string('create_new_course','theme_schools');?></a></li>
						<?php endif;?>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown">
								<span>quick links<i class="fa fa-bars"></i></span>
							</a>
							<ul class = "dropdown-menu">
								<?php 
								$custom_menu = $OUTPUT->custom_menu();
								if(!empty($custom_menu))
									echo $custom_menu;
								if(!empty($custom_menu) && !empty($quick_links))
									echo '<hr>';
								if(!empty($quick_links)){
									echo html_writer::start_tag('ul'); 
									foreach($quick_links as $quick_link){
										echo html_writer::start_tag('li'); 
										echo html_writer::tag('label','<i class="fa fa-times"></i>',array('link_id'=>$quick_link->id,'class'=>'bookmark-delete')); 
										echo html_writer::link($quick_link->url,$quick_link->name); 
										echo html_writer::end_tag('li'); 
									}
									echo html_writer::end_tag('ul'); 
								}								
								if(empty($custom_menu) && empty($quick_links)){
									echo html_writer::tag('li', get_string('no_quick_links','theme_schools'), array('class'=>'no_quick_links')); 
								}
								?>
							</ul>	
						</li>
					</ul>
					<div class="dropdown bookmark">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bookmark-o"></i></a>
						<form class = "dropdown-menu" id="bookmark-form">
							<h5>Bookmark this page</h5>
							<input type="hidden" id="bookmark-userid" value="<?php echo $USER->id?>">
							<input type="text" placeholder="Name" id="bookmark-name">
							<input type="submit" value="Done">
						</form>
					</div>
				</div>
			</header>
	
			<?php if ($COURSE->id > 1): ?>
				<div class="course-heading clearfix">
					Course: <?php echo html_writer::link( new moodle_url('/course/view.php',array('id'=>$COURSE->id)),$COURSE->fullname); ?>
				</div>
			<?php endif; ?>
			
			<?php if (is_siteadmin()): ?>
				<div id="page-navbar" class="clearfix">
					<nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
				</div>
			<?php endif; ?>
	
			<?php if ( $PAGE->pagetype == 'my-index') echo theme_schools_get_schools_statistic();?>

			<div class="<?php echo $PAGE->pagelayout; ?>">
			 <?php
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
				if(empty($my_courses) && $PAGE->pagetype == 'my-index') echo theme_schools_get_empty_my_index();
                echo $OUTPUT->course_content_footer();
				echo $OUTPUT->standard_footer_html();  
			?>
			</div>		
		</div>		
		</div>	<!-- hack for user profile page -->	
		<div class="rightside">
			<div class="container">

				<?php 
					echo html_writer::start_tag('div', array('class'=>'user-header clearfix'));
					echo $OUTPUT->user_picture($USER, array('size' => 50, 'class'=>'avatare'));
					echo html_writer::tag('h4',  
											html_writer::tag('span',$time.", ".$USER->firstname, array('class'=>'user')).
											html_writer::tag('span',"Welcome to ".$SITE->fullname, array('class'=>'course')).
											html_writer::tag('a', get_string('myprofile'), array('class'=>'link', "href"=>"$CFG->wwwroot/user/profile.php?id=".$USER->id)).
											html_writer::tag('i', "-").
											html_writer::tag('a', "My files", array('class'=>'link', "href"=>"$CFG->wwwroot/user/files.php"))
											);
					echo html_writer::end_tag('div');
				?>

				<?php if($COURSE->format == 'site' and $PAGE->pagelayout == 'course') echo theme_schools_get_site_progress();?>
				<?php if($COURSE->format != 'site') echo theme_schools_get_course_progress();?>
				<?php echo ($hassidepre) ? $OUTPUT->blocks('side-pre', 'side-pre') : ''; ?>
				<?php echo ($hassidepost) ? $OUTPUT->blocks('side-post', 'side-post') : ''; ?>
				
				<footer>
					<?php echo $html->footnote;?>
				</footer>

			</div>
		</div>
		<?php echo $OUTPUT->standard_end_of_body_html() ?>
	</section>
</body>
</html>
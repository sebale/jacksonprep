<?php
// Get the HTML for the settings bits.
$PAGE->requires->jquery();
$html = theme_schools_get_html_for_settings($OUTPUT, $PAGE);

$facebookclientid = get_config('auth/googleoauth2', 'facebookclientid');
$googleclientid = get_config('auth/googleoauth2', 'googleclientid');
$oidc = get_config('auth_oidc');

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_schools_generate_favicon('faviconico'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_schools_generate_favicon('favicon'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_schools_generate_favicon('favicon'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_schools_generate_favicon('favicon'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body class="guest">
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<div id="page" class="page">
    <div id="page-content" class="clearfix">
		<div class="login-info">
			<div class="logo">
				<a href="<?php echo $CFG->wwwroot; ?>" ><?php echo theme_schools_get_logo(); ?></a>
			</div>
		</div>
        <div class="login-form clearfix">			
			<?php if($PAGE->pagelayout == 'login'): ?>
				<section id="region-main" class="hidden"><?php echo $OUTPUT->main_content();  ?></section>
				
				<form action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post" id="login" class="clearfix">	
					<div class="title">
						<span class="active"><?php echo get_string('login','theme_schools');?></span>
						<?php if (!empty($CFG->registerauth)):?>
						/
						<a href="<?php echo $CFG->wwwroot; ?>/login/signup.php"><?php echo get_string('sign_up','theme_schools');?></a>
						<?php endif;?>
					</div>
					<input placeholder="Email" id="username" class="validate-username" size="25" value="" name="username" type="text">
					<input placeholder="Password" id="password" class="validate-password" size="25" value="" name="password" type="password">
					<div class="button-box clearfix">
						<button type="submit" class="button"><?php echo get_string('sign_in','theme_schools');?></button>
						<div class="social">
						<?php if(!empty($facebookclientid)):?>
							<a class="facebook" href="https://www.facebook.com/dialog/oauth?client_id=<?php echo $facebookclientid;?>&redirect_uri=<?php echo $CFG->wwwroot;?>/auth/googleoauth2/facebook_redirect.php&state=<?php echo auth_googleoauth2_get_state_token();?>&scope=email&response_type=code"><i class="fa fa-facebook"></i></a>
						<?php endif;?>
						<?php if(!empty($googleclientid)):?>
							<a class="googleplus" href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $googleclientid;?>&redirect_uri=<?php echo $CFG->wwwroot;?>/auth/googleoauth2/google_redirect.php&state=<?php echo auth_googleoauth2_get_state_token();?>&scope=profile email&response_type=code"><i class="fa fa-google"></i></a>
						<?php endif;?>
						<?php if(!empty($oidc->clientsecret)):?>
							<a class="o365" href="<?php echo $CFG->wwwroot;?>/auth/oidc/"><img src="<?php echo $CFG->wwwroot; ?>/auth/oidc/pix/o365.png" alt="<?php echo $oidc->opname;?>"></a>
						<?php endif;?>
						</div>
					</div>
				</form>
			<?php else: ?>
				<section id="region-main">
					<?php echo $OUTPUT->main_content();  ?>
				</section>
			<?php endif; ?>
		</div>
    </div>
	<div class="login-bottom">
		<span>Don't have an </span><a href="<?php echo $CFG->wwwroot; ?>/login/signup.php">account?</a> <span class="separator">/</span> <a href="<?php echo $CFG->wwwroot; ?>/login/forgot_password.php">Forgot password?</a>		
	</div>
</div>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>

<?php
/**
 * @package   theme_schools
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
if($CFG->theme == 'schools'){
	$CFG->defaultblocks_topics =  ' ';
	$CFG->defaultblocks_weeks =  ' ';
}
function theme_schools_page_init(moodle_page $page) {

}

function theme_schools_process_css($css, $theme) {

    // Set the backgroundhead image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_schools_set_logo($css, $logo);

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_schools_set_customcss($css, $customcss);
	
    if (!empty($theme->settings->backgrounduserblock)) {
        $backgrounduserblock = $theme->settings->backgrounduserblock;
    } else {
        $backgrounduserblock = null;
    }
    $css = theme_schools_set_backgrounduserblock($css, $backgrounduserblock);
	
    if (!empty($theme->settings->backgroundcourseprogress)) {
        $backgroundcourseprogress = $theme->settings->backgroundcourseprogress;
    } else {
        $backgroundcourseprogress = null;
    }
    $css = theme_schools_set_backgroundcourseprogress($css, $backgroundcourseprogress);	
	
    if (!empty($theme->settings->backgroundleftside)) {
        $backgroundleftside = $theme->settings->backgroundleftside;
    } else {
        $backgroundleftside = null;
    }
    $css = theme_schools_set_backgroundleftside($css, $backgroundleftside);
	
    if (!empty($theme->settings->backgroundrightside)) {
        $backgroundrightside = $theme->settings->backgroundrightside;
    } else {
        $backgroundrightside = null;
    }
    $css = theme_schools_set_backgroundrightside($css, $backgroundrightside);
	
    return $css;
}


/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_schools_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_schools_set_backgrounduserblock($css, $backgrounduserblock) {
    $tag = '[[setting:backgrounduserblock]]';
    $replacement = $backgrounduserblock;
    if (is_null($replacement)) {
        $replacement = '#f96b5d';
    }
    $css = str_replace($tag, $replacement, $css);
	return $css;
}
function theme_schools_set_backgroundcourseprogress($css, $backgroundcourseprogress) {
    $tag = '[[setting:backgroundcourseprogress]]';
    $replacement = $backgroundcourseprogress;
    if (is_null($replacement)) {
        $replacement = '#f16645';
    }
    $css = str_replace($tag, $replacement, $css);
	return $css;
}
function theme_schools_set_backgroundleftside($css, $backgroundleftside) {
    $tag = '[[setting:backgroundleftside]]';
    $replacement = $backgroundleftside;
    if (is_null($replacement)) {
        $replacement = '#171d1f';
    }
    $css = str_replace($tag, $replacement, $css);
	return $css;
}
function theme_schools_set_backgroundrightside($css, $backgroundrightside) {
    $tag = '[[setting:backgroundrightside]]';
    $replacement = $backgroundrightside;
    if (is_null($replacement)) {
        $replacement = '#171d1f';
    }
    $css = str_replace($tag, $replacement, $css);
	return $css;
}

/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */
function theme_schools_get_logo() {
	global $SITE, $CFG;
	
	$logo  = get_config('theme_schools', 'logo');
	if($logo){
		$path = $CFG->wwwroot.'/pluginfile.php/1/theme_schools/logo/'.context_system::instance()->id.$logo;		
		return '<img src="'.$path.'" title="'.$SITE->shortname.'" alt="'.$SITE->shortname.'"/>';
	}else{
		return "<h2>$SITE->shortname</h2>";
	}
}
function theme_schools_set_logo($css, $logo) {
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}
function theme_schools_generate_favicon($type = 'favicon') {
	global $SITE, $CFG, $OUTPUT;
    $path = '';
	
    if ($type == 'favicon'){
        $logo  = get_config('theme_schools', 'favicon');    
    } else {
        $logo  = get_config('theme_schools', 'faviconico');    
    }
    
	if($logo){
		$path = $CFG->wwwroot.'/pluginfile.php/1/theme_schools/'.$type.'/'.context_system::instance()->id.$logo;
	} else {
	    $path = $OUTPUT->favicon();
	}
    return $path;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_schools_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM) {
        $theme = theme_config::load('schools');
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}



/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * Do not add schools specific logic in here, child themes should be able to
 * rely on that function just by declaring settings with similar names.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_schools_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }

	$return->welcome_user = '';
    if (!empty($page->theme->settings->welcome_user)) {
        $return->welcome_user =format_text($page->theme->settings->welcome_user);
    }
	$return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = $page->theme->settings->footnote;
    }

    return $return;
}

/**
 * All theme functions should start with theme_schools_
 * @deprecated since 2.5.1
 */
function schools_process_css() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_schools_
 * @deprecated since 2.5.1
 */
function schools_set_logo() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_schools_
 * @deprecated since 2.5.1
 */
function schools_set_customcss() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}


function theme_schools_get_course_progress(){
	global $DB,$COURSE,$USER,$OUTPUT,$cm;
	$teachers = $DB->get_record_sql("SELECT u.*, COUNT(ra.id) AS count_teachers                    
					FROM {course} AS c
					JOIN {context} AS ct ON c.id = ct.instanceid
					JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
					JOIN {user} AS u ON u.id = ra.userid
					WHERE c.id=".$COURSE->id);
	$context = context_course::instance($COURSE->id);
	
	if(!empty($teachers->firstname) and !has_capability('moodle/course:update', $context)){
		$html = html_writer::start_tag('div',array('class' => 'user-right clearfix'));		
		$html .= $OUTPUT->user_picture($teachers, array('size' => 50));
		$html .= html_writer::tag('span','Course by',array('class' => 'by'));
		$html .= html_writer::tag('span',$teachers->firstname.' '.$teachers->lastname,array('class' => 'name'));
		$html .= html_writer::end_tag('div');
	}
	if (!user_has_role_assignment($USER->id,1) && !user_has_role_assignment($USER->id,3) && !user_has_role_assignment($USER->id,4)){
		$course = $COURSE;
		$data = $DB->get_record_sql("SELECT
									(SELECT (gg.finalgrade/gg.rawgrademax)*100 FROM  {grade_items} gi, {grade_grades} gg  WHERE gi.itemtype = 'course' AND gg.itemid = gi.id AND gi.courseid=$course->id AND gg.userid=$USER->id) AS average,
									(SELECT count(cm.id) FROM {course_modules} cm, {modules} m WHERE m.id = cm.module AND cm.completion > 0 AND cm.course = $course->id) as modules,
									(SELECT count(cmc.id) FROM {course_modules} cm, {course_modules_completion} cmc, {modules} m WHERE m.id = cm.module AND cm.course = $course->id AND cmc.coursemoduleid = cm.id AND cmc.userid=$USER->id AND cmc.completionstate > 0) as passed,
									(SELECT timecompleted FROM {course_completions} WHERE course = $course->id AND userid=$USER->id) as completed
								");
		$progress = ($data->modules and $data->passed) ? ($data->passed / $data->modules) : 0;
		
		if($data->modules > 0){
			$html .= html_writer::tag('div',
																html_writer::tag('span',intval($data->passed).'/'.intval($data->modules),array('class' => 'progres-step')).
																html_writer::start_tag('div',array('class' => 'progres-bar')).
																html_writer::tag('div','',array('class' => 'progres','style'=>'width:'.($progress*100).'%;')).
																html_writer::end_tag('div'),
																array('class'=>'course-progress clearfix')
															);
		}
	}
	
	return $html;
}

function theme_schools_get_site_progress(){
	global $DB,$COURSE,$USER,$OUTPUT;
	
	if (!user_has_role_assignment($USER->id,1) && !user_has_role_assignment($USER->id,3) && !user_has_role_assignment($USER->id,4) && !is_siteadmin()){
		$course = $COURSE;
		$data = $DB->get_record_sql("SELECT
									(SELECT count(c.id) FROM  {course} c WHERE c.visible=1 AND c.id>1) AS all_courses,
									(SELECT count(cc.timecompleted > 0) FROM {course_completions} cc WHERE cc.userid = $USER->id ) as completed_courses
								");
		$progress = ($data->all_courses and $data->completed_courses) ? ($data->completed_courses / $data->all_courses) : 0;

		$progress_info = html_writer::tag('div',
																html_writer::tag('span',intval($data->completed_courses).'/'.intval($data->all_courses),array('class' => 'progres-step')).
																html_writer::start_tag('div',array('class' => 'progres-bar')).
																html_writer::tag('div','',array('class' => 'progres','style'=>'width:'.($progress*100).'%;')).
																html_writer::end_tag('div'),
																array('class'=>'course-progress clearfix')
															);
			
		return $progress_info;
	}else{
		return html_writer::tag('div',$OUTPUT->page_heading_button(),array('class'=>'course-progress clearfix')	);
	}
}

function  theme_schools_get_schools_statistic() {
    global $USER, $CFG, $DB;
	if(user_has_role_assignment($USER->id,3,1)){
		$courses = enrol_get_my_courses();
		$my_teach_courses = array();
		foreach($courses as $item){
			$context = context_course::instance($item->id, IGNORE_MISSING);
			if (has_capability('moodle/course:update', $context)) {
				$my_teach_courses[] = $item;
			}
		}
		return theme_schools_statistic_teacher($my_teach_courses);
	}
	
     $now = time();
    $active  = ENROL_USER_ACTIVE;
    $enabled = ENROL_INSTANCE_ENABLED;
    $next_week = strtotime("+1 week 4 day");
    $prev_week = strtotime("-1 week 4 day");
    $after_next_week = strtotime("+2 week 4 day");
    $before_prev_week = strtotime("-2 week 4 day");
    $after_next_2week = strtotime("+3 week 4 day");
    $before_prev_2week = strtotime("-3 week 4 day");

    $data = $DB->get_record_sql("SELECT ROUND(sum(IF(((((gg.finalgrade/gg.rawgrademax)*100)-50)/10)<0,0,(((gg.finalgrade/gg.rawgrademax)*100)-50)/10)*(SELECT count(id) FROM mdl_course_modules WHERE course = cc.course))/sum((SELECT count(id) FROM mdl_course_modules WHERE course = cc.course)),2) AS grade, 
    																	count(DISTINCT ue.id) AS my_courses, 
    																	count(DISTINCT cc.id) AS completed_courses, 
		    															count(DISTINCT ass_t.id) as assignment_today, 
																			count(DISTINCT q_t.id) as quiz_today, 
																			count(DISTINCT ass_all.id) as assignment_all, 
																			count(DISTINCT q_all.id) as quiz_all
															FROM  {user_enrolments} ue  																			
																LEFT JOIN  {course_completions} cc ON cc.userid = ue.userid AND cc.timecompleted > 0
																LEFT JOIN  {grade_items} gi ON gi.itemtype = 'course' AND gi.courseid=cc.course
																LEFT JOIN  {grade_grades} gg ON gg.itemid = gi.id AND gg.userid=cc.userid
																LEFT JOIN {enrol} e ON ue.enrolid = e.id AND ue.status = $active AND e.status = $enabled AND ue.timestart < $now AND (ue.timeend = 0 OR ue.timeend > $now)
																LEFT JOIN {assign} ass_t ON ass_t.duedate > ($now - 43200) AND ass_t.duedate < ($now + 43200)  AND ass_t.course = e.courseid																				
																LEFT JOIN {quiz} q_t ON q_t.timeclose > ($now - 43200) AND q_t.timeclose < ($now + 43200)  AND q_t.course = e.courseid
																
																LEFT JOIN {assign} ass_all ON  ass_all.course = e.courseid																				
																LEFT JOIN {quiz} q_all ON q_all.course = e.courseid	
																
															WHERE ue.userid = $USER->id
							");
    $ext = 2592000; //by month
    $timestart = strtotime('-7 month');
    $timefinish = time() - (date('j')*86400);
    
  
    $grades['month'] =$DB->get_records_sql("(SELECT gg.timemodified  as time, AVG((gg.finalgrade/gg.rawgrademax)*100) as grade
																FROM {grade_grades} gg
																 WHERE gg.userid=$USER->id AND gg.timemodified BETWEEN $timestart AND $timefinish
																  GROUP BY floor(gg.timemodified / $ext) * $ext
																   ORDER BY gg.timemodified DESC)");
    $timestart = time() - (date('j')*86400);
    $timefinish = time();
    
    $grades['current_month'] =$DB->get_records_sql("(SELECT gg.timemodified  as time, AVG((gg.finalgrade/gg.rawgrademax)*100) as grade
																FROM {grade_grades} gg
																 WHERE gg.userid=$USER->id AND gg.timemodified BETWEEN $timestart AND $timefinish
																  GROUP BY floor(gg.timemodified / $ext) * $ext
																   ORDER BY gg.timemodified DESC)");
    $grades['month'][] = array_shift($grades['current_month']);
    
    return  theme_schools_statistic($data,$grades);
}
    
function  theme_schools_statistic($data,$grades) {
	global $USER, $OUTPUT,$CFG;
    $html = '';
    $html .= html_writer::start_tag('div',array('class'=>'dashboard row-fluid clearfix'));
		
		$html .= html_writer::start_tag('div',array('class'=>'coll5 clearfix'));			
			$html .= html_writer::tag('div',$OUTPUT->user_picture($USER, array('size'=>100)),array('class'=>'image'));
			$html .= html_writer::start_tag('div',array('class'=>'wrapper clearfix'));
				$html .= html_writer::tag('h2',fullname($USER));
				$html .= html_writer::start_tag('h3');
					$html .= html_writer::start_tag('p');
						$html .= html_writer::tag('strong',($data->completed_courses)?$data->completed_courses:0);
						$html .= html_writer::tag('span',get_string('completed_courses','theme_schools'));
					$html .= html_writer::end_tag('p');
					$html .= html_writer::start_tag('p');
						$html .= html_writer::tag('strong',($data->my_courses)?$data->my_courses:0);
						$html .= html_writer::tag('span',get_string('enrol_courses','theme_schools'));
					$html .= html_writer::end_tag('p');
				$html .= html_writer::end_tag('h3');		
			$html .= html_writer::end_tag('div');		
		$html .= html_writer::end_tag('div');
		
		$html .= html_writer::start_tag('div',array('class'=>'coll3'));
		
			$html .= html_writer::start_tag('div',array('class'=>'item'));
				$html .= html_writer::start_tag('h6');
					$html .= get_string('ass_due_today','theme_schools');
					$html .= html_writer::tag('strong',($data->assignment_today)?$data->assignment_today:0);
				$html .= html_writer::end_tag('h6');	
				$html .= html_writer::start_tag('div',array('class'=>'progress thin'));	
					$procent = ($data->assignment_today && $data->assignment_all)?($data->assignment_today / $data->assignment_all)*100:0;
					$html .= html_writer::tag('div','',array('style'=>"width: $procent%", 'class'=>'progress-bar progress-bar-danger'));						
				$html .= html_writer::end_tag('div');	
					
			$html .= html_writer::end_tag('div');
			$html .= html_writer::start_tag('div',array('class'=>'item'));
				$html .= html_writer::start_tag('h6');
					$html .= get_string('quiz_due_today','theme_schools');
					$html .= html_writer::tag('strong',($data->quiz_today)?$data->quiz_today:0);
				$html .= html_writer::end_tag('h6');
				$html .= html_writer::start_tag('div',array('class'=>'progress thin'));	
					$procent = ($data->quiz_today && $data->quiz_all)?($data->quiz_today / $data->quiz_all)*100:0;
					$html .= html_writer::tag('div','',array('style'=>"width: $procent%", 'class'=>'progress-bar progress-bar-danger'));						
				$html .= html_writer::end_tag('div');						
			$html .= html_writer::end_tag('div');		
				
		$html .= html_writer::end_tag('div');
			
		$html .= html_writer::start_tag('div',array('class'=>'coll4'));	
			$html .= (count($grades['month'])>0)?html_writer::tag('div','',array('id'=>'line-month','style'=>'width:100%;height:130px;')):'';	
			
			$script ="";
			$month = "";
			if(count($grades['month'])>0){
				foreach($grades['month'] as $grade){
					if(empty($grade->grade))continue;
					$time = date('Y',$grade->time)."-".date('m',$grade->time);
					$month .= "{ y: '$time', a:  $grade->grade },";
				}
				if($month == "")
					$month = "{ y: '".date('Y-m')."', a:  0 },";
				$script .= "$(window).ready(function(){		
									var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];					
									Morris.Line({
									  element: 'line-month',
									  data: [
										$month
									  ],
									  lineColors:['#ccc'],
									  pointFillColors:['#F3643C'],
									  lineWidth:1,
									  pointSize:3,
									  smooth: false,
									  resize: true,
									  xkey: 'y',
									  ykeys: ['a'],
									  labels: [''],
									  hideHover:'always',
									  xLabelFormat: function(x){  var d = new Date(x); return monthNames[d.getMonth()];},
									});								
								});";
			}

				
		$html .= html_writer::tag('script','',array('src'=>'//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js'));
		$html .= html_writer::tag('script','',array('src'=>$CFG->wwwroot.'/theme/schools/javascript/morris.min.js'));			
		$html .= html_writer::tag('script','',array('src'=>$CFG->wwwroot.'/theme/schools/javascript/morris_script.js'));			
		$html .= html_writer::tag('script',$script);
			
		$html .= html_writer::end_tag('div');			
			
    $html .= html_writer::end_tag('div');
				
    return $html;
}
	
		
function theme_schools_statistic_teacher($courses){
	global $DB;
	if(empty($courses)){
		$course = new stdClass();
		$course->id = -1;
		$courses[] = $course;
	}
	$where_course = '';
	$i = 0;
	foreach($courses as $course){
		if($i != 0 ) $where_course .= " OR ";
		$i++;
		$where_course .= "c.id = ".$course->id;
	}
		
	$lastaccess = strtotime("-1 month");		
	$now = time();
	$siteadmins = get_config('core','siteadmins');
 	$data = $DB->get_record_sql("SELECT 
									(SELECT count(DISTINCT ra.userid) FROM {context} c 
										LEFT JOIN {role} r ON r.shortname='student'
										LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
									WHERE c.contextlevel=50 AND (".str_replace('c.id','c.instanceid',$where_course).")) as users,
									
									(SELECT COUNT(asb.id)
											FROM {assign} a
												LEFT JOIN {assign_submission} asb ON a.id=asb.assignment AND asb.status='submitted' AND asb.userid NOT IN($siteadmins)
												LEFT JOIN {assign_grades} ag ON asb.assignment=ag.assignment AND ag.userid=asb.userid 
											WHERE (".str_replace('c.id','a.course',$where_course).") AND ag.id IS NULL AND asb.id IS NOT NULL 											
											) as subm,
									
									(SELECT COUNT(aliasqas.id) 
												FROM {context} c
													JOIN {role} r ON r.shortname='student'
													JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
													JOIN {quiz} qz ON qz.course=c.instanceid
													JOIN {quiz_attempts} qza ON qza.quiz=qz.id AND qza.userid=ra.userid AND qza.userid NOT IN($siteadmins)
													JOIN {question_attempts} aliasqa ON aliasqa.questionusageid = qza.uniqueid
													JOIN {question_attempt_steps} aliasqas ON aliasqas.questionattemptid = aliasqa.id
																		AND aliasqas.sequencenumber = (
																			SELECT MAX(sequencenumber)
																			FROM {question_attempt_steps}
																			WHERE questionattemptid = aliasqa.id
																		)
													JOIN {modules} m ON m.name='quiz'
													JOIN {course_modules} cm ON cm.module=m.id AND cm.course=c.instanceid AND cm.instance=qz.id
													JOIN {course} cou ON cou.id=c.instanceid														
												WHERE aliasqa.questionusageid >0
														AND aliasqa.behaviour = 'manualgraded'
														AND aliasqas.state = 'needsgrading'
														AND c.contextlevel=50 AND (".str_replace('c.id','c.instanceid',$where_course).")) as quiz,
									
									(SELECT count(e.id) FROM {event} e WHERE e.timestart>$now AND (e.courseid=1 OR ".str_replace('c.id','e.courseid',$where_course).")) as events,
									(SELECT count(DISTINCT u.id) FROM {context} c 
										LEFT JOIN {role} r ON r.shortname='student'
										LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
										LEFT JOIN {user} u ON ra.userid=u.id AND u.lastaccess<$lastaccess
									WHERE c.contextlevel=50 AND (".str_replace('c.id','c.instanceid',$where_course).")) as inactive_users,
									(SELECT SUM(IF(gi.gradepass>gg.finalgrade,1,0))
										FROM {context} c
											LEFT JOIN {role} r ON r.shortname='student'
											LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
											
											LEFT JOIN {grade_items} gi ON gi.itemtype = 'course' AND gi.courseid=c.instanceid
											LEFT JOIN {grade_grades} gg ON gg.itemid = gi.id AND gg.userid=ra.userid
									WHERE gg.id IS NOT NULL AND c.contextlevel=50 AND (".str_replace('c.id','c.instanceid',$where_course).")  ) as failuere_users
						") ; 
/*  	$data = $DB->get_record_sql("SELECT 
									(SELECT count(DISTINCT ra.userid) FROM {context} c 
										LEFT JOIN {role} r ON r.shortname='student'
										LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
									WHERE c.contextlevel=50 AND (".str_replace('c.id','c.instanceid',$where_course).")) as users,
									
									(SELECT COUNT(asb.id)
											FROM {assign} a
												LEFT JOIN {assign_submission} asb ON a.id=asb.assignment AND asb.status='submitted' AND asb.userid NOT IN($siteadmins)
												LEFT JOIN {assign_grades} ag ON asb.assignment=ag.assignment AND ag.userid=asb.userid 
											WHERE (".str_replace('c.id','a.course',$where_course).") AND ag.id IS NULL AND asb.id IS NOT NULL 											
											) as subm,
									
									(SELECT count(e.id) FROM {event} e WHERE e.timestart>$now AND (e.courseid=1 OR ".str_replace('c.id','e.courseid',$where_course).")) as events,
									(SELECT count(DISTINCT u.id) FROM {context} c 
										LEFT JOIN {role} r ON r.shortname='student'
										LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
										LEFT JOIN {user} u ON ra.userid=u.id AND u.lastaccess<$lastaccess
									WHERE c.contextlevel=50 AND (".str_replace('c.id','c.instanceid',$where_course).")) as inactive_users,
									(SELECT count(g.grade)
										FROM {context} c
											LEFT JOIN {role} r ON r.shortname='student'
											LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
											LEFT JOIN {course_completions} as cc ON cc.course = c.instanceid AND cc.userid = ra.userid AND (cc.timecompleted IS NULL OR cc.timecompleted = 0)
											LEFT JOIN (SELECT gi.courseid, gg.userid, (gg.finalgrade/gg.rawgrademax)*100 AS grade 
														FROM 	{grade_items} gi, 
																{grade_grades} gg 
														WHERE gi.itemtype = 'course' AND gg.itemid = gi.id GROUP BY  gi.courseid, gg.userid) as g 
													ON g.courseid = cc.course AND g.userid = cc.userid AND g.grade>0
								WHERE g.userid IS NOT NULL AND c.contextlevel=50 AND (".str_replace('c.id','c.instanceid',$where_course).")  ) as failuere_users
						") ; */ 
						
	$html = '';
	$html .= html_writer::start_tag('ul',array('class'=>'dashboard row-fluid clearfix'));
		$html .= html_writer::tag('li',
							html_writer::tag('strong',$data->users).
							html_writer::tag('p',get_string('enroled_users','theme_schools')),
						array('for'=>'enrolled-users','class'=>'action'));
		$html .= html_writer::tag('li',
							html_writer::tag('strong',count($courses)).
							html_writer::tag('p',get_string('courses')),
						array('for'=>'my_teach_courses','class'=>'action'));
		$html .= html_writer::tag('li',
							html_writer::tag('strong',$data->subm+$data->quiz).
							html_writer::tag('p',get_string('subm_need_grading','theme_schools')),
						array('for'=>'assign_need_gradding','class'=>'action'));
		$html .= html_writer::tag('li',
							html_writer::tag('strong',$data->events).
							html_writer::tag('p',get_string('upcoming_events','theme_schools')),
						array('for'=>'event-list','class'=>'action'));
		$html .= html_writer::tag('li',
							html_writer::tag('strong',$data->inactive_users).
							html_writer::tag('p',get_string('inactive_users','theme_schools')),
						array('for'=>'inactive-users','class'=>'action'));
		$html .= html_writer::tag('li',
							html_writer::tag('strong',$data->failuere_users).
							html_writer::tag('p',get_string('failuere_users','theme_schools')),
						array('for'=>'failing-students','class'=>'action'));
		
	$html .= html_writer::end_tag('ul');
		
	return $html;
}
	
function theme_schools_get_empty_my_index(){
	global $PAGE;
	$renderer = $PAGE->get_renderer('block_course_overview');
	$content = $renderer->course_overview(array(), array());
	$html = html_writer::start_tag('div',array('class'=>'block_course_overview  block'));
	$html .= html_writer::tag('div',$content,array('class'=>'content'));
	$html .= html_writer::end_tag('div');
	return $html;
}


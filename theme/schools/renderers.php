<?php
//error_reporting(E_ALL);ini_set('display_errors', '1');$CFG->debug = 32767;  $CFG->debugdisplay = true;

require_once($CFG->libdir. "/../course/renderer.php");
require_once($CFG->libdir. "/../blocks/course_overview/renderer.php");
require_once($CFG->libdir. "/../files/renderer.php");
require_once($CFG->libdir. "/../enrol/renderer.php");
require_once($CFG->libdir. "/../user/renderer.php");

require_once $CFG->dirroot.'/grade/lib.php';


class theme_schools_core_enrol_renderer extends core_enrol_renderer{
	public function render_course_enrolment_users_table(course_enrolment_users_table $table,moodleform $mform) {
		$table->head[0] = get_string('user');

        $table->initialise_javascript();

        $buttons = $table->get_manual_enrol_buttons();
        $buttonhtml = '';
        if (count($buttons) > 0) {
            $buttonhtml .= html_writer::start_tag('div', array('class' => 'enrol_user_buttons'));
            foreach ($buttons as $button) {
                $buttonhtml .= $this->render($button);
            }
            $buttonhtml .= html_writer::end_tag('div');
        }

        $content = '';
        if (!empty($buttonhtml)) {
            $content .= $buttonhtml;
        }
        $content .= $mform->render();

        $content .= $this->output->render($table->get_paging_bar());

        // Check if the table has any bulk operations. If it does we want to wrap the table in a
        // form so that we can capture and perform any required bulk operations.
        if ($table->has_bulk_user_enrolment_operations()) {
            $content .= html_writer::start_tag('form', array('action' => new moodle_url('/enrol/bulkchange.php'), 'method' => 'post'));
            foreach ($table->get_combined_url_params() as $key => $value) {
                if ($key == 'action') {
                    continue;
                }
                $content .= html_writer::empty_tag('input', array('type' => 'hidden', 'name' => $key, 'value' => $value));
            }
            $content .= html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'action', 'value' => 'bulkchange'));
            $content .= html_writer::table($table);
            $content .= html_writer::start_tag('div', array('class' => 'singleselect bulkuserop'));
            $content .= html_writer::start_tag('select', array('name' => 'bulkuserop'));
            $content .= html_writer::tag('option', get_string('withselectedusers', 'enrol'), array('value' => ''));
            $options = array('' => get_string('withselectedusers', 'enrol'));
            foreach ($table->get_bulk_user_enrolment_operations() as $operation) {
                $content .= html_writer::tag('option', $operation->get_title(), array('value' => $operation->get_identifier()));
            }
            $content .= html_writer::end_tag('select');
            $content .= html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('go')));
            $content .= html_writer::end_tag('div');

            $content .= html_writer::end_tag('form');
        } else {
            $content .= html_writer::table($table);
        }
        $content .= $this->output->render($table->get_paging_bar());
        if (!empty($buttonhtml)) {
            $content .= $buttonhtml;
        }
        return $content;
    }
}

class theme_schools_core_course_renderer extends core_course_renderer{
	public function course_section_cm_list_item($course, &$completioninfo, cm_info $mod, $sectionreturn, $displayoptions = array()) {
		$output = '';
		if ($modulehtml = $this->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions)) {
			$modclasses = 'activity ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;
			$output .= html_writer::tag('li', $modulehtml, array('class' => $modclasses, 'id' => 'module-' . $mod->id));
		}
		return $output;
	}

	public function course_section_cm_list($course, $section, $sectionreturn = null, $displayoptions = array()) {
		global $USER, $tabs,$PAGE,$DB;
		$tabs = array('all' => get_string('allactivities'));
		$output = '';
		$modinfo = get_fast_modinfo($course);
		if (is_object($section)) {
			$section = $modinfo->get_section_info($section->section);
		} else {
			$section = $modinfo->get_section_info($section);
		}
		$completioninfo = new completion_info($course);

		// check if we are currently in the process of moving a module with JavaScript disabled
		$ismoving = $this->page->user_is_editing() && ismoving($course->id);
		if ($ismoving) {
			$movingpix = new pix_icon('movehere', get_string('movehere'), 'moodle', array('class' => 'movetarget'));
			$strmovefull = strip_tags(get_string("movefull", "", "'$USER->activitycopyname'"));
		}

		// Get the list of modules visible to user (excluding the module being moved if there is one)
		$moduleshtml = array();
		if (!empty($modinfo->sections[$section->section])) {
			foreach ($modinfo->sections[$section->section] as $modnumber) {
				$mod = $modinfo->cms[$modnumber];

				if ($ismoving and $mod->id == $USER->activitycopy) {
					// do not display moving mod
					continue;
				}

				if ($modulehtml = $this->course_section_cm_list_item($course,
						$completioninfo, $mod, $sectionreturn, $displayoptions)) {
					$moduleshtml[$modnumber] = $modulehtml;
				}
			}
		}

		$sectionoutput = '';
		if (!empty($moduleshtml) || $ismoving) {
			foreach ($moduleshtml as $modnumber => $modulehtml) {
				if ($ismoving) {
					$movingurl = new moodle_url('/course/mod.php', array('moveto' => $modnumber, 'sesskey' => sesskey()));
					$sectionoutput .= html_writer::tag('li',
							html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)),
							array('class' => 'movehere'));
				}

				$sectionoutput .= $modulehtml;
			}

			if ($ismoving) {
				$movingurl = new moodle_url('/course/mod.php', array('movetosection' => $section->id, 'sesskey' => sesskey()));
				$sectionoutput .= html_writer::tag('li',
						html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)),
						array('class' => 'movehere'));
			}
		}
		$output .= '<i class="fa fa-angle-up show-activity"></i>';

		// Always output the section module list.
		$output .= html_writer::tag('ul', $sectionoutput, array('class' => 'section img-text'));

		return $output;
	}

    public function course_section_cm_name(cm_info $mod, $displayoptions = array()) {
        global $CFG;
        $output = '';
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            // nothing to be displayed to the user
            return $output;
        }
        $url = $mod->url;
        if (!$url) {
            return $output;
        }

        //Accessibility: for files get description via icon, this is very ugly hack!
        $instancename = $mod->get_formatted_name();
        $altname = $mod->modfullname;
        // Avoid unnecessary duplication: if e.g. a forum name already
        // includes the word forum (or Forum, etc) then it is unhelpful
        // to include that in the accessible description that is added.
        if (false !== strpos(core_text::strtolower($instancename),
                core_text::strtolower($altname))) {
            $altname = '';
        }
        // File type after name, for alphabetic lists (screen reader).
        if ($altname) {
            $altname = get_accesshide(' '.$altname);
        }

        // For items which are hidden but available to current user
        // ($mod->uservisible), we show those as dimmed only if the user has
        // viewhiddenactivities, so that teachers see 'items which might not
        // be available to some students' dimmed but students do not see 'item
        // which is actually available to current student' dimmed.
        $linkclasses = '';
        $accesstext = '';
        $textclasses = '';
        if ($mod->uservisible) {
            $conditionalhidden = $this->is_cm_conditionally_hidden($mod);
            $accessiblebutdim = (!$mod->visible || $conditionalhidden) &&
                has_capability('moodle/course:viewhiddenactivities', $mod->context);
            if ($accessiblebutdim) {
                $linkclasses .= ' dimmed';
                $textclasses .= ' dimmed_text';
                if ($conditionalhidden) {
                    $linkclasses .= ' conditionalhidden';
                    $textclasses .= ' conditionalhidden';
                }
                // Show accessibility note only if user can access the module himself.
                $accesstext = get_accesshide(get_string('hiddenfromstudents').':'. $mod->modfullname);
            }
        } else {
            $linkclasses .= ' dimmed';
            $textclasses .= ' dimmed_text';
        }

        // Get on-click attribute value if specified and decode the onclick - it
        // has already been encoded for display (puke).
        $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);

        $groupinglabel = $mod->get_grouping_label($textclasses);

        // Display link itself.
		 $img = file_get_contents($mod->get_icon_url());
			if(strpos($img,'svg') === false){
				$img = html_writer::empty_tag('img', array('src' => $mod->get_icon_url(),'class' => 'iconlarge activityicon', 'alt' => ' ', 'role' => 'presentation'));
			}else{
				$img = str_replace('xmlns:xlink="http://www.w3.org/1999/xlink">',' class="iconlarge activityicon" xmlns:xlink="http://www.w3.org/1999/xlink">', $img);
			}
        $activitylink = $img . $accesstext .
                html_writer::tag('span', $instancename . $altname, array('class' => 'instancename'));
        if ($mod->uservisible) {
            $output .= html_writer::link($url, $activitylink, array('class' => $linkclasses, 'onclick' => $onclick)) .
                    $groupinglabel;
        } else {
            // We may be displaying this just in order to show information
            // about visibility, without the actual link ($mod->uservisible)
            $output .= html_writer::tag('div', $activitylink, array('class' => $textclasses)) .
                    $groupinglabel;
        }
        return $output;
    }

	public function course_modchooser($modules, $course) {
        if (!$this->page->requires->should_create_one_time_item_now('core_course_modchooser')) {
            return '';
        }

        // Add the module chooser
        $this->page->requires->yui_module('moodle-course-modchooser',
        'M.course.init_chooser',
        array(array('courseid' => $course->id, 'closeButtonTitle' => get_string('close', 'editor')))
        );
        $this->page->requires->strings_for_js(array(
                'addresourceoractivity',
                'modchooserenable',
                'modchooserdisable',
        ), 'moodle');

        // Add the header
        $header = html_writer::tag('div', get_string('addresourceoractivity', 'moodle'),
                array('class' => 'hd choosertitle'));

        $formcontent = html_writer::start_tag('form', array('action' => new moodle_url('/course/jumpto.php'),
                'id' => 'chooserform', 'method' => 'post'));
        $formcontent .= html_writer::start_tag('div', array('id' => 'typeformdiv'));
        $formcontent .= html_writer::tag('input', '', array('type' => 'hidden', 'id' => 'course',
                'name' => 'course', 'value' => $course->id));
        $formcontent .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'sesskey',
                'value' => sesskey()));
        $formcontent .= html_writer::end_tag('div');

        // Put everything into one tag 'options'
        $formcontent .= html_writer::start_tag('div', array('class' => 'options'));
        $formcontent .= html_writer::tag('div', get_string('selectmoduletoviewhelp', 'moodle'),
                array('class' => 'instruction'));
        // Put all options into one tag 'alloptions' to allow us to handle scrolling
        $formcontent .= html_writer::start_tag('div', array('class' => 'alloptions'));

         // Activities
        $activities = array_filter($modules, create_function('$mod', 'return ($mod->archetype !== MOD_ARCHETYPE_RESOURCE && $mod->archetype !== MOD_ARCHETYPE_SYSTEM);'));
		$resources = array_filter($modules, create_function('$mod', 'return ($mod->archetype === MOD_ARCHETYPE_RESOURCE);'));
		$formcontent .= html_writer::start_tag('div', array('class' => 'module_title clearfix'));
        if (count($activities)) $formcontent .= $this->course_modchooser_title('activities');
        if (count($resources)) $formcontent .= $this->course_modchooser_title('resources');
		$formcontent .= html_writer::end_tag('div');
        if (count($activities)) $formcontent .= html_writer::tag('div',$this->course_modchooser_module_types($activities),array('class'=>'module_activities clearfix'));
        if (count($resources)) $formcontent .= html_writer::tag('div',$this->course_modchooser_module_types($resources),array('class'=>'module_resources clearfix'));


        $formcontent .= html_writer::end_tag('div'); // modoptions
        $formcontent .= html_writer::end_tag('div'); // types

        $formcontent .= html_writer::start_tag('div', array('class' => 'submitbuttons'));
        $formcontent .= html_writer::tag('input', '',
                array('type' => 'submit', 'name' => 'submitbutton', 'class' => 'submitbutton', 'value' => get_string('add')));
        $formcontent .= html_writer::tag('input', '',
                array('type' => 'submit', 'name' => 'addcancel', 'class' => 'addcancel', 'value' => get_string('cancel')));
        $formcontent .= html_writer::end_tag('div');
        $formcontent .= html_writer::end_tag('form');

        // Wrap the whole form in a div
        $formcontent = html_writer::tag('div', $formcontent, array('id' => 'chooseform'));

        // Put all of the content together
        $content = $formcontent;

        $content = html_writer::tag('div', $content, array('class' => 'choosercontainer'));
        return $header . html_writer::tag('div', $content, array('class' => 'chooserdialoguebody'));
    }

	public function course_section_cm_edit_actions($actions, cm_info $mod = null, $displayoptions = array()) {
		global $CFG;

		if (empty($actions)) {
			return '';
		}

		if (isset($displayoptions['ownerselector'])) {
			$ownerselector = $displayoptions['ownerselector'];
		} else if ($mod) {
			$ownerselector = '#module-'.$mod->id;
		} else {
			debugging('You should upgrade your call to '.__FUNCTION__.' and provide $mod', DEBUG_DEVELOPER);
			$ownerselector = 'li.activity';
		}

		if (isset($displayoptions['constraintselector'])) {
			$constraint = $displayoptions['constraintselector'];
		} else {
			$constraint = '.course-content';
		}

		$menu = new action_menu();
		$menu->set_owner_selector($ownerselector);
		$menu->set_constraint($constraint);
		$menu->set_alignment(action_menu::TR, action_menu::BR);
		$menu->set_menu_trigger(get_string('edit'));
		if (isset($CFG->modeditingmenu) && !$CFG->modeditingmenu || !empty($displayoptions['donotenhance'])) {
			$menu->do_not_enhance();

			// Swap the left/right icons.
			// Normally we have have right, then left but this does not
			// make sense when modactionmenu is disabled.
			$moveright = null;
			$_actions = array();
			foreach ($actions as $key => $value) {
				if ($key === 'moveright') {
					// Save moveright for later.
					$moveright = $value;
				} else if ($moveright) {

					// This assumes that the order was moveright, moveleft.
					// If we have a moveright, then we should place it immediately after the current value.
					$_actions[$key] = $value;
					$_actions['moveright'] = $moveright;

					// Clear the value to prevent it being used multiple times.
					$moveright = null;
				} else {
					$_actions[$key] = $value;
				}
			}
			$actions = $_actions;
			unset($_actions);
		}
		foreach ($actions as $action) {
			if ($action instanceof action_menu_link) {
				$action->add_class('cm-edit-action');
			}

			if($action instanceof action_menu_link_primary){
				$action = new action_menu_link_secondary($action->url,$action->icon,$action->text,$action->attributes);
				$menu->add(new action_menu_filler());
			}
			$menu->add($action);
		}
		$menu->attributes['class'] .= ' section-cm-edit-actions commands';

		// Prioritise the menu ahead of all other actions.
		$menu->prioritise = true;

		return $this->render($menu);
	}
	public function search_courses($searchcriteria) {
        global $CFG;
        $content = '';
		$admin_attr = (is_siteadmin())?'admin':'';
        if (!empty($searchcriteria)) {
			if (!empty($searchcriteria['search'])) {
                // print search form only if there was a search by search string, otherwise it is confusing
                $content .= $this->box_start('generalbox mdl-align coursesearch-heading '.$admin_attr);
                $content .= $this->course_search_form($searchcriteria['search']);
                $content .= $this->box_end();
            }

            // print search results
            require_once($CFG->libdir. '/coursecatlib.php');

            $displayoptions = array('sort' => array('displayname' => 1));
            // take the current page and number of results per page from query
            $perpage = optional_param('perpage', 0, PARAM_RAW);
            if ($perpage !== 'all') {
                $displayoptions['limit'] = ((int)$perpage <= 0) ? $CFG->coursesperpage : (int)$perpage;
                $page = optional_param('page', 0, PARAM_INT);
                $displayoptions['offset'] = $displayoptions['limit'] * $page;
            }
            // options 'paginationurl' and 'paginationallowall' are only used in method coursecat_courses()
            $displayoptions['paginationurl'] = new moodle_url('/course/search.php', $searchcriteria);
            $displayoptions['paginationallowall'] = true; // allow adding link 'View all'

            $class = 'course-search-result';
            foreach ($searchcriteria as $key => $value) {
                if (!empty($value)) {
                    $class .= ' course-search-result-'. $key;
                }
            }
            $chelper = new coursecat_helper();
            $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT)->
                    set_courses_display_options($displayoptions)->
                    set_search_criteria($searchcriteria)->
                    set_attributes(array('class' => $class));

            $courses = coursecat::search_courses($searchcriteria, $chelper->get_courses_display_options());
            $totalcount = coursecat::search_courses_count($searchcriteria);
            $courseslist = $this->schools_coursecat_courses($chelper, $courses, $totalcount);


            if (!$totalcount) {
                if (!empty($searchcriteria['search'])) {
                    $content .= $this->heading(get_string('nocoursesfound', '', $searchcriteria['search']));
                } else {
                    $content .= $this->heading(get_string('novalidcourses'));
                }
            } else {
                $content .= $this->heading(get_string('searchresults'). ": $totalcount");
                $content .= $courseslist;
            }

        } else {
            // just print search form
            $content .= $this->box_start('generalbox mdl-align');
            $content .= $this->box_start('coursesearch-heading '.$admin_attr);
            $content .= $this->course_search_form();
			$content .= $this->box_end();
            $content .= $this->box_end();
            $content .= $this->heading(get_string('entersearch','theme_schools'));
        }
        return $content;
    }

	protected function schools_coursecat_courses(coursecat_helper $chelper, $courses, $totalcount = null) {
        global $CFG;
        if ($totalcount === null) {
            $totalcount = count($courses);
        }
        if (!$totalcount) {
            // Courses count is cached during courses retrieval.
            return '';
        }

        if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_AUTO) {
            // In 'auto' course display mode we analyse if number of courses is more or less than $CFG->courseswithsummarieslimit
            if ($totalcount <= $CFG->courseswithsummarieslimit) {
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED);
            } else {
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_COLLAPSED);
            }
        }

        // prepare content of paging bar if it is needed
        $paginationurl = $chelper->get_courses_display_option('paginationurl');
        $paginationallowall = $chelper->get_courses_display_option('paginationallowall');
        if ($totalcount > count($courses)) {
            // there are more results that can fit on one page
            if ($paginationurl) {
                // the option paginationurl was specified, display pagingbar
                $perpage = $chelper->get_courses_display_option('limit', $CFG->coursesperpage);
                $page = $chelper->get_courses_display_option('offset') / $perpage;
                $pagingbar = $this->paging_bar($totalcount, $page, $perpage,
                        $paginationurl->out(false, array('perpage' => $perpage)));
                if ($paginationallowall) {
                    $pagingbar .= html_writer::tag('div', html_writer::link($paginationurl->out(false, array('perpage' => 'all')),
                            get_string('showall', '', $totalcount)), array('class' => 'paging paging-showall'));
                }
            } else if ($viewmoreurl = $chelper->get_courses_display_option('viewmoreurl')) {
                // the option for 'View more' link was specified, display more link
                $viewmoretext = $chelper->get_courses_display_option('viewmoretext', new lang_string('viewmore'));
                $morelink = html_writer::tag('div', html_writer::link($viewmoreurl, $viewmoretext),
                        array('class' => 'paging paging-morelink'));
            }
        } else if (($totalcount > $CFG->coursesperpage) && $paginationurl && $paginationallowall) {
            // there are more than one page of results and we are in 'view all' mode, suggest to go back to paginated view mode
            $pagingbar = html_writer::tag('div', html_writer::link($paginationurl->out(false, array('perpage' => $CFG->coursesperpage)),
                get_string('showperpage', '', $CFG->coursesperpage)), array('class' => 'paging paging-showperpage'));
        }

        // display list of courses
        $attributes = $chelper->get_and_erase_attributes('courses courses-list');
        $content = html_writer::start_tag('div', $attributes);

        if (!empty($pagingbar)) {
            $content .= $pagingbar;
        }

        $coursecount = 0;
        foreach ($courses as $course) {
            $coursecount ++;
            $classes = ($coursecount%2) ? 'odd' : 'even';
            if ($coursecount == 1) {
                $classes .= ' first';
            }
            if ($coursecount >= count($courses)) {
                $classes .= ' last';
            }
            $content .= $this->schools_coursecat_coursebox($chelper, $course, $classes);
        }

        if (!empty($pagingbar)) {
            $content .= $pagingbar;
        }
        if (!empty($morelink)) {
            $content .= $morelink;
        }

		$content .= '<style>#page-course-search .standard:after,#page-course-index-category .coursecategory:after{display:block!important;}</style>';
        $content .= html_writer::end_tag('div'); // .courses
        return $content;
    }

	protected function schools_coursecat_coursebox(coursecat_helper $chelper, $course, $additionalclasses = '') {
        global $CFG;
        if (!isset($this->strings->summary)) {
            $this->strings->summary = get_string('summary');
        }
        if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
            return '';
        }
        if ($course instanceof stdClass) {
            require_once($CFG->libdir. '/coursecatlib.php');
            $course = new course_in_list($course);
        }
        $content = '';
        $classes = trim('coursebox clearfix '. $additionalclasses);
        if ($chelper->get_show_courses() >= self::COURSECAT_SHOW_COURSES_EXPANDED) {
            $nametag = 'h3';
        } else {
            $classes .= ' collapsed';
            $nametag = 'div';
        }

        // .coursebox
        $content .= html_writer::start_tag('div', array(
            'class' => $classes,
            'data-courseid' => $course->id,
            'data-type' => self::COURSECAT_TYPE_COURSE,
        ));

        $content .= html_writer::tag('div',
						html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)),get_string('view'), array('class' => $course->visible ? 'btn' : 'btn dimmed'))
					, array('class' => 'view-link'));

		$content .= html_writer::start_tag('div', array('class' => 'info'));
        // course name
        $coursename = $chelper->get_course_formatted_name($course);
        $content .= html_writer::tag($nametag, $coursename, array('class' => 'coursename'));

		if ($course->has_course_contacts()) {
            $content .= html_writer::start_tag('div', array('class' => 'teachers'));
			$content .= get_string('teachers').': ';
			$contacts = $course->get_course_contacts();
            foreach ($contacts as $userid => $coursecontact) {
                $content .= html_writer::link(new moodle_url('/user/view.php',
                                array('id' => $userid, 'course' => SITEID)),
                            $coursecontact['username']);
				if (next($contacts)) $content .= ', ';
            }
            $content .= html_writer::end_tag('div'); // .teachers
        }

        $content .= html_writer::end_tag('div'); // .info

        if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT) {
            require_once($CFG->libdir. '/coursecatlib.php');
            if ($cat = coursecat::get($course->category, IGNORE_MISSING)) {
                $content .= html_writer::start_tag('div', array('class' => 'coursecat'));
                $content .= get_string('category').': '.
                        html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $cat->id)),
                                $cat->get_formatted_name(), array('class' => $cat->visible ? '' : 'dimmed'));
                $content .= html_writer::end_tag('div'); // .coursecat
            }
        }

        $content .= html_writer::end_tag('div'); // .coursebox
        return $content;
    }

	public function course_category($category) {
        global $CFG;
        require_once($CFG->libdir. '/coursecatlib.php');
        $coursecat = coursecat::get(is_object($category) ? $category->id : $category);
        $site = get_site();
		$admin_attr = (is_siteadmin())?'admin':'';
        $output = '';

        if (can_edit_in_category($category)) {
            // Add 'Manage' button if user has permissions to edit this category.
            $managebutton = $this->single_button(new moodle_url('/course/management.php'), get_string('managecourses'), 'get');
            $this->page->set_button($managebutton);
        }
        if (!$coursecat->id) {
            if (coursecat::count_all() == 1) {
                // There exists only one category in the system, do not display link to it
                $coursecat = coursecat::get_default();
                $strfulllistofcourses = get_string('fulllistofcourses');
                $this->page->set_title("$site->shortname: $strfulllistofcourses");
            } else {
                $strcategories = get_string('categories');
                $this->page->set_title("$site->shortname: $strcategories");
            }
        } else {
            $this->page->set_title("$site->shortname: ". $coursecat->get_formatted_name());

            // Print the category selector
			$output = $this->box_start('coursesearch-heading '.$admin_attr);
            $output .= html_writer::start_tag('div', array('class' => 'categorypicker'));
            $select = new single_select(new moodle_url('/course/index.php'), 'categoryid',
                    coursecat::make_categories_list(), $coursecat->id, null, 'switchcategory');
            $select->set_label(get_string('categories').':');
            $output .= $this->render($select);
            $output .= html_writer::end_tag('div'); // .categorypicker
			$output .= $this->box_end();
        }

        // Print current category description
        $chelper = new coursecat_helper();
        $description = $chelper->get_category_formatted_description($coursecat);

        // Prepare parameters for courses and categories lists in the tree
        $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_AUTO)
                ->set_attributes(array('class' => 'category-browse category-browse-'.$coursecat->id));

        $coursedisplayoptions = array();
        $catdisplayoptions = array();
        $browse = optional_param('browse', null, PARAM_ALPHA);
        $perpage = optional_param('perpage', $CFG->coursesperpage, PARAM_INT);
        $page = optional_param('page', 0, PARAM_INT);
        $baseurl = new moodle_url('/course/index.php');
        if ($coursecat->id) {
            $baseurl->param('categoryid', $coursecat->id);
        }
        if ($perpage != $CFG->coursesperpage) {
            $baseurl->param('perpage', $perpage);
        }
        $coursedisplayoptions['limit'] = $perpage;
        $catdisplayoptions['limit'] = $perpage;
        if ($browse === 'courses' || !$coursecat->has_children()) {
            $coursedisplayoptions['offset'] = $page * $perpage;
            $coursedisplayoptions['paginationurl'] = new moodle_url($baseurl, array('browse' => 'courses'));
            $catdisplayoptions['nodisplay'] = true;
            $catdisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'categories'));
            $catdisplayoptions['viewmoretext'] = new lang_string('viewallsubcategories');
        } else if ($browse === 'categories' || !$coursecat->has_courses()) {
            $coursedisplayoptions['nodisplay'] = true;
            $catdisplayoptions['offset'] = $page * $perpage;
            $catdisplayoptions['paginationurl'] = new moodle_url($baseurl, array('browse' => 'categories'));
            $coursedisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'courses'));
            $coursedisplayoptions['viewmoretext'] = new lang_string('viewallcourses');
        } else {
            // we have a category that has both subcategories and courses, display pagination separately
            $coursedisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'courses', 'page' => 1));
            $catdisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'categories', 'page' => 1));
        }
        $chelper->set_courses_display_options($coursedisplayoptions)->set_categories_display_options($catdisplayoptions);

        // Display course category tree.
        $output .= $this->coursecat_tree($chelper, $coursecat);

        // Add action buttons
        $output .= $this->container_start('buttons');
        $context = get_category_or_system_context($coursecat->id);
        if (has_capability('moodle/course:create', $context)) {
            // Print link to create a new course, for the 1st available category.
            if ($coursecat->id) {
                $url = new moodle_url('/course/edit.php', array('category' => $coursecat->id, 'returnto' => 'category'));
            } else {
                $url = new moodle_url('/course/edit.php', array('category' => $CFG->defaultrequestcategory, 'returnto' => 'topcat'));
            }
            $output .= $this->single_button($url, get_string('addnewcourse'), 'get');
        }
        ob_start();
        if (coursecat::count_all() == 1) {
            print_course_request_buttons(context_system::instance());
        } else {
            print_course_request_buttons($context);
        }
        $output .= ob_get_contents();
        ob_end_clean();
        $output .= $this->container_end();

        return $output;
    }

    protected function coursecat_category_content(coursecat_helper $chelper, $coursecat, $depth) {
        $content = '';
        // Subcategories
        $content .= $this->coursecat_subcategories($chelper, $coursecat, $depth);

        // AUTO show courses: Courses will be shown expanded if this is not nested category,
        // and number of courses no bigger than $CFG->courseswithsummarieslimit.
        $showcoursesauto = $chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_AUTO;
        if ($showcoursesauto && $depth) {
            // this is definitely collapsed mode
            $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_COLLAPSED);
        }

        // Courses
        if ($chelper->get_show_courses() > core_course_renderer::COURSECAT_SHOW_COURSES_COUNT) {
            $courses = array();
            if (!$chelper->get_courses_display_option('nodisplay')) {
                $courses = $coursecat->get_courses($chelper->get_courses_display_options());
            }
            if ($viewmoreurl = $chelper->get_courses_display_option('viewmoreurl')) {
                // the option for 'View more' link was specified, display more link (if it is link to category view page, add category id)
                if ($viewmoreurl->compare(new moodle_url('/course/index.php'), URL_MATCH_BASE)) {
                    $chelper->set_courses_display_option('viewmoreurl', new moodle_url($viewmoreurl, array('categoryid' => $coursecat->id)));
                }
            }
            $content .= $this->schools_coursecat_courses($chelper, $courses, $coursecat->get_courses_count());
        }

        if ($showcoursesauto) {
            // restore the show_courses back to AUTO
            $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_AUTO);
        }

        return $content;
    }
}

class theme_schools_block_course_overview_renderer extends block_course_overview_renderer{
    public function course_overview($courses, $overviews) {
		global $DB, $USER, $CFG;
        $html = '';
        $config = get_config('block_course_overview');
        if ($config->showcategories != BLOCKS_COURSE_OVERVIEW_SHOWCATEGORIES_NONE) {
            global $CFG;
            require_once($CFG->libdir.'/coursecatlib.php');
        }
		$courses = enrol_get_my_courses();
        $ismovingcourse = false;
        $courseordernumber = 0;
        $maxcourses = count($courses);
        $userediting = false;
        // Intialise string/icon etc if user is editing and courses > 1
        if ($this->page->user_is_editing() && (count($courses) > 1)) {
            $userediting = true;
            $this->page->requires->js_init_call('M.block_course_overview.add_handles');

            // Check if course is moving
            $ismovingcourse = optional_param('movecourse', FALSE, PARAM_BOOL);
            $movingcourseid = optional_param('courseid', 0, PARAM_INT);
        }

        // Render first movehere icon.
        if ($ismovingcourse) {
            // Remove movecourse param from url.
            $this->page->ensure_param_not_in_url('movecourse');

            // Show moving course notice, so user knows what is being moved.
            $html .= $this->output->box_start('notice');
            $a = new stdClass();
            $a->fullname = $courses[$movingcourseid]->fullname;
            $a->cancellink = html_writer::link($this->page->url, get_string('cancel'));
            $html .= get_string('movingcourse', 'block_course_overview', $a);
            $html .= $this->output->box_end();

            $moveurl = new moodle_url('/blocks/course_overview/move.php',
                        array('sesskey' => sesskey(), 'moveto' => 0, 'courseid' => $movingcourseid));
            // Create move icon, so it can be used.
            $movetofirsticon = html_writer::empty_tag('img',
                    array('src' => $this->output->pix_url('movehere'),
                        'alt' => get_string('movetofirst', 'block_course_overview', $courses[$movingcourseid]->fullname),
                        'title' => get_string('movehere')));
            $moveurl = html_writer::link($moveurl, $movetofirsticon);
            $html .= html_writer::tag('div', $moveurl, array('class' => 'movehere'));
        }

		if(count($courses)>0){
			$my_teach_courses = array();
			foreach($courses as $item){
				$context = context_course::instance($item->id, IGNORE_MISSING);
				if (has_capability('moodle/course:update', $context)) {
					$my_teach_courses[] = $item;
				}
			}
			if(empty($my_teach_courses) && !user_has_role_assignment($USER->id,3,1)){
				$my_table = html_writer::start_tag('div', array('class' => 'my_courses course-wrapper clearfix'));
				$now = time();

				foreach ($courses as $key => $course) {
					// If moving course, then don't show course which needs to be moved.
					if ($ismovingcourse && ($course->id == $movingcourseid)) {
						continue;
					}
					$data = $DB->get_record_sql("SELECT
										(SELECT (gg.finalgrade/gg.rawgrademax)*100 FROM  {grade_items} gi, {grade_grades} gg  WHERE gi.itemtype = 'course' AND gg.itemid = gi.id AND gi.courseid=$course->id AND gg.userid=$USER->id) AS average,
										(SELECT count(cm.id) FROM {course_modules} cm, {modules} m WHERE m.id = cm.module AND cm.completion > 0 AND cm.course = $course->id) as modules,
										(SELECT count(cmc.id) FROM {course_modules} cm, {course_modules_completion} cmc, {modules} m WHERE m.id = cm.module AND cm.course = $course->id AND cmc.coursemoduleid = cm.id AND cmc.userid=$USER->id AND cmc.completionstate > 0) as passed,
										(SELECT timecompleted FROM {course_completions} WHERE course = $course->id AND userid=$USER->id) as completed,
										(SELECT name FROM {course_categories} WHERE id = $course->category) as category,
										(SELECT MAX(cmc.coursemoduleid) FROM {course_modules} cm, {course_modules_completion} cmc, {modules} m WHERE m.id = cm.module AND cm.course = $course->id AND cmc.coursemoduleid = cm.id AND cmc.userid=$USER->id AND cmc.completionstate > 0) as prev_module_id,
										(SELECT cm.id FROM {course_modules} cm WHERE cm.id > prev_module_id AND cm.course = $course->id AND cm.completion = 1 LIMIT 1) as curent_module,
										(SELECT MIN(cm.id) FROM {course_modules} cm WHERE cm.course = $course->id) as first_module,
										(SELECT COUNT(DISTINCT cm.id)
											FROM {course_modules} cm
												LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid=cm.id AND cmc.userid=$USER->id
											WHERE cm.course = $course->id AND cm.visible=1 AND cm.completionexpected<$now AND cm.completionexpected>0 AND (cmc.id IS NULL OR cmc.completionstate=0)) as assign_missing,
										(SELECT COUNT(DISTINCT cm.id)
											FROM {course_modules} cm
											WHERE cm.course = $course->id AND cm.visible=1 AND cm.completionexpected>0) as assign_all
									");

					$my_table .= html_writer::start_tag('div', array('class' => 'course-row clearfix'));

					$progress_temp = ($data->modules and $data->passed) ? ($data->passed / $data->modules) : 0;
					$progress = ($data->completed)?1:$progress_temp;
					$progress_ico = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 width="50px" height="50px" viewBox="0 0 800 800" enable-background="new 0 0 800 800" xml:space="preserve">
									<circle fill="transparent" stroke="#DCE4EA" stroke-width="60" stroke-miterlimit="10" cx="50%" cy="50%" r="40%" style="stroke-dasharray: 260%, 0%;"/>
									<circle fill="transparent" stroke="#346A96" stroke-width="60" stroke-dashoffset="62%" stroke-miterlimit="10" cx="50%" cy="50%" r="40%" style="stroke-dasharray: '.($progress*250).'%, '.(250-($progress*250)).'%;"/>
									</svg>';
					$progress_ico .= ($data->completed)?'<i class="fa fa-check"></i>':'<i class="caret-right"></i>';

					$row = html_writer::link( new moodle_url('/course/view.php',array('id'=>$course->id)),$progress_ico,array('class'=>'prog'));
					$my_table .= html_writer::tag('div', $row,array('class' => 'course_progress'));

					$info = html_writer::start_tag('ul', array('class' => 'course_info clearfix'));

					$info .= ($course->startdate)?html_writer::tag('li', get_string('start_date','theme_schools').date('M d, Y',$course->startdate)):'';
					$info .= ($data->completed)?html_writer::tag('li', '<span class="separator">|</span>'.get_string('completed_date','theme_schools').date('M d, Y',$data->completed)):'';

					$modules = ($data->passed)?$data->passed:'-';
					$modules .= ' / ';
					$modules .= ($data->modules)?$data->modules:'-';
					$info .= html_writer::tag('li', '<span class="separator">|</span>'.get_string('curent_prodress','theme_schools',$modules));

					if($data->assign_all>0){
						$assign = ($data->assign_missing)?$data->assign_missing:'-';
						$assign .= ' / ';
						$assign .= ($data->assign_all)?$data->assign_all:'-';
						$info .= html_writer::tag('li', '<span class="separator">|</span>'.get_string('missing_assignments','theme_schools',$assign));
					}
					$info .= html_writer::end_tag('ul');

					$row = html_writer::tag('h3',$course->fullname).$info;
					$my_table .= html_writer::tag('div',$row,array('class'=>'course-content'));

					$avg = ($data->average)?$this->schools_get_grade_letter($data->average,$course->id):$this->schools_get_grade_letter(0,$course->id);
					$my_table .= html_writer::tag('div',$avg,array('class'=>'course-grade-number'));

					$row = ($data->completed)?$this->schools_get_grade_letter($data->average,$course->id):$this->schools_get_grade_letter(0,$course->id);

					$my_table .= html_writer::tag('div',$row,array('class'=>'course-grade'));

					$my_table .= html_writer::end_tag('div');
				}
				$my_table  .= html_writer::end_tag('div');
			}else{
				$my_teach_table = html_writer::start_tag('div', array('class' => 'my_teach_courses course-wrapper clearfix'));
					foreach ($my_teach_courses as $key => $course) {
						$data = $DB->get_record_sql("SELECT
										(SELECT count(cm.id) FROM {course_modules} cm WHERE cm.course = $course->id) as modules,
										(SELECT count(cm.id) FROM {course_modules} cm, {modules} m WHERE m.name='quiz' AND m.id = cm.module AND cm.course = $course->id) as quizes,
										(SELECT count(DISTINCT ra.userid) FROM {context} c
											LEFT JOIN {role} r ON r.shortname='student'
											LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
										WHERE c.contextlevel=50 AND c.instanceid= $course->id ) as users
									");

						$my_teach_table .= html_writer::start_tag('div', array('class' => 'course-row clearfix'));
							$progress_ico = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											 width="50px" height="50px" viewBox="0 0 800 800" enable-background="new 0 0 800 800" xml:space="preserve">
										<circle fill="transparent" stroke="#DCE4EA" stroke-width="60" stroke-miterlimit="10" cx="50%" cy="50%" r="40%" style="stroke-dasharray: 260%, 0%;"/>
										<circle fill="transparent" stroke="#346A96" stroke-width="60" stroke-dashoffset="62%" stroke-miterlimit="10" cx="50%" cy="50%" r="40%" style="stroke-dasharray: 250%, 0%;"/>
										</svg>';
							$progress_ico .= '<i class="caret-right"></i>';
							$row = html_writer::link( new moodle_url('/course/view.php',array('id'=>$course->id)),$progress_ico,array('class'=>'prog'));
							$my_teach_table .= html_writer::tag('div', $row,array('class' => 'course_progress'));

							$info = html_writer::start_tag('ul', array('class' => 'course_info clearfix'));

							$info .= html_writer::tag('li', html_writer::link( new moodle_url('/course/edit.php',array('id'=>$course->id)),get_string('edit')));
							$info .= html_writer::tag('li', html_writer::link( new moodle_url('/course/delete.php',array('id'=>$course->id)),get_string('delete')));
							$info .= html_writer::tag('li', html_writer::link( new moodle_url('/enrol/users.php',array('id'=>$course->id)),get_string('enrol','theme_schools')));

							$info .= html_writer::end_tag('ul');

							$row = html_writer::tag('h3',$course->fullname).$info;
							$my_teach_table .= html_writer::tag('div',$row,array('class'=>'course-content'));

							$info = html_writer::start_tag('ul', array('class' => 'clearfix'));

							$info .= html_writer::tag('li',$data->users.get_string('users_enroled','theme_schools'));
							$info .= html_writer::tag('li',$data->modules.get_string('modules','theme_schools'));
							$info .= html_writer::tag('li',$data->quizes.get_string('quizzes','theme_schools'));

							$info .= html_writer::end_tag('ul');

							$my_teach_table .= html_writer::tag('div',$info,array('class'=>'additional_info'));

						$my_teach_table .= html_writer::end_tag('div');
					}
				$my_teach_table .= html_writer::end_tag('div');
			}
		}else{
			$my_table = html_writer::start_tag('div',array('class'=>'my_courses course-wrapper clearfix'));
			$my_table .= html_writer::tag('div',get_string('nocourses'),array('class'=>'course-row clearfix'));
			$my_table .= html_writer::end_tag('div');
		}

		$all_courses = get_courses();
		foreach($courses as $id=>$val){
			unset($all_courses[$id]);
		}
		unset($all_courses[1]);

		if(count($all_courses)>0){
			$all_table = html_writer::start_tag('div', array('class' => 'all_courses course-wrapper clearfix'));
			$teacher = user_has_role_assignment($USER->id,3,1);
			foreach ($all_courses as $key => $course) {
				// If moving course, then don't show course which needs to be moved.
				if ($ismovingcourse && ($course->id == $movingcourseid)) {
					continue;
				}
				$all_table .= html_writer::start_tag('div', array('class' => 'course-row clearfix'));
				if(!$teacher){
					$data = $DB->get_record_sql("SELECT
										(SELECT (gg.finalgrade/gg.rawgrademax)*100 FROM  {grade_items} gi, {grade_grades} gg  WHERE gi.itemtype = 'course' AND gg.itemid = gi.id AND gi.courseid=$course->id AND gg.userid=$USER->id) AS average,
										(SELECT count(cm.id) FROM {course_modules} cm, {modules} m WHERE m.id = cm.module AND cm.completion > 0 AND cm.course = $course->id) as modules,
										(SELECT count(cmc.id) FROM {course_modules} cm, {course_modules_completion} cmc, {modules} m WHERE m.id = cm.module AND cm.course = $course->id AND cmc.coursemoduleid = cm.id AND cmc.userid=$USER->id AND cmc.completionstate > 0) as passed,
										(SELECT timecompleted FROM {course_completions} WHERE course=$course->id AND userid=$USER->id) as completed,
										(SELECT name FROM {course_categories} WHERE id = $course->category) as category,
										(SELECT MAX(cmc.coursemoduleid) FROM {course_modules} cm, {course_modules_completion} cmc, {modules} m WHERE m.id = cm.module AND cm.course = $course->id AND cmc.coursemoduleid = cm.id AND cmc.userid=$USER->id AND cmc.completionstate > 0) as prev_module_id,
										(SELECT cm.id FROM {course_modules} cm WHERE cm.id > prev_module_id AND cm.course = $course->id AND cm.completion = 1 LIMIT 1) as curent_module,
										(SELECT MIN(cm.id) FROM {course_modules} cm WHERE cm.course = $course->id LIMIT 1, 1) as first_module
									");

					$progress = ($data->modules and $data->passed) ? ($data->passed / $data->modules) : 0;

					$progress_ico = html_writer::link(new moodle_url('/enrol/index.php',array('id'=>$course->id)),"Enroll",array('class'=>'btn'));
					$module_id = ($data->curent_module)?$data->curent_module:$data->first_module;
					if(!empty($module_id)){
						$modinfo = get_fast_modinfo($course);
						$row = html_writer::link($modinfo->get_cm($module_id)->url,$progress_ico,array('class'=>'prog'));
					}else{
						$row = html_writer::link( new moodle_url('/course/view.php',array('id'=>$course->id)),$progress_ico,array('class'=>'prog'));
					}
				}else{
					$row = html_writer::link( new moodle_url('/course/view.php',array('id'=>$course->id)),get_string('view')	,array('class'=>'btn'));
				}
				$all_table .= html_writer::tag('div', $row,array('class' => 'course_progress'));

				$info = html_writer::start_tag('ul', array('class' => 'course_info clearfix'));

				$info .= ($course->startdate)?html_writer::tag('li', get_string('start_date','theme_schools').date('M d, Y',$course->startdate)):'';
				$info .= (isset($data->completed))?html_writer::tag('li', '<span class="separator">|</span>'.get_string('completed_date','theme_schools').date('m/d/Y',$data->completed)):'';

				$modules = (isset($data->passed))?$data->passed:'-';
				$modules .= ' / ';
				$modules .= ($data->modules)?$data->modules:'-';
				//$info .= html_writer::tag('li', '<span class="separator">|</span>'.get_string('curent_prodress','theme_schools').$modules);
				$info .= html_writer::end_tag('ul');

				$row = html_writer::tag('h3',$course->fullname).$info;
				$all_table .= html_writer::tag('div',$row,array('class'=>'course-content'));

				$data->average = (isset($data->average))?$data->average:0;
				$avg = ($data->average)?$this->schools_get_grade_letter($data->average,$course->id):$this->schools_get_grade_letter(0,$course->id);
				$all_table .= html_writer::tag('div',$avg,array('class'=>'course-grade-number'));

				$row = $this->schools_get_grade_letter($data->average,$course->id);
				$all_table .= html_writer::tag('div',$row,array('class'=>'course-grade'));

				$all_table .= html_writer::end_tag('div');
			}
			$all_table .= html_writer::end_tag('div');
		}else{
			$all_table = html_writer::start_tag('div',array('class'=>'all_courses course-wrapper clearfix'));
			$all_table .= html_writer::tag('div',get_string('nocourses'),array('class'=>'course-row clearfix'));
			$all_table .= html_writer::end_tag('div');
		}
		if(user_has_role_assignment($USER->id,3,1) || is_siteadmin()){
			$grades = $DB->get_records_sql("SELECT gi.courseid, AVG( (gg.finalgrade/gg.rawgrademax)*100 ) AS average, c.fullname,
											(SELECT count(DISTINCT ra.userid) FROM {context} c
												LEFT JOIN {role} r ON r.shortname='student'
												LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
											WHERE c.contextlevel=50 AND c.instanceid= gi.courseid ) as students
												FROM {user_enrolments} ue
													LEFT JOIN {enrol} e ON e.id=ue.enrolid
													LEFT JOIN {grade_items} gi ON gi.courseid=e.courseid
													LEFT JOIN {grade_grades} gg ON gg.itemid=gi.id AND gg.finalgrade IS NOT NULL
													LEFT JOIN {course} c ON c.id=gi.courseid
												WHERE ue.userid=$USER->id GROUP BY gi.courseid");
			if(count($grades)>0){
				$grades_boock = html_writer::start_tag('div',array('class'=>'grades_boock course-wrapper clearfix'));

				foreach($grades as $grade){
					if(empty($grade->courseid))
						continue;
					$grade->average = ($grade->average>0)?$grade->average:0;
					$row = $this->schools_get_grade_letter($grade->average,$grade->courseid);

					$grade->average = ($grade->average>0)?(int)$grade->average.'%':'-';
					$grade->students = ($grade->students>0)?$grade->students:'-';

					$info = html_writer::start_tag('ul', array('class' => 'course_info clearfix'));
					$info .= html_writer::tag('li', get_string('students').': '.$grade->students.'<span class="separator">|</span>');
					$info .= html_writer::tag('li', get_string('average','theme_schools').': '.$grade->average);
					$info .= html_writer::end_tag('ul');

					$row = html_writer::tag('div',$row,array('class'=>'course-grade'));
					$row .= html_writer::tag('div',html_writer::tag('h3',$grade->fullname).$info,array('class'=>'course-content'));
					$row .= html_writer::link( new moodle_url('/grade/report/grader/index.php',array('id'=>$grade->courseid)),get_string('view'),array('class'=>'btn'));
					$grades_boock .= html_writer::tag('div',$row,array('class'=>'course-row clearfix'));
				}

				$grades_boock .= html_writer::end_tag('div');
			}else{
				$grades_boock = html_writer::start_tag('div',array('class'=>'grades_boock course-wrapper clearfix'));
				$grades_boock .= html_writer::tag('div',get_string('nograde'),array('class'=>'course-row clearfix'));
				$grades_boock .= html_writer::end_tag('div');
			}

			/* SELECT
					asb.*,
					ag.*
				FROM `mdl_assign` a
					LEFT JOIN `mdl_assign_submission` asb ON a.id=asb.assignment AND asb.status='submitted'
					LEFT JOIN `mdl_assign_grades` ag ON asb.assignment=ag.assignment AND ag.userid=asb.userid
				WHERE a.course=92 AND ag.id IS NULL AND asb.id IS NOT NULL */

			$where = 'a.course='.implode(' OR a.course=',array_keys($courses));
			$assign_neegd_grad = ''; $all_items = array();
			$siteadmins = get_config('core','siteadmins');
			$assigns = $DB->get_records_sql("SELECT
												cm.id as course_module,
												COUNT(asb.id) as count,
												c.fullname as course_name,
												c.id as course_id,
												a.name as assign_name,
												m.name as module
											FROM {assign} a
												LEFT JOIN {assign_submission} asb ON a.id=asb.assignment AND asb.status='submitted' AND asb.userid NOT IN($siteadmins)
												LEFT JOIN {assign_grades} ag ON asb.assignment=ag.assignment AND ag.userid=asb.userid
												LEFT JOIN {modules} m ON m.name='assign'
												LEFT JOIN {course_modules} cm ON cm.module=m.id AND cm.instance=asb.assignment
												LEFT JOIN {course} c ON c.id=a.course
											WHERE ($where) AND ag.id IS NULL AND asb.id IS NOT NULL GROUP BY asb.assignment");

			$where = 'c.instanceid='.implode(' OR c.instanceid=',array_keys($courses));
			$questions = $DB->get_records_sql(" SELECT aliasqa.id,
														COUNT(aliasqas.id) as count,
														cou.fullname AS course_name,
														c.instanceid AS course_id,
														qz.name AS quiz_name,
														cm.id AS course_module,
														m.name AS module

												FROM {context} c
													JOIN {role} r ON r.shortname='student'
													JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id

													JOIN {quiz} qz ON qz.course=c.instanceid
													JOIN {quiz_attempts} qza ON qza.quiz=qz.id AND qza.userid=ra.userid AND qza.userid NOT IN($siteadmins)
													JOIN {question_attempts} aliasqa ON aliasqa.questionusageid = qza.uniqueid
													JOIN {question_attempt_steps} aliasqas ON aliasqas.questionattemptid = aliasqa.id
																		AND aliasqas.sequencenumber = (
																			SELECT MAX(sequencenumber)
																			FROM {question_attempt_steps}
																			WHERE questionattemptid = aliasqa.id
																		)
													JOIN {modules} m ON m.name='quiz'
													JOIN {course_modules} cm ON cm.module=m.id AND cm.course=c.instanceid AND cm.instance=qz.id
													JOIN {course} cou ON cou.id=c.instanceid

												WHERE aliasqa.questionusageid >0
														AND aliasqa.behaviour = 'manualgraded'
														AND aliasqas.state = 'needsgrading'
														AND c.contextlevel=50 AND ($where)
												 GROUP BY qz.id");

			foreach($assigns as $item){
				$all_items[$item->course_id]['items'][]=$item;
				$all_items[$item->course_id]['course_name']=$item->course_name;
				$all_items[$item->course_id]['count'] += $item->count;
			}

			foreach($questions as $item){
				$all_items[$item->course_id]['items'][]=$item;
				$all_items[$item->course_id]['course_name']=$item->course_name;
				$all_items[$item->course_id]['count'] += $item->count;
			}

			if(!empty($all_items)){
					$assign_need_grad = html_writer::start_tag('div', array('class' => 'assign_need_gradding list course-wrapper clearfix'));
					foreach($all_items as $item){
						$info = html_writer::start_tag('ul', array('class' => 'course-content clearfix'));
							$info .= html_writer::tag('li',html_writer::tag('h3',$item['course_name']));
							$info .= html_writer::tag('li',html_writer::tag('span','',array('class'=>'fa more fa-angle-down')));
						$info .= html_writer::end_tag('ul');

						$info .= html_writer::start_tag('div', array('class' => 'modules_info clearfix'));
							foreach($item['items'] as $module){
								if($module->module == 'quiz')
									$link = html_writer::link( new moodle_url('/mod/quiz/report.php',array('id'=>$module->course_module,'mode'=>'grading')),$module->quiz_name);
								else
									$link = html_writer::link( new moodle_url('/mod/assign/view.php',array('id'=>$module->course_module,'action'=>'grading')),$module->assign_name);

								$info .= html_writer::start_tag('ul', array('class' => 'clearfix'));
									$info .= html_writer::tag('li',html_writer::tag('h3',$link));
									$info .= html_writer::tag('li',get_string('tograde','quiz_grading').' '.$module->count);
								$info .= html_writer::end_tag('ul');
							}
						$info .= html_writer::end_tag('div');
						$assign_need_grad .= html_writer::tag('div', $info, array('class' => 'course-row clearfix'));
					}

				$assign_need_grad .= html_writer::end_tag('div');
			}else{
				$assign_need_grad = html_writer::start_tag('div',array('class'=>'assign_need_gradding course-wrapper clearfix'));
				$assign_need_grad .= html_writer::tag('div',get_string('no').' '.get_string('submissions','core_grades'),array('class'=>'course-row clearfix'));
				$assign_need_grad .= html_writer::end_tag('div');
			}

			$where = 'c.instanceid='.implode(' OR c.instanceid=',array_keys($courses));
			$failing = $DB->get_records_sql("SELECT ra.id, us.id as user_id, us.firstname as user_firstname, us.lastname as user_lastname, cou.id as course_id, cou.fullname as course_name, gg.finalgrade
														FROM {context} c
															LEFT JOIN {role} r ON r.shortname='student'
															LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id

															LEFT JOIN {grade_items} gi ON gi.itemtype = 'course' AND gi.courseid=c.instanceid
															LEFT JOIN {grade_grades} gg ON gg.itemid = gi.id AND gg.userid=ra.userid

															LEFT JOIN {course} cou ON cou.id=gi.courseid
															LEFT JOIN {user} us ON us.id=gg.userid
														WHERE gi.gradepass>gg.finalgrade AND us.id IS NOT NULL AND c.contextlevel=50 AND ($where) ");

			if(!empty($failing)){
				$failing_by_courses = array();
				foreach($failing as $item){
					$failing_by_courses[$item->course_id][] = $item;
				}

				$failing_students = html_writer::start_tag('div', array('class' => 'failing-students list course-wrapper clearfix'));
				foreach($failing_by_courses as $item){
					$info = html_writer::start_tag('ul', array('class' => 'course-content clearfix'));
						$link = html_writer::link( new moodle_url('/grade/report/grader/index.php',array('id'=>$item[0]->course_id)),$item[0]->course_name);
						$info .= html_writer::tag('li',html_writer::tag('h3',$link));
						$info .= html_writer::tag('li',html_writer::tag('span','',array('class'=>'fa more fa-angle-down')));
					$info .= html_writer::end_tag('ul');

					$info .= html_writer::start_tag('div', array('class' => 'users_info clearfix'));
						foreach($item as $user){
							$link = html_writer::link( new moodle_url('/grade/report/user/index.php',array('id'=>$user->course_id,'userid'=>$user->user_id)),$user->user_firstname.' '.$user->user_lastname,array('class'=>'prog'));
							$info .= html_writer::start_tag('ul', array('class' => 'clearfix'));
								$info .= html_writer::tag('li',html_writer::tag('h3',$link));
								$info .= html_writer::tag('li',get_string('grade','core_grades').' '.round($user->finalgrade,2));
							$info .= html_writer::end_tag('ul');
						}
					$info .= html_writer::end_tag('div');
					$failing_students .= html_writer::tag('div', $info, array('class' => 'course-row clearfix'));
				}
				$failing_students .= html_writer::end_tag('div');
			}else{
				$failing_students = html_writer::start_tag('div',array('class'=>'failing-students course-wrapper clearfix'));
				$failing_students .= html_writer::tag('div',get_string('no').' '.get_string('students'),array('class'=>'course-row clearfix'));
				$failing_students .= html_writer::end_tag('div');
			}

			$in = implode(',',array_keys($courses));
			$enrolled = $DB->get_records_sql("SELECT ra.id, us.id as user_id, CONCAT(us.firstname,' ',us.lastname) as user_name, cou.id as course_id, cou.fullname as course_name, ra.timemodified
													FROM {context} c
														LEFT JOIN {role} r ON r.shortname='student'
														LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
														LEFT JOIN {course} cou ON cou.id=c.instanceid
														LEFT JOIN {user} us ON us.id=ra.userid
													WHERE c.contextlevel=50 AND c.instanceid IN ($in)
													GROUP BY us.id,cou.id
												");
			if(!empty($enrolled)){
				$users_by_course = array();
				foreach($enrolled as $item){
					$users_by_course[$item->course_id][] = $item;
				}

				$enrolled_users = html_writer::start_tag('div', array('class' => 'enrolled-users list course-wrapper clearfix'));
				foreach($users_by_course as $item){
					$info = html_writer::start_tag('ul', array('class' => 'course-content clearfix'));
						$info .= html_writer::tag('li',html_writer::tag('h3',$item[0]->course_name));
						$info .= html_writer::tag('li',html_writer::tag('span','',array('class'=>'fa more fa-angle-down')));
					$info .= html_writer::end_tag('ul');

					$info .= html_writer::start_tag('div', array('class' => 'users_info clearfix'));
						foreach($item as $user){
							$link = html_writer::link( new moodle_url('/user/profile.php',array('id'=>$user->user_id)),$user->user_name);
							$info .= html_writer::start_tag('ul', array('class' => 'clearfix'));
								$info .= html_writer::tag('li',html_writer::tag('h3',$link));
								$info .= html_writer::tag('li',get_string('date').': '.date('m/d/Y',$user->timemodified));
							$info .= html_writer::end_tag('ul');
						}
					$info .= html_writer::end_tag('div');
					$enrolled_users .= html_writer::tag('div', $info, array('class' => 'course-row clearfix'));
				}
				$enrolled_users .= html_writer::end_tag('div');
			}else{
				$enrolled_users = html_writer::start_tag('div',array('class'=>'enrolled-users course-wrapper clearfix'));
				$enrolled_users .= html_writer::tag('div',get_string('no').' '.get_string('students'),array('class'=>'course-row clearfix'));
				$enrolled_users .= html_writer::end_tag('div');
			}

			$lastaccess = strtotime("-1 month");
			$inactive = $DB->get_records_sql("SELECT u.id as user_id, CONCAT(u.firstname,' ',u.lastname) as user_name,  u.lastaccess
											FROM {context} c
												LEFT JOIN {role} r ON r.shortname='student'
												LEFT JOIN {role_assignments} ra ON ra.contextid=c.id AND ra.roleid=r.id
												LEFT JOIN {user} u ON ra.userid=u.id AND u.lastaccess<$lastaccess
											WHERE c.contextlevel=50 AND c.instanceid IN ($in) AND u.id IS NOT NULL");

			if(!empty($inactive)){
				$inactive_users = html_writer::start_tag('div', array('class' => 'inactive-users list course-wrapper clearfix'));
				foreach($inactive as $item){
					$link = html_writer::link( new moodle_url('/user/profile.php',array('id'=>$item->user_id)),$item->user_name);
					$lastaccess = ($item->lastaccess>0)?date('m/d/Y',$item->lastaccess):' - ';
					$info = html_writer::start_tag('ul', array('class' => 'course-content clearfix'));
						$info .= html_writer::tag('li',html_writer::tag('h3',$link));
						$info .= html_writer::tag('li',get_string('lastaccess').': '.$lastaccess);
					$info .= html_writer::end_tag('ul');
					$inactive_users .= html_writer::tag('div', $info, array('class' => 'course-row clearfix'));
				}
				$inactive_users .= html_writer::end_tag('div');
			}else{
				$inactive_users = html_writer::start_tag('div',array('class'=>'inactive-users course-wrapper clearfix'));
				$inactive_users .= html_writer::tag('div',get_string('no').' '.get_string('students'),array('class'=>'course-row clearfix'));
				$inactive_users .= html_writer::end_tag('div');
			}

			$now = time();
			$events = $DB->get_records_sql("SELECT e.id,e.name,e.timestart,c.id as course_id, c.fullname as course_name
											FROM {event} e
												LEFT JOIN {course} c ON c.id=e.courseid
											WHERE e.timestart>$now AND e.courseid IN (1,$in) ORDER BY e.timestart");

			if(!empty($events)){
				$progress_ico = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
											 width="50px" height="50px" viewBox="0 0 800 800" enable-background="new 0 0 800 800" xml:space="preserve">
										<circle fill="transparent" stroke="#DCE4EA" stroke-width="60" stroke-miterlimit="10" cx="50%" cy="50%" r="40%" style="stroke-dasharray: 260%, 0%;"/>
										<circle fill="transparent" stroke="#346A96" stroke-width="60" stroke-dashoffset="62%" stroke-miterlimit="10" cx="50%" cy="50%" r="40%" style="stroke-dasharray: 250%, 0%;"/>
										</svg>';
				$progress_ico .= '<i class="caret-right"></i>';

					$event_list = html_writer::start_tag('div', array('class' => 'event-list course-wrapper clearfix'));
					foreach ($events as $event) {
						$event_list .= html_writer::start_tag('div', array('class' => 'course-row clearfix'));
							$row = html_writer::link( new moodle_url('/calendar/view.php',array('view'=>'month','time'=>$event->timestart)),$progress_ico,array('class'=>'prog'));
							$event_list .= html_writer::tag('div', $row,array('class' => 'course_progress'));

							$info = html_writer::start_tag('ul', array('class' => 'course_info clearfix'));
							$info .= html_writer::tag('li', $event->course_name);
							$info .= html_writer::end_tag('ul');

							$row = html_writer::tag('h3',$event->name).$info;
							$event_list .= html_writer::tag('div',$row,array('class'=>'course-content'));

							$info = html_writer::start_tag('ul', array('class' => 'clearfix'));
							$info .= html_writer::tag('li',get_string('date').': '.date('m/d/Y',$event->timestart));
							$info .= html_writer::end_tag('ul');

							$event_list .= html_writer::tag('div',$info,array('class'=>'additional_info'));
						$event_list .= html_writer::end_tag('div');
					}
				$event_list .= html_writer::end_tag('div');
			}else{
				$event_list = html_writer::start_tag('div',array('class'=>'event-list course-wrapper clearfix'));
				$event_list .= html_writer::tag('div',get_string('eventnone','core_calendar'),array('class'=>'course-row clearfix'));
				$event_list .= html_writer::end_tag('div');
			}
			//print_object(strtotime("-1 month"));
		}else{
			if(count($courses)>0){
				$grades_table = html_writer::start_tag('div', array('class' => 'course_grades course-wrapper clearfix'));
				$now = time();
				foreach($courses as $course){
					$course_info = $DB->get_record_sql("SELECT
															(SELECT timecompleted FROM {course_completions} WHERE course = $course->id AND userid=$USER->id) as completed,
															(SELECT (gg.finalgrade/gg.rawgrademax)*100 FROM {grade_grades} gg, {grade_items} gi WHERE gg.itemid=gi.id AND gg.userid= $USER->id AND gi.courseid= $course->id AND gi.itemtype='course') as curent_grade,
															(SELECT gg.rawgrademax FROM {grade_grades} gg, {grade_items} gi WHERE gg.itemid=gi.id AND gg.userid= $USER->id AND gi.courseid= $course->id AND gi.itemtype='course') as max_grade,
															(SELECT COUNT(DISTINCT cm.id)
																FROM {course_modules} cm
																	LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid=cm.id AND cmc.userid=$USER->id
																WHERE cm.course = $course->id AND cm.visible=1 AND cm.completionexpected<$now AND cm.completionexpected>0 AND (cmc.id IS NULL OR cmc.completionstate=0)) as assign_missing

															");
					/* 					$course_info = $DB->get_record_sql("SELECT
															(SELECT timecompleted FROM {course_completions} WHERE course = $course->id AND userid=$USER->id) as completed,
															(SELECT (gg.finalgrade/gg.rawgrademax)*100 FROM {grade_grades} gg, {grade_items} gi WHERE gg.itemid=gi.id AND gg.userid= $USER->id AND gi.courseid= $course->id AND gi.itemtype='course') as curent_grade,
															(SELECT gg.rawgrademax FROM {grade_grades} gg, {grade_items} gi WHERE gg.itemid=gi.id AND gg.userid= $USER->id AND gi.courseid= $course->id AND gi.itemtype='course') as max_grade,
															(SELECT SUM(IF(gg.finalgrade<gi.gradepass,1,0)) AS assign_missing
																FROM {assign} a
																	LEFT JOIN {grade_items} as gi ON gi.courseid=a.course AND gi.itemtype='mod' AND gi.itemmodule='assign' AND gi.iteminstance=a.id
																	LEFT JOIN {grade_grades} as gg ON gg.itemid=gi.id AND gg.userid=$USER->id
																WHERE a.course= $course->id AND gg.finalgrade IS NOT NULL) as assign_missing

															"); */

					$curent_grade = ($course_info->curent_grade)?$this->schools_get_grade_letter($course_info->curent_grade,$course->id):'-';
					$curent_grade .= '/'.$this->schools_get_grade_letter(100,$course->id);
					$completed = ($course_info->completed)?get_string('completed','theme_schools'):get_string('inprogress','theme_schools');
					$final_grade = ($course_info->completed)?$curent_grade:'-';
					$link = html_writer::link( new moodle_url('/course/view.php',array('id'=>$course->id)),get_string('activity_missing','theme_schools',$course_info->assign_missing));
					$assign_missing = ($course_info->assign_missing>0)?html_writer::tag('li',$link,array('class'=>'missing')):'';

					$info = html_writer::start_tag('ul', array('class' => 'course-content clearfix'));
						$info .= html_writer::tag('li',html_writer::tag('h3',$course->fullname));
						$info .= html_writer::tag('li',html_writer::tag('span','',array('class'=>'fa more fa-angle-down')));
						$info .= html_writer::tag('li',get_string('final_grade','theme_schools',$final_grade));
						//$info .= html_writer::tag('li',get_string('course_status','theme_schools',$completed));
						$info .= html_writer::tag('li',get_string('curent_grade','theme_schools',$curent_grade));
						$info .= $assign_missing;
					$info .= html_writer::end_tag('ul');

					$gtree = new grade_tree($course->id, false, false);
					$info .= $this->print_category($gtree->top_element,$course);
					$grades_table .= html_writer::tag('div', $info, array('class' => 'course-row clearfix'));

				}
				$grades_table .= html_writer::end_tag('div');

			}else{
				$grades_table = html_writer::tag('div',get_string('nograde'),array('class'=>'course_grades course-wrapper alert alert-info'));
				$grades_table = html_writer::start_tag('div',array('class'=>'course_grades course-wrapper clearfix'));
				$grades_table .= html_writer::tag('div',get_string('nograde'),array('class'=>'course-row clearfix'));
				$grades_table .= html_writer::end_tag('div');
			}
		}
		$html .= html_writer::start_tag('div',array('class'=>'dashboard-menu table_nav'));
		$html .= (isset($my_table))?html_writer::tag('label',get_string('mycourses'),array('class'=>'active','for'=>'my_courses')):'';
		$html .= (isset($my_teach_table))?html_writer::tag('label',get_string('myteachcourses','theme_schools'),array('class'=>'active','for'=>'my_teach_courses')):'';
		$html .= (isset($all_table))?html_writer::tag('label',get_string('fulllistofcourses'),array('for'=>'all_courses')):'';
		$html .= (isset($grades_table))?html_writer::tag('label',get_string('report_card','theme_schools'),array('for'=>'course_grades')):'';
		$html .= (isset($grades_boock))?html_writer::tag('label',get_string('grades_boock','theme_schools'),array('for'=>'grades_boock')):"";
		$html .= (isset($assign_need_grad))?html_writer::tag('label',get_string('subm_need_grading','theme_schools'),array('for'=>'assign_need_gradding','class'=>'hidden')):"";
		$html .= (isset($failing_students))?html_writer::tag('label',get_string('failuere_users','theme_schools'),array('for'=>'failing-students','class'=>'hidden')):"";
		$html .= (isset($enrolled_users))?html_writer::tag('label',get_string('enroled_users','theme_schools'),array('for'=>'enrolled-users','class'=>'hidden')):"";
		$html .= (isset($inactive_users))?html_writer::tag('label',get_string('inactive_users','theme_schools'),array('for'=>'inactive-users','class'=>'hidden')):"";
		$html .= (isset($event_list))?html_writer::tag('label',get_string('upcoming_events','theme_schools'),array('for'=>'event-list','class'=>'hidden')):"";
		$html .= html_writer::end_tag('div');

		$html .= (isset($my_table))?$my_table:'';
		$html .= (isset($my_teach_table))?$my_teach_table:'';
		$html .= (isset($all_table))?$all_table:'';
		$html .= (isset($grades_boock))?$grades_boock:'';
		$html .= (isset($grades_table))?$grades_table:'';
		$html .= (isset($assign_need_grad))?$assign_need_grad:'';
		$html .= (isset($failing_students))?$failing_students:'';
		$html .= (isset($enrolled_users))?$enrolled_users:'';
		$html .= (isset($inactive_users))?$inactive_users:'';
		$html .= (isset($event_list))?$event_list:'';

        // Wrap course list in a div and return.
        return html_writer::tag('div', $html, array('class' => 'course_list'));
    }

	private function print_category($element,$course){
		global $DB, $USER;
		$courseid = ($course->id)?$course->id:$course;
		$info = html_writer::start_tag('div', array('class' => 'modules_info clearfix'));
		foreach($element['children'] as $item){
			if($item['object']->itemtype == 'mod' && !$item['object']->hidden){
				//print_object($item);
				$last_submit = new stdClass();
				$grade = $DB->get_record_sql("SELECT (gg.finalgrade/gg.rawgrademax)*100 AS scale, gg.rawgrademax, gg.finalgrade,gg.timemodified, cm.completionexpected
												FROM {grade_grades} gg
													JOIN {modules} m ON m.name='".$item['object']->itemmodule."'
													LEFT JOIN {course_modules} cm ON cm.module=m.id AND cm.instance=".$item['object']->iteminstance."
												WHERE gg.userid = $USER->id AND gg.itemid=".$item['object']->id);
				if($item['object']->itemmodule == 'quiz'){
					$last_submit = $DB->get_record_sql("SELECT id,timefinish AS time FROM {quiz_attempts} WHERE quiz=".$item['object']->iteminstance." AND userid=$USER->id AND state='finished' ORDER BY timefinish LIMIT 1");
				}elseif($item['object']->itemmodule == 'assign'){
					$last_submit = $DB->get_record_sql("SELECT id,timecreated AS time FROM {assign_submission} WHERE assignment=".$item['object']->iteminstance." AND userid=$USER->id AND status='submitted' ORDER BY timecreated LIMIT 1");
				}
				@$mod_grade = ($grade->scale)?$this->schools_get_grade_letter($grade->scale,$courseid):'-';
				//@$timemodified = ($last_submit->time)?date('m/d/Y',$last_submit->time):'-';
				//@$timemodified = ($grade->timemodified)?date('m/d/Y',$grade->timemodified):'-';
				$timemodified = '';
				if($last_submit->time)
					$timemodified = get_string('mod_time_submited', 'theme_schools', date('m/d/Y',$last_submit->time));
				elseif($grade->timemodified)
					$timemodified = html_writer::tag('li',get_string('mod_time_completed', 'theme_schools', date('m/d/Y',$grade->timemodified)));

				$structure = new grade_structure();
				$structure->modinfo = get_fast_modinfo($course);
				//print_object($structure->get_element_header($item, true, false, false, false, true));

				$info .= html_writer::start_tag('ul', array('class' => 'module clearfix'));
					$info .= html_writer::tag('li',html_writer::tag('h3',$structure->get_element_header($item, true, false, false, false, true)));
					//$info .= html_writer::tag('li',get_string('mod_time_submited', 'theme_schools', $timemodified));
					$info .= (!empty($timemodified))?html_writer::tag('li',$timemodified):'';
					$info .= html_writer::tag('li',get_string('mod_grade','theme_schools',$mod_grade));
					if($grade->completionexpected>0){
						$info .= html_writer::tag('li',get_string('completed_date', 'theme_schools').' '.date('m/d/Y',$grade->completionexpected));
					}
				$info .= html_writer::end_tag('ul');
			}

			if($item['object'] instanceof grade_category){
				$grade = $DB->get_record_sql("SELECT (gg.finalgrade/gg.rawgrademax)*100 AS scale, gg.finalgrade,gg.timemodified, gi.hidden
												FROM {grade_items} as gi
													LEFT JOIN {grade_grades} gg ON gg.userid = $USER->id AND gg.itemid=gi.id
												WHERE gi.itemtype='category' AND gi.iteminstance=".$item['object']->id);
				@$mod_grade = ($grade->scale)?$this->schools_get_grade_letter($grade->scale,$courseid):'-';
				//@$timemodified = ($grade->timemodified)?date('m/d/Y',$grade->timemodified):'-';

				$structure = new grade_structure();
				$structure->modinfo = get_fast_modinfo($course);

				$info .= html_writer::start_tag('ul', array('class' => 'module clearfix'));
					$info .= html_writer::tag('li',html_writer::tag('h3',$structure->get_element_header($item, true, false, false, false, true)));
				$info .= html_writer::end_tag('ul');
				$info .= $this->print_category($item,$item['object']->courseid);
				if(!$grade->hidden){
					$info .= html_writer::start_tag('ul', array('class' => 'category clearfix'));
						$info .= html_writer::tag('li',html_writer::tag('h3',get_string('total')));
						//$info .= html_writer::tag('li',get_string('mod_time_completed', 'theme_schools', $timemodified));
						$info .= html_writer::tag('li',get_string('mod_grade','theme_schools',$mod_grade));
					$info .= html_writer::end_tag('ul');
				}
			}
		}
		$info .= html_writer::end_tag('div');
		return $info;
	}

	private function schools_get_grade_letter($grade=0,$courseid){
		$context = context_course::instance($courseid,IGNORE_MISSING);
		$letters = grade_get_letters($context);
		foreach($letters as $lowerboundary=>$value){
			if($grade >= $lowerboundary){
				$class = 'g'.str_replace('.','-',$value);
				$row = html_writer::tag('span',$value,array('class'=>'grade '.$class));
				break;
			}
		}
		return $row;
	}

}


class theme_schools_core_user_myprofile_renderer extends core_user\output\myprofile\renderer{

    public function render_node(core_user\output\myprofile\node $node) {
		global $USER, $DB, $PAGE, $CFG;
        $return = '';
		$content = $node->content;
		if($node->parentcat == 'badges'){
			$userid = required_param('id',PARAM_INT);
			$badges = badges_get_user_badges($userid);
			//print_object($badges);
			foreach ($badges as $badge) {
				$context = ($badge->type == BADGE_TYPE_SITE) ? context_system::instance() : context_course::instance($badge->courseid);
				$bname = $badge->name;
				$imageurl = moodle_url::make_pluginfile_url($context->id, 'badges', 'badgeimage', $badge->id, '/', 'f1', false);

				$name = html_writer::tag('span', $bname, array('class' => 'badge-name'));

				$course_name = $DB->get_record_sql("SELECT fullname FROM {course} WHERE id=".$badge->courseid)->fullname;

				$course_name = html_writer::tag('p', $course_name, array('class' => 'cname'));

				$image = html_writer::empty_tag('img', array('src' => $imageurl, 'class' => 'badge-image'));
				if (!empty($badge->dateexpire) && $badge->dateexpire < time()) {
					$image .= $this->output->pix_icon('i/expired',
							get_string('expireddate', 'badges', userdate($badge->dateexpire)),
							'moodle',
							array('class' => 'expireimage'));
					$name .= '(' . get_string('expired', 'badges') . ')';
				}


				$url = new moodle_url('/badges/badge.php', array('hash' => $badge->uniquehash));

				$items[] = $course_name . html_writer::link($url, $image . $name, array('title' => $bname));
			}

			$content = html_writer::alist($items, array('class' => 'badges'));
		}
        if (is_object($node->url)) {
            $header = \html_writer::link($node->url, $node->title);
        } else {
            $header = $node->title;
        }
        $icon = $node->icon;
        if (!empty($icon)) {
            $header .= $this->render($icon);
        }

        $classes = $node->classes;
        if (!empty($content)) {
            // There is some content to display below this make this a header.
            $return = \html_writer::tag('dt', $header);
			$class = ($node->parentcat == 'badges')?array('class'=>'badges'):array();
            $return .= \html_writer::tag('dd', $content, $class);

            $return = \html_writer::tag('dl', $return);
            if ($classes) {
                $return = \html_writer::tag('li', $return, array('class' => 'contentnode ' . $classes));
            } else {
                $return = \html_writer::tag('li', $return, array('class' => 'contentnode'));
            }
        } else {
            $return = \html_writer::span($header);
            $return = \html_writer::tag('li', $return, array('class' => $classes));
        }

        return $return;
    }
}

class theme_schools_core_user_renderer extends core_user_renderer{
	public function user_search($url, $firstinitial, $lastinitial, $usercount, $totalcount, $heading = null) {
        global $OUTPUT,$PAGE,$CFG,$COURSE;


        $strall = get_string('all');
        $alpha  = explode(',', get_string('alphabet', 'langconfig'));

        if (!isset($heading)) {
            $heading = get_string('allparticipants');
        }

        $content = html_writer::start_tag('form', array('action' => new moodle_url($url)));
        $content .= html_writer::start_tag('div');

        // Search utility heading.
        $content .= $OUTPUT->heading($heading.get_string('labelsep', 'langconfig').$usercount.'/'.$totalcount, 3);

        // Bar of first initials.
        $content .= html_writer::start_tag('div', array('class' => 'initialbar firstinitial'));
        $content .= html_writer::label(get_string('firstname').' : ', null);

        if (!empty($firstinitial)) {
            $content .= html_writer::link($url.'&sifirst=', $strall);
        } else {
            $content .= html_writer::tag('strong', $strall);
        }

        foreach ($alpha as $letter) {
            if ($letter == $firstinitial) {
                $content .= html_writer::tag('strong', $letter);
            } else {
                $content .= html_writer::link($url.'&sifirst='.$letter, $letter);
            }
        }
        $content .= html_writer::end_tag('div');

         // Bar of last initials.
        $content .= html_writer::start_tag('div', array('class' => 'initialbar lastinitial'));
        $content .= html_writer::label(get_string('lastname').' : ', null);

        if (!empty($lastinitial)) {
            $content .= html_writer::link($url.'&silast=', $strall);
        } else {
            $content .= html_writer::tag('strong', $strall);
        }

        foreach ($alpha as $letter) {
            if ($letter == $lastinitial) {
                $content .= html_writer::tag('strong', $letter);
            } else {
                $content .= html_writer::link($url.'&silast='.$letter, $letter);
            }
        }
        $content .= html_writer::end_tag('div');

        $content .= html_writer::end_tag('div');
        $content .= html_writer::tag('div', '&nbsp;');
        $content .= html_writer::end_tag('form');

		if($PAGE->pagetype == 'grade-report-grader-index'){
			$link = html_writer::link(new moodle_url('/grade/edit/tree/index.php',array('id'=>$COURSE->id)),get_string('categoriesanditems','core_grades'),array('class'=>'btn categories'));
			$content .= html_writer::tag('script',"$('div.urlselect').append('$link'); $('div.urlselect').addClass('clearfix');");
		}

        return $content;
    }
}


class theme_schools_core_files_renderer extends core_files_renderer {

    public function files_tree_viewer(file_info $file_info, array $options = null) {
        $tree = new files_tree_viewer($file_info, $options);
        return $this->render($tree);
    }

    public function render_files_tree_viewer(files_tree_viewer $tree) {
        $html = $this->output->heading_with_help(get_string('coursefiles'), 'courselegacyfiles', 'moodle');

        $html .= $this->output->container_start('coursefilesbreadcrumb');
        foreach($tree->path as $path) {
            $html .= $path;
            $html .= ' / ';
        }
        $html .= $this->output->container_end();

        $html .= $this->output->box_start();
        $table = new html_table();
        $table->head = array(get_string('name'), get_string('lastmodified'), get_string('size', 'repository'), get_string('type', 'repository'));
        $table->align = array('left', 'left', 'left', 'left');
        $table->width = '100%';
        $table->data = array();

        foreach ($tree->tree as $file) {
            $filedate = $filesize = $filetype = '';
            if ($file['filedate']) {
                $filedate = userdate($file['filedate'], get_string('strftimedatetimeshort', 'langconfig'));
            }
            if (empty($file['isdir'])) {
                if ($file['filesize']) {
                    $filesize = display_size($file['filesize']);
                }
                $fileicon = file_file_icon($file, 24);
                $filetype = get_mimetype_description($file);
            } else {
                $fileicon = file_folder_icon(24);
            }
            $table->data[] = array(
                html_writer::link($file['url'], $this->output->pix_icon($fileicon, get_string('icon')) . ' ' . $file['filename']),
                $filedate,
                $filesize,
                $filetype
                );
        }

        $html .= html_writer::table($table);
        $html .= $this->output->single_button(new moodle_url('/files/coursefilesedit.php', array('contextid'=>$tree->context->id)), get_string('coursefilesedit'), 'get');
        $html .= $this->output->box_end();
        return $html;
    }

    public function render_form_filemanager($fm) {
        $html = $this->fm_print_generallayout($fm);
        $module = array(
            'name'=>'form_filemanager',
            'fullpath'=>'/lib/form/filemanager.js',
            'requires' => array('moodle-core-notification-dialogue', 'core_filepicker', 'base', 'io-base', 'node', 'json', 'core_dndupload', 'panel', 'resize-plugin', 'dd-plugin'),
            'strings' => array(
                array('error', 'moodle'), array('info', 'moodle'), array('confirmdeletefile', 'repository'),
                array('draftareanofiles', 'repository'), array('entername', 'repository'), array('enternewname', 'repository'),
                array('invalidjson', 'repository'), array('popupblockeddownload', 'repository'),
                array('unknownoriginal', 'repository'), array('confirmdeletefolder', 'repository'),
                array('confirmdeletefilewithhref', 'repository'), array('confirmrenamefolder', 'repository'),
                array('confirmrenamefile', 'repository'), array('newfolder', 'repository'), array('edit', 'moodle')
            )
        );
        if ($this->page->requires->should_create_one_time_item_now('core_file_managertemplate')) {
            $this->page->requires->js_init_call('M.form_filemanager.set_templates',
                    array($this->filemanager_js_templates()), true, $module);
        }
        $this->page->requires->js_init_call('M.form_filemanager.init', array($fm->options), true, $module);

        // non javascript file manager
        $html .= '<noscript>';
        $html .= "<div><object type='text/html' data='".$fm->get_nonjsurl()."' height='160' width='600' style='border:1px solid #000'></object></div>";
        $html .= '</noscript>';


        return $html;
    }

    private function fm_print_generallayout($fm) {
        global $OUTPUT, $context;
        $options = $fm->options;
        $client_id = $options->client_id;
        $straddfile  = get_string('addfile', 'repository');
        $strmakedir  = get_string('makeafolder', 'moodle');
        $strdownload = get_string('downloadfolder', 'repository');
        $strloading  = get_string('loading', 'repository');
        $strdroptoupload = get_string('droptoupload', 'moodle');
        $icon_progress = $OUTPUT->pix_icon('i/loading_small', $strloading).'';
        $restrictions = $this->fm_print_restrictions($fm);
        $strdndnotsupported = get_string('dndnotsupported_insentence', 'moodle').$OUTPUT->help_icon('dndnotsupported');
        $strdndenabledinbox = get_string('dndenabled_inbox', 'moodle');
        $loading = get_string('loading', 'repository');
        $straddfiletext = get_string('addfiletext', 'repository');
        $strcreatefolder = get_string('createfolder', 'repository');
        $strdownloadallfiles = get_string('downloadallfiles', 'repository');



		$repo_instances = repository::get_instances();
		$repositories = '';
		foreach($repo_instances as $instance){
			if(!$instance->is_visible() || $instance->id == 1 || $instance->id == 8)continue;
			$meta = $instance->get_meta();
			$repositories .= '<div class="fp-btn-additional" data-id="'.$meta->id.'">
                    <a role="button" title="' . $meta->name . '" href="#">
                        <img src="' . $meta->icon . '" alt="' . $meta->name . '" />
                    </a>
                </div>';
		}
		$repositories .= '<script>
			$(".fp-toolbar .fp-btn-additional").click(function(){
				$(\'.fp-btn-add\').click();
				$("li[id$=-"+$(this).attr("data-id")+"]").click();
			});
		</script>';

        $html = '
<div id="filemanager-'.$client_id.'" class="filemanager fm-loading">
    <div class="fp-restrictions">
        '.$restrictions.'
        <span class="dnduploadnotsupported-message"> - '.$strdndnotsupported.' </span>
    </div>
    <div class="fp-navbar">
        <div class="filemanager-toolbar">
            <div class="fp-toolbar">
                <div class="fp-btn-add">
                    <a role="button" title="' . $straddfile . '" href="#">
                        <img src="' . $this->pix_url('a/add_file') . '" alt="' . $straddfiletext . '" />
                    </a>
                </div>
                <div class="fp-btn-mkdir">
                    <a role="button" title="' . $strmakedir . '" href="#">
                        <img src="' . $this->pix_url('a/create_folder') . '" alt="' . $strcreatefolder . '" />
                    </a>
                </div>
				'.$repositories.'
                <div class="fp-btn-download">
                    <a role="button" title="' . $strdownload . '" href="#">
                        <img src="' . $this->pix_url('a/download_all').'" alt="' . $strdownloadallfiles . '" />
                    </a>
                </div>
                <img class="fp-img-downloading" src="'.$this->pix_url('i/loading_small').'" alt="" />
            </div>
            <div class="fp-viewbar">
                <a title="'. get_string('displayicons', 'repository') .'" class="fp-vb-icons" href="#">
                    <img alt="'. get_string('displayasicons', 'repository') .'" src="'. $this->pix_url('fp/view_icon_active', 'theme') .'" />
                </a>
                <a title="'. get_string('displaydetails', 'repository') .'" class="fp-vb-details" href="#">
                    <img alt="'. get_string('displayasdetails', 'repository') .'" src="'. $this->pix_url('fp/view_list_active', 'theme') .'" />
                </a>
                <a title="'. get_string('displaytree', 'repository') .'" class="fp-vb-tree" href="#">
                    <img alt="'. get_string('displayastree', 'repository') .'" src="'. $this->pix_url('fp/view_tree_active', 'theme') .'" />
                </a>
            </div>
        </div>
        <div class="fp-pathbar">
            <span class="fp-path-folder"><a class="fp-path-folder-name" href="#"></a></span>
        </div>
    </div>
    <div class="filemanager-loading mdl-align">'.$icon_progress.'</div>
    <div class="filemanager-container" >
        <div class="fm-content-wrapper">
            <div class="fp-content"></div>
            <div class="fm-empty-container">
                <div class="dndupload-message">'.$strdndenabledinbox.'<br/><div class="dndupload-arrow"></div></div>
            </div>
            <div class="dndupload-target">'.$strdroptoupload.'<br/><div class="dndupload-arrow"></div></div>
            <div class="dndupload-progressbars"></div>
            <div class="dndupload-uploadinprogress">'.$icon_progress.'</div>
        </div>
        <div class="filemanager-updating">'.$icon_progress.'</div>
    </div>
</div>';
        return $html;
    }

    private function fm_js_template_iconfilename() {
        $rv = '
<div class="fp-file">
    <a href="#">
    <div style="position:relative;">
        <div class="fp-thumbnail"></div>
        <div class="fp-reficons1"></div>
        <div class="fp-reficons2"></div>
    </div>
    <div class="fp-filename-field">
        <div class="fp-filename"></div>
    </div>
    </a>
    <a class="fp-contextmenu" href="#">'.$this->pix_icon('i/menu', '▶').'</a>
</div>';
        return $rv;
    }

    private function fm_js_template_listfilename() {
        $rv = '
<span class="fp-filename-icon">
    <a href="#">
    <span class="fp-icon"></span>
    <span class="fp-reficons1"></span>
    <span class="fp-reficons2"></span>
    <span class="fp-filename"></span>
    </a>
    <a class="fp-contextmenu" href="#" onclick="return false;">'.$this->pix_icon('i/menu', '▶').'</a>
</span>';
        return $rv;
    }

    private function fm_js_template_mkdir() {
        $rv = '
<div class="filemanager fp-mkdir-dlg" role="dialog" aria-live="assertive" aria-labelledby="fp-mkdir-dlg-title">
    <div class="fp-mkdir-dlg-text">
        <label id="fp-mkdir-dlg-title">' . get_string('newfoldername', 'repository') . '</label><br/>
        <input type="text" />
    </div>
    <button class="fp-dlg-butcreate btn-primary btn">'.get_string('makeafolder').'</button>
    <button class="fp-dlg-butcancel btn-cancel btn">'.get_string('cancel').'</button>
</div>';
        return $rv;
    }

    private function fm_js_template_message() {
        return $this->fp_js_template_message();
    }

    private function fm_js_template_fileselectlayout() {
        global $OUTPUT;
        $strloading  = get_string('loading', 'repository');
        $iconprogress = $this->pix_icon('i/loading_small', $strloading).'';
        $rv = '
<div class="filemanager fp-select">
    <div class="fp-select-loading">
        <img src="'.$this->pix_url('i/loading_small').'" />
    </div>
    <form class="form-horizontal">
        <button class="fp-file-download">'.get_string('download').'</button>
        <button class="fp-file-delete">'.get_string('delete').'</button>
        <button class="fp-file-setmain">'.get_string('setmainfile', 'repository').'</button>
        <span class="fp-file-setmain-help">'.$OUTPUT->help_icon('setmainfile', 'repository').'</span>
        <button class="fp-file-zip">'.get_string('zip', 'editor').'</button>
        <button class="fp-file-unzip">'.get_string('unzip').'</button>
        <div class="fp-hr"></div>

        <div class="fp-forminset">
                <div class="fp-saveas control-group clearfix">
                    <label class="control-label">'.get_string('name', 'repository').'</label>
                    <div class="controls">
                        <input type="text"/>
                    </div>
                </div>
                <div class="fp-author control-group clearfix">
                    <label class="control-label">'.get_string('author', 'repository').'</label>
                    <div class="controls">
                        <input type="text"/>
                    </div>
                </div>
                <div class="fp-license control-group clearfix">
                    <label class="control-label">'.get_string('chooselicense', 'repository').'</label>
                    <div class="controls">
                        <select></select>
                    </div>
                </div>
                <div class="fp-path control-group clearfix">
                    <label class="control-label">'.get_string('path', 'repository').'</label>
                    <div class="controls">
                        <select></select>
                    </div>
                </div>
                <div class="fp-original control-group clearfix">
                    <label class="control-label">'.get_string('original', 'repository').'</label>
                    <div class="controls">
                        <span class="fp-originloading">'.$iconprogress.' '.$strloading.'</span><span class="fp-value"></span>
                    </div>
                </div>
                <div class="fp-reflist control-group clearfix">
                    <label class="control-label">'.get_string('referenceslist', 'repository').'</label>
                    <div class="controls">
                        <p class="fp-refcount"></p>
                        <span class="fp-reflistloading">'.$iconprogress.' '.$strloading.'</span>
                        <ul class="fp-value"></ul>
                    </div>
                </div>
        </div>
        <div class="fp-select-buttons">
            <button class="fp-file-update btn-primary btn">'.get_string('update', 'moodle').'</button>
            <button class="fp-file-cancel btn-cancel btn">'.get_string('cancel').'</button>
        </div>
    </form>
    <div class="fp-info clearfix">
        <div class="fp-hr"></div>
        <p class="fp-thumbnail"></p>
        <div class="fp-fileinfo">
            <div class="fp-datemodified">'.get_string('lastmodified', 'repository').' <span class="fp-value"></span></div>
            <div class="fp-datecreated">'.get_string('datecreated', 'repository').' <span class="fp-value"></span></div>
            <div class="fp-size">'.get_string('size', 'repository').' <span class="fp-value"></span></div>
            <div class="fp-dimensions">'.get_string('dimensions', 'repository').' <span class="fp-value"></span></div>
        </div>
    </div>
</div>';
        return $rv;
    }

    private function fm_js_template_confirmdialog() {
        $rv = '
<div class="filemanager fp-dlg">
    <div class="fp-dlg-text"></div>
    <button class="fp-dlg-butconfirm btn-primary btn">'.get_string('ok').'</button>
    <button class="fp-dlg-butcancel btn-cancel btn">'.get_string('cancel').'</button>
</div>';
        return $rv;
    }

    public function filemanager_js_templates() {
        $class_methods = get_class_methods($this);
        $templates = array();
        foreach ($class_methods as $method_name) {
            if (preg_match('/^fm_js_template_(.*)$/', $method_name, $matches))
            $templates[$matches[1]] = $this->$method_name();
        }
        return $templates;
    }

    private function fm_print_restrictions($fm) {
        $maxbytes = display_size($fm->options->maxbytes);
        $strparam = (object) array('size' => $maxbytes, 'attachments' => $fm->options->maxfiles,
            'areasize' => display_size($fm->options->areamaxbytes));
        $hasmaxfiles = !empty($fm->options->maxfiles) && $fm->options->maxfiles > 0;
        $hasarealimit = !empty($fm->options->areamaxbytes) && $fm->options->areamaxbytes != -1;
        if ($hasmaxfiles && $hasarealimit) {
            $maxsize = get_string('maxsizeandattachmentsandareasize', 'moodle', $strparam);
        } else if ($hasmaxfiles) {
            $maxsize = get_string('maxsizeandattachments', 'moodle', $strparam);
        } else if ($hasarealimit) {
            $maxsize = get_string('maxsizeandareasize', 'moodle', $strparam);
        } else {
            $maxsize = get_string('maxfilesize', 'moodle', $maxbytes);
        }
        // TODO MDL-32020 also should say about 'File types accepted'
        return '<span>'. $maxsize . '</span>';
    }

    private function fp_js_template_generallayout() {
        $rv = '
<div tabindex="0" class="file-picker fp-generallayout" role="dialog" aria-live="assertive">
    <div class="fp-repo-area">
        <ul class="fp-list">
            <li class="fp-repo">
                <a href="#"><img class="fp-repo-icon" alt=" " width="16" height="16" />&nbsp;<span class="fp-repo-name"></span></a>
            </li>
        </ul>
    </div>
    <div class="fp-repo-items" tabindex="0">
        <div class="fp-navbar">
            <div>
                <div class="fp-toolbar">
                    <div class="fp-tb-back">
                        <a href="#">'.get_string('back', 'repository').'</a>
                    </div>
                    <div class="fp-tb-search">
                        <form></form>
                    </div>
                    <div class="fp-tb-refresh">
                        <a title="'. get_string('refresh', 'repository') .'" href="#">
                            <img alt=""  src="'.$this->pix_url('a/refresh').'" />
                        </a>
                    </div>
                    <div class="fp-tb-logout">
                        <a title="'. get_string('logout', 'repository') .'" href="#">
                            <img alt="" src="'.$this->pix_url('a/logout').'" />
                        </a>
                    </div>
                    <div class="fp-tb-manage">
                        <a title="'. get_string('settings', 'repository') .'" href="#">
                            <img alt="" src="'.$this->pix_url('a/setting').'" />
                        </a>
                    </div>
                    <div class="fp-tb-help">
                        <a title="'. get_string('help', 'repository') .'" href="#">
                            <img alt="" src="'.$this->pix_url('a/help').'" />
                        </a>
                    </div>
                    <div class="fp-tb-message"></div>
                </div>
                <div class="fp-viewbar">
                    <a role="button" title="'. get_string('displayicons', 'repository') .'" class="fp-vb-icons" href="#">
                        <img alt="" src="'. $this->pix_url('fp/view_icon_active', 'theme') .'" />
                    </a>
                    <a role="button" title="'. get_string('displaydetails', 'repository') .'" class="fp-vb-details" href="#">
                        <img alt="" src="'. $this->pix_url('fp/view_list_active', 'theme') .'" />
                    </a>
                    <a role="button" title="'. get_string('displaytree', 'repository') .'" class="fp-vb-tree" href="#">
                        <img alt="" src="'. $this->pix_url('fp/view_tree_active', 'theme') .'" />
                    </a>
                </div>
                <div class="fp-clear-left"></div>
            </div>
            <div class="fp-pathbar">
                 <span class="fp-path-folder"><a class="fp-path-folder-name" href="#"></a></span>
            </div>
        </div>
        <div class="fp-content"></div>
    </div>
</div>';
        return $rv;
    }

    private function fp_js_template_iconfilename() {
        $rv = '
<a class="fp-file" href="#" >
    <div style="position:relative;">
        <div class="fp-thumbnail"></div>
        <div class="fp-reficons1"></div>
        <div class="fp-reficons2"></div>
    </div>
    <div class="fp-filename-field">
        <p class="fp-filename"></p>
    </div>
</a>';
        return $rv;
    }

    private function fp_js_template_listfilename() {
        $rv = '
<span class="fp-filename-icon">
    <a href="#">
        <span class="fp-icon"></span>
        <span class="fp-filename"></span>
    </a>
</span>';
        return $rv;
    }

    private function fp_js_template_nextpage() {
        $rv = '
<div class="fp-nextpage">
    <div class="fp-nextpage-link"><a href="#">'.get_string('more').'</a></div>
    <div class="fp-nextpage-loading">
        <img src="'.$this->pix_url('i/loading_small').'" />
    </div>
</div>';
        return $rv;
    }

    private function fp_js_template_selectlayout() {
        $rv = '
<div class="file-picker fp-select">
    <div class="fp-select-loading">
        <img src="'.$this->pix_url('i/loading_small').'" />
    </div>
    <form class="form-horizontal">
        <div class="fp-forminset">
                <div class="fp-linktype-2 control-group control-radio clearfix">
                    <label class="control-label control-radio">'.get_string('makefileinternal', 'repository').'</label>
                    <div class="controls control-radio">
                        <input type="radio"/>
                    </div>
                </div>
                <div class="fp-linktype-1 control-group control-radio clearfix">
                    <label class="control-label control-radio">'.get_string('makefilelink', 'repository').'</label>
                    <div class="controls control-radio">
                        <input type="radio"/>
                    </div>
                </div>
                <div class="fp-linktype-4 control-group control-radio clearfix">
                    <label class="control-label control-radio">'.get_string('makefilereference', 'repository').'</label>
                    <div class="controls control-radio">
                        <input type="radio"/>
                    </div>
                </div>
                <div class="fp-saveas control-group clearfix">
                    <label class="control-label">'.get_string('saveas', 'repository').'</label>
                    <div class="controls">
                        <input type="text"/>
                    </div>
                </div>
                <div class="fp-setauthor control-group clearfix">
                    <label class="control-label">'.get_string('author', 'repository').'</label>
                    <div class="controls">
                        <input type="text"/>
                    </div>
                </div>
                <div class="fp-setlicense control-group clearfix">
                    <label class="control-label">'.get_string('chooselicense', 'repository').'</label>
                    <div class="controls">
                        <select></select>
                    </div>
                </div>
        </div>
       <div class="fp-select-buttons">
            <button class="fp-select-confirm btn-primary btn">'.get_string('getfile', 'repository').'</button>
            <button class="fp-select-cancel btn-cancel btn">'.get_string('cancel').'</button>
        </div>
    </form>
    <div class="fp-info clearfix">
        <div class="fp-hr"></div>
        <p class="fp-thumbnail"></p>
        <div class="fp-fileinfo">
            <div class="fp-datemodified">'.get_string('lastmodified', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-datecreated">'.get_string('datecreated', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-size">'.get_string('size', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-license">'.get_string('license', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-author">'.get_string('author', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-dimensions">'.get_string('dimensions', 'repository').'<span class="fp-value"></span></div>
        </div>
    </div>
</div>';
        return $rv;
    }

    private function fp_js_template_uploadform() {
        $rv = '
<div class="fp-upload-form">
    <div class="fp-content-center">
        <form enctype="multipart/form-data" method="POST" class="form-horizontal">
            <div class="fp-formset">
                <div class="fp-file control-group clearfix">
                    <label class="control-label">'.get_string('attachment', 'repository').'</label>
                    <div class="controls">
                        <input type="file"/>
                    </div>
                </div>
                <div class="fp-saveas control-group clearfix">
                    <label class="control-label">'.get_string('saveas', 'repository').'</label>
                    <div class="controls">
                        <input type="text"/>
                    </div>
                </div>
                <div class="fp-setauthor control-group clearfix">
                    <label class="control-label">'.get_string('author', 'repository').'</label>
                    <div class="controls">
                        <input type="text"/>
                    </div>
                </div>
                <div class="fp-setlicense control-group clearfix">
                    <label class="control-label">'.get_string('chooselicense', 'repository').'</label>
                    <div class="controls">
                        <select ></select>
                    </div>
                </div>
            </div>
        </form>
        <div class="mdl-align">
            <button class="fp-upload-btn btn-primary btn">'.get_string('upload', 'repository').'</button>
        </div>
    </div>
</div> ';
        return $rv;
    }

    private function fp_js_template_loading() {
        return '
<div class="fp-content-loading">
    <div class="fp-content-center">
        <img src="'.$this->pix_url('i/loading_small').'" />
    </div>
</div>';
    }

    private function fp_js_template_error() {
        $rv = '
<div class="fp-content-error" ><div class="fp-error"></div></div>';
        return $rv;
    }

    private function fp_js_template_message() {
        $rv = '
<div class="file-picker fp-msg" role="alertdialog" aria-live="assertive" aria-labelledby="fp-msg-labelledby">
    <p class="fp-msg-text" id="fp-msg-labelledby"></p>
    <button class="fp-msg-butok btn-primary btn">'.get_string('ok').'</button>
</div>';
        return $rv;
    }

    private function fp_js_template_processexistingfile() {
        $rv = '
<div class="file-picker fp-dlg">
    <p class="fp-dlg-text"></p>
    <div class="fp-dlg-buttons">
        <button class="fp-dlg-butoverwrite btn">'.get_string('overwrite', 'repository').'</button>
        <button class="fp-dlg-butrename btn"></button>
        <button class="fp-dlg-butcancel btn btn-cancel">'.get_string('cancel').'</button>
    </div>
</div>';
        return $rv;
    }

    private function fp_js_template_processexistingfilemultiple() {
        $rv = '
<div class="file-picker fp-dlg">
    <p class="fp-dlg-text"></p>
    <a class="fp-dlg-butoverwrite fp-panel-button" href="#">'.get_string('overwrite', 'repository').'</a>
    <a class="fp-dlg-butcancel fp-panel-button" href="#">'.get_string('cancel').'</a>
    <a class="fp-dlg-butrename fp-panel-button" href="#"></a>
    <br/>
    <a class="fp-dlg-butoverwriteall fp-panel-button" href="#">'.get_string('overwriteall', 'repository').'</a>
    <a class="fp-dlg-butrenameall fp-panel-button" href="#">'.get_string('renameall', 'repository').'</a>
</div>';
        return $rv;
    }

    private function fp_js_template_loginform() {
        $rv = '
<div class="fp-login-form">
    <div class="fp-content-center">
        <form class="form-horizontal">
            <div class="fp-formset">
                <div class="fp-login-popup control-group clearfix">
                    <div class="controls fp-popup">
                        <button class="fp-login-popup-but btn-primary btn">'.get_string('login', 'repository').'</button>
                    </div>
                </div>
                <div class="fp-login-textarea control-group clearfix">
                    <div class="controls"><textarea></textarea></div>
                </div>
                <div class="fp-login-select control-group clearfix">
                    <label class="control-label"></label>

                    <div class="controls"><select></select></div>
                </div>
                <div class="fp-login-input control-group clearfix">
                    <label class="control-label"></label>
                    <div class="controls"><input/></div>
                </div>
                <div class="fp-login-radiogroup control-group clearfix">
                    <label class="control-label"></label>
                    <div class="controls fp-login-radio"><input /> <label></label></div>
                </div>
            </div>
            <p><button class="fp-login-submit btn-primary btn">'.get_string('submit', 'repository').'</button></p>
        </form>
    </div>
</div>';
        return $rv;
    }

    public function filepicker_js_templates() {
        $class_methods = get_class_methods($this);
        $templates = array();
        foreach ($class_methods as $method_name) {
            if (preg_match('/^fp_js_template_(.*)$/', $method_name, $matches))
            $templates[$matches[1]] = $this->$method_name();
        }
        return $templates;
    }

    public function repository_default_searchform() {
        $searchinput = html_writer::label(get_string('searchrepo', 'repository'),
            'reposearch', false, array('class' => 'accesshide'));
        $searchinput .= html_writer::empty_tag('input', array('type' => 'text',
            'id' => 'reposearch', 'name' => 's', 'value' => get_string('search', 'repository')));
        $str = html_writer::tag('div', $searchinput, array('class' => "fp-def-search"));

        return $str;
    }
}

?>

<?php
/**
 * @package   theme_schools
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    // Logo file setting.
    $name = 'theme_schools/logo';
    $title = get_string('logo','theme_schools');
    $description = get_string('logodesc', 'theme_schools');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
    
    $name = 'theme_schools/faviconico';
    $title = get_string('faviconico','theme_schools');
    $description = get_string('faviconicodesc', 'theme_schools');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'faviconico');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
    
    $name = 'theme_schools/favicon';
    $title = get_string('favicon','theme_schools');
    $description = get_string('favicondesc', 'theme_schools');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    $name = 'theme_schools/backgrounduserblock';
    $title = get_string('backgrounduserblock', 'theme_schools');
    $description = get_string('backgrounduserblock_desc', 'theme_schools');
	$default = '#f96b5d';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
    $name = 'theme_schools/backgroundcourseprogress';
    $title = get_string('backgroundcourseprogress', 'theme_schools');
    $description = get_string('backgroundcourseprogress_desc', 'theme_schools');
	$default = '#f16645';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
    $name = 'theme_schools/backgroundleftside';
    $title = get_string('backgroundleftside', 'theme_schools');
    $description = get_string('backgroundleftside_desc', 'theme_schools');
	$default = '#171d1f';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
    $name = 'theme_schools/backgroundrightside';
    $title = get_string('backgroundrightside', 'theme_schools');
    $description = get_string('backgroundrightside_desc', 'theme_schools');
	$default = '#171d1f';
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
    // Custom CSS file.
    $name = 'theme_schools/customcss';
    $title = get_string('customcss', 'theme_schools');
    $description = get_string('customcssdesc', 'theme_schools');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
	// Custom front Welcome to login user.
    $name = 'theme_schools/welcome_user';
    $title = get_string('welcome_user', 'theme_schools');
    $description = get_string('welcome_user_desc', 'theme_schools');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
    $name = 'theme_schools/footnote';
    $title = get_string('footnote', 'theme_schools');
    $description = get_string('footnote_desc', 'theme_schools');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
		
}

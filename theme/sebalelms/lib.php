<?php
/**
 * @package   theme_sebalelms
 * @copyright 2014 SEBALE, sebale.net
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function theme_sebalelms_page_init(moodle_page $page) {
    //$page->requires->jquery();
    //$page->requires->jquery_plugin('slider', 'theme_sebalelms');
}

function theme_sebalelms_process_css($css, $theme) {

    // Set the backgroundhead image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_sebalelms_set_logo($css, $logo);

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_sebalelms_set_customcss($css, $customcss);
	
    if (!empty($theme->settings->backgroundhead)) {
        $backgroundhead = $theme->settings->backgroundhead;
    } else {
        $backgroundhead = null;
    }
    $css = theme_sebalelms_set_backgroundhead($css, $backgroundhead);
	
    if (!empty($theme->settings->backgroundfoot)) {
        $backgroundfoot = $theme->settings->backgroundfoot;
    } else {
        $backgroundfoot = null;
    }
    $css = theme_sebalelms_set_backgroundfoot($css, $backgroundfoot);
	
	
    return $css;
}

function theme_sebalelms_hex2rgba($hex,$a) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgba = "rgba($r,$g,$b,$a)";/* array("r"=>$r, "g"=>$g, "b"=>$b) */
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgba; // returns an array with the rgb values
}

/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_sebalelms_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_sebalelms_set_backgroundhead($css, $backgroundhead) {
    $tag = '[[setting:backgroundhead]]';
    $replacement = $backgroundhead;
    if (is_null($replacement)) {
        $replacement = '#0a4478';
    }
    $css = str_replace($tag, $replacement, $css);
	
    $tag = '[[setting:backgroundcourse]]';
    $replacement = $backgroundhead;
    if (is_null($replacement)) {
        $replacement = '#0a4478';
    }
    
	$rgba = theme_sebalelms_hex2rgba($replacement,"0.7");
    $css = str_replace($tag, $rgba, $css);
    return $css;
}
function theme_sebalelms_set_backgroundfoot($css, $backgroundfoot) {
    $tag = '[[setting:backgroundfoot]]';
    $replacement = $backgroundfoot;
    if (is_null($replacement)) {
        $replacement = '#0a4478';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */
function theme_sebalelms_get_logo() {
	global $SITE, $CFG;
	
	$logo  = get_config('theme_sebalelms', 'logo');
	if($logo){
		$path = $CFG->wwwroot.'/pluginfile.php/1/theme_sebalelms/logo/'.context_system::instance()->id.'/'.$logo;
		
		return '<img src="'.$path.'" title="'.$SITE->shortname.'" alt="'.$SITE->shortname.'"/>';
	}else{
		$path = $CFG->wwwroot.'/theme/sebalelms/pix/logo.png';
		return '<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Oswald:400,300">
					<span style="color: #555; font: 400 24px/24px \'Oswald\',sans-serif; letter-spacing: 0.8px;  text-transform: uppercase;">sebale<span style="font-size: 25px;font-weight: 300;">lms</span></span>';
	}
}
function theme_sebalelms_set_logo($css, $logo) {
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

function theme_sebalelms_generate_favicon($type = 'favicon') {
	global $SITE, $CFG, $OUTPUT;
    $path = '';
	
    if ($type == 'favicon'){
        $logo  = get_config('theme_sebalelms', 'favicon');    
    } else {
        $logo  = get_config('theme_sebalelms', 'faviconico');    
    }
    
	if($logo){
		$path = $CFG->wwwroot.'/pluginfile.php/1/theme_sebalelms/'.$type.'/'.context_system::instance()->id.$logo;
	} else {
	    $path = $OUTPUT->favicon();
	}
    return $path;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_sebalelms_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM) {
        $theme = theme_config::load('sebalelms');
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}



/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * Do not add sebalelms specific logic in here, child themes should be able to
 * rely on that function just by declaring settings with similar names.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_sebalelms_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }

    $return->footnote = sebalelms_course_custom_navigation();
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote text-center">'.format_text($page->theme->settings->footnote).'</div>';
    } 

	$return->front_welcome_login_user = 'SEBALE Learning Management System';
    if (!empty($page->theme->settings->front_welcome_login_user)) {
        $return->front_welcome_login_user =format_text($page->theme->settings->front_welcome_login_user);
    }
	
	$return->front_welcome_no_login_user = 'Designing Your Learning Endeavour!';
    if (!empty($page->theme->settings->front_welcome_no_login_user)) {
        $return->front_welcome_no_login_user =format_text($page->theme->settings->front_welcome_no_login_user);
    }	
	
	$return->front_welcome_full_no_login_user = 'Welcome to SEBALE LMS, simple yet intuitive and easy to set up learning management system running on Moodle.<a href="https://sebalelms.com" style="display:block;"><button style="margin:15px 0 0;">Learn More</button></a>';
    if (!empty($page->theme->settings->front_welcome_full_no_login_user)) {
        $return->front_welcome_full_no_login_user =format_text($page->theme->settings->front_welcome_full_no_login_user);
    }	

    return $return;
}

/**
 * All theme functions should start with theme_sebalelms_
 * @deprecated since 2.5.1
 */
function sebalelms_process_css() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_sebalelms_
 * @deprecated since 2.5.1
 */
function sebalelms_set_logo() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_sebalelms_
 * @deprecated since 2.5.1
 */
function sebalelms_set_customcss() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

function sebalelms_course_custom_navigation(){
	global $COURSE, $cm, $section,$DB,$mods;
	if($cm){
		$section = $DB->get_record('course_sections', array('id'=> $cm->section));
		$section_module = explode(",", $section->sequence);			
		foreach($section_module as $key=>$value){
			if($value == $cm->id){
				$index = $key;
				break;
			}
		}
		
		get_all_mods($COURSE->id, $mods, $modnames, $modnamesplural, $modnamesused);			
		if(isset($section_module[$index - 1])){
			$prev_link = html_writer::link($mods[$section_module[$index - 1]]->get_url(), '' , array('class' => 'fa fa-arrow-left', 'title' => get_string('prev').': '.$mods[$section_module[$index - 1]]->name));
		}
		if(isset($section_module[$index + 1])){
			$next_link = html_writer::link($mods[$section_module[$index + 1]]->get_url(), '' , array('class' => 'fa fa-arrow-right', 'title' => get_string('next').': '.$mods[$section_module[$index + 1]]->name));						
		}
		$links = html_writer::tag('nav', $prev_link.' '.$next_link,array('class' => 'course-module-nav'));	
		echo"<script>
									$('#maincontent + h2').append('$links');
				</script>"; 
	}else{
		echo '';
	}
}

